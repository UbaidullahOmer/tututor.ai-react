import React from "react";
import SnackBar from "./app/components/common/snackBar/SnackBar";
import ReactRoute from "./app/reactRoute/ReactRoute";
import GlobalLoader from "./app/components/loader/GlobalLoader";

function App() {
  return (
    <>
      <SnackBar />
      <GlobalLoader />
      <ReactRoute />
    </>
  );
}

export default App;