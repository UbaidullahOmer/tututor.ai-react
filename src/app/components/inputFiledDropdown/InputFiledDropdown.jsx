import React, { useState, useEffect, useRef } from "react";
import useOutsideClick from "../../hooks/useOutsideClick";

function InputFieldDropdown({
  placeholder,
  options,
  onChange,
  className,
  style,
  name,
  id,
  value,
  onKeyDown,
  disabled = false,
  label,
  require = false,
}) {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState(value);
  const [searchQuery, setSearchQuery] = useState("");
  const dropdownRef = useRef(null);
  useOutsideClick(dropdownRef, () => setIsOpen(false));

  useEffect(() => {
    if (value) {
      setSelectedOption(value);
    }
  }, [value]);

  const toggleDropdown = () => {
    if (!disabled) {
      if (isOpen) {
        setIsOpen(false);
      }else {
        setIsOpen(true);
      }
    }
  };

  const handleOptionClick = (option) => {
    setSelectedOption(option);
    setIsOpen(false);
    if (onChange) {
      onChange(option);
    }
  };

  const handleSearchChange = (e) => {
    setSearchQuery(e.target.value);
    onChange(e.target.value);
  };

  const filteredOptions = options.filter((option) =>
    option.toLowerCase().includes(searchQuery.toLowerCase())
  );

  return (
    <div className={`relative ${className}`} style={style} id={id}>
      <div className="flex flex-col gap-2 cursor-pointer">
        <span className="text-[#2B3D70] cursor-pointer">
          {require && "*"}
          {label || placeholder || name}
        </span>
        <div
          className={`flex items-center gap-2 px-3 py-[15px] rounded-lg border cursor-pointer ${
            disabled ? "cursor-not-allowed opacity-50" : ""
          }`}
          onClick={toggleDropdown}
          aria-haspopup="listbox"
          aria-expanded={isOpen}
          onKeyDown={onKeyDown}
          tabIndex={disabled ? -1 : 0}
        >
          <input
            type="text"
            placeholder={placeholder}
            value={searchQuery || selectedOption}
            onChange={handleSearchChange}
            className="w-full cursor-pointer outline-none placeholder:text-[#CECFD8] text-[#737791]"
            disabled={disabled}
          />
        </div>
      </div>

      {isOpen && (
        <div
          ref={dropdownRef}
          className="w-full  absolute top-full left-0 z-10 bg-white divide-y divide-gray-100 rounded-lg shadow dark:bg-gray-700"
        >
          <ul
            className="py-2 text-sm text-gray-700 dark:text-gray-200 max-h-[450px] overflow-y-scroll"
            aria-labelledby="dropdownDefaultButton"
            role="listbox"
          >
            {filteredOptions.map((option, index) => (
              <li key={index} role="option">
                <button
                  className="block w-full px-4 py-2 text-left hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                  onClick={() => handleOptionClick(option)}
                >
                  {option}
                </button>
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}

export default InputFieldDropdown;
