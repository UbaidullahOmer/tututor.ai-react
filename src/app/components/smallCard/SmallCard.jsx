import React, { useEffect, useState } from "react";
import MeText from "../meText/MeText";
import { Link } from "react-router-dom";
import { categoryColors } from "../../constants/colors";
import { useDispatch, useSelector } from "react-redux";
import { selectTools, toggleFavorite } from "../../redux/FavToolsReducer";

function SmallCard({ data }) {
  const dispatch = useDispatch();
  const favoriteToolIds = useSelector(selectTools);
  const [isFavorite, setIsFavorite] = useState(false);
  useEffect(() => {
    setIsFavorite(favoriteToolIds?.find((fav) => fav?.id === data?.id));
  }, [favoriteToolIds, data?.id]);

  const handleFavorite = (e) => {
    e.stopPropagation();
    e.preventDefault();
    dispatch(toggleFavorite(data?.id));
  };
  const categoryColor = categoryColors[data.category] || "#00ffff";
  const isGradient = categoryColor.includes("linear-gradient");
  const mainColor = isGradient ? categoryColor.match(/#[a-fA-F0-9]{6}/)[0] : categoryColor;

  return (
    <div className="relative w-full">
      <Link
        to={data?.route}
        className="block bg-white rounded-xl overflow-hidden transition-all duration-300 hover:scale-102 w-full h-full"
        style={{
          borderLeft: `4px solid ${mainColor}`,
          boxShadow: `0 4px 6px rgba(0, 0, 0, 0.1), 0 1px 3px rgba(0, 0, 0, 0.08)`
        }}
      >
        <div className="p-2 sm:p-3 md:p-4 flex items-start space-x-2 sm:space-x-3 md:space-x-4">
          <div
            className="flex-shrink-0 w-8 h-8 sm:w-14 sm:h-14 flex items-center justify-center rounded-lg text-xl sm:text-2xl text-white"
            style={{ background: categoryColor }}
          >
            {data?.icon || "?"}
          </div>
          <div className="flex-grow min-w-0">
            <MeText className="text-base sm:text-lg font-semibold text-gray-800 mb-1 truncate">
              {data?.name?.sp || data?.name?.en}
            </MeText>
            <MeText className="text-xs sm:text-sm text-gray-600 line-clamp-2" maxLength={110}>
              {data?.detail?.sp || data?.detail?.en}
            </MeText>
          </div>
          <button
            onClick={handleFavorite}
            className="absolute top-1 sm:top-2 right-1 sm:right-2 z-10 p-1 bg-white rounded-full shadow-sm transition-transform duration-200 hover:scale-110"
          >
            <i className={`text-lg sm:text-xl ${isFavorite ? 'ri-star-fill' : 'ri-star-line'}`} style={{ color: isFavorite ? '#FFD700' : mainColor }}></i>
          </button>
        </div>
      </Link>
    </div>
  );
}

export default SmallCard;
