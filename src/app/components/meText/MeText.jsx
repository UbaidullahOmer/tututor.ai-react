import React from "react";
import { Config } from "../../constants/Index";
import { getLocalStorage } from "../local_storage";

const MeText = ({
  children,
  className,
  style,
  upperCase,
  onClick,
  title,
  maxLength,
}) => {
  const selectedLanguage = "en";
  let showText =
    typeof children === "object"
      ? children?.[selectedLanguage] ?? ""
      : children;

  if (maxLength && typeof showText === "string") {
    showText = showText.slice(0, maxLength);
  }

  const mergedStyle = upperCase
    ? { ...style, textTransform: "uppercase" }
    : style;

  // Only set the title attribute if it's truthy and not false
  const titleAttribute = title && title !== false ? showText : undefined;

  return (
    <span
      title={titleAttribute}
      className={className}
      onClick={onClick}
      style={mergedStyle}
    >
      {showText}
    </span>
  );
};

export default React.memo(MeText);