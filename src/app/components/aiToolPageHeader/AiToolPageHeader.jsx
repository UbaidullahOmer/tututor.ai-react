import React from "react";

function AiToolPageHeader({
  title = "Escribir con IA",
  description = "Escribe cualquier cosa en segundos",
  icon = "ri-edit-2-line text-[32px] text-[#2B3D70]",
}) {
  return (
    <div className="flex items-center gap-3">
      <i className={icon}></i>
      <div className="flex flex-col gap-0">
        <h2 className="text-[24px] font-[600] text-[#2B3D70]">{title}</h2>
        <span className="text-[#737791] text-[14px]">{description}</span>
      </div>
    </div>
  );
}

export default AiToolPageHeader;
