import React from "react";
import { Link } from "react-router-dom";
import MeText from "../meText/MeText";
import { categoryColors } from "../../constants/colors";

function BigCard({ data }) {
  const categoryColor = categoryColors[data.category] || "#02E8AC";
  const isGradient = categoryColor.includes("linear-gradient");
  const mainColor = isGradient ? categoryColor.match(/#[a-fA-F0-9]{6}/)[0] : categoryColor;

  return (
    <Link
      to={data?.route}
      className="block bg-white rounded-xl overflow-hidden transition-all duration-300 hover:scale-102 h-full"
      style={{
        borderLeft: `4px solid ${mainColor}`,
        boxShadow: `0 4px 6px rgba(0, 0, 0, 0.1), 0 1px 3px rgba(0, 0, 0, 0.08)`
      }}
    >
      <div className="p-4 flex flex-col gap-3 h-full">
        <div className="flex items-center justify-between mb-2">
          <MeText className="text-lg font-bold text-[#2B3D70] leading-tight">
            {data?.name}
          </MeText>
          <div
            className="rounded-lg p-2 text-xl text-white flex-shrink-0"
            style={{ background: categoryColor }}
          >
            {data?.icon}
          </div>
        </div>
        <MeText className="text-[#737791] leading-tight text-sm flex-grow">
          {data?.detail}
        </MeText>
        <div className="flex items-center">
          <MeText 
            className="text-white text-sm px-3 py-1 rounded-md" 
            style={{ background: categoryColor }}
          >
            {data?.category}
          </MeText>
        </div>
      </div>
    </Link>
  );
}

export default BigCard;
