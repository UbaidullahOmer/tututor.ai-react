import React, { useState, useRef, useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import useAuth from '../../hooks/useAuth';
import { useSelector } from 'react-redux';
import { selectUser } from '../../redux/UserReducer';
import useOutsideClick from '../../hooks/useOutsideClick';
import { RouteConstant } from '../../constants/RouteConstant';
import { CSSTransition } from 'react-transition-group';
import './Navbar.css';

const Navbar = () => {
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);
  const [isResourcesOpen, setIsResourcesOpen] = useState(false);
  const [imageError, setImageError] = useState(false);
  const dropdownRef = useRef(null);
  const resourcesRef = useRef(null);
  const navigate = useNavigate();
  const { signOutUser } = useAuth();
  const user = useSelector(selectUser);

  const handleProfileClick = useCallback(() => {
    setIsDropdownOpen((prev) => !prev);
  }, []);

  useOutsideClick(dropdownRef, () => { setIsDropdownOpen(false) });

  const handleLogout = useCallback(async () => {
    try {
      await signOutUser();
      localStorage.removeItem('user');
      navigate('/login');
    } catch (error) {
      console.error('Logout error:', error);
    }
  }, [navigate, signOutUser]);

  const handleImageError = useCallback(() => {
    setImageError(true);
  }, []);

  return (
    <div>
      <nav className="bg-[#4EA7FF] p-6 flex justify-between items-center shadow-lg">
        <div className="text-white font-bold text-2xl cursor-pointer" onClick={() => navigate('/')}>
          TUTUTOR.AI
        </div>
        <div className="flex space-x-16">
          <button
            onClick={() => navigate(RouteConstant.SALA_ESTUDIO)}
            className="text-white hover:text-gray-300 text-lg font-medium tracking-wide"
          >
            Salas de Estudio
          </button>
          <button
            onClick={() => navigate(RouteConstant.ASSISTANTS_PAGE)}
            className="text-white hover:text-gray-300 text-lg font-medium tracking-wide"
          >
            Asistentes del Profesor
          </button>
          <button
            onClick={() => navigate(RouteConstant.PRICING_PLANS)}
            className="text-white hover:text-gray-300 text-lg font-medium tracking-wide"
          >
            Precios
          </button>
          <div 
            className="relative" 
            ref={resourcesRef}
            onMouseEnter={() => setIsResourcesOpen(true)}
            onMouseLeave={() => setIsResourcesOpen(false)}
          >
            <button
              className="text-white hover:text-gray-300 text-lg font-medium tracking-wide"
            >
              Recursos
            </button>
            <CSSTransition
              in={isResourcesOpen}
              timeout={300}
              classNames="dropdown"
              unmountOnExit
            >
              <div className="absolute mt-2 w-48 bg-white rounded-md shadow-lg z-20 dropdown">
                <ul className="py-1">
                  <li>
                    <div
                      onClick={() => { navigate(RouteConstant.FAQ); }}
                      className="block px-4 py-2 text-gray-800 hover:bg-gray-100 cursor-pointer"
                    >
                      Preguntas Frecuentes
                    </div>
                  </li>
                  <li>
                    <div
                      onClick={() => { navigate(RouteConstant.ACADEMIA); }}
                      className="block px-4 py-2 text-gray-800 hover:bg-gray-100 cursor-pointer"
                    >
                      Academia
                    </div>
                  </li>
                  <li>
                    <div
                      onClick={() => { navigate(RouteConstant.BLOG); }}
                      className="block px-4 py-2 text-gray-800 hover:bg-gray-100 cursor-pointer"
                    >
                      Blog
                    </div>
                  </li>
                  <li>
                    <div
                      onClick={() => { navigate(RouteConstant.UNETE_RRSS); }}
                      className="block px-4 py-2 text-gray-800 hover:bg-gray-100 cursor-pointer"
                    >
                      Únete a nuestras RRSS
                    </div>
                  </li>
                </ul>
              </div>
            </CSSTransition>
          </div>
          <button
            onClick={() => navigate('/contacta')}
            className="text-white hover:text-gray-300 text-lg font-medium tracking-wide"
          >
            Contacta
          </button>
        </div>
        {user && (
          <div className="relative" ref={dropdownRef}>
            <div className="flex items-center cursor-pointer" onClick={handleProfileClick}>
              {!imageError ? (
                <img
                  src={user.photoURL || "https://via.placeholder.com/40"}
                  alt="Profile"
                  className={`w-12 h-12 object-cover rounded-full mr-2 ${isDropdownOpen ? "border-2 border-white" : ""}`}
                  onError={handleImageError}
                />
              ) : (
                <div className={`w-12 h-12 rounded-full mr-2 bg-gray-300 flex items-center justify-center text-gray-600 font-bold ${isDropdownOpen ? "border-2 border-white" : ""}`}>
                  {user.displayName ? user.displayName.charAt(0).toUpperCase() : 'U'}
                </div>
              )}
              <span className="text-white text-lg font-medium tracking-wide">{user.displayName || 'User'}</span>
            </div>
            {isDropdownOpen && (
              <div className="absolute right-0 mt-2 w-48 bg-white rounded-md shadow-lg z-20">
                <ul className="py-1">
                  <li>
                    <div
                      onClick={() => { navigate(RouteConstant.PROFILE_PAGE); setIsDropdownOpen(false); }}
                      className="block px-4 py-2 text-gray-800 hover:bg-gray-100 cursor-pointer"
                    >
                      Perfil
                    </div>
                  </li>
                  <li>
                    <a
                      href="/settings"
                      className="block px-4 py-2 text-gray-800 hover:bg-gray-100"
                      onClick={() => setIsDropdownOpen(false)}
                    >
                      Ajustes
                    </a>
                  </li>
                  <li>
                    <div
                      className="block px-4 py-2 text-gray-800 hover:bg-gray-100 cursor-pointer"
                      onClick={() => {
                        handleLogout();
                        setIsDropdownOpen(false);
                      }}
                    >
                      Cerrar Sesión
                    </div>
                  </li>
                </ul>
              </div>
            )}
          </div>
        )}
      </nav>
    </div>
  );
};

export default Navbar;
