import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { selectIsLoading } from "../../redux/LoadingReducer";

function GlobalLoader({ className, logoClassName, show = false }) {
  const isLoading = useSelector(selectIsLoading);
  if (!isLoading && !show) return null;
  return (
    <div
      className={`fixed top-0 left-0 w-full h-full flex justify-center items-center bg-white bg-opacity-80 z-[9999] ${className}`}
      style={{ pointerEvents: "all" }}
    >
      <img
        src="https://cdn.dribbble.com/users/1158489/screenshots/4143687/gama_.gif"
        alt="globalLoader"
        className="w-[200px] rounded-full"
      />
    </div>
  );
}

export default GlobalLoader;
