import React from "react";
import TextArea from "../textArea/TextArea";
import FileReaderComponent from "../fileReaderComponent/FileReaderComponent";

function FileReaderInputFIled({ data, setData, onChange }) {
  const handleFileChange = (filesContent) => {
    const newPrompt = filesContent
      .map((file) => file.content)
      .filter((content) => content !== "")
      .join("\n\n");
    const newSelectedFiles = filesContent.map((file) => file.file);

    setData({
      ...data,
      prompt: data.prompt ? `${data.prompt}\n\n${newPrompt}` : newPrompt,
      selectedFiles: [...(data.selectedFiles || []), ...newSelectedFiles],
    });
  };

  const handleRemoveFile = (index) => {
    const updatedFiles = [...data.selectedFiles];
    updatedFiles.splice(index, 1);
    setData({ ...data, selectedFiles: updatedFiles });
  };

  const getFileIcon = (fileType) => {
    if (fileType.startsWith("image/")) return "📷";
    if (fileType === "application/pdf") return "📄";
    if (fileType.includes("word")) return "📝";
    if (fileType.includes("excel") || fileType.includes("spreadsheet"))
      return "📊";
    return "📁";
  };

  return (
    <div className="flex w-full items-end justify-start gap-4 col-span-2">
      <div className="w-[70%]">
        <TextArea
          placeholder="Escribe aquí sobre lo que quieras generar el contenido o sube tu archivo para generar el contenido basado en el temario que tengas. Puedes subir PDF, DOC y JPEG"
          onChange={onChange}
          className="w-full"
          value={data.prompt}
          name="prompt"
          rows={4}
          label={"Sube tu archivo o escribe sobre lo que quieras generar contenido con esta herramienta"}
        />
      </div>
      <div className="flex flex-col items-center w-[30%]">
        <div className="w-full max-h-[100px] overflow-y-auto">
          {data.selectedFiles &&
            data.selectedFiles.map((file, index) => (
              <div
                key={index}
                className="flex items-center mb-2 bg-gray-100 rounded-lg p-2"
              >
                {file.type.startsWith("image/") ? (
                  <img
                    src={URL.createObjectURL(file)}
                    alt={file.name}
                    className="w-10 h-10 object-cover rounded mr-2"
                  />
                ) : (
                  <span className="text-2xl mr-2">
                    {getFileIcon(file.type)}
                  </span>
                )}
                <div className="flex-grow">
                  <div className="text-sm font-medium truncate w-[200px]">
                    {file.name}
                  </div>
                  <div className="text-xs text-gray-500">
                    {(file.size / 1024).toFixed(2)} KB
                  </div>
                </div>
                <button
                  onClick={() => handleRemoveFile(index)}
                  className="ml-2 text-red-500 hover:text-red-700"
                >
                  ✖
                </button>
              </div>
            ))}
        </div>
        <FileReaderComponent
          className="w-full "
          onChange={handleFileChange}
        />
      </div>
    </div>
  );
}

export default FileReaderInputFIled;
