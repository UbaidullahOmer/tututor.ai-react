import React from 'react';

const TextArea = ({className, id, label, value, onChange, name, placeholder, type, require = "true", rows = 3 }) => {
  return (
    <div className={`${className}`}>
      <p className="mb-2 text text-[#737791]">{label}</p>
      <textarea
        id={id}
        value={value}
        onChange={onChange}
        className="w-full p-2 border border-gray-300 rounded-md text-gray-700 focus:outline-none focus:ring-1 focus:ring-blue-500 text-sm  overflow-y-auto"
        name={name}
        typeof={type}
        required={require}
        placeholder={placeholder}
        rows={rows}
      />
    </div>
  );
};

export default TextArea;