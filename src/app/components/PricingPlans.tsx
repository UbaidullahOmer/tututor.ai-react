import React, { useState } from 'react';

const plans = [
  {
    title: "Básico",
    words: "40.000 palabras",
    monthlyPrice: 8.99,
    annualPrice: 83.88,
    color: "bg-blue-600",
    features: [
      "40.000 palabras al mes (60-65 páginas)",
      "Acceso a todas las herramientas",
      "Generación y edición de texto",
      "Generación de exámenes tipo test",
      "Chat con documentos PDF",
      "Extensión de Chrome (próximamente)"
    ]
  },
  {
    title: "Profesional",
    words: "80.000 palabras",
    monthlyPrice: 14.99,
    annualPrice: 143.88,
    color: "bg-green-600",
    features: [
      "80.000 palabras al mes (120-130 páginas)",
      "Acceso a todas las herramientas",
      "Generación y edición avanzada de texto",
      "Generación y personalización de exámenes",
      "Chat avanzado con documentos PDF",
      "Extensión de Chrome (próximamente)"
    ]
  },
  {
    title: "Experto",
    words: "200.000 palabras",
    monthlyPrice: 29.99,
    annualPrice: 288,
    color: "bg-purple-600",
    features: [
      "200.000 palabras al mes (300 páginas)",
      "Acceso prioritario a nuevas funciones",
      "Generación y edición de texto premium",
      "Herramientas avanzadas de exámenes",
      "Chat y análisis de documentos PDF",
      "Extensión de Chrome con funciones premium"
    ]
  },
  {
    title: "Centros Educativos",
    words: "Sin límite de generaciones",
    monthlyPrice: "Contactar",
    annualPrice: "Contactar",
    color: "bg-red-600",
    features: [
      "Generaciones ilimitadas",
      "Funciones exclusivas para centros educativos",
      "Herramientas de colaboración avanzadas",
      "Personalización de marca en exámenes",
      "Análisis detallado de documentos PDF",
      "Soporte prioritario 24/7"
    ]
  }
];

const PricingPlans = () => {
  const [isAnnual, setIsAnnual] = useState(true);

  return (
    <div className="bg-gradient-to-br from-gray-50 to-gray-100 min-h-screen py-16 px-4 sm:px-6 lg:px-8 font-roboto">
      <div className="max-w-7xl mx-auto">
        <div className="text-center mb-16">
          <h2 className="text-4xl font-extrabold text-gray-900 sm:text-5xl mb-4">
            Planes de suscripción
          </h2>
          <p className="text-xl text-gray-600 max-w-2xl mx-auto">
            Elige el plan que mejor se adapte a tus necesidades y empieza a potenciar tu productividad hoy mismo.
          </p>
        </div>

        <div className="flex justify-center mb-12">
          <div className="bg-white rounded-lg p-1 inline-flex shadow-md">
            <button
              className={`px-4 py-2 text-sm font-medium rounded-md ${
                !isAnnual ? 'bg-indigo-600 text-white' : 'text-gray-700 hover:bg-gray-100'
              } transition-colors duration-200`}
              onClick={() => setIsAnnual(false)}
            >
              Mensual
            </button>
            <button
              className={`px-4 py-2 text-sm font-medium rounded-md ${
                isAnnual ? 'bg-indigo-600 text-white' : 'text-gray-700 hover:bg-gray-100'
              } transition-colors duration-200`}
              onClick={() => setIsAnnual(true)}
            >
              Anual
            </button>
          </div>
        </div>

        <div className="grid gap-8 lg:grid-cols-4">
          {plans.map((plan) => (
            <div
              key={plan.title}
              className="bg-white rounded-2xl shadow-xl overflow-hidden transition-all duration-300 hover:shadow-2xl hover:-translate-y-1 flex flex-col"
            >
              <div className={`${plan.color} p-6 text-white`}>
                <h3 className="text-2xl font-bold">{plan.title}</h3>
                <p className="text-sm opacity-80">{plan.words}</p>
              </div>
              <div className="p-6 flex-grow flex flex-col justify-between">
                <div>
                  <p className="text-4xl font-bold text-gray-900 mb-4">
                    {typeof plan.monthlyPrice === 'number' ? 
                      `€${isAnnual ? (plan.annualPrice / 12).toFixed(2) : plan.monthlyPrice.toFixed(2)}` : 
                      plan.monthlyPrice}
                    {typeof plan.monthlyPrice === 'number' && <span className="text-base font-normal text-gray-600">/mes</span>}
                  </p>
                  {isAnnual && typeof plan.annualPrice === 'number' && (
                    <p className="text-sm text-gray-600 mb-6">
                      Facturado anualmente como €{plan.annualPrice.toFixed(2)}
                    </p>
                  )}
                  <ul className="space-y-4 mb-6">
                    {plan.features.map((feature, index) => (
                      <li key={index} className="flex items-start">
                        <svg className="w-5 h-5 text-green-500 mr-2 mt-1 flex-shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7" />
                        </svg>
                        <span className="text-gray-600">{feature}</span>
                      </li>
                    ))}
                  </ul>
                </div>
                <button
                  className={`w-full py-3 px-4 ${plan.color} text-white rounded-lg font-semibold hover:bg-opacity-90 transition-colors duration-200 mt-auto`}
                >
                  {plan.title === "Centros Educativos" ? "Contactar" : "Seleccionar plan"}
                </button>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default PricingPlans;
