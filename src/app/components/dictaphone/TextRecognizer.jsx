import React, { useState, useCallback } from "react";
import SpeechRecognition, {
  useSpeechRecognition,
} from "react-speech-recognition";

const TextRecognizer = ({ onTextRecognized }) => {
  const [isListening, setIsListening] = useState(false);
  const {
    transcript,
    resetTranscript,
    browserSupportsSpeechRecognition,
  } = useSpeechRecognition();

  const handleListening = useCallback(() => {
    if (isListening) {
      SpeechRecognition.stopListening();
      if (transcript && onTextRecognized) {
        onTextRecognized(transcript);
      }
      resetTranscript();
    } else {
      SpeechRecognition.startListening({ continuous: true });
    }
    setIsListening(!isListening);
  }, [isListening, transcript, onTextRecognized, resetTranscript]);

  if (!browserSupportsSpeechRecognition) {
    return <span>Browser doesn't support speech recognition.</span>;
  }

  return (
    <div>
      <button 
        onClick={handleListening}
      >
        {isListening ? <i className="ri-mic-off-line"></i> : <i className="ri-mic-line"></i>}
      </button>
    </div>
  );
};

export default TextRecognizer;