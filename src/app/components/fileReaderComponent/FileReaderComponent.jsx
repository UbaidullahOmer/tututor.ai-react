import React from "react";
import mammoth from "mammoth/mammoth.browser";
import pdfToText from "react-pdftotext";

const FileReaderComponent = ({ className, onChange }) => {
  const handleFileChange = async (event) => {
    const files = Array.from(event.target.files);
    if (files.length === 0) return;

    const filesContent = await Promise.all(files.map(processFile));
    onChange(filesContent);
  };

  const processFile = async (file) => {
    const fileType = file.type;

    if (fileType.startsWith("image/")) {
      return { content: "", file };
    } else if (fileType === "text/plain") {
      return { content: await readTxtFile(file), file };
    } else if (
      fileType ===
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    ) {
      return { content: await readDocxFile(file), file };
    } else if (fileType === "application/pdf") {
      return { content: await readPdfFile(file), file };
    } else {
      console.error("Unsupported file type.");
      return { content: "", file };
    }
  };

  const readTxtFile = (file) => {
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.onload = (event) => resolve(event.target.result);
      reader.readAsText(file);
    });
  };

  const readDocxFile = async (file) => {
    try {
      const arrayBuffer = await file.arrayBuffer();
      const result = await mammoth.extractRawText({ arrayBuffer });
      return result.value;
    } catch (error) {
      console.error("Error reading DOCX file:", error);
      return "";
    }
  };

  const readPdfFile = async (file) => {
    try {
      return await pdfToText(file);
    } catch (error) {
      console.error("Error reading PDF file:", error);
      return "";
    }
  };

  return (
    <div
      className={`flex items-center justify-center relative rounded-[8px] h-fit bg-[#4EA7FF] py-[16px] cursor-pointer text-[#FFF] outline-none transition-all hover:scale-[0.99] ${className}`}
    >
      Subir Archivo
      <input
        className="opacity-0 absolute max-w-[100%] max-h-[100%] cursor-pointer"
        type="file"
        onChange={handleFileChange}
        accept=".txt,.docx,.pdf,image/*"
        multiple
      />
    </div>
  );
};

export default FileReaderComponent;