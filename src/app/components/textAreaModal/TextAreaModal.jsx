import React, { useEffect, useState } from "react";

function TextAreaModal({
  title,
  description,
  placeholder,
  onClose,
  onSubmit,
  show,
  characterCount,
  rows,
  submitText = "Generate",
  cancelText = "Cancel",
}) {
  const [value, setValue] = useState("");
  const onChange = (e) => setValue(e.target.value);
  const [isVisible, setIsVisible] = useState(false);
  useEffect(() => {
    if (show) {
      setIsVisible(true);
    } else {
      setTimeout(() => setIsVisible(false), 300);
    }
  }, [show]);

  return (
   isVisible && <>
      <div
        className={`fixed inset-0 bg-gray-600 bg-opacity-50 flex items-center justify-center z-[999] transition-opacity duration-300 ${
          isVisible ? "opacity-100 visible" : "opacity-0 hi"
        }`}
      >
        <div
          className={`bg-white rounded-lg shadow-lg p-6 w-11/12 md:w-1/2 lg:w-1/3 transform transition-transform duration-300 ${
            isVisible ? "scale-100" : "scale-90"
          }`}
        >
          <div className="flex justify-between items-center pb-3">
            {title && (
              <h3 className="text-lg font-medium text-gray-900">{title}</h3>
            )}
          </div>
          {description && <p className="text-gray-700 mb-4">{description}</p>}
          <textarea
            className="w-full h-40 p-2 border rounded-lg focus:outline-none focus:ring-2 focus:ring-blue-500"
            placeholder={placeholder}
            value={value}
            autoFocus
            onChange={onChange}
            rows={rows}
          />
          {characterCount && (
            <div className="text-right text-gray-600 text-sm mt-1">
              {characterCount}
            </div>
          )}
          <div className="flex justify-end pt-4">
            <button
              className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-semibold py-2 px-4 rounded mr-2"
              onClick={onClose}
              onMouseDown={(e) => e.preventDefault()}
            >
              {cancelText}
            </button>
            <button
              className="bg-blue-500 hover:bg-blue-600 text-white font-semibold py-2 px-4 rounded"
              onClick={() => onSubmit(value)}
              onMouseDown={(e) => e.preventDefault()}
            >
              {submitText}
            </button>
          </div>
        </div>
      </div>
    </>
  );
}

export default TextAreaModal;