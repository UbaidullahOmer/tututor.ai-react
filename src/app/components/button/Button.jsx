import React from "react";

function Button({ onClick, className, style, children = "Button", disabled }) {
  return (
    <button
      onClick={onClick}
      className={` rounded-[8px] bg-[#4EA7FF] py-[12px] text-[#FFF] outline-none transition-all hover:scale-[0.99] ${disabled ? "cursor-not-allowed opacity-50" : ""} ${className}`}
      style={style}
      disabled={disabled}
    >
      {children}
    </button>
  );
}

export default Button;
