import React, { useState, useEffect } from "react";
import {
  BlogIcon,
  ContactIcon,
  CurrentIcon,
  GenerationHistory,
  MindMapIcon,
  QuizIcon,
  ToolPanelIcon,
  WriteIcon,
  // VirtualTeacherIcon is not defined, using a fallback icon
} from "../../icons/Icons";
import { Link, useNavigate, useLocation } from "react-router-dom";
import { getLocalStorage } from "../local_storage";
import { RouteConstant } from "../../constants/RouteConstant.jsx";
import useAuth from "../../hooks/useAuth";
import { useSelector } from "react-redux";
import { selectUser } from "../../redux/UserReducer";
import { AiTools } from "../../constants/AiTools.jsx";
import { selectTools } from "../../redux/FavToolsReducer.js";
import './styles.css';

function Sidebar() {
  const favoriteToolIds = useSelector(selectTools);
  const { pathname } = useLocation();
  const [isSubMenuOpen, setIsSubMenuOpen] = useState(false);
  const toggleSubMenu = () => {
    setIsSubMenuOpen(!isSubMenuOpen);
  };
  const favPages = AiTools.filter((page) =>
    favoriteToolIds.some(fav => fav.id === page.id)
  );
  return (
    <div className="max-h-[100vh] overflow-y-scroll min-h-[100vh] min-w-[300px] max-w-[300px] ">
      <div className="max-h-[100vh] overflow-y-scroll min-h-[100vh] flex w-full flex-col items-start justify-between bg-gradient-to-b gap-[16px] 2xl:gap-[24px] from-[#4EA7FF] to-[#007CF5] p-[20px] 2xl:p-[32px]">
        <div className="flex w-full flex-col items-start gap-[12px] 2xl:gap-[20px]">
          <Link to={"/"} className="flex items-center gap-[6px]">
            <span className="text-[24px] font-[700] uppercase text-[#ECF6FF]">
              Tututor.AI
            </span>
          </Link>
          <div className="h-[2px] w-full rounded-full bg-[#FFFFFF30]"></div>
          <div className="flex w-full flex-col gap-[5px]">
            <span className="mb-[10px] text-[12px] uppercase text-[#ECF6FF] px-[18px]">
              Principal
            </span>
            <Link
              to={"/"}
              className={`flex w-full items-center gap-[10px] rounded-[8px]  2xl:px-[24px] py-[8px] px-[16px] 2xl:py-[16px] ${pathname === "/" ? "bg-[#e9f4ff] " : ""}`}
            >
              <ToolPanelIcon iconColor={pathname === "/" ? "#379BFC" : "#e9f4ff"} className={"h-[20px] w-[20px]"} />
              <span className={`font-[600] text-[#379BFC] max-2xl:text-[14px] leading-[16px] ${pathname === "/" ? "text-[#379BFC]" : "text-[#e9f4ff]"}`}>
                Panel de Inicio
              </span>
            </Link>
            <Link
              to={RouteConstant.HISTORY_OF_PROMTS}
              className={`flex w-full items-center gap-[10px] rounded-[8px]  2xl:px-[24px] py-[8px] px-[16px] 2xl:py-[16px] ${pathname === RouteConstant.HISTORY_OF_PROMTS ? "bg-[#e9f4ff] " : ""}`}
            >
              <GenerationHistory iconColor={pathname === RouteConstant.HISTORY_OF_PROMTS ? "#379BFC" : "#e9f4ff"} className={"h-[20px] w-[20px]"} />
              <span className={`font-[600] max-2xl:text-[14px] leading-[16px] ${pathname === RouteConstant.HISTORY_OF_PROMTS ? "text-[#379BFC]" : "text-[#e9f4ff]"}`}>
                Historial de Generaciones
              </span>
            </Link>
            <div
              className="flex w-full items-center gap-[10px] justify-between cursor-pointer rounded-[8px] bg-transparent 2xl:px-[24px] py-[8px] px-[16px] 2xl:py-[16px]"
              onClick={toggleSubMenu}
            >
              <div className="flex items-center gap-[12px]">
                <QuizIcon className={"h-[20px] w-[20px]"} />
                <span className="font-[600] text-[#FFF] max-2xl:text-[14px] leading-[16px]">
                  Profesores Virtuales
                </span>
              </div>
              <i className={`ri-arrow-${isSubMenuOpen ? "up" : "down"}-s-line text-[#FFF]`}></i>
            </div>
            {isSubMenuOpen && (
              <div className="flex ml-[50px] flex-col gap-[10px]">
                <Link to={RouteConstant.MATH_SCIENCE_TEACHER} className="text-[#FFF] max-2xl:text-[14px] text-[14px]">
                  Profesor de Matemática y Ciencias
                </Link>
                <Link to={RouteConstant.LENGUA_LITERATURA} className="text-[#FFF] max-2xl:text-[14px] text-[14px]">
                  Profesor de Lengua, Literatura y Comunicación
                </Link>
                <Link to={RouteConstant.TECNOLOGIA_EDUCACION} className="text-[#FFF] max-2xl:text-[14px] text-[14px]">
                  Profesor de Tecnología
                </Link>
                <Link to={RouteConstant.ENGLISH_TEACHER} className="text-[#FFF] max-2xl:text-[14px] text-[14px]">
                  Profesor de Inglés
                </Link>
                <Link to={RouteConstant.SOCIAL_SCIENCE_TEACHER} className="text-[#FFF] max-2xl:text-[14px] text-[14px]">
                  Profesor de Ciencias Sociales
                </Link>
                <Link to={RouteConstant.ARTS_EDUCATION} className="text-[#FFF] max-2xl:text-[14px] text-[14px]">
                  Profesor de Arte, Educación Física y Desarrollo Personal
                </Link>
                <Link to={RouteConstant.UNIVERSITY_ORIENTATOR} className="text-[#FFF] max-2xl:text-[14px] text-[14px]">
                  Orientador Pre-Universitario
                </Link>
              </div>
            )}
          </div>
          <div className="h-[2px] w-full rounded-full bg-[#FFFFFF30]"></div>
          <div className="flex flex-col gap-[10px] mt-[10px]">
            {favPages.slice(0, 4).map((page, index) => (
              <Link to={page.route} key={index} className={`flex w-full items-center gap-[6px] rounded-[8px] 2xl:px-[24px] py-[8px] px-[16px] 2xl:py-[16px] max-w-[260px]`}>
                <span className="h-[40px] w-[40px]">{page.icon}</span>
                <span className="font-[600] text-[#fff] max-2xl:text-[14px] leading-[16px]">
                  {page.name.sp}
                </span>
              </Link>
            ))}
          </div>
        </div>
        <div className="flex w-full flex-col gap-[16px] rounded-[12px] bg-[#FFFFFF15] p-[16px]">
          <div className="flex w-full items-center justify-between bg-[#CAE5FF] p-[10px] max-w-[38px] max-h-[38px] rounded-[8px]">
            <CurrentIcon iconColor="#FBFBFD" />
          </div>
          <div className="flex flex-col gap-[8px]">
            <div className="flex w-full items-center justify-between">
              <span className="text-[14px] text-[#FBFBFD]">Generaciones gratis</span>
              <span className="font-[600] text-[#FFF]"> 4/156 </span>
            </div>
            <div className="h-[8px] w-full overflow-hidden rounded-[3px] bg-[#06FEBE30]">
              <div className="h-full w-[70%] rounded-[3px] bg-[#06FEBE]"></div>
            </div>
          </div>
          <span className="w-full rounded-[8px] bg-[#CAE5FF] p-[10px] text-center font-[500] text-[#379BFC] cursor-pointer transition-all hover:scale-95">
            Actualiza Ahora
          </span>
        </div>
      </div>
    </div>
  );
}

export default Sidebar;
