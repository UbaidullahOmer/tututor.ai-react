import jsPDF from "jspdf";
import React, { useEffect, useRef, useState } from "react";
import ActionModalBar from "./ActionModalBar";
import { useLocation } from "react-router-dom";
import { getLocalStorage, setLocalStorage } from "../local_storage";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextAreaModal from "../textAreaModal/TextAreaModal";
import "jspdf-autotable";
import "./styles.css";

const editorStyles = `
  * { margin: 0; padding: 0; }
  h1, h2, h3, h4, h5, h6 { margin-top: 1em; margin-bottom: 0.5em; font-weight: bold; }
  h1 { font-size: 1.8em; }
  h2 { font-size: 1.5em; }
  h3 { font-size: 1.17em; }
  p { margin-bottom: 1em; line-height: 1.6; }
  ul, ol { padding-left: 1.5em; margin-bottom: 1em; list-style: auto; }
  li { margin-bottom: 0.5em; }
`;

const preserveParagraphs = (text) => {
  return text
    .split("\n")
    .map((paragraph) => `<p>${paragraph}</p>`)
    .join("");
};

function TextEditor({ result, showShare = true, showSave = true }) {
  const [text, setText] = useState("");
  const [showModalBar, setShowModalBar] = useState(false);
  const [modalPosition, setModalPosition] = useState({ top: 0, left: 0 });
  const textAreaRef = useRef(null);
  const actionBarRef = useRef(null);
  const [selectedTextIds, setSelectedTextIds] = useState({});
  const [selectedRange, setSelectedRange] = useState(null);
  const [selectedText, setSelectedText] = useState("");
  const [undoStack, setUndoStack] = useState([]);
  const [redoStack, setRedoStack] = useState([]);
  const [selectedFontSize, setSelectedFontSize] = useState();
  const [changesMade, setChangesMade] = useState(true);
  const location = useLocation();
  const { handleHitAi } = useHandleHitAi({ ai: "gemini", store: false });
  const [showCustomizeTextModal, setShowCustomizeTextModal] = useState(false);
  const [isTextSelected, setIsTextSelected] = useState(false);
  const [showModalBarForModifyText, setShowModalBarForModifyText] =
    useState(false);

  useEffect(() => {
    if (result) {
      const cleanedText = result
        .replace(/```html/g, "")
        .replace(/```/g, "")
        .replace(/<style[^>]*>[\s\S]*?<\/style>/gi, "");

      setText(cleanedText);

      if (textAreaRef.current) {
        textAreaRef.current.innerHTML = cleanedText;
      }

      setUndoStack([cleanedText]);
      setRedoStack([]);
    }
  }, [result]);

  const handleTextChange = () => {
    if (textAreaRef.current) {
      const newText = textAreaRef.current.innerHTML;
      if (newText !== text) {
        setText(newText);
        setUndoStack((prevStack) => [...prevStack, newText]);
        setRedoStack([]);
      }
    }
  };

  const handleTextSelect = () => {
    const selection = window.getSelection();
    if (selection && selection.toString().length > 0) {
      const range = selection.getRangeAt(0);
      setSelectedText(range.toString());
      setSelectedRange(range);
      const rect = range.getBoundingClientRect();
      setModalPosition({ top: rect.top, left: rect.left });
      setShowModalBar(true);
      const fontSize = document.queryCommandValue("fontSize") || "3";
      setSelectedFontSize(fontSize);
      setIsTextSelected(true);
    } else {
      setSelectedRange(null);
      setIsTextSelected(false);
      setShowModalBar(false);
    }
  };

  const handleUndo = () => {
    if (undoStack.length > 1) {
      const currentText = undoStack.pop();
      setRedoStack((prevStack) => [...prevStack, currentText]);
      const previousText = undoStack[undoStack.length - 1];
      setText(previousText);
      if (textAreaRef.current) {
        textAreaRef.current.innerHTML = previousText;
      }
      setUndoStack([...undoStack]);
    }
  };

  const handleRedo = () => {
    if (redoStack.length > 0) {
      const nextText = redoStack.pop();
      setText(nextText);
      if (textAreaRef.current) {
        textAreaRef.current.innerHTML = nextText;
      }
      setUndoStack((prevStack) => [...prevStack, nextText]);
      setRedoStack([...redoStack]);
    }
  };

  const handleDelete = () => {
    document.execCommand("delete", false, null);
    handleTextChange();
  };

  const exportToPDF = () => {
    const input = textAreaRef.current;
    if (!input) {
      console.error("Referencia a textAreaRef no encontrada.");
      return;
    }

    const hasTables = input.querySelectorAll("table").length > 0;

    const doc = new jsPDF({
      orientation: hasTables ? "landscape" : "portrait",
      unit: "pt",
      format: "a4",
    });

    const margins = {
      top: 40,
      bottom: 40,
      left: 40,
      right: 40,
    };
    const pageWidth = doc.internal.pageSize.width;
    const pageHeight = doc.internal.pageSize.height;
    const contentWidth = pageWidth - margins.left - margins.right;
    let cursorY = margins.top;

    const renderTable = (table) => {
      const headers = Array.from(table.querySelectorAll("th")).map((th) =>
        th.textContent.trim()
      );
      const rows = Array.from(table.querySelectorAll("tbody tr")).map((row) =>
        Array.from(row.querySelectorAll("td")).map((td) =>
          td.textContent.trim()
        )
      );

      doc.autoTable({
        head: [headers],
        body: rows,
        startY: cursorY,
        theme: "grid",
        styles: {
          fontSize: 8,
          cellPadding: 5,
          font: "helvetica",
          textColor: "#555",
        },
        headStyles: {
          fillColor: "#2E7D32",
          textColor: "#FFFFFF",
          fontSize: 10,
          fontStyle: "bold",
        },
        bodyStyles: {
          textColor: "#555",
        },
        alternateRowStyles: {
          fillColor: "#f4f4f4",
        },
        columnStyles: {
          0: {
            fillColor: "#E8F5E9",
            textColor: "#2E7D32",
            fontStyle: "bold",
          },
        },
        didDrawPage: (data) => {
          doc.setFontSize(8);
          doc.text(
            "Página " + doc.internal.getNumberOfPages(),
            data.settings.margin.left,
            pageHeight - 10
          );
        },
      });

      cursorY = doc.lastAutoTable.finalY + 10;
    };

    const renderElement = (element, indent = 0) => {
      const lineHeight = 20; // Aumentado para más espacio entre líneas
      const paragraphSpacing = 10;
      let headerSpacing = 0;

      if (element.nodeName === "TABLE") {
        renderTable(element);
      } else {
        switch (element.nodeName) {
          case "H1":
            doc.setFontSize(18);
            doc.setFont("helvetica", "bold");
            cursorY += 24;
            headerSpacing = 16;
            break;
          case "H2":
            doc.setFontSize(16);
            doc.setFont("helvetica", "bold");
            cursorY += 20;
            headerSpacing = 14;
            break;
          case "H3":
            doc.setFontSize(14);
            doc.setFont("helvetica", "bold");
            cursorY += 18;
            headerSpacing = 12;
            break;
          case "UL":
          case "OL":
            indent += 20;
            break;
          case "P":
          default:
            doc.setFontSize(12);
            doc.setFont("helvetica", "normal");
        }

        if (element.childNodes.length === 0) {
          const text = element.textContent.trim();
          if (text) {
            const lines = doc.splitTextToSize(text, contentWidth - indent);
            lines.forEach((line) => {
              if (cursorY + lineHeight > pageHeight - margins.bottom) {
                doc.addPage();
                cursorY = margins.top;
              }
              doc.text(line, margins.left + indent, cursorY);
              cursorY += lineHeight;
            });
            cursorY += paragraphSpacing + headerSpacing;
          }
        } else {
          Array.from(element.childNodes).forEach((child) => {
            if (child.nodeType === Node.ELEMENT_NODE) {
              const originalFont = doc.getFont();
              const originalFontSize = doc.getFontSize();

              if (child.nodeName === "STRONG" || child.nodeName === "B") {
                doc.setFont(doc.getFont().fontName, "bold");
              } else if (child.nodeName === "EM" || child.nodeName === "I") {
                doc.setFont(doc.getFont().fontName, "italic");
              }

              renderElement(child, indent);

              doc.setFont(originalFont.fontName, originalFont.fontStyle);
              doc.setFontSize(originalFontSize);
            } else if (child.nodeType === Node.TEXT_NODE) {
              const text = child.textContent.trim();
              if (text) {
                const lines = doc.splitTextToSize(text, contentWidth - indent);
                lines.forEach((line) => {
                  if (cursorY + lineHeight > pageHeight - margins.bottom) {
                    doc.addPage();
                    cursorY = margins.top;
                  }
                  doc.text(line, margins.left + indent, cursorY);
                  cursorY += lineHeight;
                });
              }
            }
          });
        }

        if (element.nodeName === "UL" || element.nodeName === "OL") {
          cursorY += paragraphSpacing;
        }
      }
    };

    Array.from(input.childNodes).forEach((element) => {
      renderElement(element);
    });

    doc.save("Exportación PDF Tututor.ai.pdf");
  };

  const expandText = async () => {
    const selection = window.getSelection();
    if (selection && selection.toString().length > 0) {
      const selectedText = selection.toString();
      const expandedText = await handleHitAi(
        `Expand the following text: ${selectedText}. Maintain the exact same meaning, tone, and style. Return only the expanded text in Spanish.`
      );
      if (expandedText) {
        const range = selection.getRangeAt(0);
        const newNode = document.createElement("div");
        newNode.innerHTML = preserveParagraphs(expandedText);

        const fragment = document.createDocumentFragment();
        while (newNode.firstChild) {
          fragment.appendChild(newNode.firstChild);
        }

        range.deleteContents();
        range.insertNode(fragment);

        selection.removeAllRanges();
        handleTextChange();
      }
    }
  };

  const modifyText = () => {
    setShowModalBarForModifyText(true);
  };

  const handleSubmitModifyText = async (value) => {
    if (selectedText && selectedText.trim().length > 0) {
      try {
        const aiPrompt = `Modifica el siguiente texto: ${selectedText}. Asegúrate de mantener el mismo significado y tono del texto original. ${value}. Devuelve solo el texto modificado en español, sin etiquetas HTML.`;

        const rewrittenText = await handleHitAi(aiPrompt);
        if (rewrittenText && rewrittenText.trim().length > 0) {
          const formattedText = rewrittenText.replace(/\n\n/g, "\n");
          replaceSelectedText(formattedText);
          setShowModalBarForModifyText(false);
        } else {
          console.warn("La IA no devolvió un texto válido.");
        }
      } catch (error) {
        console.error("Error al modificar el texto:", error);
      }
    } else {
      console.warn(
        "No hay texto seleccionado o el texto seleccionado está vacío."
      );
    }
  };

  const replaceSelectedText = (newText) => {
    if (textAreaRef.current && selectedText) {
      const range = window.getSelection().getRangeAt(0);
      range.deleteContents();

      const tempDiv = document.createElement("div");
      tempDiv.innerHTML = newText;

      const fragment = document.createDocumentFragment();
      while (tempDiv.firstChild) {
        fragment.appendChild(tempDiv.firstChild);
      }

      range.insertNode(fragment);

      const selection = window.getSelection();
      selection.removeAllRanges();
      const newRange = document.createRange();
      newRange.setStartBefore(fragment.firstChild);
      newRange.setEndAfter(fragment.lastChild);
      selection.addRange(newRange);

      handleTextChange();
    } else {
      console.error(
        "No se pudo reemplazar el texto. El textAreaRef o el texto seleccionado no son válidos."
      );
    }
  };

  const paraphraseText = () => {
    setCurrentAction("paraphrase");
    setShowCustomizeTextModal(true);
  };

  const shortenText = async () => {
    const selection = window.getSelection();
    if (selection && selection.toString().length > 0) {
      const selectedText = selection.toString();
      const shortenedText = await handleHitAi(
        `Reduce el siguiente texto de manera drástica, eliminando contenido redundante, repeticiones y palabras innecesarias, y asegurando que el significado original se mantenga claro. Acorta el texto al menos un 50%:

        "${selectedText}"
        
        Devuelve solo el texto reducido en español, sin formato adicional.`
      );
      if (shortenedText) {
        const range = selection.getRangeAt(0);
        const newNode = document.createElement("div");
        newNode.innerHTML = preserveParagraphs(shortenedText);

        const fragment = document.createDocumentFragment();
        while (newNode.firstChild) {
          fragment.appendChild(newNode.firstChild);
        }

        range.deleteContents();
        range.insertNode(fragment);

        selection.removeAllRanges();
        handleTextChange();
      }
    }
  };

  const rewriteText = async () => {
    const selection = window.getSelection();
    if (selection && !selection.isCollapsed) {
      let selectedText = "";
      for (let i = 0; i < selection.rangeCount; i++) {
        selectedText += selection.getRangeAt(i).toString();
      }

      if (selectedText.trim().length > 0) {
        try {
          const rewrittenText = await handleHitAi(
            `Reescribe el siguiente texto manteniendo el mismo significado y tono del texto original. Mantén las etiquetas HTML como <strong>, <em>, <ul>, <ol>, etc. Devuelve el texto reescrito en formato HTML:

            "${selectedText}"`
          );

          if (rewrittenText && typeof rewrittenText === "string") {
            const range = selection.getRangeAt(0);
            range.deleteContents();

            const tempDiv = document.createElement("div");
            tempDiv.innerHTML = rewrittenText;
            const fragment = document.createDocumentFragment();
            while (tempDiv.firstChild) {
              fragment.appendChild(tempDiv.firstChild);
            }

            range.insertNode(fragment);

            selection.removeAllRanges();
            const newRange = document.createRange();
            newRange.setStartBefore(fragment.firstChild);
            newRange.setEndAfter(fragment.lastChild);
            selection.addRange(newRange);

            if (typeof handleTextChange === "function") {
              handleTextChange();
            }
          } else {
            console.error("La respuesta de la IA no es válida:", rewrittenText);
          }
        } catch (error) {
          console.error("Error al reescribir el texto:", error);
        }
      } else {
        console.log(
          "La selección está vacía después de quitar espacios en blanco."
        );
      }
    } else {
      console.log("No hay texto seleccionado para reescribir.");
    }
  };

  console.log(selectedText, "selectedText");

  return (
    <div className="flex h-[80dvh] overflow-y-scroll flex-col items-start p-[20px] gap-[28px]">
      <TextAreaModal
        title="Personaliza el texto escribiendo lo que quieras que te añada, modifique o quite"
        show={showModalBarForModifyText}
        onClose={() => {
          setShowModalBarForModifyText(false);
        }}
        onSubmit={handleSubmitModifyText}
      />
      {showModalBar && (
        <ActionModalBar
          position={modalPosition}
          onBold={() => document.execCommand("bold", false, null)}
          onItalic={() => document.execCommand("italic", false, null)}
          onUnderline={() => document.execCommand("underline", false, null)}
          onUndo={handleUndo}
          onRedo={handleRedo}
          expandText={expandText}
          modifyText={modifyText}
          paraphraseText={paraphraseText}
          shortenText={shortenText}
          rewriteText={rewriteText}
          handleDelete={handleDelete}
          onAlignLeft={() => document.execCommand("justifyLeft", false, null)}
          onAlignCenter={() =>
            document.execCommand("justifyCenter", false, null)
          }
          onAlignRight={() => document.execCommand("justifyRight", false, null)}
          handleFontSizeIncrease={() => {
            const newSize = Math.min(7, parseInt(selectedFontSize) + 1);
            setSelectedFontSize(newSize.toString());
            document.execCommand("fontSize", false, newSize);
          }}
          handleFontSizeDecrease={() => {
            const newSize = Math.max(1, parseInt(selectedFontSize) - 1);
            setSelectedFontSize(newSize.toString());
            document.execCommand("fontSize", false, newSize);
          }}
          selectedFontSize={selectedFontSize}
          setShowModalBar={setShowModalBar}
        />
      )}
      <div className="flex gap-[16px] w-full justify-end">
        {showShare && (
          <div className="flex gap-[16px] w-full justify-end">
            <button
              className="w-[140px] bg-gradient-to-r from-[#4EA7FF] to-[#3B8AD9] text-[#FFF] py-[12px] px-[8px] rounded-[8px] transition duration-300 ease-in-out transform hover:scale-105 hover:shadow-lg"
              onClick={() => {
                navigator.clipboard.writeText(text);
                alert("Texto copiado al portapapeles");
              }}
            >
              Compartir
            </button>
          </div>
        )}
        {showSave && (
          <button
            className="w-[140px] bg-gradient-to-r from-[#4EA7FF] to-[#3B8AD9] text-[#FFF] py-[12px] px-[8px] rounded-[8px] transition duration-300 ease-in-out transform hover:scale-105 hover:shadow-lg"
            onClick={() => {
              setLocalStorage(location.pathname, textAreaRef.current.innerHTML);
              setChangesMade(false);
              alert("Documento guardado localmente");
            }}
          >
            Guardar
          </button>
        )}
        <button
          className="w-[140px] bg-gradient-to-r from-[#f73838] to-[#c72424] text-[#FFF] py-[12px] px-[8px] rounded-[8px] transition duration-30 ease-in-out transform hover:scale-105 hover:shadow-lg"
          onClick={exportToPDF}
        >
          Descargar
        </button>
      </div>
      <style>{editorStyles}</style>
      <div
        className={`flex flex-col w-full border-[1px] outline-none rounded-[20px] ${
          !changesMade ? "" : "border-[#4EA7FF] shadow-xl"
        }`}
      >
        <style>{editorStyles}</style>
        <div
          contentEditable
          suppressContentEditableWarning
          role="textbox"
          spellCheck="true"
          data-lexical-editor="true"
          onMouseUp={handleTextSelect}
          onInput={handleTextChange}
          ref={textAreaRef}
          className="w-full p-[30px] outline-none [&>*:first-child]:mt-0 prose prose-sm max-w-none"
          onBlur={() => {
            if (!isTextSelected) {
              setShowModalBar(false);
            }
          }}
        />
      </div>
    </div>
  );
}

export default TextEditor;
