import React, { useEffect, useRef, useState } from "react";
import "./ActionModalBar.css";
import AIActionsDropdown from './AIActionsDropdown';
import {
  ArrowMoveLeftIcon,
  ArrowMoveRightIcon,
  DeleteIcon,
  TextAlignCenterIcon,
  TextAlignLeftIcon,
  TextAlignRightIcon,
  UnderlineIcon,
} from "../../icons/Icons";
import useOutsideClick from "../../hooks/useOutsideClick";

function ActionModalBar({
  position, 
  onBold,
  onItalic,
  onUnderline,
  onUndo,
  onRedo,
  handleDelete,
  onAlignLeft,
  onAlignCenter,
  onAlignRight,
  handleFontSizeIncrease,
  handleFontSizeDecrease,
  selectedFontSize,
  setShowModalBar,
  expandText,
  paraphraseText,
  shortenText,
  rewriteText,
  modifyText, // Asegúrate de pasar modifyText aquí
}) {
  const actionModalRef = useRef(null);
  const [showTextAlignModalBar, setShowTextAlignModalBar] = useState(false);
  const [modalPosition, setModalPosition] = useState({ top: 0, left: 0 });
  const [isVisible, setIsVisible] = useState(false);
  useOutsideClick(actionModalRef, () => setShowModalBar(false));

  const updatePosition = () => {
    const selection = window.getSelection();
    if (selection && !selection.isCollapsed) {
      const range = selection.getRangeAt(0);
      const rect = range.getBoundingClientRect();
      const scrollTop = window.pageYOffset || document.documentElement.scrollTop;

      const modalWidth = 900;
      const modalHeight = 70;

      let top = rect.top + scrollTop - modalHeight - 10;
      let left = rect.left + (rect.width / 2) - (modalWidth / 2);

      const viewportWidth = window.innerWidth || document.documentElement.clientWidth;
      const viewportHeight = window.innerHeight || document.documentElement.clientHeight;

      if (left < 0) left = 0;
      if (left + modalWidth > viewportWidth) left = viewportWidth - modalWidth;
      if (top < 0) top = rect.bottom + scrollTop + 10;
      if (top + modalHeight > viewportHeight) top = viewportHeight - modalHeight;

      setModalPosition({ top, left });
      setIsVisible(true);
    } else {
      setIsVisible(false);
    }
  };

  useEffect(() => {
    const handleSelectionChange = () => {
      updatePosition();
    };

    document.addEventListener('selectionchange', handleSelectionChange);
    window.addEventListener('scroll', updatePosition);
    window.addEventListener('resize', updatePosition);

    // Inicializar la posición
    updatePosition();

    return () => {
      document.removeEventListener('selectionchange', handleSelectionChange);
      window.removeEventListener('scroll', updatePosition);
      window.removeEventListener('resize', updatePosition);
    };
  }, []);

  const handleAction = (action) => {
    action();
    // Mantener la selección después de la acción
    setTimeout(() => {
      updatePosition();
    }, 0);
  };

  if (!isVisible) return null;

  return (
    <div
      ref={actionModalRef}
      className="action-modal-bar gradient-bg"
      style={{
        position: "fixed",
        top: `${modalPosition.top}px`,
        left: `${modalPosition.left}px`,
        width: "900px",
        height: "70px",
        display: "flex",
        alignItems: "center",
        padding: "0 10px",
        borderRadius: "30px",
        boxShadow: "0 4px 6px rgba(0, 0, 0, 0.1)",
        zIndex: 1000,
      }}
    >
      <div className="flex-container">
        <div className="flex items-center">
          <AIActionsDropdown
            expandText={expandText}
            paraphraseText={paraphraseText}
            shortenText={shortenText}
            rewriteText={rewriteText}
            modifyText={modifyText} // Pasa modifyText aquí también
          />
        </div>
        <div className="flex items-center gap-3">
          <div className="flex items-center justify-center gap-3">
            <button
              className="text-white hover:bg-white hover:bg-opacity-20 rounded-full p-2 transition-all duration-200"
              onClick={() => handleAction(handleFontSizeDecrease)}
            >
              <i className="ri-subtract-line text-xl"></i>
            </button>
            <div className="text-white border border-white px-2 py-1 rounded-full text-sm">
              {selectedFontSize}
            </div>
            <button
              className="text-white hover:bg-white hover:bg-opacity-20 rounded-full p-2 transition-all duration-200"
              onClick={() => handleAction(handleFontSizeIncrease)}
            >
              <i className="ri-add-line text-xl"></i>
            </button>
          </div>
          <div className="flex items-center gap-2">
            <button
              className="text-white hover:bg-white hover:bg-opacity-20 rounded-full p-2 transition-all duration-200"
              onClick={() => handleAction(onBold)}
            >
              <i className="ri-bold text-xl"></i>
            </button>
            <button
              className="text-white hover:bg-white hover:bg-opacity-20 rounded-full p-2 transition-all duration-200"
              onClick={() => handleAction(onItalic)}
            >
              <i className="ri-italic text-xl"></i>
            </button>
            <button
              className="text-white hover:bg-white hover:bg-opacity-20 rounded-full p-2 transition-all duration-200"
              onClick={() => handleAction(onUnderline)}
            >
              <UnderlineIcon className="w-5 h-5" iconColor="#fff" />
            </button>
          </div>
          <div className="relative">
            <button
              className="text-white hover:bg-white hover:bg-opacity-20 rounded-full p-2 transition-all duration-200"
              onClick={() => setShowTextAlignModalBar(!showTextAlignModalBar)}
            >
              <TextAlignLeftIcon className="w-5 h-5" iconColor="#fff" />
            </button>
            {showTextAlignModalBar && (
              <div className="absolute top-full left-0 mt-1 bg-white rounded-lg shadow-lg py-1 z-10">
                <button
                  onClick={() => handleAction(onAlignLeft)}
                  className="flex items-center w-full px-2 py-1 text-gray-700 hover:bg-gray-100"
                >
                  <TextAlignLeftIcon className="w-6 h-6 mr-1" iconColor="#374151" />
                  <span className="text-sm">Izquierda</span>
                </button>
                <button
                  onClick={() => handleAction(onAlignCenter)}
                  className="flex items-center w-full px-2 py-1 text-gray-700 hover:bg-gray-100"
                >
                  <TextAlignCenterIcon className="w-6 h-6 mr-1" iconColor="#374151" />
                  <span className="text-sm">Centrar</span>
                </button>
                <button
                  onClick={() => handleAction(onAlignRight)}
                  className="flex items-center w-full px-2 py-1 text-gray-700 hover:bg-gray-100"
                >
                  <TextAlignRightIcon className="w-6 h-6 mr-1" iconColor="#374151" />
                  <span className="text-sm">Derecha</span>
                </button>
              </div>
            )}
          </div>
          <button
            className="text-white hover:bg-white hover:bg-opacity-20 rounded-full p-2 transition-all duration-200"
            onClick={() => handleAction(onUndo)}
          >
            <ArrowMoveLeftIcon className="w-5 h-5" iconColor="#fff" />
          </button>
          <button
            className="text-white hover:bg-white hover:bg-opacity-20 rounded-full p-2 transition-all duration-200"
            onClick={() => handleAction(onRedo)}
          >
            <ArrowMoveRightIcon className="w-5 h-5" iconColor="#fff" />
          </button>
          <button
            className="text-white hover:bg-white hover:bg-opacity-20 rounded-full p-2 transition-all duration-200"
            onClick={() => handleAction(handleDelete)}
          >
            <DeleteIcon className="w-5 h-5" iconColor="#fff" />
          </button>
        </div>
      </div>
    </div>
  );
}

export default ActionModalBar;
