import React, { useState, useRef } from "react";
import { AiStartIcon } from "../../icons/Icons";
import useOutsideClick from "../../hooks/useOutsideClick";

function AIActionsDropdown({
  expandText,
  paraphraseText,
  shortenText,
  rewriteText,
  modifyText, // Recibe modifyText aquí
}) {
  const [isOpen, setIsOpen] = useState(false);
  const dropdownRef = useRef(null);
  useOutsideClick(dropdownRef, () => setIsOpen(false));

  const handleAIAction = async (action) => {
    const selectedText = window.getSelection().toString();
    if (selectedText) {
      const newText = await action(selectedText);
      if (newText) {
        document.execCommand("insertText", false, newText);
      } else {
        console.error("La acción no devolvió un nuevo texto");
      }
    }
    setIsOpen(false);
  };

  return (
    <div className="relative" ref={dropdownRef}>
      <button
        onClick={() => setIsOpen(!isOpen)}
        className="flex items-center gap-6 bg-white bg-opacity-20 hover:bg-opacity-30 transition-all duration-300 px-6 py-3 rounded-full text-white font-medium text-lg"
      >
        <AiStartIcon className="w-8 h-8" />
        <span>IA Mágica</span>
      </button>
      {isOpen && (
        <div className="absolute top-full left-0 mt-1 w-56 gradient-bg rounded-lg shadow-lg py-2 z-10">
          {[
            { label: "Expandir texto", action: expandText },
            { label: "Acortar texto", action: shortenText },
            { label: "Re-escribir por IA", action: rewriteText },
            { label: "Personalizar", action: modifyText }, // Asocia modifyText con el botón de personalizar
          ].map((item, index) => (
            <button
              key={index}
              onClick={() => handleAIAction(item.action)}
              className="block w-full text-left px-4 py-2 text-white hover:bg-white hover:bg-opacity-20 transition-all duration-200"
            >
              <div className="flex items-center gap-2">
                <span className="text-lg font-medium">{item.label}</span>
              </div>
            </button>
          ))}
        </div>
      )}
    </div>
  );
}

export default AIActionsDropdown;
