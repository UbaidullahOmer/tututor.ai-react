import React, { useState, useEffect, useCallback, useRef } from "react";
import TextRecognizer from "../dictaphone/TextRecognizer";

function InputField({
  type = "text",
  placeholder = "",
  name = "",
  id = "",
  value = "",
  onChange = () => {},
  onKeyDown = () => {},
  className = "",
  disabled = false,
  maxLength,
  icon = null,
  autoComplete,
  error,
  errorColor = "#ff1919",
  label,
  require = false
}) {
  const [showPassword, setShowPassword] = useState(false);
  const [inputValue, setInputValue] = useState(value);
  const inputRef = useRef(null);

  useEffect(() => {
    setInputValue(value);
  }, [value]);

  const handleTextRecognized = useCallback((recognizedText) => {
    const input = inputRef.current;
    if (input) {
      const start = input.selectionStart;
      const end = input.selectionEnd;
      const newValue = inputValue.substring(0, start) + recognizedText + inputValue.substring(end);
      setInputValue(newValue);
      onChange({ target: { name, value: newValue } });

      // Set cursor position after the inserted text
      setTimeout(() => {
        input.setSelectionRange(start + recognizedText.length, start + recognizedText.length);
        input.focus();
      }, 0);
    }
  }, [inputValue, onChange, name]);

  const handleChange = (e) => {
    setInputValue(e.target.value);
    onChange(e);
  };

  return (
    <div className={`flex flex-col gap-[10px] ${className} `}>
      {placeholder && (
        <span className="text-[#2B3D70]">
          {require && "*"}{label || placeholder || name}
        </span>
      )}
      <div className="w-full">
        <div
          className={`flex items-center gap-[10px] px-[10px] py-[16px] rounded-[8px] border-[1px] ${
            error ? `border-[${errorColor}]` : "border-[#91737335]"
          }`}
        >
          {icon}
          <input
            ref={inputRef}
            type={showPassword ? "text" : type}
            name={name}
            id={id}
            value={inputValue}
            onChange={handleChange}
            onKeyDown={onKeyDown}
            placeholder={placeholder}
            disabled={disabled}
            maxLength={maxLength}
            autoComplete={autoComplete}
            className={`outline-none flex-grow  ${
              error
                ? `text-[${errorColor}] placeholder:text-[${errorColor}]`
                : "text-[#737791] placeholder:text-[#CECFD8]"
            }`}
            aria-invalid={!!error}
            aria-describedby={error ? `${id}-error` : undefined}
          />
          {type === "password" && (
            <i
              className={`ri-eye${
                showPassword ? "" : "-off"
              }-line cursor-pointer text-[18px] ${
                error ? `text-[${errorColor}]` : "text-[#737791]"
              }`}
              onClick={() => setShowPassword(!showPassword)}
            ></i>
          )}
          <TextRecognizer onTextRecognized={handleTextRecognized} />
        </div>
        {error && (
          <span id={`${id}-error`} className="text-[#ff1919] mt-1 text-sm">
            {error}
          </span>
        )}
      </div>
    </div>
  );
}

export default InputField;