export const categoryColors = {
  "Generador de Contenido": "linear-gradient(-45deg, #1a7bdd, #3c66b5, #5cb3ff, #1a7bdd)", // azul
  "Planificador y Diseño Educativo": "linear-gradient(-45deg, #00ff00, #33cc33, #66ff66, #009900)", // verde
  "Evaluación y Retroalimentación": "linear-gradient(-45deg, #ff0000, #ff3333, #ff6666, #cc0000)", // rojo
  "Herramientas de Aprendizaje Adaptativo": "linear-gradient(-45deg, #ff8c00, #cc6600, #ffb266, #ff8c00)", // naranja
  "Recursos de Apoyo al Aprendizaje": "linear-gradient(-45deg, #ff00ff, #ff66ff, #ff99ff, #cc00cc)", // magenta
};
