export const Config = {
  serverApiUrl: "https://api.tututor.ai",
  lang: "Language",
  showCurrency: function (amount) {
    return "DKK " + amount ?? 0;
  },
  findMaxNumber: function (arr) {
    if (arr.length === 0) {
      return 0; 
    }

    return Math.max(...arr);
  },
  findMinNumber: function (arr) {
    if (arr.length === 0) {
      return 0;
    }

    return Math.min(...arr);
  },
  fnShowCurrency: function (amount) {
    return "DKK " + amount;
  },
  fnGetDefaultProductPrice: function (prod, productSize) {
    const prodPrices = prod?.prices;
    const defaultPriceObj =
      productSize == PRODUCT_SIZES.ALL
        ? prodPrices?.find((price) => price?.size?.default == 1)
        : prodPrices?.find((price) => price?.size?.title?.en == productSize);
    const prod_price = defaultPriceObj?.price ?? 0;
    return prod_price;
  },
  fnGetSortHighToLow: function (data, productSize) {
    return data?.slice()?.sort((a, b) => {
      const itemA = this.fnGetDefaultProductPrice(a, productSize);
      const itemB = this.fnGetDefaultProductPrice(b, productSize);
      if (itemA < itemB) {
        return 1;
      }
      if (itemB < itemA) {
        return -1;
      }
      return 0;
    });
  },
  fnGetSortLowToHigh: function (data, productSize) {
    return data?.slice()?.sort((a, b) => {
      const itemA = this.fnGetDefaultProductPrice(a, productSize);
      const itemB = this.fnGetDefaultProductPrice(b, productSize);
      if (itemA > itemB) {
        return 1;
      }
      if (itemB > itemA) {
        return -1;
      }
      return 0;
    });
  },
  fnGetSortAToZ: function (data) {
    return data?.slice()?.sort((a, b) => {
      const itemA = a?.title?.en?.toLowerCase();
      const itemB = b?.title?.en?.toLowerCase();
      if (itemA > itemB) {
        return 1;
      }
      if (itemB > itemA) {
        return -1;
      }
      return 0;
    });
  },
  fnGetSortZToA: function (data) {
    return data?.slice()?.sort((a, b) => {
      const itemA = a?.title?.en?.toLowerCase();
      const itemB = b?.title?.en?.toLowerCase();
      if (itemA < itemB) {
        return 1;
      }
      if (itemB < itemA) {
        return -1;
      }
      return 0;
    });
  },
  fnGetSortedArray: function ({ type = "DEFAULT", data = [], productSize }) {
    switch (type) {
      case "DEFAULT":
        return data;
      case "A_TO_Z":
        return this.fnGetSortAToZ(data);
      case "Z_TO_A":
        return this.fnGetSortZToA(data);
      case "HIGH_TO_LOW":
        return this.fnGetSortHighToLow(data, productSize);
      case "LOW_TO_HIGH":
        return this.fnGetSortLowToHigh(data, productSize);
      default:
        return data;
    }
  },

  fnGetWebsiteName: function (websiteUrl) {
    const linkWithoutProtocol = websiteUrl
      ? websiteUrl?.replace(/^https?:\/\//, "")
      : "#";
    const linkWithoutWww = linkWithoutProtocol?.replace(/^www\./, "");
    const linkWithoutCom = linkWithoutWww?.replace(/\.[a-zA-Z]+\/?$/, "");
    const websiteName =
      linkWithoutCom?.charAt(0)?.toUpperCase() + linkWithoutCom?.slice(1);
    return websiteName;
  },
};
