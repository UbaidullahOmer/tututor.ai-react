import React, { lazy } from 'react';

// Lazily import the components
const QuizGenerator = lazy(() => import("../pages/quizGenerator/QuizGenerator"));
const QuizOutput = lazy(() => import("../pages/quizGenerator/QuizOutput"));
const SummarizeText = lazy(() => import("../pages/summarizeText/SummarizeText"));
const SummarizeOutput = lazy(() => import("../pages/summarizeText/SummarizeOutput"));
const ExplainText = lazy(() => import("../pages/explainText/ExplainText"));
const ExplainOutput = lazy(() => import("../pages/explainText/ExplainOutput"));
const WriteText = lazy(() => import("../pages/writeText/WriteText"));
const WriteOutput = lazy(() => import("../pages/writeText/WriteOutput"));
const TranslationText = lazy(() => import("../pages/translationText/TranslationText"));
const TranslationOutput = lazy(() => import("../pages/translationText/TranslationOutput"));
const AdaptedGroupText = lazy(() => import("../pages/adaptedGroupText/AdaptedGroupText"));
const AdaptedGroupOutput = lazy(() => import("../pages/adaptedGroupText/AdaptedGroupOutput"));
const MathScienceTeacher = lazy(() => import("../pages/mathScienceTeacher/MathScienceTeacher"));
const GamificationsText = lazy(() => import("../pages/gamificationsText/GamificationsText"));
const GamificationsOutput = lazy(() => import("../pages/gamificationsText/GamificationsOutput"));
const TaskDesignerText = lazy(() => import("../pages/taskDesignerText/TaskDesignerText"));
const TaskDesignerOutput = lazy(() => import("../pages/taskDesignerText/TaskDesignerOutput"));
const LearningSituationsText = lazy(() => import("../pages/learningSituationsText/LearningSituationsText"));
const LearningSituationsOutput = lazy(() => import("../pages/learningSituationsText/LearningSituationsOutput"));
const ExercisesAdaptedText = lazy(() => import("../pages/exercisesAdaptedText/ExercisesAdaptedText"));
const ExercisesAdaptedOutput = lazy(() => import("../pages/exercisesAdaptedText/ExercisesAdaptedOutput"));
const TaskFeedbackText = lazy(() => import("../pages/taskFeedbackText/TaskFeedbackText"));
const TaskFeedbackOutput = lazy(() => import("../pages/taskFeedbackText/TaskFeedbackOutput"));
const VirtualEnglishTeacher = lazy(() => import("../pages/englishteacher/VirtualEnglishTeacher"));
const ClassIdeatorText = lazy(() => import("../pages/classIdeatorText/ClassIdeatorText"));
const ClassIdeatorOutput = lazy(() => import("../pages/classIdeatorText/ClassIdeatorOutput"));
const PersonalizedEducationText = lazy(() => import("../pages/personalizedEducationText/PersonalizedEducationText"));
const PersonalizedEducationOutput = lazy(() => import("../pages/personalizedEducationText/PersonalizedEducationOutput"));
const VocabularyGeneratorText = lazy(() => import("../pages/vocabularyGeneratorText/VocabularyGeneratorText"));
const VocabularyGeneratorOutput = lazy(() => import("../pages/vocabularyGeneratorText/VocabularyGeneratorOutput"));
const DidacticUnitsText = lazy(() => import("../pages/didacticUnitsText/DidacticUnitsText"));
const DidacticUnitsOutput = lazy(() => import("../pages/didacticUnitsText/DidacticUnitsOutput"));
const VocabularyListText = lazy(() => import("../pages/vocabularyListText/VocabularyListText"));
const VocabularyListOutput = lazy(() => import("../pages/vocabularyListText/VocabularyListOutput"));
const EvalutionReportText = lazy(() => import("../pages/evalutionReportText/EvalutionReportText"));
const EvalutionReportOutput = lazy(() => import("../pages/evalutionReportText/EvalutionReportOutput"));
const EducationalActivitesText = lazy(() => import("../pages/educationalActivitesText/EducationalActivitesText"));
const EducationalActivitesOutput = lazy(() => import("../pages/educationalActivitesText/EducationalActivitesOutput"));
const PersonalLaboratoryText = lazy(() => import("../pages/personalLaboratoryText/PersonalLaboratoryText"));
const PersonalLaboratoryOutput = lazy(() => import("../pages/personalLaboratoryText/PersonalLaboratoryOutput"));
const ReflectionQuestionnaireOutput = lazy(() => import("../pages/reflectionQuestionnaireText/ReflectionQuestionnaireOutput"));
const ReflectionQuestionnaireText = lazy(() => import("../pages/reflectionQuestionnaireText/ReflectionQuestionnaireText"));
const ConnectAndCaptivateText = lazy(() => import("../pages/connectAndCaptivateText/ConnectAndCaptivateText"));
const ConnectAndCaptivateOutput = lazy(() => import("../pages/connectAndCaptivateText/ConnectAndCaptivateOutput"));
const ConceptualUnderstandingGeneratorText = lazy(() => import("../pages/conceptualUnderstandingGeneratorText/ConceptualUnderstandingGeneratorText"));
const MisconceptionsOutput = lazy(() => import("../pages/misconceptionsText/MisconceptionsOutput"));
const MisconceptionsText = lazy(() => import("../pages/misconceptionsText/MisconceptionsText"));
const ConceptualUnderstandingGeneratorOutput = lazy(() => import("../pages/conceptualUnderstandingGeneratorText/ConceptualUnderstandingGeneratorOutput"));
const RealWorldText = lazy(() => import("../pages/realWorldText/RealWorldText"));
const RealWorldOutput = lazy(() => import("../pages/realWorldText/RealWorldOutput"));
const TeamWorkOutput = lazy(() => import("../pages/teamWorkText/TeamWorkOutput"));
const TeamWorkText = lazy(() => import("../pages/teamWorkText/TeamWorkText"));
const EvaluationRubricsText = lazy(() => import("../pages/evaluationRubricsText/EvaluationRubricsText"));
const EvaluationRubricsOutput = lazy(() => import("../pages/evaluationRubricsText/EvaluationRubricsOutput"));
const StoryTellingText = lazy(() => import("../pages/storyTellingText/StoryTellingText"));
const StoryTellingOutput = lazy(() => import("../pages/storyTellingText/StoryTellingOutput"));
const UniversityOrientator = lazy(() => import("../pages/universityOrientator/UniversityOrientator"));
const SocialScienceTeacher = lazy(() => import("../pages/socialScienceTeacher/SocialScienceTeacher"));
const LenguaLiteratura = lazy(() => import("../pages/lenguaLiteratura/lenguaLiteratura"));
const TecnologiaEducacion = lazy(() => import("../pages/tecnologiaEducacion/tecnologiaEducacion"));
const ArtsEducation = lazy(() => import("../pages/artsEducation/artsEducation"));
const DevelopQuestionsText = lazy(() => import("../pages/developQuestions/DevelopQuestionsText"));
const DevelopQuestionsOutput = lazy(() => import("../pages/developQuestions/DevelopQuestionsOutput"));
const Login = lazy(() => import("../pages/login/Login"));
const AiTeachers = lazy(() => import("../pages/aiTeachers/AiTeachers"));
const MindMaps = lazy(() => import("../pages/mindMaps/MindMaps"));
const MindMapsOutput = lazy(() => import("../pages/mindMaps/MindMapsOutput"));
const OnlineText = lazy(() => import("../pages/onlineText/OnlineText"));
const TakeOnlineTest = lazy(() => import("../pages/quizGenerator/TakeOnlineTest"));
const HistoryOfPromts = lazy(() => import("../pages/promptHistory/PromptHistory"));
const ProfilePage = lazy(() => import("../pages/profilePage/ProfilePage"));
const AssistantsPage = lazy(() => import("../pages/assistantsPage/AssistantsPage"));
const PricingPlans = lazy(() => import("../components/PricingPlans"));
const EducationalAssistant = lazy(() => import("../pages/EducationalAssistant/EducationalAssistant"));
const DigitalLiteracyMentor = lazy(() => import("../pages/DigitalLiteracyMentor/DigitalLiteracyMentor"));
const FieldTripPlanner = lazy(() => import("../pages/FieldTripPlanner/FieldTripPlanner"));
const ProjectActivityExpert = lazy(() => import("../pages/ProjectActivityExpert/ProjectActivityExpert"));
const EnvironmentalEducationGuide = lazy(() => import("../pages/EnvironmentalEducationGuide/EnvironmentalEducationGuide"));
const RecyclingProjectAssistant = lazy(() => import("../pages/RecyclingProjectAssistant/RecyclingProjectAssistant"));
const CarbonFootprintAssistant = lazy(() => import("../pages/CarbonFootprintAssistant/CarbonFootprintAssistant"));
const FAQ = lazy(() => import("../pages/FAQ/FAQ"));
const Academia = lazy(() => import("../pages/Academia/Academia"));
const Blog = lazy(() => import("../pages/Blog/Blog"));
const UneteRRSS = lazy(() => import("../pages/UneteRRSS/UneteRRSS"));
const Contact = lazy(() => import("../pages/contact/contact"));
const SalaEstudio = lazy(() => import("../pages/salaEstudio/salaEstudio"));
const ProfeMagico = lazy(() => import("../pages/ProfeMagico/ProfeMagico"));





export const RouteConstant = {
  LOGIN: "/login",
  SIGNUP: "/signup",
  HOME: "/",
  SHARE_TEXT: "/share-text/:slug",
  GAMIFICATIONS_Text: "/gamification-text",
  GAMIFICATIONS_OUTPUT: "/gamification-output",
  EXPLAIN_TEXT: "/explain-text",
  EXPLAIN_OUTPUT: "/explain-output",
  TRANSLATOR_TEXT: "/translator",
  TRANSLATOR_OUTPUT: "/translator-output",
  MATH_SCIENCE_TEACHER: "/math-science-teacher",
  SOCIAL_SCIENCE_TEACHER: "/social-science-teacher",
  ASSISTANTS_PAGE: "/assistants-page",
  WRITE_TEXT: "/write-text",
  WRITE_OUTPUT: "/write-output",
  SUMMARIZE_TEXT: "/summarize-text",
  PRICING_PLANS: "/pricing-plans",
  SUMMARIZE_OUTPUT: "/summarize-output",
  QUIZ_GENERATOR: "/generador-de-examenes",
  QUIZ_RESPONSE: "/quiz-response",
  ADAPTED_GROUPS_TEXT: "/adapted-groups-text",
  ADAPTED_GROUPS_OUTPUT: "/adapted-groups-output",
  TASK_DESIGNER_TEXT: "/task-designer",
  TASK_DESIGNER_OUTPUT: "/task-designer-output",
  LEARNING_SITUATIONS_TEXT: "/learning-situations-text",
  LEARNING_SITUATIONS_OUTPUT: "/learning-situations-output",
  EXERCISES_ADAPTED_TEXT: "/exercises-adapted-text",
  EXERCISES_ADAPTED_OUTPUT: "/exercises-adapted-output",
  TASK_FEEDBACK_TEXT: "/task-feedback-text",
  TASK_FEEDBACK_OUTPUT: "/task-feedback-output",
  CLASS_IDEATOR_TEXT: "/class-ideator",
  CLASS_IDEATOR_OUTPUT: "/class-ideator-output",
  PERSONALIZED_EDUCATION_TEXT: "/personalized-education-text",
  PERSONALIZED_EDUCATION_OUTPUT: "/personalized-education-output",
  DIDACTIC_UNITS_TEXT: "/didactic-units-text",
  DIDACTIC_UNITS_OUTPUT: "/didactic-units-output",
  VOCABULARY_GENERATOR_TEXT: "/vocabulary-generator-text",
  VOCABULARY_GENERATOR_OUTPUT: "/vocabulary-generator-output",
  VOCABULARY_LIST_TEXT: "/vocabulary-list-text",
  VOCABULARY_LIST_OUTPUT: "/vocabulary-list-output",
  DEVELOP_QUESTIONS_TEXT: "/develop-questions-text",
  DEVELOP_QUESTIONS_OUTPUT: "/develop-questions-output",
  EVALUATION_REPORT_TEXT: "/evaluation-report-text",
  EVALUATION_REPORT_OUTPUT: "/evaluation-report-output",
  EDUCATIONAL_ACTIVITIES_TEXT: "/educational-activities-text",
  EDUCATIONAL_ACTIVITIES_OUTPUT: "/educational-activities-output",
  PERSONAL_LABORATORY_TEXT: "/personal-laboratory-text",
  PERSONAL_LABORATORY_OUTPUT: "/personal-laboratory-output",
  REFLECTION_QUESTIONNAIRE_TEXT: "/reflection-questionnaire-text",
  REFLECTION_QUESTIONNAIRE_OUTPUT: "/reflection-questionnaire-output",
  CONNECT_AND_CAPTIVATE_TEXT: "/connect-and-captivate-text",
  CONNECT_AND_CAPTIVATE_OUTPUT: "/connect-and-captivate-output",
  CONCEPTUAL_UNDERSTANDING_GENERATOR_TEXT: "/conceptual-understanding-generator-text",
  MISCONCEPTIONS_TEXT: "/misconceptions-text",
  MISCONCEPTIONS_OUTPUT: "/misconceptions-output",
  CONCEPTUAL_UNDERSTANDING_GENERATOR_OUTPUT: "/conceptual-understanding-generator-output",
  REAL_WORLD_TEXT: "/real-world-text",
  REAL_WORLD_OUTPUT: "/real-world-output",
  TEAM_WORK_TEXT: "/team-work-text",
  TEAM_WORK_OUTPUT: "/team-work-output",
  EVALUATION_RUBRICS_TEXT: "/evaluation-rubrics-text",
  EVALUATION_RUBRICS_OUTPUT: "/evaluation-rubrics-output",
  STORY_TELLING_TEXT: "/story-telling-text",
  STORY_TELLING_OUTPUT: "/story-telling-output",
  ENGLISH_TEACHER: "/virtual-english-teacher",
  ONLINE_TEXT: "/online-text",
  MIND_MAPS: "/mind-maps",
  MIND_MAPS_OUTPUT: "/mind-maps-output",
  UNIVERSITY_ORIENTATOR: "/university-orientator",
  LENGUA_LITERATURA: "/lengua-literatura",
  TECNOLOGIA_EDUCACION: "/tecnologia-educacion",
  ARTS_EDUCATION: "/arts-education",
  AI_TEACHERS: "/ai-teachers",
  TAKE_ONLINE_TEST: "/take-online-test",
  SHARE_ONLINE_TEST: "/share-online-test/:slug",
  HISTORY_OF_PROMTS: "/history-of-promts",
  PROFILE_PAGE: "/profile-page",
  EDUCATIONAL_ASSISTANT: "/asistente-todo-en-uno",
  DIGITAL_LITERACY_MENTOR: "/mentor-competencias-digitales",
  FIELD_TRIP_PLANNER: "/planificador-salidas-educativas",
  PROJECT_ACTIVITY_EXPERT: "/experto-actividades-proyectos",
  ENVIRONMENTAL_EDUCATION_GUIDE: "/guia-educacion-ambiental",
  RECYCLING_PROJECT_ASSISTANT: "/asistente-proyectos-reciclaje",
  CARBON_FOOTPRINT_ASSISTANT: "/asistente-huella-carbono",
  FAQ: "/faq",
  ACADEMIA: "/academia",
  UNETE_RRSS: "/unete-redes-sociales",
  BLOG: "/blog",
  CONTACT: "/contacta",
  SALA_ESTUDIO: "/sala-estudio",
  PROFE_MAGICO: "/profe-magico",

};


export const RouteList = [
  
  {
    path: RouteConstant.PROFILE_PAGE,
    element: (
      <ProfilePage />
    ),
  },
  {
    path: RouteConstant.HISTORY_OF_PROMTS,
    element: (
      <HistoryOfPromts />
    ),
  },
  {
    path: RouteConstant.HOME,
    element: (
      <AiTeachers />
    ),
  },
  {
    path: RouteConstant.PRICING_PLANS,
    element: <PricingPlans />,
  },
  {
    path: RouteConstant.GAMIFICATIONS_Text,
    element: (
      <GamificationsText />
    ),
  },
  {
    path: RouteConstant.GAMIFICATIONS_OUTPUT,
    element: (
      <GamificationsOutput />
    ),
  },
  {
    path: RouteConstant.EXPLAIN_TEXT,
    element: (
      <ExplainText />
    ),
  },
  {
    path: RouteConstant.EXPLAIN_OUTPUT,
    element: (
      <ExplainOutput />
    ),
  },
  {
    path: RouteConstant.TRANSLATOR_TEXT,
    element: (
      <TranslationText />
    ),
  },
  {
    path: RouteConstant.TRANSLATOR_OUTPUT,
    element: (
      <TranslationOutput />
    ),
  },
  {
    path: RouteConstant.MATH_SCIENCE_TEACHER,
    element: (
      <MathScienceTeacher />
    ),
  },
  {
    path: RouteConstant.SOCIAL_SCIENCE_TEACHER,
    element: (
      <SocialScienceTeacher />
    ),
  },
  {
    path: RouteConstant.WRITE_TEXT,
    element: (
      <WriteText />
    ),
  },
  {
    path: RouteConstant.EDUCATIONAL_ASSISTANT,
    element: (
      <EducationalAssistant />
    ),
  },
  {
    path: RouteConstant.DIGITAL_LITERACY_MENTOR,
    element: (
      <DigitalLiteracyMentor />
    ),
  },
  {
    path: RouteConstant.FIELD_TRIP_PLANNER,
    element: (
      <FieldTripPlanner />
    ),
  },
  {
    path: RouteConstant.PROFE_MAGICO,
    element: <ProfeMagico />,
  },
  
  {
    path: RouteConstant.PROJECT_ACTIVITY_EXPERT,
    element: (
      <ProjectActivityExpert />
    ),
  },
  {
    path: RouteConstant.ENVIRONMENTAL_EDUCATION_GUIDE,
    element: (
      <EnvironmentalEducationGuide />
    ),
  },
  {
    path: RouteConstant.RECYCLING_PROJECT_ASSISTANT,
    element: (
      <RecyclingProjectAssistant />
    ),
  },
  {
    path: RouteConstant.CARBON_FOOTPRINT_ASSISTANT,
    element: (
      <CarbonFootprintAssistant />
    ),
  },
  {
    path: RouteConstant.ACADEMIA,
    element: <Academia />,
  },
  {
    path: RouteConstant.BLOG,
    element: <Blog />,
  },
  {
    path: RouteConstant.SALA_ESTUDIO,
    element: <SalaEstudio />,
  },
  {
    path: RouteConstant.BLOG,
    element: <Blog />,
  },
  {
    path: RouteConstant.UNETE_RRSS,
    element: <UneteRRSS />,
  },
  
  {
    path: RouteConstant.WRITE_OUTPUT,
    element: (
      <WriteOutput />
    ),
  },
  {
    path: RouteConstant.SUMMARIZE_TEXT,
    element: (
      <SummarizeText />
    ),
  },
  {
    path: RouteConstant.SUMMARIZE_OUTPUT,
    element: (
      <SummarizeOutput />
    ),
  },
  {
    path: RouteConstant.CONTACT,
    element: (
      <Contact />
    ),
  },
  {
    path: RouteConstant.QUIZ_GENERATOR,
    element: (
      <QuizGenerator />
    ),
  },
  {
    path: RouteConstant.QUIZ_RESPONSE,
    element: (
      <QuizOutput />
    ),
  },
  {
    path: RouteConstant.ADAPTED_GROUPS_TEXT,
    element: (
      <AdaptedGroupText />
    ),
  },
  {
    path: RouteConstant.ADAPTED_GROUPS_OUTPUT,
    element: (
      <AdaptedGroupOutput />
    ),
  },
  {
    path: RouteConstant.FAQ,
    element: <FAQ />, // Asegúrate de que la ruta apunte al componente FAQ importado
  },  
  {
    path: RouteConstant.TASK_DESIGNER_TEXT,
    element: (
      <TaskDesignerText />
    ),
  },
  {
    path: RouteConstant.TASK_DESIGNER_OUTPUT,
    element: (
      <TaskDesignerOutput />
    ),
  },
  {
    path: RouteConstant.LEARNING_SITUATIONS_TEXT,
    element: (
      <LearningSituationsText />
    ),
  },
  {
    path: RouteConstant.LEARNING_SITUATIONS_OUTPUT,
    element: (
      <LearningSituationsOutput />
    ),
  },
  {
    path: RouteConstant.EXERCISES_ADAPTED_TEXT,
    element: (
      <ExercisesAdaptedText />
    ),
  },
  {
    path: RouteConstant.EXERCISES_ADAPTED_OUTPUT,
    element: (
      <ExercisesAdaptedOutput />
    ),
  },
  {
    path: RouteConstant.TASK_FEEDBACK_TEXT,
    element: (
      <TaskFeedbackText />
    ),
  },
  {
    path: RouteConstant.TASK_FEEDBACK_OUTPUT,
    element: (
      <TaskFeedbackOutput />
    ),
  },
  {
    path: RouteConstant.CLASS_IDEATOR_TEXT,
    element: (
      <ClassIdeatorText />
    ),
  },
  {
    path: RouteConstant.CLASS_IDEATOR_OUTPUT,
    element: (
      <ClassIdeatorOutput />
    ),
  },
  {
    path: RouteConstant.PERSONALIZED_EDUCATION_TEXT,
    element: (
      <PersonalizedEducationText />
    ),
  },
  {
    path: RouteConstant.PERSONALIZED_EDUCATION_OUTPUT,
    element: (
      <PersonalizedEducationOutput />
    ),
  },
  {
    path: RouteConstant.DIDACTIC_UNITS_TEXT,
    element: (
      <DidacticUnitsText />
    ),
  },
  {
    path: RouteConstant.DIDACTIC_UNITS_OUTPUT,
    element: (
      <DidacticUnitsOutput />
    ),
  },
  {
    path: RouteConstant.VOCABULARY_GENERATOR_TEXT,
    element: (

      <VocabularyGeneratorText />

    ),
  },
  {
    path: RouteConstant.VOCABULARY_GENERATOR_OUTPUT,
    element: (
      <VocabularyGeneratorOutput />
    ),
  },
  {
    path: RouteConstant.VOCABULARY_LIST_TEXT,
    element: (

      <VocabularyListText />

    ),
  },
  {
    path: RouteConstant.VOCABULARY_LIST_OUTPUT,
    element: (
      <VocabularyListOutput />
    ),
  },
  {
    path: RouteConstant.DEVELOP_QUESTIONS_TEXT,
    element: (
      <DevelopQuestionsText />
    ),
  },
  {
    path: RouteConstant.DEVELOP_QUESTIONS_OUTPUT,
    element: (
      <DevelopQuestionsOutput />
    ),
  }, 
  {
    path: RouteConstant.EVALUATION_REPORT_TEXT,
    element: (
      <EvalutionReportText />
    ),
  },
  {
    path: RouteConstant.EVALUATION_REPORT_OUTPUT,
    element: (
      <EvalutionReportOutput />
    ),
  },
  {
    path: RouteConstant.EDUCATIONAL_ACTIVITIES_TEXT,
    element: (
      <EducationalActivitesText />
    ),
  },
  {
    path: RouteConstant.EDUCATIONAL_ACTIVITIES_OUTPUT,
    element: (
      <EducationalActivitesOutput />
    ),
  },
  {
    path: RouteConstant.PERSONAL_LABORATORY_TEXT,
    element: (
      <PersonalLaboratoryText />
    ),
  },
  {
    path: RouteConstant.PERSONAL_LABORATORY_OUTPUT,
    element: (
      <PersonalLaboratoryOutput />
    ),
  },
  {
    path: RouteConstant.REFLECTION_QUESTIONNAIRE_TEXT,
    element: (
      <ReflectionQuestionnaireText />
    ),
  },
  {
    path: RouteConstant.REFLECTION_QUESTIONNAIRE_OUTPUT,
    element: (
      <ReflectionQuestionnaireOutput />
    ),
  },
  {
    path: RouteConstant.CONNECT_AND_CAPTIVATE_TEXT,
    element: (
      <ConnectAndCaptivateText />
    ),
  },
  {
    path: RouteConstant.CONNECT_AND_CAPTIVATE_OUTPUT,
    element: (
      <ConnectAndCaptivateOutput />
    ),
  },
  {
    path: RouteConstant.CONCEPTUAL_UNDERSTANDING_GENERATOR_TEXT,
    element: (
      <ConceptualUnderstandingGeneratorText />
    ),
  },
  {
    path: RouteConstant.CONCEPTUAL_UNDERSTANDING_GENERATOR_OUTPUT,
    element: (
      <ConceptualUnderstandingGeneratorOutput />
    ),
  },
  {
    path: RouteConstant.MISCONCEPTIONS_TEXT,
    element: (
      <MisconceptionsText />
    ),
  },
  {
    path: RouteConstant.MISCONCEPTIONS_OUTPUT,
    element: (
      <MisconceptionsOutput />
    ),
  },
  {
    path: RouteConstant.REAL_WORLD_TEXT,
    element: (
      <RealWorldText />
    ),
  },
  {
    path: RouteConstant.REAL_WORLD_OUTPUT,
    element: (
      <RealWorldOutput />
    ),
  },
  {
    path: RouteConstant.TEAM_WORK_TEXT,
    element: (
      <TeamWorkText />
    ),
  },
  {
    path: RouteConstant.TEAM_WORK_OUTPUT,
    element: (
      <TeamWorkOutput />
    ),
  },
  {
    path: RouteConstant.EVALUATION_RUBRICS_TEXT,
    element: (
      <EvaluationRubricsText />
    ),
  },
  {
    path: RouteConstant.EVALUATION_RUBRICS_OUTPUT,
    element: (
      <EvaluationRubricsOutput />
    ),
  },
  {
    path: RouteConstant.STORY_TELLING_TEXT,
    element: (
      <StoryTellingText />
    ),
  },
  {
    path: RouteConstant.STORY_TELLING_OUTPUT,
    element: (
      <StoryTellingOutput />
    ),
  },
  {
    path: RouteConstant.ENGLISH_TEACHER,
    element: (
      <VirtualEnglishTeacher />
    ),
  },
  {
    path: RouteConstant.ONLINE_TEXT,
    element: (
      <OnlineText />
    ),
  },
  {
    path: RouteConstant.MIND_MAPS,
    element: (
      <MindMaps />
    ),
  },
  {
    path: RouteConstant.MIND_MAPS_OUTPUT,
    element: (
      <MindMapsOutput />
    ),
  },
  {
    path: RouteConstant.UNIVERSITY_ORIENTATOR,
    element: (
      <UniversityOrientator />
    ),
  },
  {
    path: RouteConstant.LENGUA_LITERATURA,
    element: (
      <LenguaLiteratura />
    ),
  },
  {
    path: RouteConstant.TECNOLOGIA_EDUCACION,
    element: (
      <TecnologiaEducacion />
    ),
  },
  {
    path: RouteConstant.ARTS_EDUCATION,
    element: (
      <ArtsEducation />
    ),
  },
  {
    path: RouteConstant.AI_TEACHERS,
    element: (
      <AiTeachers />
    ),
  },
  {
    path: RouteConstant.ASSISTANTS_PAGE,
    element: (
      <AssistantsPage />
    ),
  },
  {
    path: RouteConstant.TAKE_ONLINE_TEST,
    element: (
      <TakeOnlineTest />
    ),
  },
];