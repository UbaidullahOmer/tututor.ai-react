import React from "react";
import { RouteConstant } from "./RouteConstant.jsx";

export const AiTools = [
  {
    id: 1,
    name: {
      en: "Generador de cuestionarios tipo test",
      sp: "Generador de cuestionarios tipo test",
    },
    detail: {
      en: "Crea cuestionarios tipo test sobre cualquier tema propio o que tú elijas con solo un clic.",
      sp: "Crea cuestionarios tipo test sobre cualquier tema propio o que tú elijas con solo un clic.",
    },
    icon: <img width={"55px"} src="icons/quiz.png" />,
    route: RouteConstant.QUIZ_GENERATOR,
    type: ["student", "teacher"],
    category: "Generador de Contenido",
  },
  { 
    id: 2,
    name: {
      en: "Creador de esquemas / mapas mentales",
      sp: "Creador de esquemas / mapas mentales",
    },
    detail: {
      en: "Crea esquemas / mapas mentales de tus temarios o cualquier otro cotenido con solo un click.",
      sp: "Crea esquemas / mapas mentales de tus temarios o cualquier otro cotenido con solo un click.",
    },
    icon: <img width={"55px"} src="icons/mindmap.png" />,
    route: RouteConstant.MIND_MAPS,
    type: ["student", "teacher"],
    category: "Generador de Contenido",
  },
  {
    id: 3,
    name: {
      en: "Resumidor inteligente",
      sp: "Resumidor inteligente",
    },
    detail: {
      en: "Con esta herramienta puedes generar un resumen de texto con solo un clic.",
      sp: "Genera resúmenes de textos largos o temas propios en las palabras que tú quieras.",
    },
    icon: <img width={"55px"} src="icons/summarize.png" />,
    route: RouteConstant.SUMMARIZE_TEXT,
    type: ["student", "teacher"],
    category: "Generador de Contenido",
  },
  {
    id: 4,
    name: {
      en: "Escritor Inteligente con IA",
      sp: "Escritor Inteligente con IA",
    },
    detail: {
      en: "En esta herramienta puedes generar cualquier tipo de cualquier temática de texto con solo un clic.",
      sp: "En esta herramienta puedes generar cualquier tipo de cualquier temática de texto con solo un clic.",
    },
    icon: <img width={"55px"} src="icons/writeai.png" />,
    route: RouteConstant.WRITE_TEXT,
    type: ["student", "teacher"],
    category: "Generador de Contenido",
  },
  {
    id: 5,
    name: {
      en: "Gamificaciones personalizadas",
      sp: "Gamificaciones",
    },
    detail: {
      en: "La gamificación es el proceso de mejorar el rendimiento de una persona. En esta herramienta puedes generar cualquier tipo de texto con solo un clic.",
      sp: "Crea gamificaciones personalizadas, divertidas e innovadoras para mantener enganchados a tus alumnos.",
    },
    icon: <img width={"55px"} src="icons/gamification.png" />,
    route: RouteConstant.GAMIFICATIONS_Text,
    type: "teacher",
    category: "Planificador y Diseño Educativo",
  },
  {
    id: 6,
    name: {
      en: "Explicador de textos inteligente",
      sp: "Explicador de textos inteligente",
    },
    detail: {
      en: "Con esta herramienta de IA puedes generar una explicación para cualquier texto con solo un clic.",
      sp: "¿Te cuesta comprender ciertos temas o conceptos? Esta herramienta te ayudará mucho.",
    },
    icon: <img width={"55px"} src="icons/explain.png" />,
    route: RouteConstant.EXPLAIN_TEXT,
    type: ["student", "teacher"],
    category: "Generador de Contenido",
  },
  {
    id: 7,
    name: {
      en: "Ejercicios adaptados al estudiante",
      sp: "Ejercicios adaptados al estudiante",
    },
    detail: {
      en: "Genera ejercicios personalizados según las necesidades e intereses del estudiante con solo un clic.",
      sp: "Genera ejercicios personalizados según las necesidades e intereses del estudiante con solo un clic.",
    },
    icon: <img width={"55px"} src="icons/exercisesadaptedToTheStudent.png" />,
    route: RouteConstant.EXERCISES_ADAPTED_TEXT,
    type: ["student", "teacher"],
    category: "Herramientas de Aprendizaje Adaptativo",
  },
  {
    id: 8,
    name: {
      en: "Generador de Preguntas o Exámenes Tipo Desarrollo",
      sp: "Generador de Preguntas o Exámenes Tipo Desarrollo",
    },
    detail: {
      en: "Genera preguntas de desarrollo para tus alumnos o para practicar de un tema o de un archivo tuyo",
      sp: "Genera preguntas de desarrollo para tus alumnos o para practicar de un tema o de un archivo tuyo.",
    },
    icon: <img width={"55px"} src="icons/questions.png" />,
    route: RouteConstant.DEVELOP_QUESTIONS_TEXT,
    type: ["student", "teacher"],
    category: "Generador de Contenido",
  },
  {
    id: 9,
    name: {
      en:"Virtual Teacher of Mathematics, Science and Physics and Chemistry",
      sp: "Profesor de Matemática, Ciencia y Física y Química",
    },
    detail: {
      en: "Aprende y practica matemáticas, ciencias, física o química con tu profesor virtual. Resuelve problemas, aclara conceptos y mejora tus habilidades en estas materias.",
      sp: "Aprende y practica matemáticas, ciencias, física o química con tu profesor virtual. Aquí tienes tu profesor particular ",
    },
    icon: <img width={"55px"} src="icons/mathteacher.png" />,
    route: RouteConstant.MATH_SCIENCE_TEACHER,
    type: ["student", "ai_teacher"],
  },
  {
    id: 10,
    name: {
      en: "Virtual Teacher of Arts, Physical Education and Personal Development",
      sp: "Profesor Virtual de Artes, Educación Física y Desarrollo Personal",
    },
    detail: {
      en: "Aprende y disfruta de Arte, Educación Física y Educación Religiosa con tu profesor virtual.",
      sp: "Aprende y disfruta de Arte, Educación Física y Educación Religiosa con tu profesor virtual.",
    },
    icon: <img width={"55px"} src="icons/mathteacher.png" />,
    route: RouteConstant.MATH_SCIENCE_TEACHER,
    type: ["student", "ai_teacher"],
  },
  {
    id: 11,
    name: {
      en: "Profesor Virtual de Tecnología y Educación para el Trabajo",
      sp: "Profesor Virtual de Tecnología y Educación para el Trabajo",
    },
    detail: {
      en: "Aprende y mejora tus habilidades en informática y tecnología con nuestro chatbot. Recibe ayuda y orientación en temas digitales y educación para el trabajo",
      sp: "Aprende y mejora tus habilidades en informática y tecnología con nuestro chatbot. Tu profesor particular",
    },
    icon: <img width={"55px"} src="icons/tech.png" />,
    route: RouteConstant.TECNOLOGIA_EDUCACION,
    type: ["student", "ai_teacher"],
  },
  {
    id: 12,
    name: {
      en: "Profesor Virtual de Lengua, Literatura y Comunicación",
      sp: "Profesor Virtual de Lengua, Literatura y Comunicación",
    },
    detail: {
      en: "Soy experto en Lengua y Literatura, a la vez que en comunicación. Estoy aquí para ayudarte en lo que necesites.",
      sp: "",
    },
    icon: <img width={"55px"} src="icons/literature.png" />,
    route: RouteConstant.LENGUA_LITERATURA,
    type: ["student", "ai_teacher"],
  },
  {
    id: 13,
    name: {
      en: "Profesor Virtual de Ciencias Sociales y Humanidades",
      sp: "Profesor de Ciencias Sociales",
    },
    detail: {
      en: "Aprende y explora temas de historia, geografía, economía y otras ciencias sociales con tu profesor virtual. Resuelve dudas, analiza eventos históricos y comprende mejor el mundo que te rodea.",
      sp: "Aprende y explora temas de historia, geografía, economía y otras ciencias sociales con tu profesor virtual.",
    },
    icon: <img width={"55px"} src="icons/socialscience.png" />,
    route: RouteConstant.SOCIAL_SCIENCE_TEACHER,
    type: ["student", "ai_teacher"],
  },
  {
    id: 14,
    name: {
      en: "Ideador de clases / sesiones",
      sp: "Ideador de clases / sesiones",
    },
    detail: {
      en: "Genera una sesión de clase atractiva utilizando los parámetros especificados con solo un clic.",
      sp: "Genera una sesión de clase atractiva e innovadora personalizada para tus necesidades como docente.",
    },
    icon: <img width={"55px"} src="icons/classIdeator.png" />,
    route: RouteConstant.CLASS_IDEATOR_TEXT,
    type: "teacher",
    category: "Planificador y Diseño Educativo",
  },
  {
    id: 15,
    name: {
      en: "Plan de educación personalizado",
      sp: "Plan de educación personalizado",
    },
    detail: {
      en: "Crea un plan de educación personalizado para estudiantes con necesidades especiales teniendo en cuenta estos criterios con solo un clic.",
      sp: "Crea un plan de educación personalizado para estudiantes con necesidades especiales.",
    },
    icon: <img width={"55px"} src="icons/personalizedEducation.png" />,
    route: RouteConstant.PERSONALIZED_EDUCATION_TEXT,
    type: "teacher",
    category: "Herramientas de Aprendizaje Adaptativo",
  },
  {
    id: 16,
    name: {
      en: "Creador de unidades didácticas",
      sp: "Creador de unidades didácticas",
    },
    detail: {
      en: "Diseña una unidad didáctica completa que cubra todos los aspectos especificados con solo un clic.",
      sp: "Diseña una unidad didáctica completa que cubra todos los aspectos especificados con solo un clic.",
    },
    icon: <img width={"55px"} src="icons/didacticUnitCreator.png" />,
    route: RouteConstant.DIDACTIC_UNITS_TEXT,
    type: "teacher",
    category: "Planificador y Diseño Educativo",
  },
  {
    id: 17,
    name: {
      en: "Lista de vocabulario para un tema",
      sp: "Lista de vocabulario para un tema",
    },
    detail: {
      en: "Crea listas de vocabulario a partir de un tema",
      sp: "Crea una lista concisa de palabras clave de vocabulario para el tema dado con solo un clic.",
    },
    icon: <img width={"55px"} src="icons/VocabularyIcon.png" />,
    route: RouteConstant.VOCABULARY_LIST_TEXT,
    type: ["student", "teacher"],
    category: "Generador de Contenido", 
  },
  {
    id: 18,
    name: {
      en: "Laboratorio personal",
      sp: "Laboratorio personal",
    },
    detail: {
      en: "Planifica una sesión de laboratorio adecuada para los parámetros dados. El experimento debe ser creativo, original y posible de realizar con estos criterios con solo un clic.",
      sp: "Crea experimentos que atraigan a tus alumnos o incluso que los alumnos puedan hacer en casa. ",
    },
    icon: <img width={"55px"} src="icons/Laboratory.png" />,
    route: RouteConstant.PERSONAL_LABORATORY_TEXT,
    type: ["student", "teacher"],
    category: "Recursos de Apoyo al Aprendizaje",
  },
  {
    id: 19,
    name: {
      en: "Rúbricas de evaluación",
      sp: "Rúbricas de evaluación",
    },
    detail: {
      en: "Crea una rúbrica detallada para evaluar el trabajo de los estudiantes con solo un clic.",
      sp: "Crea una rúbrica detallada para evaluar el trabajo de los estudiantes con solo un clic.",
    },
    icon: <img width={"55px"} src="icons/evaluationRubrics.png" />,
    route: RouteConstant.EVALUATION_RUBRICS_TEXT,
    type: "teacher",
    category: "Evaluación y Retroalimentación",
  },
  {
    id: 20,
    name: {
      en: "Conexión con el mundo real de un tema",
      sp: "Conexión con el mundo real de un tema",
    },
    detail: {
      en: "Proporciona aplicaciones prácticas del mundo real del contenido académico con solo un clic.",
      sp: "¿No entiendes para qué sirve lo que aprendes en la escuela? ¡Esta herramienta te lo muestra!",
    },
    icon: <img width={"55px"} src="icons/realWorldConnectionToTopic.png" />,
    route: RouteConstant.REAL_WORLD_TEXT,
    type: ["student", "teacher"],
    category: "Recursos de Apoyo al Aprendizaje",
  },
  {
    id: 21,
    name: {
      en: "Traductor de texto",
      sp: "Traductor de texto",
    },
    detail: {
      en: "En esta herramienta puedes traducir cualquier texto con solo un clic.",
      sp: "En esta herramienta puedes traducir cualquier texto a cualquier idioma con solo un clic.",
    },
    icon: <img width={"55px"} src="icons/practiceEnglish.png" />,
    route: RouteConstant.TRANSLATOR_TEXT,
    type: ["student", "teacher"],
    category: "Recursos de Apoyo al Aprendizaje",
  },
  {
    id: 22,
    name: {
      en: "Creador de grupos adaptados",
      sp: "Creador de grupos adaptados",
    },
    detail: {
      en: "Crea grupos de estudiantes equilibrados basados en los criterios dados con solo un clic.",
      sp: "Crea grupos de estudiantes equilibrados basados en los criterios dados con solo un clic.",
    },
    icon: <img width={"55px"} src="icons/adaptedGroupsCreator.png" />,
    route: RouteConstant.ADAPTED_GROUPS_TEXT,
    type: "teacher",
    category: "Herramientas de Aprendizaje Adaptativo",
  },
  {
    id: 23,
    name: {
      en: "Diseñador de tareas",
      sp: "Diseñador de tareas",
    },
    detail: {
      en: "Diseña tareas educativas alineadas con el objetivo de aprendizaje y el tema con solo un clic.",
      sp: "Diseña tareas educativas alineadas con el objetivo de aprendizaje y el tema con solo un clic.",
    },
    icon: <img width={"55px"} src="icons/taskDesigner.png" />,
    route: RouteConstant.TASK_DESIGNER_TEXT,
    type: "teacher",
    category: "Planificador y Diseño Educativo",
  },
  {
    id: 24,
    name: {
      en: "Creador de situaciones de aprendizaje",
      sp: "Creador de situaciones de aprendizaje",
    },
    detail: {
      en: "Desarrolla escenarios de aprendizaje adaptados a las características de la clase y el contenido con solo un clic.",
      sp: "Desarrolla Situaciones de Aprendizaje personalizadas y adaptadas a las características de la clase.",
    },
    icon: <img width={"55px"} src="icons/learningSituationsCreator.png" />,
    route: RouteConstant.LEARNING_SITUATIONS_TEXT,
    type: "teacher",
    category: "Planificador y Diseño Educativo",
  },
  {
    id: 25,
    name: {
      en: "Retroalimentación de tareas",
      sp: "Retroalimentación de tareas",
    },
    detail: {
      en: "Proporciona retroalimentación constructiva sobre las tareas de los estudiantes basada en criterios de evaluación con solo un clic.",
      sp: "Ofrece feedback constructivo basado en criterios de evaluación.",
    },
    icon: <img width={"55px"} src="icons/TaskFeedback.png" />,
    route: RouteConstant.TASK_FEEDBACK_TEXT,
    type: ["student", "teacher"],    category: "Evaluación y Retroalimentación",
  },
  {
    id: 26,
    name: {
      en: "Generador de texto a partir de vocabulario de un tema",
      sp: "Generador de texto a partir de vocabulario de un tema",
    },
    detail: {
      en: "Genera un texto a partir de vocabulario especifico",
      sp: "Genera un texto a partir de vocabulario especifico.",
    },
    icon: <img width={"55px"} src="icons/vocabularyListForTopic.png" />,
    route: RouteConstant.VOCABULARY_GENERATOR_TEXT,
    type: ["student", "teacher"],
    category: "Generador de Contenido",
  },
  // ...
  // {
  //   id: 27,
  //   name: {
  //     en: "Generador de actividades educativas",
  //     sp: "Generador de actividades educativas",
  //   },
  //   detail: {
  //     en: "Diseña actividades educativas que cumplan con los objetivos de aprendizaje con solo un clic.",
  //     sp: "Diseña actividades educativas que cumplan con los objetivos de aprendizaje con solo un clic.",
  //   },
  //   icon: <img width={"55px"} src="icons/educationalActivities.png" />,
  //   route: RouteConstant.EDUCATIONAL_ACTIVITIES_TEXT,
  //   type: "teacher",
  //   category: "Planificador y Diseño Educativo",
  // },
  {
    id: 28,
    name: {
      en: "Generador de informes de evaluación",
      sp: "Generador de informes de evaluación",
    },
    detail: {
      en: "Produce un informe de evaluación detallado para el estudiante teniendo en cuenta todos los aspectos mencionados a continuación con solo un clic.",
      sp: "Genera un informe de evaluación detallado para el estudiante con solo un clic.",
    },
    icon: <img width={"55px"} src="icons/evaluation.png" />,
    route: RouteConstant.EVALUATION_REPORT_TEXT,
    type: ["teacher", "student"],
    category: "Evaluación y Retroalimentación",
  },
  {
    id: 29,
    name: {
      en: "Cuestionario de reflexión para estudiantes conflictivos",
      sp: "Cuestionario de reflexión para estudiantes conflictivos",
    },
    detail: {
      en: "Crea un cuestionario reflexivo para promover la autorreflexión con solo un clic.",
      sp: "Crea un cuestionario reflexivo para promover la autorreflexión de alumnoscon.",
    },
    icon: <img width={"55px"} src="icons/ReflectionIcon.png" />,
    route: RouteConstant.REFLECTION_QUESTIONNAIRE_TEXT,
    type: "teacher",
    category: "Evaluación y Retroalimentación",
  },
  {
    id: 30,
    name: {
      en: "Conecta y cautiva",
      sp: "Conectar y captar",
    },
    detail: {
      en: "Sugiere formas de hacer que el contenido sea atractivo y relevante para los estudiantes, aplicable a la vida real, con solo un clic.",
      sp: "Escribe el tema a tratar y obtén estrategias creativas y prácticas para captar y mantener la atención de tus estudiantes.",
    },
    icon: <img width={"55px"} src="icons/connectAndCaptivate.png" />,
    route: RouteConstant.CONNECT_AND_CAPTIVATE_TEXT,
    type: ["student", "teacher"],
    category: "Recursos de Apoyo al Aprendizaje",
  },
  {
    id: 31,
    name: {
      en: "Generador de comprensión conceptual",
      sp: "Generador de comprensión conceptual",
    },
    detail: {
      en: "Explica conceptos complejos de una manera fácilmente comprensible con solo un clic.",
      sp: "",
    },
    icon: (
      <img width={"55px"} src="icons/conceptualUnderstandingGenerator.png" />
    ),
    route: RouteConstant.CONCEPTUAL_UNDERSTANDING_GENERATOR_TEXT,
    type: ["student", "teacher"],
    category: "Recursos de Apoyo al Aprendizaje",
  },
  {
    id: 32,
    name: {
      en: "Corrector de Ideas Comunes Equivocadas",
      sp: "Conceptos erróneos",
    },
    detail: {
      en: "Identifica y aborda los malentendidos comunes sobre un contenido y los explica.",
      sp: "",
    },
    icon: <img width={"55px"} src="icons/MisconceptionIcon.png" />,
    route: RouteConstant.MISCONCEPTIONS_TEXT,
    type: ["student", "teacher"],
    category: "Recursos de Apoyo al Aprendizaje",
  },
  {
    id: 33,
    name: {
      en: "Creador y organizador de trabajo en equipo",
      sp: "Creador y organizador de trabajo en equipo",
    },
    detail: {
      en: "Diseña una actividad grupal profesional, personalizada y bien estructurada para tu clase con solo unos clicks.",
      sp: "",
    },
    icon: <img width={"55px"} src="icons/teamworkOrganizerAndCreator.png" />,
    route: RouteConstant.TEAM_WORK_TEXT,
    type: "teacher",
    category: "Recursos de Apoyo al Aprendizaje",
  },
  {
    id: 34,
    name: {
      en: "Cuenta Cuentos",
      sp: "Narración de historias",
    },
    detail: {
      en: "Crea historias y cuentos educativos personalizadas con todo lo que necesitas para tus alumnos.",
      sp: "",
    },
    icon: <img width={"55px"} src="icons/storytelling.png" />,
    route: RouteConstant.STORY_TELLING_TEXT,
    type: "teacher",
  },
  {
    id: 35,
    name: {
      en: "Orientador Pre-Universitario",
      sp: "Orientador Pre-Universitario",
    },
    detail: {
      en: "Ve qué posibles carreras podrías estudiar dependiendo de tus gustos y aficiones. A la vez, también podrás ver qué salidas laborales tendrán las carreras que quieras saber. Estoy aquí para guiarte en tu futuro",
      sp: "",
    },
    icon: <img width={"55px"} src="icons/university.png" />,
    route: RouteConstant.UNIVERSITY_ORIENTATOR,
    type: ["student", "ai_teacher"],
  },
  {
    id: 36,
    name: {
      en: "Profesor Virtual de Inglés",
      sp: "Profesor Virtual de Inglés",
    },
    detail: {
      en: "Practice English with your AI tutor. You can speak, write, or even send images for exercise corrections.",
      sp: "Practica inglés con tu profesor virtual. Puedes hablar, escribir o incluso corregir ejercicios.",
    },
    icon: <img width={"55px"} src="icons/practiceEnglish.png" />,
    route: RouteConstant.ENGLISH_TEACHER,
    type: ["ai_teacher"],
  },
];

export const toolsCategories = [
  "Generador de Contenido",
  "Planificador y Diseño Educativo",
  "Evaluación y Retroalimentación",
  "Herramientas de Aprendizaje Adaptativo",
  "Recursos de Apoyo al Aprendizaje",
];
