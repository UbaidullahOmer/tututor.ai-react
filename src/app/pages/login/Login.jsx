import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import useAuth from '../../hooks/useAuth';
import { useSnackBarManager } from '../../hooks/useSnackBarManager';
import './styles.css';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { signInWithEmailAndPassword, signInWithGoogle, signInWithFacebook } = useAuth();
  const { fnShowSnackBar } = useSnackBarManager();
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await signInWithEmailAndPassword(email, password);
      fnShowSnackBar("Logged in successfully", );
      navigate("/");
    } catch (error) {
      fnShowSnackBar(error.message, "error");
    }
  };

  const handleGoogleSignIn = async () => {
    try {
      await signInWithGoogle();
      fnShowSnackBar("Signed in with Google successfully");
      navigate("/");
    } catch (error) {
      fnShowSnackBar(error.message, "error");
    }
  };

  const handleFacebookSignIn = async () => {
    try {
      await signInWithFacebook();
      fnShowSnackBar("Signed in with Facebook successfully" );
      navigate("/");
    } catch (error) {
      fnShowSnackBar(error.message, "error");
    }
  };

  return (
    <div className="flex flex-col items-center justify-center min-h-screen bg-white p-4">
      <div className="w-full max-w-md">
        <div className="flex justify-center mb-4">
          <img src="/icons/logobackground.png" alt="Logo" className="h-20" />
        </div>
        <h2 className="text-2xl font-bold text-center mb-6">Iniciar sesión</h2>
        <form onSubmit={handleSubmit} className="space-y-4">
          <div>
            <input
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              className="w-full p-3 border border-gray-300 rounded-md focus:ring-2 focus:ring-purple-600 focus:border-purple-600"
              placeholder="Correo electrónico"
              required
            />
          </div>
          <div>
            <input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              className="w-full p-3 border border-gray-300 rounded-md focus:ring-2 focus:ring-purple-600 focus:border-purple-600"
              placeholder="Contraseña"
              required
            />
          </div>
          <button
            type="submit"
            className="w-full gradient-bg button-style"
          >
            Iniciar sesión
          </button>
        </form>
        <div className="mt-4 flex items-center justify-center gap-2">
          <button
            onClick={handleGoogleSignIn}
            className="w-full p-3 border border-gray-300 rounded-md flex items-center justify-center space-x-2 hover:bg-gray-50 transition duration-300"
          >
            <img src="/icons/google.png" alt="Google" className="w-6 h-6" />
            <span>Iniciar sesión con Google</span>
          </button>
          <button
            onClick={handleFacebookSignIn}
            className="w-full p-3 border border-gray-300 rounded-md flex items-center justify-center space-x-2 hover:bg-gray-50 transition duration-300"
          >
            <img src="/icons/facebook.png" alt="Facebook" className="w-6 h-6" />
            <span>Iniciar sesión con Facebook</span>
          </button>
        </div>
        <div className="mt-4 text-center">
          <Link to="/forgot-password" className="text-purple-600 hover:underline">
            ¿Olvidaste tu contraseña?
          </Link>
        </div>
        <div className="mt-4 text-center">
          <p className="text-gray-600">
            ¿No tienes una cuenta?{' '}
            <Link to="/signup" className="text-purple-600 hover:underline">
              Regístrate
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
};


export default Login;