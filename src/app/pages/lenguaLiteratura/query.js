// query.js
const url = "https://flowise-production-ea49.up.railway.app/api/v1/prediction/6d86e9e8-700d-4f16-acbf-c72ec7fd92f6";
const headers = {
  "Content-Type": "application/json",
  "Authorization": "Bearer DKUyxdD7sYQpyxcUW8gu_eQUeRz5UVG_EBTCfMaJFGE"
};

async function query(data) {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers,
      body: JSON.stringify(data)
    });

    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }

    const result = await response.json();
    return result;
  } catch (error) {
    console.error('Error in query function:', error.message);
    return null;
  }
}

export default query;
