import React, { useEffect, useState } from "react";
import SmallCard from "../../components/smallCard/SmallCard";
import BigCard from "../../components/bigCard/BigCard";
import { AiTools, toolsCategories } from "../../constants/AiTools";
import MeText from "../../components/meText/MeText";
import {
  getLocalStorage,
  setLocalStorage,
} from "../../components/local_storage";
import { categoryColors } from "../../constants/colors";
import { selectTools } from "../../redux/FavToolsReducer";
import { useSelector } from "react-redux";

function Home() {
  const [cardType, setCardType] = useState("vertical");
  const [search, setSearch] = useState("");
  const [toolType, setToolType] = useState("teacher");
  const [selectedCategory, setSelectedCategory] = useState("all");
  const favoriteTools = useSelector(selectTools);

  useEffect(() => {
    setCardType(getLocalStorage("cardType") || "vertical");
    setToolType(getLocalStorage("toolType") || "teacher");
    setSearch(getLocalStorage("search") || "");
    setSelectedCategory(getLocalStorage("selectedCategory") || "all");
  }, []);

  const filterData = AiTools?.filter((item) => {
    if (toolType === "favorite") {
      return favoriteTools.some(fav => fav.id === item.id);
    } else {
      return item?.type.includes(toolType);
    }
  })
  ?.filter((item) =>
    item?.name?.sp
      ?.toLowerCase()
      ?.includes(typeof search === "string" ? search.toLowerCase() : "")
  )
  ?.filter((item) => {
    if (selectedCategory === "all") {
      return true;
    } else {
      return item.category === selectedCategory;
    }
  });

  return (
    <div className="overflow-scroll max-h-[90vh] min-h-[90vh] 2xl:p-[32px] p-[14px]">
      <div className="flex flex-col items-center 2xl:gap-[36px] gap-[20px]">
        <div className="flex gap-[32px] max-xl:flex-col items-center justify-between w-full">
          <div
            className={
              "flex min-w-[670px] h-[53px] items-center overflow-hidden rounded-full border-[1px] border-[#4EA7FF] p-[2px] bg-white"
            }
          >
            <MeText
              onClick={() => {
                setToolType("teacher");
                setLocalStorage("toolType", "teacher");
              }}
              className={`w-full cursor-pointer rounded-full  p-[12px] text-center ${
                toolType === "teacher"
                  ? "bg-[#4EA7FF] text-[#FFF]"
                  : "bg-[#FFF] text-[#4EA7FF]"
              }`}
            >
              Profesor
            </MeText>
            <MeText
              onClick={() => {
                setToolType("student");
                setLocalStorage("toolType", "student");
              }}
              className={`w-full cursor-pointer rounded-full  p-[12px] text-center ${
                toolType === "student"
                  ? "bg-[#4EA7FF] text-[#FFF]"
                  : "bg-[#FFF] text-[#4EA7FF]"
              }`}
            >
              Estudiante
            </MeText>
            <MeText
              onClick={() => {
                setToolType("ai_teacher");
                setLocalStorage("toolType", "ai_teacher");
              }}
              className={`w-full cursor-pointer rounded-full  p-[12px] text-center ${
                toolType === "ai_teacher"
                  ? "bg-[#4EA7FF] text-[#FFF]"
                  : "bg-[#FFF] text-[#4EA7FF]"
              }`}
            >
              Profesores Virtuales
            </MeText>
            <MeText
              onClick={() => {
                setToolType("favorite");
                setLocalStorage("toolType", "favorite");
              }}
              className={`w-full cursor-pointer rounded-full  p-[12px] text-center ${
                toolType === "favorite"
                  ? "bg-[#4EA7FF] text-[#FFF]"
                  : "bg-[#FFF] text-[#4EA7FF]"
              }`}
            >
              Favoritos 
            </MeText>
          </div>
          <div
            className="flex max-w-[486px] w-[486px] items-center justify-between rounded-[8px] p-[12px] bg-white"
            style={{ boxShadow: "0px 4px 40px 0px rgba(0, 0, 0, 0.05)" }}
          >
            <input
              onChange={(e) => {
                setSearch(e.target.value);
                setLocalStorage("search", e.target.value);
              }}
              value={search}
              type="text"
              className="w-full font-[500] text-[#4EA7FF] outline-none placeholder:text-[#2B3D70]"
              placeholder="Buscar herramientas aquí"
              onFocus={(e) => e.target.placeholder = ""}
              onBlur={(e) => e.target.placeholder = "Buscar herramientas aquí"}
            />
            <div className="flex cursor-pointer items-center justify-center gap-[6px] rounded-[8px] bg-[#4EA7FF] px-[12px] py-[10px] transition-all hover:scale-95">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                viewBox="0 0 16 16"
                fill="none"
                className="h-[20px] w-[20px]"
              >
                <path
                  d="M14 14L11.1046 11.1046M11.1046 11.1046C12.0697 10.1394 12.6667 8.80609 12.6667 7.33333C12.6667 4.38781 10.2789 2 7.33333 2C4.38781 2 2 4.38781 2 7.33333C2 10.2789 4.38781 12.6667 7.33333 12.6667C8.80609 12.6667 10.1394 12.0697 11.1046 11.1046Z"
                  stroke="white"
                  strokeWidth="1.33333"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
              <span className="text-[#FFF]"> Buscar </span>
            </div>
          </div>
        </div>
        <div className="flex items-center w-full border-b-[2px] border-[#73779109]">
          <span
            onClick={() => {
              setSelectedCategory("all");
              setLocalStorage("selectedCategory", "all");
            }}
            className={`w-fit cursor-pointer p-[12px] font-[500] text-center ${
              selectedCategory === "all"
                ? "text-[#4EA7FF] border-b-[2px] border-[#4EA7FF]"
                : " text-[#737791]"
            }`}
          >
            Todos
          </span>
          {toolsCategories.map((category, index) => (
            <span
              key={index}
              onClick={() => {
                setSelectedCategory(category);
                setLocalStorage("selectedCategory", category);
              }}
              className={`w-fit cursor-pointer p-[12px] font-[500] text-center  ${
                selectedCategory === category
                  ? "text-[#4EA7FF]! border-b-[2px] border-[#4EA7FF]! "
                  : ""
              }`}
              style={{
                color: categoryColors[selectedCategory] || "#737791",
                borderColor: categoryColors[selectedCategory] || "#737791",
              }}
            >
              {category}
            </span>
          ))}
        </div>
        <div className="flex w-full items-center justify-between">
          <div className="flex flex-col gap-[4px]">
            <span className="text-[18px] font-[500] text-[#2B3D70]">
              Mostrando {filterData.length} Resultados
            </span>
            <span className="text-[14px] text-[#737791]">
              Basado en tus preferencias
            </span>
          </div>
          <div className="flex items-center gap-[18px]">
            <div className="flex items-center justify-center gap-[12px] rounded-[10px] border-[1px] border-[#4EA7FF] p-[10px]">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                width="20"
                height="20"
                color="#4EA7FF"
                fill="none"
                className="h-[20px] w-[20px]"
              >
                <path
                  d="M6 15.5L18.0001 15.5001"
                  stroke="currentColor"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M3 11.5H21"
                  stroke="currentColor"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M8 2.5V8.5M8 8.5L6 6.5M8 8.5L10 6.5"
                  stroke="currentColor"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M12 15.5V21.5M12 21.5L10 19.5M12 21.5L14 19.5"
                  stroke="currentColor"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M16 2.5V8.5M16 8.5L14 6.5M16 8.5L18 6.5"
                  stroke="currentColor"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
              <span className="text-[#2B3D70]">Nuevas</span>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                width="20"
                height="20"
                color="#4EA7FF"
                fill="none"
                className="h-[20px] w-[20px]"
              >
                <path
                  d="M18 9.00005C18 9.00005 13.5811 15 12 15C10.4188 15 6 9 6 9"
                  stroke="currentColor"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
            </div>
            <div className="flex items-center justify-center gap-[12px] rounded-[10px] border-[1px] border-[#4EA7FF] p-[10px]">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                width="20"
                height="20"
                color="#4EA7FF"
                fill="none"
                className="h-[20px] w-[20px]"
              >
                <path
                  d="M7 7L17 7"
                  stroke="currentColor"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M4 12H20"
                  stroke="currentColor"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M7 17H17"
                  stroke="currentColor"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
              <span className="text-[#2B3D70]">Más usadas</span>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                width="20"
                height="20"
                color="#4EA7FF"
                fill="none"
                className="h-[20px] w-[20px]"
              >
                <path
                  d="M18 9.00005C18 9.00005 13.5811 15 12 15C10.4188 15 6 9 6 9"
                  stroke="currentColor"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
            </div>
            <div className="flex items-center gap-[8px]">
              <button
                className={`p-2 rounded-full ${
                  cardType === "vertical" ? "bg-[#4EA7FF] text-white" : "bg-white text-[#4EA7FF]"
                }`}
                onClick={() => {
                  setCardType("vertical");
                  setLocalStorage("cardType", "vertical");
                }}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  width="16"
                  height="16"
                  fill="none"
                  className="h-[16px] w-[16px]"
                >
                  <path
                    d="M4 6h16M4 12h16M4 18h16"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
              </button>
              <button
                className={`p-2 rounded-full ${
                  cardType === "horizontal" ? "bg-[#4EA7FF] text-white" : "bg-white text-[#4EA7FF]"
                }`}
                onClick={() => {
                  setCardType("horizontal");
                  setLocalStorage("cardType", "horizontal");
                }}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  width="16"
                  height="16"
                  fill="none"
                  className="h-[16px] w-[16px]"
                >
                  <path
                    d="M3 6h7v7H3zM14 6h7v7h-7zM3 15h7v7H3zM14 15h7v7h-7z"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
              </button>
            </div>
          </div>
        </div>
        <div className="grid grid-cols-1 gap-[20px] 2xl:grid-cols-3 xl:grid-cols-2">
          {filterData.map((item, index) =>
            cardType === "vertical" ? (
              <SmallCard key={index} data={item} index={index} />
            ) : (
              <BigCard key={index} data={item} />
            )
          )}
        </div>
      </div>
    </div>
  );
}

export default Home;
