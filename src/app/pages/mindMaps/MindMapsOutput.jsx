import React, { useEffect, useRef, useState } from 'react';
import { Transformer } from 'markmap-lib';
import { Markmap, loadCSS, loadJS } from 'markmap-view';
import { getLocalStorage, setLocalStorage } from "../../components/local_storage";
import { RouteConstant } from "../../constants/RouteConstant";
import { useLocation } from 'react-router-dom';
import Button from '../../components/button/Button';
import { useDrag, useDrop, DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { FaTrashAlt } from 'react-icons/fa';
import { FaArrowLeft, FaArrowRight } from 'react-icons/fa';
import { AiOutlinePlusCircle, AiOutlineDelete } from 'react-icons/ai';
import { BsGripVertical } from 'react-icons/bs';
import * as d3 from 'd3';
const ItemType = 'BOX';

const DraggableBox = ({ id, index, moveBox, handleBoxEdit, handleDelete, addNewItem, level, content, changeLevel }) => {
  const ref = useRef(null);

  const [, drop] = useDrop({
    accept: ItemType,
    hover(item) {
      if (item.index !== index) {
        moveBox(item.index, index);
        item.index = index;
      }
    },
  });

  const [{ isDragging }, drag, preview] = useDrag({
    type: ItemType,
    item: { id, index },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  drag(drop(ref));

  const handleFocus = (e) => {
    e.target.style.transform = 'scale(1.2)';
    e.target.style.transition = 'transform 0.3s ease';
  };

  const handleBlur = (e) => {
    e.target.style.transform = 'scale(1)';
  };

  const handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      handleBoxEdit(e, index);
      e.target.blur();
    }
  };

  return (
    <div
      ref={preview}
      className={`box ${isDragging ? 'opacity-50' : ''}`}
      style={{
        cursor: 'move',
        marginBottom: '12px',
        border: '1px solid #ddd',
        padding: '10px',
        borderRadius: '8px',
        backgroundColor: '#f9f9f9',
        boxShadow: '0 2px 5px rgba(0, 0, 0, 0.15)',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        transition: 'background-color 0.3s ease',
        marginLeft: `${(level - 1) * 20}px`,
      }}
      onMouseEnter={(e) => {
        e.currentTarget.style.backgroundColor = '#ffffff';
      }}
      onMouseLeave={(e) => {
        e.currentTarget.style.backgroundColor = '#f9f9f9';
      }}
    >
      <div ref={ref} style={{ marginRight: '10px', cursor: 'move' }}>
        <BsGripVertical style={{ fontSize: '20px', color: '#333' }} />
      </div>
      <div
        contentEditable={true}
        suppressContentEditableWarning={true}
        onBlur={handleBlur}
        onFocus={handleFocus}
        onKeyDown={handleKeyPress}
        style={{
          fontSize: '15px',
          flexGrow: 1,
          marginRight: '10px',
          color: '#333',
          fontFamily: "'Segoe UI', Tahoma, Geneva, Verdana, sans-serif",
        }}
      >
        {content}
      </div>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <FaArrowLeft
          onClick={() => changeLevel(index, -1)}
          style={{
            cursor: 'pointer',
            marginRight: '5px',
            fontSize: '14px',
            color: '#666',
          }}
        />
        <FaArrowRight
          onClick={() => changeLevel(index, 1)}
          style={{
            cursor: 'pointer',
            marginRight: '10px',
            fontSize: '14px',
            color: '#666',
          }}
        />
        <AiOutlinePlusCircle
          onClick={() => addNewItem(index, level)}
          style={{
            cursor: 'pointer',
            marginRight: '10px',
            fontSize: '18px',
            color: '#2f67df',
          }}
        />
        <AiOutlineDelete
          onClick={() => handleDelete(index)}
          style={{
            cursor: 'pointer',
            fontSize: '18px',
            color: '#FF5733',
          }}
        />
      </div>
    </div>
  );
};

const MindMapsOutput = () => {
  const svgRef = useRef(null);
  const markmapRef = useRef(null);
  const editorRef = useRef(null);
  const scrollbarRef = useRef(null);
  const location = useLocation();
  const initialData =
    location.state || getLocalStorage(RouteConstant.MIND_MAPS_OUTPUT) || "# Mapa Mental";
  const [mindMapData, setMindMapData] = useState(initialData);
  const [editorWidth, setEditorWidth] = useState(30); // Adjusted the width to match the desired area
  const [mapWidth, setMapWidth] = useState(70);
  const [mapTransform, setMapTransform] = useState(null);
  const [isDragging, setIsDragging] = useState(false);

  const handleBoxEdit = (event, index) => {
    const newContent = event.target.textContent;
    const lines = mindMapData.split('\n');
    const match = lines[index].match(/^(#+)\s/);
    if (match) {
      lines[index] = `${match[1]} ${newContent.trim()}`;
    } else {
      lines[index] = newContent.trim();
    }
    setMindMapData(lines.join('\n'));
    setLocalStorage(RouteConstant.MIND_MAPS_OUTPUT, lines.join('\n'));
  };

  const handleDelete = (index) => {
    const lines = mindMapData.split('\n');
    lines.splice(index, 1);
    setMindMapData(lines.join('\n'));
    setLocalStorage(RouteConstant.MIND_MAPS_OUTPUT, lines.join('\n'));
  };

  const formatVisibleData = (data) => {
    const lines = data.split('\n');
    return lines.map((line, index) => {
      const match = line.match(/^(#+)\s(.*)$/);
      if (match) {
        const level = match[1].length;
        const content = match[2];
  
        let color;
        switch (level) {
          case 1: color = '#FF0000'; break;
          case 2: color = '#0000FF'; break;
          case 3: color = '#008000'; break;
          case 4: color = '#6637b3'; break;
          case 5: color = '#FFA500'; break;
          case 6: color = '#ae1797'; break;
          case 7: color = '#5f5f5f'; break;
          default: color = '#000000';
        }
  
        return (
          <DraggableBox
            key={index}
            id={`box-${index}`}
            index={index}
            moveBox={moveBox}
            handleBoxEdit={handleBoxEdit}
            handleDelete={handleDelete}
            addNewItem={addNewItem}
            level={level}
            content={
              <span style={{ color: color, fontWeight: 'bold' }}>
                {content}
              </span>
            }
            changeLevel={changeLevel}
          />
        );
      }
      return null;
    }).filter(Boolean); // Remove any null entries
  };

  const moveBox = (fromIndex, toIndex) => {
    const data = mindMapData.split('\n');
    const [movedItem] = data.splice(fromIndex, 1);
    data.splice(toIndex, 0, movedItem);
    setMindMapData(data.join('\n'));
    setLocalStorage(RouteConstant.MIND_MAPS_OUTPUT, data.join('\n'));
  };

  const addNewItem = (index, level) => {
    const newData = mindMapData.split('\n');
    newData.splice(index + 1, 0, `${'#'.repeat(level)} Nuevo elemento`);
    setMindMapData(newData.join('\n'));
  };

  const changeLevel = (index, direction) => {
    const newData = mindMapData.split('\n');
    const match = newData[index].match(/^(#+)\s(.*)$/);
    if (match) {
      let level = match[1].length;
      level = level + direction;
      if (level < 1) level = 1;
      newData[index] = `${'#'.repeat(level)} ${match[2]}`;
      setMindMapData(newData.join('\n'));
      setLocalStorage(RouteConstant.MIND_MAPS_OUTPUT, newData.join('\n'));
    }
  };

  useEffect(() => {
    document.body.style.overflow = 'hidden';
    try {
      loadCSS();
      loadJS();
    } catch (error) {
      console.error('Error loading CSS or JS:', error);
    }
    setMindMapData(initialData);
    return () => {
      document.body.style.overflow = 'auto';
    };
  }, [initialData]);

  useEffect(() => {
    if (svgRef.current && mindMapData) {
      const transformer = new Transformer();
      const { root } = transformer.transform(mindMapData);
  
      if (markmapRef.current) {
        markmapRef.current.setData(root);
        if (mapTransform) {
          markmapRef.current.fit();
        }
      } else {
        markmapRef.current = Markmap.create(svgRef.current, null, root);
      }
    }
  }, [mindMapData, mapTransform]);

  const downloadHTML = () => {
    const transformer = new Transformer();
    const { root } = transformer.transform(mindMapData);
    const rootJson = JSON.stringify(root);

    const htmlContent = `
      <!DOCTYPE html>
      <html>
        <head>
          <meta charset="UTF-8">
          <title>Interactive Mind Map</title>
          <script src="https://cdn.jsdelivr.net/npm/d3@6"></script>
          <script src="https://cdn.jsdelivr.net/npm/markmap-view"></script>
        </head>
        <body>
          <svg id="mindmap" style="width: 100%; height: 100vh;"></svg>
          <script>
            (function() {
              const { Markmap } = window.markmap;
              const root = ${rootJson};
              Markmap.create('#mindmap', null, root);
            })();
          </script>
        </body>
      </html>
    `;

    const blob = new Blob([htmlContent], { type: 'text/html' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'interactive_mindmap.html';
    a.click();
    URL.revokeObjectURL(url);
  };

  const downloadSVG = () => {
    const svgData = new XMLSerializer().serializeToString(svgRef.current);
    const svgBlob = new Blob([svgData], {
      type: "image/svg+xml;charset=utf-8",
    });
    const url = URL.createObjectURL(svgBlob);
    const a = document.createElement("a");
    a.href = url;
    a.download = "mindmap.svg";
    a.click();
    URL.revokeObjectURL(url);
  };

  const handleDrag = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const newEditorWidth = (e.clientX / window.innerWidth) * 100;
    if (newEditorWidth > 10 && newEditorWidth < 90) {
      setEditorWidth(newEditorWidth);
      setMapWidth(100 - newEditorWidth);
    }
  };

  const stopDrag = () => {
    document.removeEventListener('mousemove', handleDrag);
    document.removeEventListener('mouseup', stopDrag);
    document.body.style.userSelect = 'auto';
  };

  const startDrag = () => {
    document.addEventListener('mousemove', handleDrag);
    document.addEventListener('mouseup', stopDrag);
    document.body.style.userSelect = 'none';
  };

  const handleMapZoom = () => {
    if (markmapRef.current) {
      const svg = markmapRef.current.svg;
      setMapTransform({
        pan: svg.pan(),
        zoom: svg.zoom()
      });
    }
  };

  const handleScroll = (e) => {
    const { scrollTop, scrollHeight, clientHeight } = e.target;
    const scrollPercentage = (scrollTop / (scrollHeight - clientHeight)) * 100;
    const scrollbarHeight = (clientHeight / scrollHeight) * 100;

    if (scrollbarRef.current) {
      scrollbarRef.current.style.top = `${scrollPercentage}%`;
      scrollbarRef.current.style.height = `${scrollbarHeight}%`;
    }
  };

  const handleScrollbarDragStart = () => {
    setIsDragging(true);
  };

  const handleScrollbarDrag = (e) => {
    if (!isDragging || !editorRef.current) return;
    const { clientY } = e;
    const { top, height } = editorRef.current.getBoundingClientRect();
    let newScrollPercentage = ((clientY - top) / height) * 100;
    newScrollPercentage = Math.max(0, Math.min(newScrollPercentage, 100));
    const scrollableHeight = editorRef.current.scrollHeight - editorRef.current.clientHeight;
    editorRef.current.scrollTop = (newScrollPercentage / 100) * scrollableHeight;

    if (scrollbarRef.current) {
      scrollbarRef.current.style.top = `${newScrollPercentage}%`;
    }
  };

  const handleScrollbarDragEnd = () => {
    setIsDragging(false);
  };

  useEffect(() => {
    if (isDragging) {
      document.addEventListener('mousemove', handleScrollbarDrag);
      document.addEventListener('mouseup', handleScrollbarDragEnd);
    } else {
      document.removeEventListener('mousemove', handleScrollbarDrag);
      document.removeEventListener('mouseup', handleScrollbarDragEnd);
    }

    return () => {
      document.removeEventListener('mousemove', handleScrollbarDrag);
      document.removeEventListener('mouseup', handleScrollbarDragEnd);
    };
  }, [isDragging]);

  return (
    <DndProvider backend={HTML5Backend}>
      <div className="flex h-[calc(100vh-64px)] bg-gray-100 p-4">
        <div
          className="flex flex-col bg-white rounded-lg shadow-lg border-2 border-blue-500 overflow-hidden"
          style={{ 
            width: `${editorWidth}%`, 
            height: 'calc(100% - 32px)', // Resta 32px para dar margen arriba y abajo
            marginRight: '8px'
          }}
        >
          <h1 className="text-2xl font-bold text-gray-800 p-4 border-b">Personaliza tu Esquema</h1>
          <div className="flex-grow overflow-hidden p-4 relative">
            <div
              ref={editorRef}
              className="editor-boxes space-y-2 h-full overflow-y-auto pr-4"
              style={{ maxHeight: 'calc(100% - 60px)' }} // Ajusta este valor según sea necesario
              onScroll={handleScroll}
            >
              {formatVisibleData(mindMapData)}
            </div>
            <div 
              ref={scrollbarRef}
              className="absolute right-0 top-0 bottom-0 w-4 cursor-pointer transition-colors"
              onMouseDown={handleScrollbarDragStart}
              style={{
                backgroundColor: '#333',
                borderRadius: '4px',
                height: '20%',
                position: 'absolute',
                transform: 'translateY(-50%)'
              }}
            ></div>
          </div>
        </div>
        <div
          className="resizer"
          style={{
            width: '8px',
            cursor: 'ew-resize',
            backgroundColor: '#e5e7eb',
            borderRadius: '4px',
          }}
          onMouseDown={startDrag}
        />
        <div
          className="flex flex-col bg-white rounded-lg shadow-lg border-2 border-blue-500 overflow-hidden"
          style={{ 
            width: `${mapWidth}%`, 
            height: 'calc(100% - 32px)', // Resta 32px para dar margen arriba y abajo
            marginLeft: '8px'
          }}
        >
          <div className="flex justify-end p-4 space-x-2 border-b">
            <Button
              onClick={downloadHTML}
              className="bg-green-600 text-white py-2 px-4 rounded-lg hover:bg-green-700 transition duration-300 ease-in-out shadow-md"
            >
              Descargar HTML ''Ábrelo con tu navegador''
            </Button>
            <Button
              onClick={downloadSVG}
              className="bg-purple-600 text-white py-2 px-4 rounded-lg hover:bg-purple-700 transition duration-300 ease-in-out shadow-md"
            >
              Descargar SVG
            </Button>
          </div>
          <div className="flex-grow overflow-hidden p-4">
            <svg ref={svgRef} style={{ width: "100%", height: "100%" }} onWheel={handleMapZoom}></svg>
          </div>
        </div>
      </div>
    </DndProvider>
  );
};

export default MindMapsOutput;
