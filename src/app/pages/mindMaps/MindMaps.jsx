import React, { useState } from "react";
import {
    getLocalStorage,
} from "../../components/local_storage";
import { RouteConstant } from "../../constants/RouteConstant";
import { useNavigate } from "react-router-dom";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import Button from "../../components/button/Button";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import FileReaderInputFIled from "../../components/fileReaderInputFIled/FileReaderInputFIled";

const mapTypes = [
    "Corto sin definiciones (no muy detallado)",
    "Corto con definiciones (detallado)",
    "Largo sin definiciones",
    "Largo con definiciones"
];

function MindMaps() {
    const [data, setData] = useState({
        selectedFiles: [],
        prompt: "",
        mapType: "",
    });

    const { handleHitAi, loading, error } = useHandleHitAi({ 
        ai: "chatgpt",
        selectedFiles: data.selectedFiles,
        toolName: "Esquemas o Mapas Mentales"
    });

    const prvResponse = getLocalStorage(RouteConstant.MIND_MAPS_OUTPUT);
    const navigate = useNavigate();
    const [formError, setFormError] = useState("");

    const handleChange = (e) => {
        setData({ ...data, [e.target.name]: e.target.value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setFormError("");

        if ((!data.prompt && data.selectedFiles.length === 0) || !data.mapType) {
            const errorMsg = "Por favor, proporciona un tema o sube al menos un archivo, y selecciona un tipo de mapa";
            setFormError(errorMsg);
            return;
        }

        let promptContent = `
      Eres un creador de mapas mentales muy potente. Siempre da el resultado en el idioma en el que se te ha preguntado, normalmente en español.
      Crea un mapa mental para el siguiente tema y/o contenido de archivos:

      ${data.prompt}

      Proporciona el texto del mapa mental en formato markmap correcto y válido como este:
      # Tema Principal
      ## Subtema 1
      ### Sub-subtema 1
      ### Sub-subtema 2
      ## Subtema 2

      Instrucciones para el mapa mental:
    `;

        if (data.mapType === "Corto sin definiciones (no muy detallado)") {
            promptContent += `
        - Mantén el mapa mental conciso y de alto nivel
        - No entres en demasiados detalles
        - No proporciones definiciones
      `;
        } else if (data.mapType === "Corto con definiciones (detallado)") {
            promptContent += `
        - Mantén el mapa mental conciso y de alto nivel
        - Incluye breves definiciones o explicaciones para términos y conceptos clave
      `;
        } else if (data.mapType === "Largo sin definiciones") {
            promptContent += `
        - Crea un mapa mental detallado y completo
        - Cubre el tema en profundidad
        - No proporciones definiciones
      `;
        } else if (data.mapType === "Largo con definiciones") {
            promptContent += `
        - Crea un mapa mental detallado y completo
        - Cubre el tema en profundidad
        - Incluye definiciones y explicaciones para términos y conceptos clave
      `;
        }

        if (data.selectedFiles.length > 0) {
            promptContent += `
        Analiza el contenido de los siguientes archivos y crea el mapa mental basado en esta información:
      `;
            data.selectedFiles.forEach((file, index) => {
                if (file.type.startsWith('image/')) {
                    promptContent += `
          Archivo ${index + 1}: Imagen "${file.name}". Por favor, analiza su contenido visual y úsalo como base para el mapa mental.
        `;
                } else if (file.type === 'application/pdf') {
                    promptContent += `
          Archivo ${index + 1}: PDF "${file.name}". Resume su contenido principal y úsalo para el mapa mental.
        `;
                } else {
                    promptContent += `
          Archivo ${index + 1}: "${file.name}". Analiza su contenido y úsalo para el mapa mental.
        `;
                }
            });
        }

        promptContent += `
      Recuerda seguir las instrucciones cuidadosamente y proporcionar el mapa mental en el formato correcto.
    `;

        try {
            const response = await handleHitAi(
                promptContent, 
                RouteConstant.MIND_MAPS_OUTPUT,
                "Generador de Mapas Mentales"
            );
            if (response) {
                navigate(RouteConstant.MIND_MAPS_OUTPUT, { state: response });
            } else {
                setFormError("No se pudo generar el mapa mental. Por favor, inténtalo de nuevo.");
            }
        } catch (error) {
            console.error("Error durante la llamada a la API:", error);
            setFormError(`Error: ${error.message || "Ocurrió un error desconocido"}`);
        }
    };

    return (
        <div className="flex flex-col gap-[24px] p-[24px]">
            <AiToolPageHeader
                title="Creador de Esquemas / Mapas Mentales"
                description="¿Necesitas esquemas más visuales para ayudarte a estudiar? Esta herramienta te creará esquemas o mapas mentales de un texto o de un tema que tengas en cuestión de segundos. ¡Prúebalo!"
                icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
            />
            <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
                {formError && (
                    <div className="col-span-2 text-red-500">{formError}</div>
                )}
                {error && (
                    <div className="col-span-2 text-red-500">Error from AI: {error}</div>
                )}
                <FileReaderInputFIled
                    onChange={handleChange}
                    data={data}
                    setData={setData}
                />
                <InputFieldDropdown
                    placeholder="Elige el tipo de mapa que quieras generar"
                    onChange={(value) => {
                        setData({ ...data, mapType: value });
                    }}
                    name="mapType"
                    value={data.mapType}
                    options={mapTypes}
                    require={true}
                    label={"Tipo de Mapa"}
                />
                <div className="col-span-2 flex align-center gap-2 ">
                    <Button
                        onClick={handleSubmit}
                        className={prvResponse ? "w-[80%]" : "w-full"}
                        disabled={loading}
                    >
                        {loading ? "Generando..." : "Generar Mapa Mental"}
                    </Button>
                    {prvResponse && (
                        <Button
                            className={"w-[20%]"}
                            onClick={() => {
                                navigate(RouteConstant.MIND_MAPS_OUTPUT);
                            }}
                        >
                            Anterior Generación
                        </Button>
                    )}
                </div>
            </div>
        </div>
    );
}

export default MindMaps;