import React, { useState } from 'react';
import TextEditor from '../../components/textEditor/TextEditor';
import { useParams } from 'react-router-dom';

function ShareText() {
    const { slug } = useParams();
    const decodedResult = decodeURIComponent(slug);

    console.log('Slug:', slug);
    console.log('Decoded Result:', decodedResult);

    return (
      <>
        <div>
          <div className="flex items-center pl-[24px]">
            <h1 className="font-[600] text-[#2B3D70] text-[32px]">
              Get your AI Generated Text in seconds
            </h1>
          </div>
        </div>
        <TextEditor result={decodedResult} showShare={false} showSave={false} />
      </>
    );
}

export default ShareText;
