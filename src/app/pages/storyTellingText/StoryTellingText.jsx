import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";

function StoryTellingText() {
  const { handleHitAi, loading } = useHandleHitAi({ ai: "chatgpt", toolName: "Cuenta Cuentos", });
  const [data, setData] = useState({
    age: "",
    storyGenre: "",
    contentToCover: "",
    storyObjective: "",
    exampleOfStory: "",
    storyLength: ""
  });
  const prvResponse = getLocalStorage(RouteConstant.STORY_TELLING_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const [generatedContent, setGeneratedContent] = useState("");

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      !data.age ||
      !data.storyGenre ||
      !data.contentToCover ||
      !data.storyObjective ||
      !data.storyLength
    ) {
      alert("Please fill in all fields");
      return;
    }
    const aiPrompt = `
Como experto en narrativa y educación, tu tarea es crear una historia atractiva y educativa basada en la siguiente información:

Edad de los estudiantes: ${data.age}
Género de la historia: ${data.storyGenre}
Contenido a cubrir: ${data.contentToCover}
Objetivo de la historia: ${data.storyObjective}
Ejemplo de una historia similar: ${data.exampleOfStory}
Extensión del cuento: ${data.storyLength}.

</body>
</html>  
<h1>Generador de Historia Educativa</h1>
  <p>Responde en español, asegurándote de que la historia sea adecuada para la edad especificada, educativa, entretenida y relevante para el contenido a cubrir. El objetivo es que los estudiantes aprendan de manera divertida y significativa a través de la historia.</p>

  <h2>Historia</h2>

  <p>[Introduce la historia capturando la atención del lector desde el inicio, estableciendo el escenario y presentando a los personajes principales.]</p>

  <p>[Desarrolla la trama en varios párrafos, incorporando el contenido a cubrir de manera natural. Utiliza diálogos, descripciones y eventos que mantengan a los estudiantes interesados.]</p>

  <p>[Presenta el clímax de la historia, donde los personajes enfrentan el mayor desafío relacionado con el contenido académico.]</p>

  <p>[Concluye la historia, resolviendo los conflictos y resaltando el objetivo de la historia. Asegúrate de que los estudiantes comprendan el contenido a través del desenlace.]</p>

  <h2>Reflexión Final</h2>

  <p>[Añade un párrafo de reflexión final donde los personajes reflexionan sobre lo aprendido. Esto debe conectar directamente con el objetivo de la historia y el contenido académico.]</p>

</body>
</html>

The content must always be generated in this format: HTML
`;

    console.log(aiPrompt);

    setIsLoading(true);
    try {
      const response = await handleHitAi(aiPrompt, RouteConstant.STORY_TELLING_OUTPUT);
      setGeneratedContent(response);
    } catch (error) {
      console.error("Error during API call:", error);
      alert("Ocurrió un error al generar la historia. Por favor, inténtalo de nuevo.");
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Cuenta Cuentos Personalizado: Historias Mágicas a Tu Medida"
        description="Obtén cuentos increíbles y personalizados en cuestión de segundos, adaptadas a tus necesidades específicas y preferencias narrativas y edad"
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <InputFieldDropdown
          placeholder={"Edad"}
          onChange={(value) => {
            setData({ ...data, age: value });
          }}
          value={data.age}
          name="age"
          options={Dropdown.age}
          require={true}
          label={"Edad"}
        />
        <InputFieldDropdown
          name={"storyGenre"}
          value={data.storyGenre}
          onChange={(value) => {
            setData({ ...data, storyGenre: value });
          }}
          placeholder={"Elige uno de la lista"}
          options={Dropdown.storyGenre}
          require={true}
          label={"Género de la Historia"}
        />
        <TextArea
          name={"contentToCover"}
          value={data.contentToCover}
          onChange={handleChange}
          placeholder={"El sistema solar"}
          require={true}
          label={"Contenido a cubrir"}
        />
        <TextArea
          name={"storyObjective"}
          value={data.storyObjective}
          onChange={handleChange}
          placeholder={"Enseñar a los estudiantes sobre los planetas del sistema solar y sus características"}
          require={true}
          label={"Objetivo de la historia"}
        />
        <InputField
          name={"exampleOfStory"}
          value={data.exampleOfStory}
          onChange={handleChange}
          placeholder={"Si tienes algún ejemplo concreto, o de algún cuento que te guste, escríbelo aquí"}
          label={"Ejemplo de historia que te guste. Opcional"}
        />
        <InputFieldDropdown
          name={"storyLength"}
          value={data.storyLength}
          onChange={(value) => {
            setData({ ...data, storyLength: value });
          }}
          placeholder={"Corto, Medio o Largo"}
          options={Dropdown.storyLength}
          require={true}
          label={"Extensión del cuento"}
        />
        <div className="col-span-2 flex align-center gap-2 ">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={loading || isLoading}
          >
            {isLoading ? "Generando..." : "Generar"}
          </Button>
          {prvResponse && (
            <Button
              className={"w-[20%]"}
              onClick={() => {
                navigate(RouteConstant.STORY_TELLING_OUTPUT);
              }}
              disabled={loading || isLoading}
            >
              Anterior
            </Button>
          )}
        </div>
      </div>
      {generatedContent && (
        <div className="bg-white p-[24px] rounded-md mt-[24px]">
          <div dangerouslySetInnerHTML={{ __html: generatedContent }} />
        </div>
      )}
    </div>
  );
}

export default StoryTellingText;
