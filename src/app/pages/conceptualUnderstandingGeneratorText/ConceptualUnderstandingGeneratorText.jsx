import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";

const FormField = ({ title, children, required = false }) => (
  <div className="mb-2">
    <label className="block font-bold text-sm mb-1">
      {title} {required && <span className="text-red-500">*</span>}
    </label>
    {children}
  </div>
);

const InputStyles = "w-full p-2 bg-gray-100 border border-gray-300 rounded-md text-sm text-gray-700 placeholder-gray-500";

const TextInput = ({ placeholder, value, onChange, name, type = "text" }) => (
  <input
    type={type}
    name={name}
    value={value}
    onChange={onChange}
    placeholder={placeholder}
    className={InputStyles}
  />
);

const TextArea = ({ placeholder, value, onChange, name, rows = 4 }) => (
  <textarea
    name={name}
    value={value}
    onChange={onChange}
    placeholder={placeholder}
    rows={rows}
    className={InputStyles}
  />
);

function ConceptualUnderstandingGeneratorText() {
  const { handleHitAi, loading } = useHandleHitAi({ ai: "chatgpt" });
  const [data, setData] = useState({
    prompt: "",
    course: "",
    subject: "",
    relevantComments: "",
    toolname: "Generador de Comprensión Conceptual"
  });
  const prvResponse = getLocalStorage(RouteConstant.CONCEPTUAL_UNDERSTANDING_GENERATOR_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!data.prompt.trim() || !data.course.trim() || !data.subject.trim()) {
      alert("Please fill in all fields");
      return;
    }

    const aiPrompt = `As an AI assistant specializing in explaining complex concepts, generate the following HTML-structured content:

<html>
<body>
  <h1>Generador de Comprensión Conceptual</h1>
  
  Detalles:
    Curso: ${data.course}
    Asignatura: ${data.subject}
    Concepto: ${data.prompt}
    Comentarios: ${data.relevantComments}

  <h2>Explicación del Concepto:</h2>
  
  <ol>
    <li>
      <h3>Introducción</h3>
      <p>Breve introducción y relevancia del concepto.</p>
    </li>
    
    <li>
      <h3>Explicación Principal</h3>
      <p>Explica el concepto de forma clara, incluyendo:</p>
      <ul>
        <li>Definición simple</li>
        <li>Aspectos clave</li>
        <li>Una analogía o ejemplo práctico</li>
      </ul>
    </li>
    
    <li>
      <h3>Aplicación</h3>
      <p>Cómo se aplica en la vida real o en la asignatura.</p>
    </li>
    
    <li>
      <h3>Comprobación</h3>
      <p>1-2 preguntas para verificar la comprensión.</p>
    </li>
  </ol>

  <p>Asegúrate de que la explicación sea clara, adaptada al nivel del curso y considere los comentarios proporcionados.</p>

  <p>Responde en español, creando una explicación conceptual concisa y comprensible.</p>
</body>
</html>
Generate always the content in this HTML format.`;

    setIsLoading(true);
    try {
      await handleHitAi(aiPrompt, RouteConstant.CONCEPTUAL_UNDERSTANDING_GENERATOR_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      alert("Ocurrió un error al generar el contenido. Por favor, inténtalo de nuevo.");
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px] max-h-[80vh] overflow-scroll">
      <AiToolPageHeader
        title="Generador de Comprensión Conceptual"
        description="Esta herramienta proporciona explicaciones claras y didácticas sobre cualquier tema en segundos. Simplemente introduce el tema, el curso, la asignatura y cualquier información adicional relevante, y recibirás una explicación comprensible y detallada"
        icon="ri-edit-2-line text-xl text-blue-600"
      />
      <form onSubmit={handleSubmit} className="flex flex-col gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <FormField title="Pega el texto o dime qué necesitas comprender" required>
          <TextInput
            name="prompt"
            value={data.prompt}
            onChange={handleChange}
            placeholder="Pega aquí el texto o dime qué necesitas comprender"
          />
        </FormField>
        <div className="grid grid-cols-2 gap-4">
          <FormField title="Curso" required>
            <InputFieldDropdown
              placeholder="Curso"
              onChange={(value) => setData({ ...data, course: value })}
              name="course"
              type="number"
              value={data.course}
              options={Dropdown.courses}
            />
          </FormField>
          <FormField title="Asignatura" required>
            <InputFieldDropdown
              name="subject"
              value={data.subject}
              onChange={(value) => setData({ ...data, subject: value })}
              placeholder="Asignatura"
              options={Dropdown.subject}
            />
          </FormField>
        </div>
        <FormField title="Comentarios adicionales">
          <TextArea
            name="relevantComments"
            value={data.relevantComments}
            onChange={handleChange}
            placeholder="Información Adicional que quieras añadir"
            rows={3}
          />
        </FormField>
        <div className="flex justify-center gap-4 mt-4">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={loading || isLoading}
          >
            {isLoading ? "Generando..." : "Generar"}
          </Button>
          {prvResponse && (
            <Button
              className="w-full py-2.5 px-3 bg-gray-200 text-gray-700 rounded-lg hover:bg-gray-300 transition duration-300"
              onClick={() => navigate(RouteConstant.CONCEPTUAL_UNDERSTANDING_GENERATOR_OUTPUT)}
              disabled={loading || isLoading}
            >
              Ver generación previa
            </Button>
          )}
        </div>
      </form>
    </div>
  );
}

export default ConceptualUnderstandingGeneratorText;
