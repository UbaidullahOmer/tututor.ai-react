import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";
import ConnectAndCaptivateOutput from "./ConnectAndCaptivateOutput";

function ConnectAndCaptivateGenerator() {
  const { handleHitAi, loading } = useHandleHitAi({ ai: "chatgpt" });
  const [data, setData] = useState({
    prompt: "",
    course: "",
    subject: "",
    classCharacteristics: "",
    toolname: "Conecta y Cautiva"
  });
  const prvResponse = getLocalStorage(RouteConstant.CONNECT_AND_CAPTIVATE_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!data.prompt) {
      alert("Por favor, rellene todos los campos");
      return;
    }
    const connectAndCaptivatePrompt = `
Eres un experto en hacer el contenido educativo atractivo y relevante para los estudiantes. Tu tarea es sugerir formas creativas de presentar el siguiente tema, haciéndolo interesante y aplicable a la vida real:

Contenido a cubrir: ${data.prompt}
Curso: ${data.course}
Asignatura: ${data.subject}
Características de la clase: ${data.classCharacteristics}

Genera 7 ideas concretas en español, utilizando la siguiente estructura HTML:

<h1>Conecta y Cautiva: Ideas para Hacer el Contenido Atractivo</h1>

<ol>
  <li>
    <h3>[Título de la Idea]</h3>
    <p>Descripción breve de cómo implementar esta idea y por qué hará el contenido más interesante y relevante para los estudiantes.</p>
  </li>
  <!-- Repite esta estructura para las 7 ideas -->
</ol>

<p>Consideraciones finales: Breve párrafo sobre cómo adaptar estas ideas al nivel del curso y las características específicas de la clase.</p>

Importante: 
- Enfócate en ideas que conecten el contenido con la vida cotidiana de los estudiantes.
- Sugiere formas de demostrar la aplicación práctica del contenido en situaciones reales.
- Incluye al menos una idea que incorpore tecnología o medios digitales.
- Asegúrate de que las ideas sean creativas, específicas y fáciles de implementar.
- Asegúrate también que el contenido es engaging para los alumnos y se sientan identificados y atrapados por él.
- Usa solo etiquetas HTML para el formato, no Markdown.
`;

    console.log(connectAndCaptivatePrompt);
    setIsLoading(true);
    try {
      await handleHitAi(connectAndCaptivatePrompt, RouteConstant.CONNECT_AND_CAPTIVATE_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      alert("Ocurrió un error al generar la explicación. Por favor, inténtalo de nuevo.");
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Conecta y Cautiva: Generador de Estrategias de Engagement"
        description="Genera estrategias para conectar y captivar a tus estudiantes. Escribe el tema a tratar y el generador te proporcionará ideas creativas y prácticas para hacer el contenido más atractivo y relevante."
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <TextArea
          placeholder="Ej: Cómo se forman las montañas"
          onChange={handleChange}
          className="col-span-2"
          value={data.prompt}
          name="prompt"
          label="Contenido a tratar"
          required={true}
        />
        <InputFieldDropdown
          placeholder="Curso"
          onChange={(value) => {
            setData({ ...data, course: value });
          }}
          name="course"
          type="number"
          value={data.course}
          options={Dropdown.courses}
          required={false}
          label="Curso"
        />
        <InputFieldDropdown
          name="subject"
          value={data.subject}
          onChange={(value) => {
            setData({ ...data, subject: value });
          }}
          required={false}
          placeholder="Asignatura"
          options={Dropdown.subject}
          label="Asignatura"
        />
        <TextArea
          name="classCharacteristics"
          value={data.classCharacteristics}
          onChange={handleChange}
          placeholder="Mis alumnos son apasionados de la tecnología y los videojuegos. Prefieren actividades prácticas, experimentos y desafíos amistosos. Tienen curiosidad por el mundo natural y disfrutan aprender sobre animales exóticos. El contenido visual, como videos e infografías, les atrae mucho. Además, son fans de los superhéroes y la ciencia ficción. Les gusta trabajar en proyectos grupales y presentar sus ideas"
          label="Características de la Clase"
          required={true}
          className="col-span-2"
        />
        <div className="col-span-2 flex align-center gap-2">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={loading || isLoading}
          >
            {isLoading ? "Generando..." : "Generar"}
          </Button>
          {prvResponse && (
            <Button
              className="w-[20%]"
              onClick={() => {
                navigate(RouteConstant.CONNECT_AND_CAPTIVATE_OUTPUT);
              }}
              disabled={loading || isLoading}
            >
              Anterior
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default ConnectAndCaptivateGenerator;