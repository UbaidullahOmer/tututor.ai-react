import React, { useState, useEffect } from "react";
import useFirebaseData from "../../hooks/useFirebaseData";

function EditPromptModal({ onClose, initialData, updateData }) {
  const [promptData, setPromptData] = useState({
    toolName: "",
  });

  useEffect(() => {
    if (initialData) {
      setPromptData({
        toolName: initialData.toolName || "",
      });
    }
  }, [initialData]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setPromptData((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async () => {
    try {
      await updateData("ai-prompt-history", initialData.id, { ...initialData, toolName: promptData.toolName });
      onClose();
    } catch (error) {
      console.error("Error updating data:", error);
    }
  };

  return (
    <div className="fixed inset-0 bg-gray-800 bg-opacity-50 flex items-center justify-center">
      <div className="bg-white p-6 rounded-lg shadow-lg min-w-[600px] max-w-[600px]">
        <h2 className="text-xl font-bold mb-4">Nombre de la generación</h2>
        <div className="mb-4">
          <label htmlFor="toolName" className="block text-sm font-medium text-gray-700">Título</label>
          <input
            type="text"
            id="toolName"
            name="toolName"
            value={promptData.toolName}
            onChange={handleChange}
            className="mt-1 block w-full border border-gray-300 rounded-md shadow-sm p-2"
          />
        </div>
        <div className="flex justify-end gap-4">
          <button
            type="button"
            onClick={onClose}
            className="bg-gray-300 text-gray-800 rounded-md px-4 py-2"
          >
            Cancelar
          </button>
          <button
            type="button"
            onClick={handleSubmit}
            className="bg-blue-500 text-white rounded-md px-4 py-2"
          >
            Guardar
          </button>
        </div>
      </div>
    </div>
  );
}

export default EditPromptModal;
