import React, { useEffect, useState } from "react";
import HistoryCard from "./HistoryCard";
import useFirebaseData from "../../hooks/useFirebaseData";
import useAuth from "../../hooks/useAuth";
import EditPromptModal from "./EditPromptModal";

function PromptHistory() {
  const { user } = useAuth();
  const { fetchData, data, removeData, updateData, removeAllData } = useFirebaseData();
  const [editPromptModal, setEditPromptModal] = useState(false);
  const [currentItem, setCurrentItem] = useState(null);
  const [search, setSearch] = useState("");

  useEffect(() => {
    fetchData("ai-prompt-history");
  }, [user]);

  const handleDelete = async (id) => {
    try {
      await removeData("ai-prompt-history", id);
      await fetchData("ai-prompt-history");
    } catch (error) {
      console.error("Error al borrar el elemento:", error);
      alert("Hubo un error al borrar el elemento. Por favor, inténtalo de nuevo.");
    }
  };

  const handleEdit = (item) => {
    setCurrentItem(item);
    setEditPromptModal(true);
  };

  const handleDeleteAllGenerations = async () => {
    try {
      await removeAllData("ai-prompt-history");
      await fetchData("ai-prompt-history");
    } catch (error) {
      console.error("Error al borrar todas las generaciones:", error);
      alert("Hubo un error al borrar todas las generaciones. Por favor, inténtalo de nuevo.");
    }
  };

  const filteredData = data?.filter((item) => {
    return search ? item?.prompt?.includes(search) || item?.toolName?.includes(search) : true;
  });

  const sortedData = filteredData?.sort((a, b) => new Date(a.date) - new Date(b.date) );

  console.log(sortedData);
  
  return (
    <div className="flex flex-col gap-[12px] p-[12px]">
      <div className="flex justify-between items-center">
        <h1 className="text-[24px] font-[600] text-[#2B3D70]">Historial de Generaciones</h1>
        <div className="flex gap-[12px]">
          <input
            type="text"
            placeholder="Search"
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            className="bg-[#F5F5F5] border border-[#F5F5F5] rounded-[10px] p-[12px] w-[200px]"
          />
          <button 
            onClick={() => setSearch("")} 
            className="bg-[#02E8AC] text-white rounded-[10px] p-[12px] w-[100px]"
          >
            Limpiar texto
          </button>
          <button 
            onClick={handleDeleteAllGenerations} 
            className="bg-[#FF6347] text-white rounded-[10px] p-[12px] w-[200px]"
          >
            Eliminar todas las generaciones
          </button>
        </div>
      </div>
      <div className="flex flex-col gap-[12px] overflow-y-auto max-h-[calc(100vh-200px)]">
        {sortedData?.map((item, index) => (
          <HistoryCard 
            handleDelete={handleDelete}
            key={index}
            item={item}
            heading={item?.toolName ? item.toolName : `Generación ${index + 1}`}
            path={item?.url}
            onEdit={() => handleEdit(item)}
            index={index}
          />
        ))}
      </div>
      {editPromptModal && (
        <EditPromptModal
          updateData={updateData}
          onClose={() => setEditPromptModal(false)}
          initialData={currentItem}
        />
      )}
    </div>
  );
}

export default PromptHistory;
