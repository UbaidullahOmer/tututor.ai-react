import React from "react";
import MeText from "../../components/meText/MeText";

function HistoryCard({
  icon,
  heading = "Heading",
  detail,
  path = "/",
  detailMaxLength = 60,
  handleDelete,
  onEdit,
  item,
  index,
}) {
  const handleNavigate = (path) => {
    window.open(path, "_blank");
  };

  // Función para eliminar etiquetas HTML
  const stripHtml = (html) => {
    let tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
  };

  return (
    <div 
      onClick={() => handleNavigate(path)}
      className="bg-white border border-gray-200 flex gap-[16px] rounded-lg p-[16px] shadow-sm hover:shadow-lg transition-shadow duration-300 ease-in-out cursor-pointer"
    >
      {icon && (
        <div className="rounded-full bg-[#02E8AC] p-[12px] flex items-center justify-center h-[40px] w-[40px]">
          {icon}
        </div>
      )}
      <div className="flex justify-between items-center w-full">
        <div className="flex flex-col gap-[8px]">
          <MeText className="text-[20px] font-semibold text-[#2B3D70]">
            {heading}
          </MeText>
          {detail && detail.trim() !== "" && (
            <MeText
              className="text-[#737791] max-w-[70vw] text-sm line-clamp-2"
              maxLength={detailMaxLength}
            >
              {stripHtml(detail)}
            </MeText>
          )}
        </div>
        <div className="flex items-center gap-4">
          <i
            onClick={(e) => {
              e.stopPropagation();
              e.preventDefault();
              handleDelete(item?.id);
            }}
            className="ri-delete-bin-2-line text-red-500 hover:text-red-700 transition-colors duration-200 ease-in-out cursor-pointer text-[24px]"
          ></i>
          <i  
            onClick={(e) => {
              e.stopPropagation();
              e.preventDefault();
              onEdit(item?.id);
            }} 
            className="ri-pencil-line text-gray-500 hover:text-gray-700 transition-colors duration-200 ease-in-out cursor-pointer text-[24px]"
          ></i>
          <i className="ri-history-line text-gray-500 hover:text-gray-700 transition-colors duration-200 ease-in-out cursor-pointer text-[24px]"></i>
        </div>
      </div>
    </div>
  );
}

export default HistoryCard;
