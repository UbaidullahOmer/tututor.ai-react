export const citationStyles = [
  "Ninguno",
  "APA",
  "MLA",
  "Chicago",
  "Harvard",
  "IEEE",
  "Vancouver",
  "Otro (escribe)"
];

export const writingToneOptions = [
  "Formal",
  "Casual",
  "Académico",
  "Persuasivo",
  "Detallado",
  "Gracioso",
  "Descriptivo",
  "Analítico",
  "Innovador",
  "Motivacional",
  "Otro (escribe en el cuadro)",


];

export const writingTypeOptions = [
  "Artículo",
  "Ensayo",
  "Informe",
  "Investigación",
  "Tesis",
  "Resumen",
  "Carta formal",
  "Proyecto de Investigación",
  "Relato Corto",
  "Tesis",
  "Tareas",
  "Otro (escribe en el cuadro)",



];
