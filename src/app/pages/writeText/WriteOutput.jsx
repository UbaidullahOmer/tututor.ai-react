import React, { useEffect, useState } from "react";
import TextEditor from "../../components/textEditor/TextEditor";
import { Link, useLocation } from "react-router-dom";
import { getLocalStorage } from "../../components/local_storage";
import { RouteConstant } from "../../constants/RouteConstant";

function WriteOutput() {
  const [result, setResult] = useState(null);
  const [selectedEmoji, setSelectedEmoji] = useState(null);
  const [feedback, setFeedback] = useState("");
  const [showFeedbackText, setShowFeedbackText] = useState(false);
  const [successMessage, setSuccessMessage] = useState(""); // Estado para el mensaje de éxito
  const location = useLocation();

  useEffect(() => {
    setResult(location.state || getLocalStorage(RouteConstant.WRITE_OUTPUT));
  }, [location.state]);

  const handleEmojiClick = (emoji) => {
    setSelectedEmoji(emoji);
    if (["🙂", "😄"].includes(emoji)) {
      setSuccessMessage("¡Muchas gracias por tu voto!");
      setShowFeedbackText(false); // No mostrar el feedback si es una cara feliz
      setTimeout(() => setSuccessMessage(""), 3000); // Ocultar el mensaje después de 3 segundos
    } else {
      setShowFeedbackText(true);
    }
  };

  const handleSendFeedback = () => {
    console.log("Feedback enviado:", { emoji: selectedEmoji, text: feedback });
    setSuccessMessage("Muchas gracias por tu comentario, lo usaremos para mejorar la herramienta");
    setSelectedEmoji(null);
    setFeedback("");
    setShowFeedbackText(false);
    setTimeout(() => setSuccessMessage(""), 3000); // Ocultar el mensaje después de 3 segundos
  };

  const handleCloseFeedback = () => {
    setSelectedEmoji(null);
    setShowFeedbackText(false);
  };

  return (
    <>
      <div className="relative flex items-center justify-between bg-white p-4 shadow-md">
        <div className="flex items-center gap-3 flex-grow">
          <Link to={RouteConstant.WRITE_TEXT}>
            <i className="ri-arrow-left-s-line text-[36px] text-[#2B3D70] hover:text-[#1E2A4D] transition-colors"></i>
          </Link>
          <h1 className="font-[600] text-[#2B3D70] text-[32px]">
            Volver a la herramienta Escritor Inteligente
          </h1>
        </div>
        <div className="relative flex flex-col items-end">
          {/* Mensaje de éxito */}
          {successMessage && (
            <div className="absolute top-[-40px] right-0 bg-green-100 text-green-700 px-4 py-2 rounded-md shadow-md animate-fade-in">
              {successMessage}
            </div>
          )}
          <span className="text-lg font-semibold text-[#2B3D70] mb-2">
            ¿Qué te ha parecido el contenido de la herramienta?
          </span>
          <div className="flex justify-between space-x-2">
            {["😞", "🙁", "😐", "🙂", "😄"].map((emoji) => (
              <button
                key={emoji}
                className={`emoji-btn text-3xl p-2 rounded-full transition-all duration-300 ${
                  selectedEmoji === emoji
                    ? 'bg-blue-100 shadow-md transform scale-110'
                    : 'hover:bg-gray-100'
                }`}
                onClick={() => handleEmojiClick(emoji)}
              >
                {emoji}
              </button>
            ))}
          </div>
          {showFeedbackText && (
            <div
              className={`absolute top-full mt-4 right-0 w-96 p-6 bg-white shadow-2xl rounded-lg z-10 transform transition-all duration-300 ease-out ${
                showFeedbackText ? 'scale-100 opacity-100' : 'scale-75 opacity-0'
              }`}
            >
              <div className="flex justify-between items-center mb-3">
                <h3 className="text-lg font-semibold text-[#2B3D70]">Valoramos mucho tu comentario</h3>
                <button
                  className="text-gray-500 hover:text-gray-700 transition-colors"
                  onClick={handleCloseFeedback}
                >
                  &times;
                </button>
              </div>
              <textarea
                rows="4"
                className="w-full p-3 border border-gray-300 rounded-md mb-4 focus:border-blue-500 focus:ring focus:ring-blue-200 transition duration-300"
                placeholder="Comparte con nosotros lo que no te ha gustado o lo que te gustaría que la herramienta mejorara"
                value={feedback}
                onChange={(e) => setFeedback(e.target.value)}
              ></textarea>
              <button
                className="w-full bg-[#2B3D70] text-white py-3 rounded-md hover:bg-[#1E2A4D] transition duration-300 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-opacity-50"
                onClick={handleSendFeedback}
              >
                Enviar
              </button>
            </div>
          )}
        </div>
      </div>

      <TextEditor result={result} />
    </>
  );
}

export default WriteOutput;
