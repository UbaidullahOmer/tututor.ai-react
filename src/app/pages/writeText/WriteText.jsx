import React, { useState } from "react";
import { RouteConstant } from "../../constants/RouteConstant";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import {
  citationStyles,
  writingToneOptions,
  writingTypeOptions,
} from "./constants";
import FileReaderComponent from "../../components/fileReaderComponent/FileReaderComponent";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import FileReaderInputFIled from "../../components/fileReaderInputFIled/FileReaderInputFIled";

const FormField = ({ title, children, required = false }) => (
  <div className="mb-4">
    <label className="block font-bold text-sm mb-2">
      {title} {required && <span className="text-red-500">*</span>}
    </label>
    {children}
  </div>
);

const InputStyles =
  "w-full p-2 bg-gray-100 border border-gray-300 rounded-md text-gray-700 placeholder-gray-500";

const TextInput = ({ placeholder, value, onChange, name, type = "text" }) => (
  <input
    type={type}
    name={name}
    value={value}
    onChange={onChange}
    placeholder={placeholder}
    className={InputStyles}
  />
);

const TextArea = ({ placeholder, value, onChange, name, rows = 4 }) => (
  <textarea
    name={name}
    value={value}
    onChange={onChange}
    placeholder={placeholder}
    rows={rows}
    className={InputStyles}
  />
);

function WriteText() {
  const [data, setData] = useState({
    selectedFiles: [],
    prompt: "",
    type: "",
    tone: "",
    citation: "",
    wordCount: "",
  });
  const { handleHitAi } = useHandleHitAi({
    ai: "chatgpt",
    selectedFiles: data.selectedFiles,
    toolName: "Escritor Inteligente con IA",
  });
  const prvResponse = getLocalStorage(RouteConstant.WRITE_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!data.wordCount) {
      alert("Por favor, proporciona un número de palabras");
      return;
    }
    setIsLoading(true);

    let aiPrompt = `Eres un poderoso asistente de IA y un escritor profesional. Crea un ${
      data.type
    } con las siguientes especificaciones:

Tipo de escrito: ${data.type}
Tono: ${data.tone}
Estilo de citación: ${data.citation}
Número de palabras: ${
      data.wordCount
    } palabras (es CRUCIAL que te acerques lo más posible a este número)

Haz este tema detallado: ${data.prompt} y con este número de token: ${
      data.wordCount
    } multiplicado por 4.

Instrucciones:
1. Escribe el ${data.type} abordando el tema o consigna dada.
2. Mantén el tono ${data.tone} especificado a lo largo de todo el escrito.
3. Si se utilizan fuentes, cítalas usando el estilo de citación ${
      data.citation
    }.
4. Estructura el escrito apropiadamente para el tipo especificado (ej. ensayo, artículo, informe, etc.).
5. Es OBLIGATORIO que el texto tenga EXACTAMENTE ${
      data.wordCount
    } + 2100 palabras más. Ni una más, ni una menos.
6. Asegúrate de que el contenido sea claro, atractivo y esté bien organizado.

Genera el resultado en el siguiente formato HTML: Nunca en formato markdown

<div className="${data.type.toLowerCase().replace(" ", "-")}">
  <h1>[Título del ${data.type}]</h1>
  <p>[Primer párrafo]</p>
  <p>[Segundo párrafo]</p>
  
  [Incluye los encabezados, listas u otros elementos estructurales apropiados para el ${
    data.type
  }]
</div>

Proporciona un ${
      data.type
    } bien elaborado que cumpla con todos los criterios especificados y aborde eficazmente el tema o consigna dada.
Genera siempre el contenido en español y en formato HTML, nunca en markdown.

IMPORTANTE: Antes de finalizar tu respuesta, cuenta el número de palabras y ajusta el contenido para que tenga EXACTAMENTE ${
      data.wordCount
    } palabras. Este requisito es absolutamente crucial y debe cumplirse sin excepciones.`;
    if (data.selectedFiles.length > 0) {
      const imageFiles = data.selectedFiles.filter((file) =>
        file.type.startsWith("image/")
      );
      if (imageFiles.length > 0) {
        aiPrompt +=
          "\nPlease analyze the uploaded images and write about the images uploaded. Bear in mind all information the user has  writen to write the content";
      }
    }
    try {
      await handleHitAi(aiPrompt, RouteConstant.WRITE_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      setIsLoading(false);
    }
  };

  return (
    <div className="flex justify-center items-center min-h-screen bg-gray-100 p-4 font-sans">
      <div className="w-full max-w-7xl bg-white p-8 rounded-xl shadow-md">
        <AiToolPageHeader
          title="Escritor Inteligente"
          description="Escribe cualquier texto que necesites, en cualquier estilo e incluso con citas de autores. Excelente para ayudarte en tus trabajos. Recuerda usarlo de manera ética y responsable"
          icon="ri-edit-2-line text-3xl text-blue-600"
        />
        <form onSubmit={handleSubmit} className="mt-6 space-y-4">
          <FileReaderInputFIled
            data={data}
            setData={setData}
            onChange={handleChange}
          />
          <FormField title="Tono de Escritura">
            <InputFieldDropdown
              name="tone"
              value={data.tone}
              onChange={(value) => setData({ ...data, tone: value })}
              options={writingToneOptions}
              placeholder="Selecciona el tono de escritura"
            />
          </FormField>
          <FormField title="Estilo de Escritura">
            <InputFieldDropdown
              name="type"
              value={data.type}
              onChange={(value) => setData({ ...data, type: value })}
              options={writingTypeOptions}
              placeholder="Selecciona el estilo de escritura"
            />
          </FormField>
          <FormField title="Estilos de Citación Bibliográfica">
            <InputFieldDropdown
              name="citation"
              value={data.citation}
              onChange={(value) => setData({ ...data, citation: value })}
              options={citationStyles}
              placeholder="Selecciona el estilo de citación"
            />
          </FormField>
          <FormField title="Número aproximado de palabras (máximo 1.200)" required>
            <TextInput
              type="number"
              name="wordCount"
              value={data.wordCount}
              onChange={handleChange}
              placeholder="Ej: 500 palabras. Con archivos adjuntados no siempre es preciso"
            />
          </FormField>
          <div className="flex justify-center gap-4 mt-8">
            <button
              type="submit"
              className={`w-full py-2.5 px-5 bg-blue-600 text-white rounded-lg hover:bg-blue-700 transition duration-300 ${
                isLoading ? "opacity-50 cursor-not-allowed" : ""
              }`}
              disabled={isLoading}
            >
              {isLoading ? "Generando..." : "Generar texto"}
            </button>
            {prvResponse && (
              <button
                type="button"
                className="w-full py-2.5 px-5 bg-gray-200 text-gray-700 rounded-lg hover:bg-gray-300 transition duration-300"
                onClick={() => navigate(RouteConstant.WRITE_OUTPUT)}
              >
                Ver generación previa
              </button>
            )}
          </div>
        </form>
      </div>
    </div>
  );
}

export default WriteText;
