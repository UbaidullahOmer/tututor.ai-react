// query.js
const url = "https://flowise-production-ea49.up.railway.app/api/v1/prediction/274ae1cd-44a8-487e-85fd-e161fe1e977e";
const headers = {
  "Content-Type": "application/json",
  "Authorization": "Bearer aYBHzxC_IYqqX_GRbbl1NY-rAUejhd9azY8MoHmM7k8"
};

async function query(data) {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers,
      body: JSON.stringify(data)
    });

    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }

    const result = await response.json();
    return result;
  } catch (error) {
    console.error('Error in query function:', error.message);
    return null;
  }
}

export default query;