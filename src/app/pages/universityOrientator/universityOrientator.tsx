import React, { useState, useEffect, useRef } from "react";
import ReactMarkdown from 'react-markdown';
import rehypeRaw from 'rehype-raw';
import query from "./query";

interface Message {
  type: 'user' | 'bot' | 'error';
  text: string;
}

interface Conversation {
  id: string;
  name: string;
  messages: Message[];
}

function UniversityOrientator() {
  const [userInput, setUserInput] = useState<string>('');
  const [chatHistory, setChatHistory] = useState<Message[]>([]);
  const [conversations, setConversations] = useState<Conversation[]>([]);
  const [activeConversation, setActiveConversation] = useState<string | null>(null);
  const [showOptionsFor, setShowOptionsFor] = useState<string | null>(null);
  const chatContainerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const savedConversations = JSON.parse(localStorage.getItem('universityOrientatorConversations') || '[]') as Conversation[];
    setConversations(savedConversations);

    if (savedConversations.length > 0) {
      setActiveConversation(savedConversations[0].id);
      setChatHistory(savedConversations[0].messages || []);
    } else {
      handleNewChat();
    }
  }, []);

  useEffect(() => {
    if (chatContainerRef.current) {
      chatContainerRef.current.scrollTop = chatContainerRef.current.scrollHeight;
    }
  }, [chatHistory]);

  useEffect(() => {
    localStorage.setItem('universityOrientatorConversations', JSON.stringify(conversations));
  }, [conversations]);

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setUserInput(e.target.value);
  };

  const formatResponse = (text: string): string => {
    let formattedText = text.replace(/\*\*\*(.*?)\*\*\*/g, '<em><strong>$1</strong></em>')
                           .replace(/\*\*(.*?)\*\*/g, '<strong>$1</strong>')
                           .replace(/\*(.*?)\*/g, '<em>$1</em>');
    
    formattedText = formattedText.replace(/^\s*\*\s(.+)$/gm, '<li>$1</li>');
    formattedText = formattedText.replace(/<li>(.+\n?)+/g, '<ul>$&</ul>');
    
    return formattedText;
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!userInput.trim()) return;

    const newMessage: Message = { type: 'user', text: userInput };
    const updatedHistory = [...chatHistory, newMessage];
    setChatHistory(updatedHistory);

    const data = {
      question: userInput,
      overrideConfig: {
        sessionId: activeConversation,
        returnSourceDocuments: true,
        systemMessagePrompt: ""
      }
    };

    const response = await query(data);

    if (response && typeof response.text === 'string') {
      const formattedText = formatResponse(response.text);
      const botMessage: Message = { type: 'bot', text: formattedText };
      const finalHistory = [...updatedHistory, botMessage];
      setChatHistory(finalHistory);

      setConversations(prevConversations => 
        prevConversations.map(conv => 
          conv.id === activeConversation 
            ? { ...conv, messages: finalHistory } 
            : conv
        )
      );
    } else {
      const errorMessage: Message = { type: 'error', text: 'Lo siento, hubo un error. Por favor, inténtalo de nuevo.' };
      setChatHistory([...updatedHistory, errorMessage]);
    }

    setUserInput('');
  };

  const handleNewChat = () => {
    const initialMessage = `
<h1 className="text-3xl font-semibold mb-2">Orientador Universitario</h1>
<p className="text-gray-600">Bienvenido a tu sesión de orientación universitaria. Aquí puedes:</p>
<ul className="list-disc list-inside mb-4">
  <li>Explorar diferentes carreras universitarias</li>
  <li>Recibir consejos sobre opciones académicas</li>
  <li>Conocer las oportunidades laborales de cada carrera</li>
  <li>Prepararte para el proceso de admisión</li>
  <li>Descubrir becas y financiamientos disponibles</li>
</ul>
<p className="text-gray-600">¿En qué tema te gustaría trabajar hoy?</p>
    `;
    const newConversation: Conversation = { 
      id: Date.now().toString(), 
      name: `Sesión ${conversations.length + 1}`,
      messages: [{ type: 'bot', text: initialMessage }]
    };
    setConversations([newConversation, ...conversations]);
    setActiveConversation(newConversation.id);
    setChatHistory(newConversation.messages);
  };

  const handleEditConversation = (id: string, newName: string) => {
    setConversations(conversations.map(conv => 
      conv.id === id ? { ...conv, name: newName } : conv
    ));
  };

  const handleDeleteConversation = (id: string) => {
    setConversations(conversations.filter(conv => conv.id !== id));
    if (activeConversation === id) {
      if (conversations.length > 1) {
        const newActiveConversation = conversations.find(conv => conv.id !== id);
        if (newActiveConversation) {
          setActiveConversation(newActiveConversation.id);
          setChatHistory(newActiveConversation.messages || []);
        }
      } else {
        handleNewChat();
      }
    }
  };

  const handleSelectConversation = (id: string) => {
    setActiveConversation(id);
    const selectedConversation = conversations.find(conv => conv.id === id);
    if (selectedConversation) {
      setChatHistory(selectedConversation.messages || []);
    }
  };

  return (
    <div className="flex flex-col h-[90dvh] bg-white text-gray-800">
      <div className="flex flex-1 overflow-hidden">
        {/* Conversations Sidebar */}
        <div className="w-64 bg-gray-50 border-r border-gray-200 overflow-y-auto">
          <div className="p-4">
            <h2 className="text-lg font-semibold mb-4">Sesiones</h2>
            <button className="w-full bg-gray-200 text-gray-800 py-2 px-4 rounded-lg mb-4 text-left" onClick={handleNewChat}>
              + Nueva sesión
            </button>
            <div className="space-y-2">
              {conversations.map((conv) => (
                <div key={conv.id} className="relative">
                  <button 
                    className={`w-full bg-white border border-gray-300 text-gray-800 py-2 px-4 rounded-lg text-left ${activeConversation === conv.id ? 'bg-blue-100' : ''}`}
                    onClick={() => handleSelectConversation(conv.id)}
                  >
                    {conv.name}
                  </button>
                  <button 
                    className="absolute right-2 top-0 bottom-0 flex items-center justify-center text-gray-500"
                    onClick={() => setShowOptionsFor(showOptionsFor === conv.id ? null : conv.id)}
                  >
                    <span className="text-xl font-bold">...</span>
                  </button>
                  {showOptionsFor === conv.id && (
                    <div className="absolute right-0 mt-2 w-48 bg-white rounded-md shadow-lg z-10">
                      <button 
                        className="block w-full text-left px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                        onClick={() => {
                          const newName = prompt("Introduce un nuevo nombre para la sesión", conv.name);
                          if (newName) handleEditConversation(conv.id, newName);
                          setShowOptionsFor(null);
                        }}
                      >
                        Editar nombre
                      </button>
                      <button 
                        className="block w-full text-left px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                        onClick={() => {
                          if (window.confirm("¿Estás seguro de que quieres eliminar esta sesión?")) {
                            handleDeleteConversation(conv.id);
                          }
                          setShowOptionsFor(null);
                        }}
                      >
                        Eliminar sesión
                      </button>
                    </div>
                  )}
                </div>
              ))}
            </div>
          </div>
        </div>

        {/* Main Content */}
        <div className="flex-1 flex flex-col overflow-hidden">
          <main className="flex-1 overflow-y-auto p-8" ref={chatContainerRef}>
            <div className="max-w-3xl mx-auto">
              {chatHistory.map((message, index) => (
                <div key={index} className={`flex items-start mb-4 ${message.type === 'user' ? 'justify-end' : ''}`}>
                  {message.type !== 'user' && (
                    <img src="/icons/virtualteacher.png" alt="Teacher Avatar" className="w-8 h-8 rounded-full mr-2" />
                  )}
                  <div className={`rounded-lg p-3 max-w-2xl ${
                    message.type === 'user' ? 'bg-blue-500 text-white' : 
                    message.type === 'error' ? 'bg-red-500 text-white' : 
                    'bg-gray-100'
                  }`}>
                    <ReactMarkdown 
                      className="markdown-content"
                      rehypePlugins={[rehypeRaw]}
                      components={{
                        p: ({node, ...props}) => <p className="mb-2" {...props} />,
                        ul: ({node, ...props}) => <ul className="list-disc list-inside mb-2" {...props} />,
                        ol: ({node, ...props}) => <ol className="list-decimal list-inside mb-2" {...props} />,
                        li: ({node, ...props}) => <li className="mb-1" {...props} />,
                        h1: ({node, ...props}) => <h1 className="text-2xl font-bold mb-2" {...props} />,
                        h2: ({node, ...props}) => <h2 className="text-xl font-semibold mb-2" {...props} />,
                        h3: ({node, ...props}) => <h3 className="text-lg font-medium mb-2" {...props} />,
                        strong: ({node, ...props}) => <strong className="font-bold" {...props} />,
                        em: ({node, ...props}) => <em className="italic" {...props} />,
                      }}
                    >
                      {message.text}
                    </ReactMarkdown>
                  </div>
                  {message.type === 'user' && (
                    <img src="/icons/mathteacher.png" alt="Student Avatar" className="w-8 h-8 rounded-full ml-2" />
                  )}
                </div>
              ))}
            </div>
          </main>
          <footer className="border-t border-gray-200 p-4">
            <div className="max-w-full mx-auto">
              <form onSubmit={handleSubmit} className="bg-gray-100 rounded-lg p-4 flex items-center">
                <input
                  type="text"
                  value={userInput}
                  onChange={handleInputChange}
                  placeholder="Haz una pregunta sobre orientación universitaria..."
                  className="flex-grow bg-transparent border-none focus:outline-none text-gray-800 placeholder-gray-500 px-2"
                />
                <button 
                  type="button" 
                  className="mr-2 p-2 rounded-full bg-gray-300"
                  onClick={() => {}} // Esta función está vacía ya que no hay funcionalidad de voz
                >
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fillRule="evenodd" d="M7 4a3 3 0 016 0v4a3 3 0 11-6 0V4zm4 10.93A7.001 7.001 0 0017 8a1 1 0 10-2 0A5 5 0 015 8a1 1 0 00-2 0 7.001 7.001 0 006 6.93V17H6a1 1 0 100 2h8a1 1 0 100-2h-3v-2.07z" clipRule="evenodd" />
                  </svg>
                </button>
                <button type="submit" className="bg-blue-500 text-white rounded-full p-2 hover:bg-blue-600 transition duration-300">
                  <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M14 5l7 7m0 0l-7 7m7-7H3"></path>
                  </svg>
                </button>
              </form>
            </div>
          </footer>
        </div>
      </div>
    </div>
  );
}

export default UniversityOrientator;
