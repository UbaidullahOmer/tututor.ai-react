// query.js
const url = "https://flowise-production-ea49.up.railway.app/api/v1/prediction/264150a4-849a-4b1e-a669-c6c3c9fdd160";
const headers = {
  "Content-Type": "application/json",
  "Authorization": "Bearer jeoZDxtmcQXSQM6A5yDb1EHpDW2M-PLL5w6RPa7X6G4"
};

async function query(data) {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers,
      body: JSON.stringify(data)
    });

    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }

    const result = await response.json();
    return result;
  } catch (error) {
    console.error('Error in query function:', error.message);
    return null;
  }
}

export default query;