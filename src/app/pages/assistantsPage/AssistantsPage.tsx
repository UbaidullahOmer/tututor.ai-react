import React from 'react';
import { useNavigate } from 'react-router-dom';

const AssistantsPage: React.FC = () => {
  const navigate = useNavigate();

  return (
    <div className="min-h-screen flex flex-col bg-gray-50">
      <main className="flex-grow container mx-auto px-4 py-8">
        <h2 className="text-2xl font-semibold text-gray-800 mb-6">
          Aprovecha los asistentes para profundizar en proyectos interesantes que puedes aplicar en el aula
        </h2>
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-6">
          
          <div className="bg-white rounded-lg shadow-md p-6 flex flex-col hover:shadow-lg transition duration-300">
            <div className="flex items-center mb-4">
              <img src="icons/coteacher.png" alt="Coteacher" className="h-10 w-10 text-green-500 mr-3" />
              <h3 className="text-xl font-semibold">Asistente Educativo todo en uno</h3>
            </div>
            <p className="text-gray-600 mb-4">Estoy aquí para ayudarte en todas tus tareas de enseñanza. ¿En qué necesitas apoyo?</p>
            <button 
              onClick={() => navigate('/asistente-todo-en-uno')}
              className="mt-auto bg-green-100 text-green-600 px-4 py-2 rounded-md hover:bg-green-200 transition duration-150 flex items-center justify-center"
            >
              <img src="icons/chat.png" alt="Chat" className="h-5 w-5 mr-2" />
              Chat
            </button>
          </div>

          <div className="bg-white rounded-lg shadow-md p-6 flex flex-col hover:shadow-lg transition duration-300">
            <div className="flex items-center mb-4">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-10 w-10 text-blue-500 mr-3"
                viewBox="0 0 64 64"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <circle cx="32" cy="32" r="12" stroke="#3B82F6" strokeWidth="2"/>
                <path d="M28 24 L36 24 L32 10 Z" fill="#3B82F6"/>
                <line x1="32" y1="24" x2="32" y2="44" stroke="#3B82F6" strokeWidth="2"/>
                <line x1="32" y1="24" x2="24" y2="30" stroke="#3B82F6" strokeWidth="2"/>
                <line x1="32" y1="24" x2="40" y2="30" stroke="#3B82F6" strokeWidth="2"/>
                <line x1="32" y1="44" x2="24" y2="52" stroke="#3B82F6" strokeWidth="2"/>
                <line x1="32" y1="44" x2="40" y2="52" stroke="#3B82F6" strokeWidth="2"/>
              </svg>
              <h3 className="text-xl font-semibold">Profesor Mágico</h3>
            </div>
            <p className="text-gray-600 mb-4">Simplifica conceptos difíciles en explicaciones fáciles y atractivas. ¡Ideal para hacer tus lecciones más comprensibles y dinámicas!</p>
            <button 
              onClick={() => navigate('/profe-magico')}
              className="mt-auto bg-blue-100 text-blue-600 px-4 py-2 rounded-md hover:bg-blue-200 transition duration-150 flex items-center justify-center"
            >
              <img src="icons/chat.png" alt="Chat" className="h-5 w-5 mr-2" />
              Chat
            </button>
          </div>

          <div className="bg-white rounded-lg shadow-md p-6 flex flex-col hover:shadow-lg transition duration-300">
            <div className="flex items-center mb-4">
              <img src="icons/digital_literacy_coach.png" alt="Digital Literacy Coach" className="h-10 w-10 text-green-500 mr-3" />
              <h3 className="text-xl font-semibold">Mentor en Competencias Digitales</h3>
            </div>
            <p className="text-gray-600 mb-4">¡La tecnología puede facilitarte la vida! ¿Qué herramientas tecnológicas necesitas aprender? ¿Tienes dudas sobre cómo implementar tecnología en el aula? ¡Estoy para ayudarte!</p>
            <button 
              onClick={() => navigate('/mentor-competencias-digitales')}
              className="mt-auto bg-green-100 text-green-600 px-4 py-2 rounded-md hover:bg-green-200 transition duration-150 flex items-center justify-center"
            >
              <img src="icons/chat.png" alt="Chat" className="h-5 w-5 mr-2" />
              Chat
            </button>
          </div>

          <div className="bg-white rounded-lg shadow-md p-6 flex flex-col hover:shadow-lg transition duration-300">
            <div className="flex items-center mb-4">
              <img src="icons/field_trip_coordinator.png" alt="Field Trip Coordinator" className="h-10 w-10 text-purple-500 mr-3" />
              <h3 className="text-xl font-semibold">Planificador de Salidas Educativas</h3>
            </div>
            <p className="text-gray-600 mb-4">Te ayudo a organizar, planificar y llevar a cabo salidas educativas exitosas. ¿Cuál es el destino?</p>
            <button 
              onClick={() => navigate('/planificador-salidas-educativas')}
              className="mt-auto bg-purple-100 text-purple-600 px-4 py-2 rounded-md hover:bg-purple-200 transition duration-150 flex items-center justify-center"
            >
              <img src="icons/chat.png" alt="Chat" className="h-5 w-5 mr-2" />
              Chat
            </button>
          </div>

          <div className="bg-white rounded-lg shadow-md p-6 flex flex-col hover:shadow-lg transition duration-300">
            <div className="flex items-center mb-4">
              <img src="icons/project_based_activity_expert.png" alt="Project-Based Activity Expert" className="h-10 w-10 text-yellow-500 mr-3" />
              <h3 className="text-xl font-semibold">Experto en Actividades de Proyectos</h3>
            </div>
            <p className="text-gray-600 mb-4">Te asisto en la creación y planificación de actividades basadas en proyectos para tus estudiantes.</p>
            <button 
              onClick={() => navigate('/experto-actividades-proyectos')}
              className="mt-auto bg-yellow-100 text-yellow-600 px-4 py-2 rounded-md hover:bg-yellow-200 transition duration-150 flex items-center justify-center"
            >
              <img src="icons/chat.png" alt="Chat" className="h-5 w-5 mr-2" />
              Chat
            </button>
          </div>

          <div className="bg-white rounded-lg shadow-md p-6 flex flex-col hover:shadow-lg transition duration-300">
            <div className="flex items-center mb-4">
              <img src="icons/environmental_education_guide.png" alt="Environmental Education Guide" className="h-10 w-10 text-green-500 mr-3" />
              <h3 className="text-xl font-semibold">Guía en Educación y Conciencia Ambiental</h3>
            </div>
            <p className="text-gray-600 mb-4">Te ofrezco recursos y actividades para enseñar sobre educación ambiental y el cambio climático en tu aula. ¿Qué tema te interesa hoy?</p>
            <button 
              onClick={() => navigate('/guia-educacion-ambiental')}
              className="mt-auto bg-green-100 text-green-600 px-4 py-2 rounded-md hover:bg-green-200 transition duration-150 flex items-center justify-center"
            >
              <img src="icons/chat.png" alt="Chat" className="h-5 w-5 mr-2" />
              Chat
            </button>
          </div>

          <div className="bg-white rounded-lg shadow-md p-6 flex flex-col hover:shadow-lg transition duration-300">
            <div className="flex items-center mb-4">
              <img src="icons/recycling_project_assistant.png" alt="Recycling Project Assistant" className="h-10 w-10 text-blue-500 mr-3" />
              <h3 className="text-xl font-semibold">Asistente de Proyectos de Reciclaje</h3>
            </div>
            <p className="text-gray-600 mb-4">Te ayudo a diseñar y gestionar proyectos de reciclaje en tu escuela. ¿Qué tipo de material reciclamos hoy?</p>
            <button 
              onClick={() => navigate('/asistente-proyectos-reciclaje')}
              className="mt-auto bg-blue-100 text-blue-600 px-4 py-2 rounded-md hover:bg-blue-200 transition duration-150 flex items-center justify-center"
            >
              <img src="icons/chat.png" alt="Chat" className="h-5 w-5 mr-2" />
              Chat
            </button>
          </div>

          <div className="bg-white rounded-lg shadow-md p-6 flex flex-col hover:shadow-lg transition duration-300">
            <div className="flex items-center mb-4">
              <img src="icons/carbon_footprint_assistant.png" alt="Carbon Footprint Assistant" className="h-10 w-10 text-purple-500 mr-3" />
              <h3 className="text-xl font-semibold">Asistente de Huella de Carbono</h3>
            </div>
            <p className="text-gray-600 mb-4">Te enseño cómo calcular y reducir la huella de carbono de tus actividades escolares. ¿Cómo te gustaría empezar?</p>
            <button 
              onClick={() => navigate('/asistente-huella-carbono')}
              className="mt-auto bg-purple-100 text-purple-600 px-4 py-2 rounded-md hover:bg-purple-200 transition duration-150 flex items-center justify-center"
            >
              <img src="icons/chat.png" alt="Chat" className="h-5 w-5 mr-2" />
              Chat
            </button>
          </div>
          
        </div>
      </main>
    </div>
  );
};

export default AssistantsPage;
