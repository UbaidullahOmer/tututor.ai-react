// query.js
const url = "https://flowise-production-ea49.up.railway.app/api/v1/prediction/776a2cc1-ceaf-4044-8757-abb44c68dc2c";
const headers = {
  "Content-Type": "application/json",
  "Authorization": "Bearer 9Vs7T_4c_PGJ4Q7RFILUqrI_tkBBRYvP2F_FbxT3O2A"
};

async function query(data) {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers,
      body: JSON.stringify(data)
    });

    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }

    const result = await response.json();
    return result;
  } catch (error) {
    console.error('Error in query function:', error.message);
    return null;
  }
}

export default query;