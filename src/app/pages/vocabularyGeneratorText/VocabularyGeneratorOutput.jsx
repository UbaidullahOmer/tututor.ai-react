import React, { useEffect, useState } from 'react'
import { Link, useLocation } from 'react-router-dom';
import { getLocalStorage } from '../../components/local_storage';
import { RouteConstant } from '../../constants/RouteConstant';
import TextEditor from '../../components/textEditor/TextEditor';

function VocabularyGeneratorOutput() {

    const [result, setResult] = useState(null);
    const location = useLocation();
  
      useEffect(()=> {
          setResult(location.state || getLocalStorage(RouteConstant.VOCABULARY_GENERATOR_OUTPUT))
      },[location.state])
  
    return (
      <>
        <div>
          <div className="flex items-center gap-3">
            <Link to={RouteConstant.VOCABULARY_GENERATOR_TEXT}>
            <i className="ri-arrow-left-s-line text-[36px] text-[#2B3D70] ml-[20px] pt-[24px] "></i>
            </Link>
            <h1 className="font-[600] text-[#2B3D70] text-[32px]">
            Volver a la herramienta Generador de Texto a partir de Vocabulario
            </h1>
          </div>
        </div>
        <TextEditor result={result} />
      </>
    );
  }
export default VocabularyGeneratorOutput