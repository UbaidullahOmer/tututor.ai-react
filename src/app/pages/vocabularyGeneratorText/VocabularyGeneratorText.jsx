import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";

function VocabularyGeneratorText() {
  const { handleHitAi, loading } = useHandleHitAi({ ai: "chatgpt", toolName: "Generador de Texto a partir de vocabulario contextualizado", });
  const [data, setData] = useState({
    subject: "",
    course: "",
    termsToDefine: "",
    wordCount: "",
  });
  const prvResponse = getLocalStorage(RouteConstant.VOCABULARY_GENERATOR_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      !data.subject ||
      !data.course ||
      !data.termsToDefine ||
      !data.wordCount
    ) {
      alert("Por favor, rellene todos los campos");
      return;
    }
    const vocabularyTextGeneratorPrompt = `
Como experto en lingüística y educación, crea un texto coherente que incorpore todos los términos de vocabulario definidos, basado en:
<ul>
  <li>Asignatura: ${data.subject}</li>
  <li>Curso: ${data.course}</li>
  <li>Términos a incluir: ${data.termsToDefine}</li>
  <li>Longitud aproximada del texto: ${data.wordCount} palabras</li>
</ul>

<h3>Instrucciones:</h3>

<ol>
  <li>Crea un texto narrativo o expositivo que:
    <ul>
      <li>Incorpore naturalmente todos los términos de vocabulario especificados.</li>
      <li>Sea apropiado para el nivel del curso y la asignatura indicados.</li>
      <li>Tenga una estructura lógica y coherente (introducción, desarrollo, conclusión).</li>
    </ul>
  </li>
  
  <li>Formato del texto:
    <pre>
    <h2>Título del Texto</h2>
    <p>[Párrafo 1]</p>
    <p>[Párrafo 2]</p>
    <p>[Párrafo 3]</p>
    ...
    <p>[Párrafo final]</p>
    </pre>
  </li>
  
  <li>Al usar cada término de vocabulario:
    <ul>
      <li>Destácalo en <strong>negrita</strong> la primera vez que aparezca.</li>
      <li>Asegúrate de que el contexto proporcione pistas sobre su significado.</li>
    </ul>
  </li>
  
  <li>Después del texto principal, incluye:
    <pre>
    <h3>Glosario</h3>
    <ul>
      <li><strong>Término 1:</strong> Breve definición</li>
      <li><strong>Término 2:</strong> Breve definición</li>
      ...
    </ul>
    </pre>
  </li>
  
  <li>Asegúrate de que:
    <ul>
      <li>El texto sea interesante y relevante para los estudiantes del curso.</li>
      <li>Se mantenga el rigor académico apropiado para la asignatura.</li>
      <li>El uso de los términos sea natural y no forzado.</li>
    </ul>
  </li>
</ol>

<p>Responde en español, creando un texto educativo que no solo use los términos de vocabulario, sino que también ayude a los estudiantes a comprender su significado y uso en contexto. El texto debe ser atractivo y fomentar el aprendizaje activo del vocabulario.</p>
The results must be given in this format: HTML`;
    console.log(vocabularyTextGeneratorPrompt);

    setIsLoading(true);
    try {
      await handleHitAi(vocabularyTextGeneratorPrompt, RouteConstant.VOCABULARY_GENERATOR_OUTPUT);
      navigate(RouteConstant.VOCABULARY_GENERATOR_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      alert("Ocurrió un error al generar el vocabulario. Por favor, inténtalo de nuevo.");
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Creador de Textos con Vocabulario Contextualizado con glosario incluido"
        description="Crea textos originales para tus clases utilizando una lista específica de vocabulario, ayudando así a practicar y reforzar las palabras en su contexto adecuado"
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <InputFieldDropdown
          placeholder="Curso"
          onChange={(value) => {
            setData({ ...data, course: value });
          }}
          name="course"
          type="number"
          value={data.course}
          require={true}
          options={Dropdown.courses}
          label="Curso"
        />
        <InputFieldDropdown
          name="subject"
          value={data.subject}
          onChange={(value) => {
            setData({ ...data, subject: value });
          }}
          placeholder="Asignatura"
          options={Dropdown.subject}
          require={true}
          label="Asignatura"
        />
        <TextArea
          name="termsToDefine"
          value={data.termsToDefine}
          onChange={handleChange}
          placeholder="Ej: fotosíntesis, ecosistema, biodiversidad, sostenibilidad, cambio climático, reciclaje, deforestación, contaminación, energía renovable, huella de carbono, hábitat, conservación, recursos naturales, capa de ozono, efecto invernadero, bioma, erosión, especies en peligro, compostaje, calentamiento global"
          require={true}
          label="Vocabulario a incluir"
        />
        <InputField
          name="wordCount"
          value={data.wordCount}
          onChange={handleChange}
          placeholder="Ej: 300 palabras"
          type="number"
          label="Número de palabras"
          require={true}
        />
        <div className="col-span-2 flex align-center gap-2">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={loading || isLoading}
          >
            {isLoading ? "Generando..." : "Generar"}
          </Button>
          {prvResponse && (
            <Button
              className="w-[20%]"
              onClick={() => {
                navigate(RouteConstant.VOCABULARY_GENERATOR_OUTPUT);
              }}
              disabled={loading || isLoading}
            >
              Anterior
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default VocabularyGeneratorText;