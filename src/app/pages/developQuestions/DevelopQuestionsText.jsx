import React, { useState, useEffect } from "react";
import {
  getLocalStorage,
  setLocalStorage,
} from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import FileReaderComponent from "../../components/fileReaderComponent/FileReaderComponent";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";
import { Dropdown } from "../../constants/Dropdowns";
import FileReaderInputFIled from "../../components/fileReaderInputFIled/FileReaderInputFIled";

function DevelopQuestions() {
  const [data, setData] = useState({
    selectedFiles: [],
    prompt: "",
    course: "",
    subject: "",
    numberOfQuestions: "",
    difficultyLevel: "",
    additionalContent: "",
  });
  const { handleHitAi, loading, error } = useHandleHitAi({
    ai: "chatgpt",
    selectedFiles: data.selectedFiles,
    toolName: "Generador de Preguntas de Desarrollo",
  });
  const [formError, setFormError] = useState("");
  const prvResponse = getLocalStorage(RouteConstant.DEVELOP_QUESTIONS_OUTPUT);
  const navigate = useNavigate();

  const handleChange = (e) => {
    console.log(`Field ${e.target.name} changed to: ${e.target.value}`);
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log("Form submitted");
    console.log("Current data:", data);

    setFormError("");

    if (
      (!data.prompt && data.selectedFiles.length === 0) ||
      !data.course ||
      !data.subject ||
      !data.numberOfQuestions ||
      !data.difficultyLevel
    ) {
      const errorMsg = "Por favor, rellena todos los campos obligatorios";
      console.error(errorMsg);
      setFormError(errorMsg);
      return;
    }

    let aiPrompt = `Genera ${data.numberOfQuestions} preguntas de desarrollo en español para el curso de ${data.course}, asignatura ${data.subject}, sobre el tema: ${data.prompt}.

Nivel de dificultad: ${data.difficultyLevel}
Contenido adicional a considerar: ${data.additionalContent}

Para cada pregunta, proporciona:
1. El texto de la pregunta

Genera el contenido directamente en formato HTML, sin usar Markdown. Utiliza etiquetas HTML apropiadas para estructurar el contenido.`;
    if (data.selectedFiles.length > 0) {
      const imageFiles = data.selectedFiles.filter((file) =>
        file.type.startsWith("image/")
      );
      if (imageFiles.length > 0) {
        aiPrompt +=
          "\nPlease analyze the uploaded images and include relevant questions based on their content.";
      }
    }
    try {
      console.log("Calling handleHitAi");
      const prompt = await handleHitAi(
        aiPrompt,
        RouteConstant.DEVELOP_QUESTIONS_OUTPUT,
        "Generador de Preguntas de Desarrollo"
      );

      if (prompt) {
        console.log("Storing response in local storage");
        setLocalStorage(RouteConstant.DEVELOP_QUESTIONS_OUTPUT, prompt);
        console.log("Navigating to output page");
        navigate(RouteConstant.DEVELOP_QUESTIONS_OUTPUT, { state: prompt });
      } else {
        console.error("No prompt received from AI");
        setFormError(
          "No se pudo generar el contenido. Por favor, inténtalo de nuevo."
        );
      }
    } catch (error) {
      console.error("Error during API call:", error);
      setFormError(`Error: ${error.message || "Ocurrió un error desconocido"}`);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Generador de Preguntas de Desarrollo"
        description="Esta herramienta genera preguntas de desarrollo para profesores o para alumnos para. Introduce los detalles del curso y el contenido para obtener preguntas que fomenten el pensamiento crítico y la comprensión profunda."
        icon="ri-questionnaire-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        {formError && (
          <div className="col-span-2 text-red-500">{formError}</div>
        )}
        {error && (
          <div className="col-span-2 text-red-500">Error from AI: {error}</div>
        )}
        <FileReaderInputFIled
          onChange={handleChange}
          data={data}
          setData={setData}
        />
        <InputFieldDropdown
          placeholder="Curso"
          onChange={(value) => {
            console.log("Course changed to:", value);
            setData({ ...data, course: value });
          }}
          name="course"
          value={data.course}
          options={Dropdown.courses}
          require={true}
          label="Curso"
        />
        <InputFieldDropdown
          name="subject"
          value={data.subject}
          onChange={(value) => {
            console.log("Subject changed to:", value);
            setData({ ...data, subject: value });
          }}
          require={true}
          placeholder="Asignatura"
          options={Dropdown.subject}
          label="Asignatura"
        />
        <InputFieldDropdown
          name="numberOfQuestions"
          value={data.numberOfQuestions}
          onChange={(value) => {
            console.log("Number of questions changed to:", value);
            setData({ ...data, numberOfQuestions: value });
          }}
          placeholder="Elige el número de preguntas que quieres generar"
          options={["5", "10", "15", "20", "25", "30"]}
          label="Número de preguntas"
        />
        <InputFieldDropdown
          name="difficultyLevel"
          value={data.difficultyLevel}
          onChange={(value) => {
            console.log("Difficulty level changed to:", value);
            setData({ ...data, difficultyLevel: value });
          }}
          placeholder="Selecciona la dificultad de las preguntas"
          options={["Muy fácil", "Fácil", "Moderado", "Difícil", "Muy difícil"]}
          label="Dificultad del test"
        />
        <TextArea
          placeholder="Contenido adicional (opcional)"
          onChange={handleChange}
          className="col-span-2"
          value={data.additionalContent}
          name="additionalContent"
          label="Contenido adicional"
        />
        <div className="col-span-2 flex align-center gap-2 ">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={loading}
          >
            {loading ? "Generando..." : "Generar"}
          </Button>
          {prvResponse && (
            <Button
              className="w-[20%]"
              onClick={() => {
                console.log("Navigating to previous response");
                navigate(RouteConstant.DEVELOP_QUESTIONS_OUTPUT);
              }}
            >
              Anterior
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default DevelopQuestions;
