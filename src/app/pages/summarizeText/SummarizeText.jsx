import React, { useState } from "react";
import { RouteConstant } from "../../constants/RouteConstant";
import { useNavigate } from "react-router-dom";
import { getLocalStorage } from "../../components/local_storage";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import InputField from "../../components/inputFiled/InputFiled";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { citationStyles, summaryTones } from "./constants";
import Button from "../../components/button/Button";
import FileReaderComponent from "../../components/fileReaderComponent/FileReaderComponent";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import "../../styles.css";
import FileReaderInputFIled from "../../components/fileReaderInputFIled/FileReaderInputFIled";

function SummarizeText() {
  const [data, setData] = useState({
    selectedFiles: [],
    prompt: "",
    wordCount: "",
    tone: "",
    citation: "",
  });
  const { handleHitAi } = useHandleHitAi({
    ai: "chatgpt",
    selectedFiles: data.selectedFiles,
    toolName: "Resumidor de Textos",
  });
  const prvResponse = getLocalStorage(RouteConstant.SUMMARIZE_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!data.wordCount || !data.tone || !data.citation) {
      alert("Por favor, rellena todos los campos");
      return;
    }

    let aiPrompt = `You are a powerful AI assistant and a summarization professional. You will respond only in Spanish and summarize the below text up to ${data.wordCount} words. The tone of summarization should be ${data.tone} and the citation style should be ${data.citation}.

Text to summarize:
${data.prompt}

Instructions:
1. Create a summary that captures the main ideas of the text.
2. Maintain a basic structure using paragraphs to separate distinct ideas.
3. Ensure the summary reflects the specified tone: ${data.tone}.
4. Use the specified citation style (${data.citation}) if referencing any sources.
5. Keep the summary within the ${data.wordCount} word limit.
6. Answer always in Spanish.
Generate the result in the following HTML format, ensuring clear separation of paragraphs:

<div className="summary">
  <h2>Summary</h2>
  <p>[First paragraph of the summary]</p>
  <p>[Second paragraph of the summary (if needed)]</p>
  <!-- Add more paragraphs as necessary -->
</div>

Ensure the summary is clear, concise, and maintains the essence of the original text while adhering to the specified parameters. Each paragraph should focus on a distinct idea, maintaining proper structure and format.
The generation must always be in Spanish and in HTML format`;

    if (data.selectedFiles.length > 0) {
      const imageFiles = data.selectedFiles.filter((file) =>
        file.type.startsWith("image/")
      );
      const pdfFiles = data.selectedFiles.filter((file) =>
        file.type === "application/pdf"
      );

      if (imageFiles.length > 0) {
        aiPrompt +=
          "\nPlease analyze the uploaded images and summarize their content based on the user's instructions.";
      }

      if (pdfFiles.length > 0) {
        aiPrompt +=
          "\nPlease analyze the uploaded PDFs and summarize their content based on the user's instructions.";
      }
    }

    setIsLoading(true);
    try {
      await handleHitAi(aiPrompt, RouteConstant.SUMMARIZE_OUTPUT);
      navigate(RouteConstant.SUMMARIZE_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Resumidor de textos"
        description="Resume textos largos en las palabras que elijas. Una muy buena función para resumir textos largos que te cueste comprender en uno más compacto"
        icon="ri-edit-2-line text-[32px] text-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <FileReaderInputFIled
          onChange={handleChange}
          data={data}
          setData={setData}
        />
        <InputField
          placeholder="Elije en cuántas palabras quieres que aproximadamente se te resuma tu contenido"
          onChange={handleChange}
          name="wordCount"
          type="number"
          value={data.wordCount}
          label={"Número de palabras a resumir"}
        />
        <InputFieldDropdown
          name="tone"
          value={data.tone}
          onChange={(value) => setData({ ...data, tone: value })}
          placeholder="Elije el estilo de escritura en el que quieres que se te resuma el texto"
          options={summaryTones}
          label={"Estilo de Escritura"}
        />
        <InputFieldDropdown
          name="citation"
          value={data.citation}
          onChange={(value) => setData({ ...data, citation: value })}
          placeholder="Elige entre uno de lo estilos de citación bibliográfica disponibles. Muy útil si necesitas citar autores"
          options={citationStyles}
          label={"Estilos de Citación Bibliográfica"}
        />
        <div className="col-span-2 flex align-center gap-2">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={isLoading}
          >
            {isLoading ? "Cargando..." : "Generar resumen"}
          </Button>
          {prvResponse && (
            <Button
              className="w-[20%]"
              onClick={() => navigate(RouteConstant.SUMMARIZE_OUTPUT)}
            >
              Generación Anterior
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default SummarizeText;
