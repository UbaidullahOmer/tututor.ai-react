export const citationStyles = [
  "Ninguno",
  "APA",
  "MLA",
  "Chicago",
  "Harvard",
  "IEEE",
  "Vancouver",
];

export const summaryTones = [
  "Formal",
  "Casual",
  "Académico",
  "Persuasivo",
  "Detallado",
  "Divertido",
  "Descriptivo",
  "Analítico",
  "Innovador",
  "Motivador",
];
