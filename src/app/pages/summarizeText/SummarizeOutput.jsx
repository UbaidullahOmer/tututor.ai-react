import React, { useEffect, useState } from "react";
import TextEditor from "../../components/textEditor/TextEditor";
import { Link, useLocation } from "react-router-dom";
import { getLocalStorage } from "../../components/local_storage";
import { RouteConstant } from "../../constants/RouteConstant";

function SummarizeOutput() {
  const [result, setResult] = useState(null);
  const location = useLocation();

  useEffect(() => {
    const output = location.state || getLocalStorage(RouteConstant.SUMMARIZE_OUTPUT);
    setResult(output);
  }, [location.state]);

  return (
    <>
      <div>
        <div className="flex items-center gap-3">
          <Link to={RouteConstant.SUMMARIZE_TEXT}>
            <i className="ri-arrow-left-s-line text-[36px] text-[#2B3D70] ml-[20px] pt-[24px] "></i>
          </Link>
          <h1 className="font-[600] text-[#2B3D70] text-[32px]">
          Volver a la herramienta Resumidor de Textos Inteligente
          </h1>
        </div>
      </div>
      {result ? (
        <TextEditor result={result} />
      ) : (
        <div className="text-center text-gray-500">No content generated yet.</div>
      )}
    </>
  );
}

export default SummarizeOutput;
