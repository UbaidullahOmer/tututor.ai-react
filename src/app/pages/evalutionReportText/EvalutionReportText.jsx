import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";

function EvaluationReportText() {
  const { handleHitAi, loading } = useHandleHitAi({ ai: "chatgpt" });
  const [data, setData] = useState({
    course: "",
    subject: "",
    studentName: "",
    achievements: "",
    areaToImprove: "",
  });
  const prvResponse = getLocalStorage(RouteConstant.EVALUTION_REPORTS_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      !data.course ||
      !data.subject ||
      !data.studentName ||
      !data.achievements ||
      !data.areaToImprove
    ) {
      alert("Por favor, rellene todos los campos");
      return;
    }
    const evaluationReportGeneratorPrompt = `Como experto en educación y evaluación, genera un informe de evaluación detallado basado en:
<ul>
  <li>Curso: ${data.course}</li>
  <li>Asignatura: ${data.subject}</li>
  <li>Nombre del estudiante: ${data.studentName}</li>
  <li>Logros y Áreas Destacadas: ${data.achievements}</li>
  <li>Áreas a mejorar: ${data.areaToImprove}</li>
</ul>

<h3>Instrucciones para el Informe de Evaluación:</h3>

<ol>
  <li>Encabezado del Informe:
    <pre>
    <h2>Informe de Evaluación</h2>
    <p><strong>Estudiante:</strong> ${data.studentName}</p>
    <p><strong>Curso:</strong> ${data.course} sin los años, solo el curso</p>
    <p><strong>Asignatura:</strong> ${data.subject}</p>
    <p><strong>Fecha:</strong> [dejar en blanco]</p>
    </pre>
  </li>
  
  <li>Introducción:
    <p>Escribe un párrafo breve que contextualice el informe y proporcione una visión general del desempeño del estudiante.</p>
  </li>
  
  <li>Logros y Áreas Destacadas:
    <pre>
    <h3>Logros y Áreas Destacadas</h3>
    <ul>
      [Enumera y describe brevemente cada logro o área destacada mencionada en ${data.achievements}]
    </ul>
    </pre>
    <p>Para cada logro o área destacada, proporciona:
      <ul>
        <li>Una explicación clara de por qué es significativo</li>
        <li>Ejemplos específicos que demuestren el logro, si es posible</li>
        <li>Cómo este logro contribuye al desarrollo general del estudiante en la asignatura</li>
      </ul>
    </p>
  </li>
  
  <li>Áreas a Mejorar:
    <pre>
    <h3>Áreas de Mejora</h3>
    <ul>
      [Enumera y describe brevemente cada área a mejorar mencionada en ${data.areaToImprove}]
    </ul>
    </pre>
    <p>Para cada área de mejora, proporciona:
      <ul>
        <li>Una explicación constructiva de por qué es importante mejorar en esta área</li>
        <li>Sugerencias específicas y prácticas para abordar esta área</li>
        <li>Recursos o estrategias que puedan ayudar al estudiante a mejorar</li>
      </ul>
    </p>
  </li>
  
  <li>Plan de Acción:
    <pre>
    <h3>Plan de Acción Recomendado</h3>
    </pre>
    <p>Desarrolla un breve plan de acción que incluya:
      <ul>
        <li>Objetivos específicos a corto plazo para abordar las áreas de mejora</li>
        <li>Estrategias concretas para mantener y desarrollar aún más las áreas destacadas</li>
        <li>Sugerencias para el seguimiento y la evaluación continua del progreso</li>
      </ul>
    </p>
  </li>
  
  <li>Conclusión:
    <p>Escribe un párrafo final que resuma el desempeño general del estudiante, enfatice sus fortalezas y exprese confianza en su capacidad para abordar las áreas de mejora.</p>
  </li>
</ol>

<p>Responde en español, asegurándote de que el informe sea personalizado, constructivo y motivador. Utiliza un tono positivo y alentador, incluso al discutir las áreas de mejora. 
El objetivo es proporcionar una evaluación clara y útil que ayude al estudiante a comprender su progreso y a establecer metas para su futuro aprendizaje.</p>
The results must be given in this format: HTML`;

    console.log(evaluationReportGeneratorPrompt);

    setIsLoading(true);
    try {
      await handleHitAi(evaluationReportGeneratorPrompt, RouteConstant.EVALUTION_REPORTS_OUTPUT);
      navigate(RouteConstant.EVALUTION_REPORTS_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      alert("Ocurrió un error al generar el informe de evaluación. Por favor, inténtalo de nuevo.");
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Creador de Informes de Evaluación: Retroalimentación Personalizada en un Instante"
        description="El Creador de Informes de Evaluación es una herramienta eficiente que genera evaluaciones detalladas y personalizadas para cada estudiante. Combinando datos académicos con observaciones cualitativas, produce informes equilibrados que destacan logros, identifican áreas de mejora y ofrecen una visión integral del progreso del alumno. Esta herramienta agiliza el proceso de evaluación, permitiendo a los educadores proporcionar retroalimentación valiosa y constructiva de manera rápida y consistente"
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <InputFieldDropdown
          placeholder="Curso"
          onChange={(value) => {
            setData({ ...data, course: value });
          }}
          name="course"
          type="number"
          value={data.course}
          options={Dropdown.courses}
          require={true}
          label="Curso"
        />
        <InputFieldDropdown
          name="subject"
          value={data.subject}
          onChange={(value) => {
            setData({ ...data, subject: value });
          }}
          require={true}
          placeholder="Asignatura"
          options={Dropdown.subject}
          label="Asignatura"
        />
        <InputField
          name="studentName"
          value={data.studentName}
          onChange={handleChange}
          placeholder="Nombre del Estudiante para generar el reporte"
          require={true}
          label="Nombre del Estudiante"
        />
        <TextArea
          name="achievements"
          value={data.achievements}
          onChange={handleChange}
          placeholder="Ej: Excelente capacidad de análisis en la interpretación de obras renacentistas. Notable mejora en la identificación de estilos arquitectónicos. Participación activa y entusiasta en debates sobre arte contemporáneo. Habilidad sobresaliente para relacionar contextos históricos con movimientos artísticos"
          require={true}
          label="Logros y Áreas Destacadas"
        />
        <TextArea
          name="areaToImprove"
          value={data.areaToImprove}
          onChange={handleChange}
          placeholder="Ej: Profundizar en el análisis técnico de las obras escultóricas. Mejorar la organización y estructura en los ensayos escritos. Ampliar el vocabulario específico relacionado con las técnicas pictóricas. Trabajar en la contextualización de obras de arte no europeas"
          require={true}
          label="Áreas a desarrollar"
        />
        <div className="col-span-2 flex align-center gap-2">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={loading || isLoading}
          >
            {isLoading ? "Generando..." : "Generar"}
          </Button>
          {prvResponse && (
            <Button
              className="w-[20%]"
              onClick={() => {
                navigate(RouteConstant.EVALUTION_REPORTS_OUTPUT);
              }}
              disabled={loading || isLoading}
            >
              Anterior
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default EvaluationReportText;