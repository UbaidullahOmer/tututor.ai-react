import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";

const FormField = ({ title, children, required = false }) => (
  <div className="mb-4">
    <label className="block font-bold text-sm mb-2">
      {title} {required && <span className="text-red-500">*</span>}
    </label>
    {children}
  </div>
);

function LearningSituationsText() {
  const { handleHitAi, loading } = useHandleHitAi({ ai: "chatgpt", toolName: "Creador de Situaciones de Aprendizaje", });
  const [data, setData] = useState({
    curso: "",
    asignatura: "",
    objetivosDidacticos: "",
    competencias: "",
    caracteristicasAlumnoClase: "",
    duracion: "",
    numeroSesiones: "",
    numeroAlumnos: "",
    contenidoTematica: "",
    gruposTrabajo: "",
    numeroGrupos: "",
    contenidoAdicional: "",
    lugar: "",
    selectedFiles: [],
  });
  const prvResponse = getLocalStorage(RouteConstant.LEARNING_SITUATIONS_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setData((prevData) => ({
      ...prevData,
      [name]: value,
      ...(name === "gruposTrabajo" && value === "No" ? { numeroGrupos: "" } : {}),
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      !data.curso ||
      !data.asignatura ||
      !data.objetivosDidacticos ||
      !data.caracteristicasAlumnoClase ||
      !data.duracion ||
      !data.numeroSesiones ||
      !data.numeroAlumnos ||
      !data.gruposTrabajo ||
      (data.gruposTrabajo === "Si" && !data.numeroGrupos) ||
      !data.contenidoTematica
    ) {
      alert("Por favor, rellene todos los campos requeridos");
      return;
    }
    
    const learningSituationGeneratorPrompt = `
Como experto en diseño educativo, crea una Situación de Aprendizaje innovadora, práctica y detallada basada en la siguiente información:

Curso: ${data.curso}
Asignatura: ${data.asignatura}
Objetivos didácticos: ${data.objetivosDidacticos}
Características del alumnado/clase: ${data.caracteristicasAlumnoClase}
Duración: ${data.duracion}
Número de sesiones: ${data.numeroSesiones}
Número de alumnos: ${data.numeroAlumnos}
Contenido/temática: ${data.contenidoTematica}
Lugar de realización: ${data.lugar}
Competencias a desarrollar: ${data.competencias || "No especificadas"}
Grupos de trabajo: ${data.gruposTrabajo}
Número de grupos: ${data.numeroGrupos || "No aplica"}
Contenido adicional: ${data.contenidoAdicional || "No especificado"}

Definición de Situación de Aprendizaje:
Una Situación de Aprendizaje es una experiencia educativa inmersiva y contextualizada donde los estudiantes aprenden a través de la participación activa en escenarios reales, SIEMPRE fuera del aula tradicional. Debe permitir a los alumnos descubrir o aplicar conocimientos in situ, conectando directamente con el mundo real y siendo protagonistas de su propio aprendizaje.

Características clave:
1. Contexto Auténtico: SIEMPRE desarrollada en un entorno real, nunca simulado ni en el aula.
2. Aprendizaje Activo: 100% de involucración de los estudiantes, nunca pasivos.
3. Descubrimiento y Aplicación: Los estudiantes aplican conocimientos previos o descubren nuevos conceptos a través de la experiencia directa en situaciones reales.
4. Relevancia: Clara conexión con la vida real y los intereses de los estudiantes.
5. Interdisciplinariedad: Incorpora elementos de diversas áreas de conocimiento cuando sea posible.

Tipos de Situaciones de Aprendizaje:
1. Culminativa: Los estudiantes aplican conocimientos previamente adquiridos en un contexto real. Ejemplo: Después de aprender sobre un tipo de pintura, los alumnos organizan una exposición de arte en un espacio público de la ciudad.
2. In Situ: Los estudiantes aprenden nuevos conceptos directamente en el entorno real, sin instrucción previa en el aula. Ejemplo: Aprender sobre multiplicación directamente en un mercado local, interactuando con vendedores y productos reales.

Estructura requerida:

<h1>Título de la Situación de Aprendizaje</h1>

<h2>1. Contextualización (3-4 líneas)</h2>
Breve descripción que capture la esencia de la situación de aprendizaje, enfatizando su conexión con el mundo real y cómo los estudiantes estarán completamente inmersos en un entorno auténtico.

<h2>2. Objetivos de Aprendizaje (3-4 puntos concisos)</h2>
<ul>
  <li>Objetivo 1</li>
  <li>Objetivo 2</li>
  <li>Objetivo 3</li>
</ul>

<h2>3. Desarrollo de la Situación de Aprendizaje</h2>

<h3>3.1 Preparación (solo para situaciones culminativas)</h3>
Breve descripción de los conocimientos o habilidades que los estudiantes han adquirido previamente y que aplicarán en esta situación real.

<h3>3.2 Experiencia Central de Aprendizaje</h3>
Describe muy detalladamente la experiencia de aprendizaje en el entorno real. Esta es la parte más importante, así que desarróllala extensamente Incluye:
<ul>
  <li>Descripción paso a paso y bien detallada de las actividades principales (mínimo 5 pasos detallados)</li>
  <li>Momentos de reflexión y discusión durante la experiencia</li>
  <li>Roles activos de los estudiantes y rol facilitador del docente</li>
</ul>

<h3>3.3 Cierre y Reflexión</h3>
<ul>
  <li>Actividad de consolidación del aprendizaje (2-3 líneas)</li>
  <li>Cómo se conectará la experiencia con futuros aprendizajes o aplicaciones en la vida real (2-3 líneas)</li>
</ul>

<h2>4. Evaluación (2-3 puntos concisos)</h2>
<ul>
  <li>Método de evaluación 1</li>
  <li>Método de evaluación 2</li>
</ul>

<h2>5. Recursos Necesarios (lista breve)</h2>
<ul>
  <li>Recurso 1</li>
  <li>Recurso 2</li>
</ul>

<h2>6. Adaptaciones (2-3 sugerencias breves)</h2>
<ul>
  <li>Adaptación 1</li>
  <li>Adaptación 2</li>
</ul>

Consideraciones importantes:
- La situación DEBE desarrollarse SIEMPRE en un entorno real, NUNCA en el aula o en un entorno simulado.
- Los estudiantes deben estar 100% activos y ser protagonistas de su aprendizaje en todo momento.
- El aprendizaje debe ocurrir a través de la experiencia directa en situaciones reales, ya sea aplicando conocimientos previos o descubriendo nuevos conceptos in situ.
- La situación debe tener un impacto real en la comunidad o el entorno donde se desarrolla.
- Asegúrate de que la situación sea realista, relevante y desafiante para la edad y contexto de los estudiantes.
- Incluye oportunidades para la colaboración y la comunicación entre los estudiantes.

Ejemplos para contextualizar:
1. Pintura (Culminativa): Los estudiantes organizan una exposición de arte en un espacio público de la ciudad, aplicando técnicas aprendidas previamente.
2. Expresión Corporal (Culminativa): Los alumnos preparan y presentan una obra de teatro en el teatro local de la ciudad.
3. Matemáticas (In Situ): Los estudiantes aprenden sobre multiplicación interactuando con vendedores en un mercado local.
4. Narrativa (In Situ): Los alumnos crean historias inspiradas directamente en un parque o bosque, sin instrucción previa en el aula.
5. Química (In Situ): Los estudiantes observan y analizan reacciones químicas durante el proceso de elaboración del pan en una panadería local.

Instrucciones adicionales:
- La situación de aprendizaje DEBE desarrollarse en un entorno completamente real y auténtico, (por ejemplo, un teatro local, un centro cultural, un parque público).
- Debe tener un impacto tangible en la comunidad o el entorno donde se desarrolla.
- Incluye una clara conexión con problemas o situaciones del mundo real donde las habilidades aprendidas se aplican de manera práctica y relevante.
- Asegúrate de que la situación involucre a profesionales reales del sector o miembros de la comunidad, no solo a los estudiantes y profesores.

Responde en español, utilizando el formato HTML proporcionado. Prioriza la descripción detallada y práctica de la situación de aprendizaje, asegurándote de que refleje una experiencia inmersiva en un entorno real, donde los estudiantes sean participantes activos y el aprendizaje ocurra a través de la interacción directa con situaciones y problemas auténticos.
`;


    setIsLoading(true);
    try {
      await handleHitAi(learningSituationGeneratorPrompt, RouteConstant.LEARNING_SITUATIONS_OUTPUT);
      navigate(RouteConstant.LEARNING_SITUATIONS_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      alert("Ocurrió un error al generar la situación de aprendizaje. Por favor, inténtalo de nuevo.");
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px] max-h-[80vh] overflow-scroll">
      <AiToolPageHeader
        title="Creador de Situaciones de Aprendizaje: Experiencias Educativas Integrales"
        description="Convierte tus clases en emocionantes aventuras de descubrimiento. Crea experiencias que conectan el aula con el mundo real, despertando la curiosidad de tus alumnos y haciendo que cada lección cobre vida. Fomenta un aprendizaje significativo que impacta realmente en tus estudiantes. ACLARACIÓN: Desde esta web, el enfoque de una Situación de Aprendizaje no es igual a una Unidad Didáctica. Sino que es una ''Actividad-Situación'' en la que los alumnos ponen en práctica lo aprendido en una situación de contexto real y competencial."
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <form onSubmit={handleSubmit} className="flex flex-col gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <div className="grid grid-cols-2 gap-4">
          <FormField title="Curso" required>
            <InputFieldDropdown
              placeholder="Curso"
              onChange={(value) => {
                setData({ ...data, curso: value });
              }}
              name="curso"
              value={data.curso}
              options={Dropdown.courses}
            />
          </FormField>
          <FormField title="Asignatura" required>
            <InputFieldDropdown
              name={"asignatura"}
              value={data.asignatura}
              onChange={(value) => {
                setData({ ...data, asignatura: value });
              }}
              placeholder={"Asignatura"}
              options={Dropdown.subject}
            />
          </FormField>
        </div>
        <FormField title="Duración de la Situación de Aprendizaje" required>
          <TextArea
            name="duracion"
            value={data.duracion}
            onChange={handleChange}
            placeholder="Ej: 50 minutos, 1 hora, 2 horas etc"
          />
        </FormField>
        <FormField title="Número de Sesiones" required>
          <TextArea
            name="numeroSesiones"
            value={data.numeroSesiones}
            onChange={handleChange}
            placeholder="Ej: 3 sesiones"
          />
        </FormField>
        <FormField title="Número de Alumnos" required>
          <TextArea
            name="numeroAlumnos"
            value={data.numeroAlumnos}
            onChange={handleChange}
            placeholder="Ej: 7 grupos de 4 estudiantes cada uno es una distribución típica para trabajos en equipo"
          />
        </FormField>
        <div>
          <label htmlFor="gruposTrabajo" className="block mb-2 text-sm font-medium text-gray-900">
            Grupos de Trabajo
          </label>
          <select
            id="gruposTrabajo"
            name="gruposTrabajo"
            value={data.gruposTrabajo}
            onChange={handleChange}
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            required
          >
            <option value="" disabled>¿Hay Grupos de Trabajo?</option>
            <option value="No">No</option>
            <option value="Si">Si</option>
          </select>
        </div>
        {data.gruposTrabajo === "Si" && (
          <FormField title="Número de Grupos" required>
            <TextArea
              name="numeroGrupos"
              value={data.numeroGrupos}
              onChange={handleChange}
              placeholder="Número de grupos de trabajo"
            />
          </FormField>
        )}
        <FormField title="Objetivos Didácticos" required>
          <TextArea
            placeholder="Ej: Comprender la estructura y función del sistema circulatorio humano, identificar sus principales componentes y explicar el proceso de circulación sanguínea"
            onChange={handleChange}
            value={data.objetivosDidacticos}
            name="objetivosDidacticos"
          />
        </FormField>
        <FormField title="Características de la Clase" required>
          <TextArea
            name={"caracteristicasAlumnoClase"}
            onChange={handleChange}
            placeholder={'Ej: 28 alumnos, incluyendo 2 con dislexia y 1 con altas capacidades. La mayoría muestra interés por temas de salud'}
            value={data.caracteristicasAlumnoClase}
          />
        </FormField>
        <FormField title="Lugar de realización" required>
          <TextArea
            name={"lugar"}
            onChange={handleChange}
            placeholder={'Ej: Si la Situación de Aprendizaje se va a realizar en un aula, en el gimnasio del centro, en el patio, centro social, mercadillo del pueblo, o cualquier otro lugar. También puedes decirle a la IA que sea creativa con el lugar y te de uno original'}
            value={data.lugar}
          />
        </FormField>
        <FormField title="Contenido o Temática de la Situación de Aprendizaje" required>
          <TextArea
            name={"contenidoTematica"}
            value={data.contenidoTematica}
            placeholder={'Ej: La Revolución Francesa en Historia'}
            onChange={handleChange}
          />
          </FormField>
        <FormField title="Competencias a Desarrollar. OPCIONAL" required>
          <TextArea
            name={"competencias"}
            onChange={handleChange}
            placeholder={'Ej: Indica las competencias clave o específicas a tener en cuenta'}
            value={data.competencias}
          />
        </FormField>
        <FormField title="Contenido Adicional. OPCIONAL" required>
          <TextArea
            name="contenidoAdicional"
            value={data.contenidoAdicional}
            onChange={handleChange}
            placeholder="Ej: Se utilizarán recursos digitales como documentales cortos y una plataforma de gamificación para un quiz histórico. Se planea una actividad de role-play donde los estudiantes representarán diferentes grupos sociales de la época"
          />
        </FormField>
        <div className="flex justify-center gap-4 mt-8">
          <Button
            type="submit"
            className={isLoading ? "opacity-50 cursor-not-allowed w-full" : "w-full"}
            disabled={isLoading}
          >
            {isLoading ? "Cargando..." : "Generar"}
          </Button>
          {prvResponse && (
            <Button
              type="button"
              className="w-full py-2.5 px-5 bg-gray-200 text-gray-700 rounded-lg hover:bg-gray-300 transition duration-300"
              onClick={() => navigate(RouteConstant.LEARNING_SITUATIONS_OUTPUT)}
            >
              Ver generación previa
            </Button>
          )}
        </div>
      </form>
    </div>
  );
}

export default LearningSituationsText;
