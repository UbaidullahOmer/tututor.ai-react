import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";
import FileReaderInputFIled from "../../components/fileReaderInputFIled/FileReaderInputFIled";

const FormField = ({ title, children, required = false }) => (
  <div className="mb-4">
    <label className="block font-bold text-sm mb-2">
      {title} {required && <span className="text-red-500">*</span>}
    </label>
    {children}
  </div>
);

function EvaluationRubricsText() {
  const [data, setData] = useState({
    prompt: "",
    course: "",
    subject: "",
    contentToEvaluate: "",
    scale: "",
    criteriosEvaluacion: "",
    contenidoAdicional: "",
    pais: "",
    selectedFiles: [],
  });

  const { handleHitAi, loading } = useHandleHitAi({ 
    ai: "chatgpt",
    selectedFiles: data.selectedFiles,
    toolName: "Rúbricas de Evaluación",
  });

  const prvResponse = getLocalStorage(RouteConstant.EVALUATION_RUBRICS_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      !data.course ||
      !data.subject ||
      !data.contentToEvaluate ||
      !data.criteriosEvaluacion ||
      !data.pais ||
      !data.scale
    ) {
      alert("Please fill in all required fields");
      return;
    }
    
    let evaluationRubricsPrompt = `
    Genera una rúbrica de evaluación en formato HTML para el siguiente contexto:
    
    Curso: ${data.course}
    Asignatura: ${data.subject}
    Contenido a evaluar: ${data.contentToEvaluate}
    Escala: ${data.scale}
    País: ${data.pais}
    Criterios de evaluación: ${data.criteriosEvaluacion}
    Contenido adicional relevante: ${data.contenidoAdicional}
    
    Instrucciones específicas:
    1. Utiliza 4 a 5 criterios clave de evaluación basados en los criterios proporcionados.
    2. Para cada criterio, proporciona descriptores claros y observables para **cada número** de la escala del 1 al 10, es decir, deben existir descriptores para 1, 2, 3,... hasta 10.
    3. Incluye una fila para el puntaje de cada criterio.
    4. Proporciona un área para comentarios para cada criterio.
    5. Incluye una sección para el puntaje total y la calificación general al final de la tabla.
    
    Formato y estilo de la tabla: Nunca saques esto en la generación que haces al usuario. Esto es solo para ti que comprendas como sacar la tabla.
    - Usa la etiqueta <table> con la clase "rubrica-table".
    - Utiliza <thead> para la fila de encabezado y <tbody> para el contenido.
    - Usa <th> para las celdas de encabezado y <td> para las celdas de datos.
    - Incluye las siguientes clases CSS en el <style> de la tabla:
      .rubrica-table { width: 100%; border-collapse: collapse; }
      .rubrica-table th, .rubrica-table td { border: 1px solid #ddd; padding: 8px; text-align: left; }
      .rubrica-table th { background-color: #2E7D32; color: white; }
      .rubrica-table tr:nth-child(even) { background-color: #f9f9f9; }
      .rubrica-table .puntaje, .rubrica-table .comentarios { font-weight: bold; }
    
    Importante:
    - **Genera ÚNICAMENTE el código HTML de la tabla, comenzando con la etiqueta <style> y terminando con la etiqueta </table>.**
    - **No incluyas texto adicional, explicaciones o contenido fuera de la tabla HTML.**
    - **Asegúrate de que la tabla sea responsive y se vea bien en dispositivos móviles.**
    - Responde en español.
    - **No incluyas ningún texto o caracteres adicionales en la respuesta.**
    `;
    


    if (data.selectedFiles.length > 0) {
      const imageFiles = data.selectedFiles.filter((file) =>
        file.type.startsWith("image/")
      );
      if (imageFiles.length > 0) {
        evaluationRubricsPrompt +=
          "\nPor favor, analiza las imágenes subidas e intégralas en la rúbrica de evaluación. Utiliza el contenido visual para enriquecer los criterios de evaluación y los descriptores, asegurando que las imágenes sean un componente central y relevante en la evaluación.";
      }
    }

    console.log(evaluationRubricsPrompt);

    setIsLoading(true);
    try {
      await handleHitAi(evaluationRubricsPrompt, RouteConstant.EVALUATION_RUBRICS_OUTPUT);
      navigate(RouteConstant.EVALUATION_RUBRICS_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px] max-h-[80vh] overflow-scroll">
      <AiToolPageHeader
        title="Rúbricas de Evaluación"
        description="Crea tus rúbricas de evaluación para evaluar a tus alumnos. Genera cualquier tipo de rúbrica que desees con la información necesaria."
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <form onSubmit={handleSubmit} className="flex flex-col gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <FormField title="Sube tu temario o escribe sobre qué quieres hacer la rúbrica de evaluación" className="w-full">
          <FileReaderInputFIled
            onChange={handleChange}
            data={data}
            setData={setData}
          />
        </FormField>
        <div className="grid grid-cols-2 gap-4">
          <FormField title="Curso" required>
            <InputFieldDropdown
              placeholder="Curso"
              onChange={(value) => {
                setData({ ...data, course: value });
              }}
              name="course"
              value={data.course}
              options={Dropdown.courses}
            />
          </FormField>
          <FormField title="Asignatura" required>
            <InputFieldDropdown
              name={"subject"}
              value={data.subject}
              onChange={(value) => {
                setData({ ...data, subject: value });
              }}
              placeholder={"Asignatura"}
              options={Dropdown.subject}
            />
          </FormField>
        </div>
        <FormField title="Escala" required>
          <InputFieldDropdown
            name={"scale"}
            value={data.scale}
            onChange={(value) => {
              setData({ ...data, scale: value });
            }}
            placeholder={"Escala"}
            options={Dropdown.scale}
          />
        </FormField>
        <FormField title="País" required>
          <InputFieldDropdown
            name="pais"
            value={data.pais}
            onChange={(value) => {
              setData({ ...data, pais: value });
            }}
            placeholder="Selecciona un país"
            options={Dropdown.spanishSpeakingCountries}
          />
        </FormField>
        <FormField title="Contenido a evaluar" required>
          <TextArea
            name={"contentToEvaluate"}
            value={data.contentToEvaluate}
            onChange={handleChange}
            placeholder={"Análisis de la Revolución Industrial"}
            rows={3}
          />
        </FormField>
        <FormField title="Criterios de Evaluación a evaluar" required>
          <TextArea
            name={"criteriosEvaluacion"}
            value={data.criteriosEvaluacion}
            onChange={handleChange}
            placeholder={"Criterios de evaluación a evaluar"}
            rows={3}
          />
        </FormField>
        <FormField title="Ítems u otros aspectos a considerar en la rúbrica">
          <TextArea
            placeholder="Escribe aquí cualquier información adicional que te gustaría añadir para la realización de la rúbrica. Por ejemplo, si estás haciendo tenis, que incluya a evaluar todos los gestos técnicos y tácticos del tenis."
            onChange={handleChange}
            value={data.contenidoAdicional}
            name="contenidoAdicional"
            rows={3}
          />
        </FormField>
        <div className="flex justify-center gap-4 mt-8">
          <Button
            type="submit"
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={loading || isLoading}
          >
            {isLoading ? "Cargando..." : "Generar Rúbrica de Evaluación"}
          </Button>
          {prvResponse && (
            <Button
              type="button"
              className={"w-[20%]"}
              onClick={() => {
                navigate(RouteConstant.EVALUATION_RUBRICS_OUTPUT);
              }}
            >
              Generación Anterior
            </Button>
          )}
        </div>
      </form>
    </div>
  );
}

export default EvaluationRubricsText;
