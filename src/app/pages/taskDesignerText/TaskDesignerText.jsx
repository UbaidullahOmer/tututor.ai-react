import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { getLocalStorage } from "../../components/local_storage";
import { RouteConstant } from "../../constants/RouteConstant";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import InputField from "../../components/inputFiled/InputFiled";
import TextArea from "../../components/textArea/TextArea";
import Button from "../../components/button/Button";
import FileReaderInputFIled from "../../components/fileReaderInputFIled/FileReaderInputFIled";

const FormField = ({ title, children, required = false }) => (
  <div className="mb-4">
    <label className="block font-bold text-sm mb-2">
      {title} {required && <span className="text-red-500">*</span>}
    </label>
    {children}
  </div>
);

const InputStyles = "w-full p-2 border border-gray-300 rounded-md text-gray-700 placeholder-gray-500";

function TaskDesignerText() {
  const { handleHitAi } = useHandleHitAi({ ai: "chatgpt", toolName: "Diseñador de Tareas", });
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState({
    topic: "",
    course: "",
    subject: "",
    learningObjective: "",
    numeroTareas: "",
    tiempoTarea: "",
    numeroAlumnos: "",
    informacionAdicional: "",
    selectedFiles: [],
  });

  const prvResponse = getLocalStorage(RouteConstant.TASK_DESIGNER_TEXT);

  const handleChange = (e) => {
    setData((prevData) => ({ ...prevData, [e.target.name]: e.target.value }));
  };

  const handleDropdownChange = (name, value) => {
    setData((prevData) => ({ ...prevData, [name]: value }));
  };

  const validateForm = () => {
    const { topic, course, subject, learningObjective } = data;
    if (!topic) return "Please provide a prompt";
    if (!course) return "Please provide course";
    if (!subject) return "Please provide subject";
    if (!learningObjective) return "Please provide learning objective";
    return null;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const error = validateForm();
    if (error) {
      alert(error);
      return;
    }

    setIsLoading(true);
    const taskDesignGeneratorPrompt = `
Eres un experto en el diseño de tareas educativas y tu objetivo es crear ${Math.min(data.numeroTareas, 5)} tareas de aprendizaje extremadamente detalladas, lúdicas e innovadoras, basadas en la siguiente información proporcionada:

- **Tema:** ${data.topic}
- **Curso:** ${data.course}
- **Asignatura:** ${data.subject}
- **Objetivo de aprendizaje:** ${data.learningObjective}
- **Número de tareas a realizar:** ${Math.min(data.numeroTareas, 5)}
- **Tiempo de la tarea:** ${data.tiempoTarea} minutos
- **Número de alumnos:** ${data.numeroAlumnos}
- **Información Adicional:** ${data.informacionAdicional}

**Directrices clave:**
1. Alinea cada tarea con el objetivo de aprendizaje especificado.
2. Diseña actividades que sean lúdicas, fomentando la participación activa y el disfrute por parte de los estudiantes.
3. Asegúrate de que la tarea sea innovadora, capturando el interés de los estudiantes y promoviendo un aprendizaje significativo.
4. Proporciona una descripción extremadamente clara y detallada para que el profesor pueda llevar a cabo la tarea sin dificultades.

**Estructura de la tarea en HTML:**

<h1>Tarea #[Número de la tarea]</h1>

<h2>1. Título de la tarea</h2>
<p>[Proporciona un título claro y atractivo que capture la esencia lúdica de la tarea]</p>

<h2>2. Descripción</h2>
<p>[Explica de manera detallada y precisa cómo se llevará a cabo la tarea. Describe el propósito de la actividad, cómo se conecta con los objetivos de aprendizaje, y qué deben hacer los estudiantes. La descripción debe ser lo suficientemente completa para que el profesor entienda perfectamente cómo ejecutar la actividad.]</p>

<h2>3. Instrucciones detalladas</h2>
<p>[Proporciona una guía clara y paso a paso para que el profesor sepa exactamente qué hacer en cada momento de la tarea. Aquí es donde debes detallar cómo se organiza la actividad, qué acciones deben tomar los estudiantes, y cómo se desarrollará la dinámica del juego o ejercicio. Las instrucciones deben estar bien estructuradas, numeradas, y ser lo suficientemente específicas para que el profesor pueda seguirlas sin necesidad de hacer ajustes.]</p>

<h3>Ejemplo de estructura de las instrucciones detalladas:</h3>
<ol>
  <li><strong>Organización inicial:</strong> [Describe cómo deben organizarse los estudiantes, cómo se distribuyen los roles, o cómo se prepara el espacio de trabajo.]</li>
  
  <li><strong>Desarrollo de la actividad:</strong> [Detalla las acciones específicas que los estudiantes deben realizar, cómo deben interactuar entre ellos, y qué pasos deben seguir para completar la tarea.]</li>
  
  <li><strong>Cierre de la actividad:</strong> [Indica cómo finalizar la actividad, incluyendo cómo recopilar los resultados, reflexionar sobre lo aprendido y hacer una transición hacia la siguiente parte de la lección.]</li>
</ol>

<h2>4. Recursos necesarios</h2>
<ul>
  <li>[Recurso 1: Descripción y enlace si es aplicable]</li>
  <li>[Recurso 2: Descripción y enlace si es aplicable]</li>
  <li>[Recurso 3: Descripción y enlace si es aplicable]</li>
  [Recomienda solo materiales proporcionados por el profesor o materiales que se puedan encontrar fácilmente y de manera común. Evita incluir videos concretos o aplicaciones para móvil.]
</ul>

<h2>5. Sugerencias de adaptación</h2>
<ul>
  <li>[Sugerencia 1: Para estudiantes que necesiten un mayor desafío]</li>
  <li>[Sugerencia 2: Para estudiantes que necesiten apoyo adicional]</li>
</ul>

**Directrices adicionales:**
- Evita incluir imágenes en la generación de tareas.
- La respuesta debe estar en español, ser clara, concisa y bien estructurada.
- Genera el contenido en formato HTML para facilitar su uso.

Este prompt debe guiar al generador para crear tareas que no solo sean lúdicas e innovadoras, sino que también proporcionen al profesor una guía detallada y clara para que pueda implementarlas de manera efectiva y sin dificultades. La descripción e instrucciones deben ser lo suficientemente específicas para que el profesor pueda llevar a cabo la actividad de manera exitosa y sin necesidad de ajustes adicionales.
`;




    try {
      await handleHitAi(taskDesignGeneratorPrompt, RouteConstant.TASK_DESIGNER_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px] max-h-[80vh] overflow-scroll">
      <AiToolPageHeader
        title="Diseñador de Tareas de Aprendizaje"
        description="¿Tienes dudas sobre cómo trabajar un contenido concreto o practicarlo? Con esta herramienta puedes generar actividades, ejercicios, o lo que desees, adaptados a tus necesidades específicas. Además, simplifica el proceso de planificación, ahorrándote tiempo y esfuerzo mientras aseguras una experiencia de aprendizaje enriquecedora para tus estudiantes."
        icon="ri-edit-2-line text-xl text-blue-600"
      />
      <form onSubmit={handleSubmit} className="flex flex-col gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <div className="col-span-2">
          <FormField title="Sube tu temario para diseñar tareas o escribe sobre lo que quieres generar tareas de aprendizaje">
            <FileReaderInputFIled
              onChange={handleChange}
              data={data}
              setData={setData}
            />
          </FormField>
        </div>
        <div className="grid grid-cols-2 gap-4">
          <FormField title="Curso" required>
            <InputFieldDropdown
              placeholder="Curso"
              onChange={(value) => handleDropdownChange("course", value)}
              name="course"
              type="number"
              value={data.course}
              options={Dropdown.courses}
            />
          </FormField>
          <FormField title="Asignatura" required>
            <InputFieldDropdown
              name="subject"
              value={data.subject}
              onChange={(value) => handleDropdownChange("subject", value)}
              placeholder="Asignatura"
              options={Dropdown.subject}
            />
          </FormField>
        </div>
        <FormField title="Tiempo de la Tarea">
          <InputField
            name="tiempoTarea"
            value={data.tiempoTarea}
            onChange={handleChange}
            placeholder="Tiempo de duración de la Tarea en minutos"
          />
        </FormField>
        <FormField title="Número de Tareas." required>
          <InputField
            name="numeroTareas"
            value={data.numeroTareas}
            onChange={handleChange}
            placeholder="Número de tareas a generar. Máximo 5"
            type="number"
          />
        </FormField>
        <FormField title="Número de Alumnos" required>
          <InputField
            name="numeroAlumnos"
            value={data.numeroAlumnos}
            onChange={handleChange}
            placeholder="Escribe el número de alumnos con el que vas a trabajar"
            type="number"
          />
        </FormField>
        <FormField title="Objetivos de Aprendizaje" required>
          <InputField
            name="learningObjective"
            value={data.learningObjective}
            onChange={handleChange}
            placeholder="Analizar las causas, los eventos clave y las consecuencias de la Segunda Guerra Mundial"
          />
        </FormField>
        <FormField title="Contenido a tratar" required>
          <TextArea
            name="topic"
            value={data.topic}
            onChange={handleChange}
            placeholder="Eventos principales de la Segunda Guerra Mundial"
            rows={4}
          />
        </FormField>
        <FormField title="Información Adicional">
          <TextArea
            name="informacionAdicional"
            value={data.informacionAdicional}
            onChange={handleChange}
            placeholder="Ej: Si quieres hacer grupos, de cuántos. Si quieres que sea individual, si quieres que sea una actividad innovadora, etc. Cuanta más información proporciones, mejor será la tarea diseñada."
            rows={4}
          />
        </FormField>
        <div className="flex justify-center gap-4 mt-8">
          <Button type="submit" className={prvResponse ? "w-[80%]" : "w-full"} disabled={isLoading}>
            {isLoading ? "Cargando..." : "Generar Tareas"}
          </Button>
          {prvResponse && (
            <Button
              className="w-[20%]"
              onClick={() => navigate(RouteConstant.TASK_FEEDBACK_OUTPUT)}
              type="button"
            >
              Previous
            </Button>
          )}
        </div>
      </form>
    </div>
  );
}

export default TaskDesignerText;
