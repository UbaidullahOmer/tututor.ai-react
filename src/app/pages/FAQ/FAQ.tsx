import React, { useState, useEffect } from 'react';
import { ChevronDown, Search } from 'lucide-react';

interface FAQItem {
  question: string;
  answer: string;
}

const faqData: FAQItem[] = [
  {
    question: "¿Qué es TUTUTOR.AI?",
    answer: "TUTUTOR.AI es una plataforma integral de SaaS que ofrece una amplia gama de herramientas y servicios para educadores y estudiantes. Nuestra plataforma está diseñada para mejorar la experiencia de enseñanza y aprendizaje mediante funciones impulsadas por IA y recursos educativos innovadores."
  },
  {
    question: "¿Quién puede beneficiarse de usar TUTUTOR.AI?",
    answer: "Nuestra plataforma está diseñada tanto para educadores como para estudiantes. Los profesores pueden usar nuestras herramientas para crear contenido atractivo, planificar lecciones y evaluar el progreso de los estudiantes. Los estudiantes pueden beneficiarse de experiencias de aprendizaje personalizadas, ayudas de estudio y recursos interactivos."
  },
  {
    question: "¿Qué tipo de herramientas están disponibles en TUTUTOR.AI?",
    answer: "Ofrecemos un conjunto muy diverso de herramientas especialmente diseñadas y creadas para las necesidades de educadores y estudiantes.  Nuestro objetivo es proporcionar un conjunto completo de recursos para la educación moderna y personalizada."
  },
  {
    question: "¿Cómo funciona el generador de contenido con IA?",
    answer: "Nuestro generador de contenido con IA utiliza los modelos de lenguaje más avanzados actualmente para crear materiales educativos adaptados a tus necesidades. Simplemente proporciona un tema o concepto, rellenas los campos y la herramienta generará el contenido completamente personalizado y adaptado a tus necesidades."
  },
  {
    question: "¿Es TUTUTOR.AI adecuado para todos los niveles educativos?",
    answer: "Sí, TUTUTOR.AI está diseñado para atender a TODOS los niveles educativos, desde la educación primaria hasta la educación superior y universidad. Todas nuestras herramientas son personalizables para adaptarse a diferentes grupos de edad y complejidades de los temas."
  },
  {
    question: "¿Cómo puedo comenzar a usar TUTUTOR.AI?",
    answer: "Para comenzar, simplemente regístrate para obtener una cuenta en nuestro sitio web. Ofrecemos diferentes niveles de suscripción para adaptarse a diversas necesidades y presupuestos. Una vez registrado, tendrás acceso a todo nuestro conjunto de herramientas y recursos."
  },
  {
    question: "¿Ofrecen algún tipo de capacitación o soporte para nuevos usuarios?",
    answer: "¡Por supuesto! Ofrecemos tutoriales completos de todas las herramientas. Consulta la sección de Academia, donde explicamos todas y cada una de las herramientas. También ofrecemos seminarios web y un equipo de soporte dedicado para ayudarte a sacar el máximo provecho de nuestra plataforma. Próximamente lanzaremos nuestro canal de Discord donde los usuarios podrán compartir consejos y mejores prácticas."
  },
  {
    question: "¿Puedo integrar TUTUTOR.AI con otras plataformas educativas?",
    answer: "Sí, ofrecemos capacidades de integración con sistemas de gestión del aprendizaje y herramientas educativas populares. Esto permite una incorporación fluida de TUTUTOR.AI en tu ecosistema educativo existente."
  },
  {
    question: "¿Con qué frecuencia actualizan sus herramientas y funciones?",
    answer: "Estamos constantemente mejorando y ampliando nuestra plataforma. Lanzamos actualizaciones y nuevas herramientas regularmente, basadas en los comentarios de los usuarios y los avances en tecnología educativa. Nuestros usuarios siempre tienen acceso a las últimas innovaciones."
  },
  {
    question: "¿Es segura mi información en TUTUTOR.AI?",
    answer: "Nos tomamos muy en serio la seguridad de los datos y la privacidad. Todos los datos de los usuarios están encriptados y se almacenan de manera segura. Cumplimos con las regulaciones de protección de datos educativos y nunca compartimos información personal con terceros sin el consentimiento explícito."
  }
];

const FAQComponent: React.FC = () => {
  const [openIndex, setOpenIndex] = useState<number | null>(null);
  const [searchTerm, setSearchTerm] = useState('');
  const [filteredFAQ, setFilteredFAQ] = useState(faqData);

  useEffect(() => {
    const filtered = faqData.filter(item =>
      item.question.toLowerCase().includes(searchTerm.toLowerCase())
    );
    setFilteredFAQ(filtered);
  }, [searchTerm]);

  const toggleQuestion = (index: number) => {
    setOpenIndex(openIndex === index ? null : index);
  };

  return (
    <div className="w-full bg-gradient-to-b from-blue-50 to-white h-screen overflow-y-auto">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 py-16">
        <h2 className="text-4xl font-extrabold text-center mb-12 text-blue-600">
          Preguntas Frecuentes
        </h2>
        
        <div className="mb-10">
          <div className="relative max-w-xl mx-auto">
            <input
              type="text"
              placeholder="Buscar preguntas..."
              className="w-full p-4 pl-12 pr-4 text-gray-900 border border-gray-300 rounded-full focus:outline-none focus:ring-2 focus:ring-blue-500 shadow-sm transition duration-300"
              value={searchTerm}
              onChange={(e) => setSearchTerm(e.target.value)}
            />
            <Search className="absolute left-4 top-4 text-gray-400" size={20} />
          </div>
        </div>
        
        <div className="space-y-4">
          {filteredFAQ.map((item, index) => (
            <div 
              key={index} 
              className="border border-gray-200 rounded-lg overflow-hidden shadow-sm hover:shadow-md transition-all duration-300"
            >
              <button
                className="flex justify-between items-center w-full p-5 text-left bg-white hover:bg-gray-50 transition-colors duration-200"
                onClick={() => toggleQuestion(index)}
              >
                <span className="text-lg font-medium text-gray-800">{item.question}</span>
                <ChevronDown
                  className={`transform transition-transform duration-300 ${
                    openIndex === index ? 'rotate-180 text-blue-500' : 'text-gray-400'
                  }`}
                  size={20}
                />
              </button>
              <div 
                className={`transition-max-height duration-300 ease-in-out overflow-hidden ${
                  openIndex === index ? 'max-h-96' : 'max-h-0'
                }`}
              >
                <div className="p-5 bg-gray-50 border-t border-gray-200">
                  <p className="text-gray-700">{item.answer}</p>
                </div>
              </div>
            </div>
          ))}
        </div>

        {filteredFAQ.length === 0 && (
          <p className="text-center text-gray-500 mt-8 animate-fade-in">
            Pregunta no encontrada. Busca otra pregunta.
          </p>
        )}
      </div>
    </div>
  );
};

export default FAQComponent;