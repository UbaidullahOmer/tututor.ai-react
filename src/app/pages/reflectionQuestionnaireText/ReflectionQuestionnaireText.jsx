import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";

function ReflectionQuestionnaireText() {
  const { handleHitAi, loading } = useHandleHitAi({ ai: "chatgpt", toolName: "Cuestionario de reflexión para estudiantes conflictivos" });
  const [data, setData] = useState({
    course: "",
    name: "",
    behaviors: "",
  });

  const prvResponse = getLocalStorage(
    RouteConstant.REFLECTION_QUESTIONNAIRE_OUTPUT
  );
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!data.course || !data.name || !data.behaviors) {
      alert("Por favor, rellene todos los campos");
      return;
    }
    const behaviorReflectionQuestionnairePrompt = `
Como experto en psicología educativa y desarrollo personal, genera un cuestionario de reflexión sobre el comportamiento basado en:
<ul>
  <li>Curso: ${data.course}</li>
  <li>Nombre del estudiante: ${data.name}</li>
  <li>Comportamientos a reflexionar: ${data.behaviors}</li>
</ul>

<h3>Instrucciones para el Cuestionario de Reflexión:</h3>

<ol>
  <li>Encabezado del Cuestionario:
    <h2>Cuestionario de Reflexión sobre el Comportamiento</h2>
    <p><strong>Estudiante:</strong> ${data.name}</p>
    <p><strong>Curso:</strong> ${data.course}</p>
    <p><strong>Fecha:</strong> [Insertar fecha actual]</p>
  </li>
  
  <li>Introducción:
    <p>Escribe un párrafo breve que explique el propósito del cuestionario y anime al estudiante a responder honestamente para fomentar su crecimiento personal.</p>
  </li>
  
  <li>Preguntas de Reflexión:
    <p>Para cada comportamiento mencionado en ${data.behaviors}, crea 3-4 preguntas de reflexión. Incluye una mezcla de los siguientes tipos de preguntas:</p>
    <ul>
      <li>Preguntas de autoevaluación</li>
      <li>Preguntas sobre las causas del comportamiento</li>
      <li>Preguntas sobre el impacto del comportamiento en uno mismo y en los demás</li>
      <li>Preguntas sobre posibles alternativas o mejoras</li>
    </ul>
    <h3>Reflexión sobre [Comportamiento específico]</h3>
    <table class="reflection-table">
      <thead>
        <tr>
          <th>Pregunta</th>
          <th>Respuesta</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>[Pregunta de autoevaluación]</td>
          <td></td>
        </tr>
        <tr>
          <td>[Pregunta sobre las causas]</td>
          <td></td>
        </tr>
        <tr>
          <td>[Pregunta sobre el impacto]</td>
          <td></td>
        </tr>
        <tr>
          <td>[Pregunta sobre alternativas o mejoras]</td>
          <td></td>
        </tr>
      </tbody>
    </table>
  </li>
  
  <li>Escala de Autoevaluación:
    <p>Después de cada conjunto de preguntas, incluye una escala de autoevaluación:</p>
    <p>En una escala del 1 al 5, donde 1 es "Necesito mejorar mucho" y 5 es "Estoy muy satisfecho", ¿cómo calificarías tu comportamiento en esta área?</p>
    <p>1 [ ] 2 [ ] 3 [ ] 4 [ ] 5 [ ]</p>
  </li>
  
  <li>Preguntas Generales de Cierre:
    <h3>Reflexión General</h3>
    <ol>
      <li>¿Qué has aprendido sobre ti mismo al reflexionar sobre estos comportamientos?</li>
      <li>¿Qué áreas crees que son tus mayores fortalezas en términos de comportamiento?</li>
      <li>¿En qué áreas sientes que tienes más oportunidades de crecimiento?</li>
      <li>¿Qué pasos concretos puedes dar para mejorar en las áreas que lo necesitan?</li>
      <li>¿Cómo crees que estos cambios en tu comportamiento podrían beneficiarte en tu vida académica y personal?</li>
    </ol>
  </li>
  
  <li>Conclusión:
    <p>Escribe un párrafo final que anime al estudiante a usar estas reflexiones como una herramienta para el crecimiento personal y el desarrollo de habilidades sociales y emocionales.</p>
  </li>
</ol>

<p>Responde en español, asegurándote de que las preguntas sean apropiadas para la edad y el nivel de madurez correspondiente al curso ${data.course}. Usa un tono respetuoso, no acusatorio y que fomente la autorreflexión honesta. El objetivo es ayudar al estudiante a desarrollar una mayor conciencia de su comportamiento y motivarlo a hacer cambios positivos.</p>
The result must always be in this format: HTML
`;
    console.log(behaviorReflectionQuestionnairePrompt);
    setIsLoading(true);
    try {
      await handleHitAi(
        behaviorReflectionQuestionnairePrompt,
        RouteConstant.REFLECTION_QUESTIONNAIRE_OUTPUT
      );
      navigate(RouteConstant.REFLECTION_QUESTIONNAIRE_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      alert("Ocurrió un error al generar el cuestionario de reflexión. Por favor, inténtalo de nuevo.");
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Cuestionario de Reflexión"
        description="Genera cuestionarios de reflexión en segundos para ayudar a los estudiantes a reflexionar sobre sus comportamientos y acciones"
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <InputFieldDropdown
          placeholder="Curso"
          onChange={(value) => {
            setData({ ...data, course: value });
          }}
          name="course"
          type="number"
          value={data.course}
          options={Dropdown.courses}
          require={true}
          label="Curso"
        />
        <InputField
          name="name"
          value={data.name}
          onChange={handleChange}
          placeholder="Nombre del estudiante"
          require={true}
          label="Nombre"
        />
        <TextArea
          name="behaviors"
          value={data.behaviors}
          onChange={handleChange}
          placeholder="Ej: Le pegó a un niño estando en el patio; Ej: Fue extremadamente grosero con sus compañeros de clase y les amenazó, etc."
          require={true}
          label="Comportamientos sobre el que reflexionar"
          className="col-span-2"
        />
        <div className="col-span-2 flex align-center gap-2">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={loading || isLoading}
          >
            {isLoading ? "Generando..." : "Generar"}
          </Button>
          {prvResponse && (
            <Button
              className="w-[20%]"
              onClick={() => {
                navigate(RouteConstant.REFLECTION_QUESTIONNAIRE_OUTPUT);
              }}
              disabled={loading || isLoading}
            >
              Anterior
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default ReflectionQuestionnaireText;