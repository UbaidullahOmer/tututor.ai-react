import React, { useState } from "react";
import Button from "../../components/button/Button";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import InputFiledDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import { getLocalStorage } from "../../components/local_storage";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import FileReaderInputFIled from "../../components/fileReaderInputFIled/FileReaderInputFIled";

function QuizGenerator() {
  const [data, setData] = useState({
    selectedFiles: [],
    prompt: "",
    numberOfQuiz: "",
    difficultyLevel: "",
    quizType: "",
    courseDifficulty: "",
  });
  const { handleHitAi, loading } = useHandleHitAi({
    ai: "chatgpt",
    selectedFiles: data.selectedFiles,
    toolName: "Generador de cuestionarios tipo test",
  });
  const prvOnlineTest = getLocalStorage(RouteConstant.TAKE_ONLINE_TEST);
  const navigate = useNavigate();

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleGiveTestOnline = async (e) => {
    e.preventDefault();
    if (!data.prompt && data.selectedFiles.length === 0) {
      alert("Please provide a prompt or upload at least one file");
      return;
    } else if (!data.numberOfQuiz) {
      alert("Please provide number of quiz");
      return;
    } else if (!data.difficultyLevel) {
      alert("Please provide difficulty level");
      return;
    } else if (!data.quizType) {
      alert("Please provide quiz type");
      return;
    }

    let aiPrompt = `You are a quiz master. 
    Generate ${data.numberOfQuiz} random questions answers from the prompt given below. 
    Also provide the correct answer. 
    The Level of difficulty should be ${data.difficultyLevel}. 
    The quiz Type should be ${data.quizType}.
    The difficulty also has to adapt to this age level: ${data.courseDifficulty}. This is OPTIONAL just in case the user fills it in.
    The quiz must always be in Spanish.
    It is incredibly important that the number of questions are the ones asked by the user, which are this: ${data.numberOfQuiz}.
    The response should be in this format: {heading: "heading", questions: [{ question: "question", options: ["option1", "option2", "option3", "option4"], correctAnswer: "correctAnswer"}]}. Just return json
    `;

    if (data.selectedFiles.length > 0) {
      const imageFiles = data.selectedFiles.filter((file) =>
        file.type.startsWith("image/")
      );
      if (imageFiles.length > 0) {
        aiPrompt +=
          "\nPlease analyze the uploaded images and include relevant questions based on their content. Always give the exact amount of questions that the user asked for";
      }
    }

    aiPrompt += `\nPrompt: ${data.prompt}`;

    try {
      await handleHitAi(
        aiPrompt,
        RouteConstant.TAKE_ONLINE_TEST,
        "/share-online-test",
        data.selectedFiles
      );
    } catch (error) {
      console.error("Error during API call:", error);
    }
  };
  return (
    <>
      <div className="flex flex-col gap-[24px] p-[24px]">
        <AiToolPageHeader
          title="Generador de Exámenes Tipo Test"
          description="Con esta herramientas podrás generar exámenes tipo test de muchas maneras, con 3 posibles preguntas, 4, 5, verdadero o falso, o incluso exámenes de rellenar los huecos"
          icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
        />
        <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
          <FileReaderInputFIled
            onChange={handleChange}
            data={data}
            setData={setData}
          /> 
          <InputFiledDropdown
            name={"numberOfQuiz"}
            value={data.numberOfQuiz}
            onChange={(value) => {
              setData({ ...data, numberOfQuiz: value });
            }}
            placeholder="Después de 30 preguntas, la IA reduce la cantidad. Si quieres 40, pide 50 y así sucesivamente."
            options={["10", "20", "30", "40", "50"]}
            label={"Seleciona el número de preguntas"}
          />
          <InputFiledDropdown
            name={"difficultyLevel"}
            value={data.difficultyLevel}
            onChange={(value) => {
              setData({ ...data, difficultyLevel: value });
            }}
            placeholder="Aquí podrás seleccionar si tu test quieres que sea fácil, normal o difícil."
            options={[
              "Muy fácil",
              "Fácil",
              "Moderado",
              "Difícil",
              "Muy difícil",
            ]}
            label={"Dificultad del test"}
          />
          <InputFiledDropdown
            name={"courseDifficulty"}
            value={data.courseDifficulty}
            onChange={(value) => {
              setData({ ...data, courseDifficulty: value });
            }}
            placeholder="Esta opción la puedes usar si quieres también adaptar el nivel a un curso académico concreto"
            label={"Dificultad por Curso. OPCIONAL"}
            options={[
              "5-6 años",
              "7-8 años",
              "9-10 años",
              "11-12 años",
              "13-14 años",
              "15-16 años",
              "17-18 años",
              "Universidad",
              "Otro (escribe cual)"
            ]}
            require={false}
          />
          <InputFiledDropdown
            name={"quizType"}
            value={data.quizType}
            onChange={(value) => {
              setData({ ...data, quizType: value });
            }}
            placeholder="3 posibles opciones, 4 posibles opciones, etc"
            options={[
              "3 Opciones",
              "4 Opciones",
              "5 Opciones",
              "Rellena los huecos",
              "Verdadero/Falso",
            ]}
            label={"Tipo de Cuestionario"}
          />
          <div className="col-span-2 flex align-center gap-2 ">
            <Button
              onClick={handleGiveTestOnline}
              className={prvOnlineTest ? "w-[80%]" : "w-[100%]"}
            >
              {loading ? "Cargando..." : "Generar test"}
            </Button>
            {prvOnlineTest && (
              <Button
                className={"w-[20%]"}
                onClick={() => {
                  navigate(RouteConstant.TAKE_ONLINE_TEST);
                }}
              >
                Ver test anterior
              </Button>
            )}
          </div>
        </div>
      </div>
    </>
  );
}

export default QuizGenerator;
