import React, { useRef, useState } from "react";
import useOutsideClick from "../../hooks/useOutsideClick";
import './styles.css'; // Asegúrate de importar el archivo CSS


const QuizQuestion = ({
  question,
  options,
  selectedAnswer,
  correctAnswer,
  onAnswerSelect,
  showAnswersImmediately,
  isQuizCompleted,
  onEdit,
  onDelete,
  onRegenerate,
}) => {
  const [openDropdown, setOpenDropdown] = useState(false);
  const quizActionDropdownRef = useRef();

  const toggleDropdown = () => {
    setOpenDropdown(!openDropdown);
  };

  useOutsideClick(quizActionDropdownRef, () => {
    setOpenDropdown(false);
  });

  return (
    <div className="flex flex-col items-center gap-[20px] w-full">
      <span className="flex items-center justify-between px-[16px] py-[28px] bg-[#CAE5FF] text-[#2B3D70] rounded-[12px] border-[#4EA6FE] border-[1px] w-full">
        <span>{question}</span>
        <div className="relative">
          <button
            onClick={toggleDropdown}
            className="text-gray-600 hover:text-gray-800"
          >
            <i className="ri-more-2-fill"></i>
          </button>
          {openDropdown && (
            <div
            ref={quizActionDropdownRef}
            className="absolute right-0 mt-2 w-48 bg-white rounded-md shadow-lg z-10 dropdown-animation"
          >
            <button
              onClick={() => {
                onRegenerate();
                setOpenDropdown(false);
              }}
              className="block px-4 py-2 text-sm text-[#4EA7FF] hover:bg-gray-100 w-full text-left"
            >
              Re-generar regunta por otra
            </button>
            <button
              onClick={() => {
                onEdit();
                setOpenDropdown(false);
              }}
              className="block px-4 py-2 text-sm text-[#4EA7FF] hover:bg-gray-100 w-full text-left"
            >
              Editar Pregunta
            </button>
            <button
              onClick={() => {
                onDelete();
                setOpenDropdown(false);
              }}
              className="block px-4 py-2 text-sm text-[#4EA7FF] hover:bg-gray-100 w-full text-left"
            >
              Eliminar Pregunta
            </button>
          </div>
          
          )}
        </div>
      </span>

      <div className="flex flex-col w-full px-[48px] items-center gap-[16px]">
        {options.map((option, index) => {
          const isSelected = option === selectedAnswer;
          const isCorrectAnswer = option === correctAnswer;

          const optionClassName = isQuizCompleted
            ? isCorrectAnswer
              ? "bg-[#E8FFF9] border-[#02E8AC] text-[#02E8AC]"
              : isSelected && !isCorrectAnswer
              ? "bg-[#FFEEEC] border-[#FF6F61] text-[#FF6F61]"
              : "bg-[#FFF] border-[#F3F3F3] text-[#2B3D70]"
            : showAnswersImmediately && isSelected
            ? isCorrectAnswer
              ? "bg-[#E8FFF9] border-[#02E8AC] text-[#02E8AC]"
              : "bg-[#FFEEEC] border-[#FF6F61] text-[#FF6F61]"
            : isSelected
            ? "bg-[#CAE5FF] border-[#4EA7FF] text-[#4EA7FF]"
            : "bg-[#FFF] border-[#F3F3F3] text-[#2B3D70]";

          return (
            <span
              key={index}
              className={`flex items-start px-[16px] py-[28px] text-[#2B3D70] rounded-[12px] border-[1px] w-full cursor-pointer ${optionClassName}`}
              onClick={() => !isQuizCompleted && onAnswerSelect(option)}
            >
              {String.fromCharCode(65 + index)}. {option}
            </span>
          );
        })}
      </div>
    </div>
  );
};

export default QuizQuestion;
