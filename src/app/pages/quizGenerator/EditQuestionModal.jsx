import React, { useState } from "react";
import './stylesss.css'; // Asegúrate de importar el archivo CSS

const EditQuestionModal = ({ question, onSave, onClose }) => {
  const [editedQuestion, setEditedQuestion] = useState(question);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setEditedQuestion({ ...editedQuestion, [name]: value });
  };

  const handleOptionChange = (index, value) => {
    const newOptions = [...editedQuestion.options];
    newOptions[index] = value;
    setEditedQuestion({ ...editedQuestion, options: newOptions });
  };

  return (
    <div className="modal-overlay">
      <div className="modal-content">
        <h2 className="modal-title">Editar Pregunta</h2>
        
        <label className="label">PREGUNTA:</label>
        <input
          type="text"
          name="question"
          value={editedQuestion.question}
          onChange={handleInputChange}
          className="input-field"
          placeholder="Escribe tu pregunta aquí"
        />
        
        <label className="label">POSIBLES RESPUESTAS:</label>
        {editedQuestion.options.map((option, index) => (
          <div key={index} className="option-container">
            <span className="option-label">{String.fromCharCode(65 + index)})</span>
            <input
              type="text"
              value={option}
              onChange={(e) => handleOptionChange(index, e.target.value)}
              className="input-field"
              placeholder={`Respuesta ${index + 1}`}
            />
          </div>
        ))}
        
        <label className="label">RESPUESTA CORRECTA:</label>
        <input
          type="text"
          name="correctAnswer"
          value={editedQuestion.correctAnswer}
          onChange={handleInputChange}
          className="input-field"
          placeholder="Escribe la respuesta correcta"
        />
        
        <div className="flex justify-end mt-4">
          <button
            onClick={() => onSave(editedQuestion)}
            className="button button-save"
          >
            GUARDAR
          </button>
          <button onClick={onClose} className="button button-cancel">
            CANCELAR
          </button>
        </div>
      </div>
    </div>
  );
};

export default EditQuestionModal;