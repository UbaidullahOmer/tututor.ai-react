import React, { useEffect, useRef, useState } from "react";
import { useLocation } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import useOutsideClick from "../../hooks/useOutsideClick";
import { PdfFileIcon } from "../../icons/Icons";
import Button from "../../components/button/Button";
import jsPDF from "jspdf";
import QuizQuestion from "./QuizQuestion";
import EditQuestionModal from "./EditQuestionModal";
import useHandleHitAi from "/src/app/hooks/useHandleHitAi";
import useFirebaseData from "../../hooks/useFirebaseData";
import './styles.css';

const GridViewIcon = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect x="3" y="3" width="7" height="7" rx="1" fill="#4EA7FF" />
    <rect x="14" y="3" width="7" height="7" rx="1" fill="#4EA7FF" />
    <rect x="3" y="14" width="7" height="7" rx="1" fill="#4EA7FF" />
    <rect x="14" y="14" width="7" height="7" rx="1" fill="#4EA7FF" />
  </svg>
);

const ListViewIcon = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect x="3" y="6" width="18" height="2" rx="1" fill="#4EA7FF" />
    <rect x="3" y="11" width="18" height="2" rx="1" fill="#4EA7FF" />
    <rect x="3" y="16" width="18" height="2" rx="1" fill="#4EA7FF" />
  </svg>
);

const StopwatchIcon = ({ time, isRunning }) => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    style={{ color: isRunning ? "#4EA7FF" : "#000" }}
  >
    {!isRunning ? (
      <>
        <circle cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="2" />
        <path
          d="M12 6V12L15 15"
          stroke="currentColor"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M9 2H15"
          stroke="currentColor"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </>
    ) : (
      <text
        x="12"
        y="15"
        textAnchor="middle"
        fontSize="10"
        fontWeight="bold"
        fill="currentColor"
      >
        {time}
      </text>
    )}
  </svg>
);

const Tooltip = ({ text, children }) => {
  const [show, setShow] = useState(false);

  return (
    <div
      className="relative inline-block"
      onMouseEnter={() => setShow(true)}
      onMouseLeave={() => setShow(false)}
    >
      {children}
      {show && (
        <div className="absolute bottom-full left-1/2 transform -translate-x-1/2 mb-2">
          <div className="relative bg-black bg-opacity-75 text-white text-xs rounded-md px-3 py-2 shadow-lg whitespace-nowrap">
            <div className="absolute bottom-[-6px] left-1/2 transform -translate-x-1/2 w-0 h-0 border-l-4 border-l-transparent border-r-4 border-r-transparent border-t-4 border-t-black" />
            {text}
          </div>
        </div>
      )}
    </div>
  );
};

const TakeOnlineTest = () => {
  const [quizData, setQuizData] = useState(null);
  const { handleHitAi } = useHandleHitAi({
    ai: "chatgpt",
    toolName: "Generador de cuestionarios tipo test",
  });

  const { addData, fetchData } = useFirebaseData();
  const [originalQuizData, setOriginalQuizData] = useState(null);
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const [selectedAnswers, setSelectedAnswers] = useState([]);
  const [isAllView, setIsAllView] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [isQuizCompleted, setIsQuizCompleted] = useState(false);
  const [score, setScore] = useState(0);
  const [openDropdown, setOpenDropdown] = useState(null);
  const [editingQuestion, setEditingQuestion] = useState(null);
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [showAnswersImmediately, setShowAnswersImmediately] = useState(true);
  const [time, setTime] = useState(0);
  const [isRunning, setIsRunning] = useState(false);
  const [showDifficultyDropdown, setShowDifficultyDropdown] = useState(false);
  const [showShuffleDropdown, setShowShuffleDropdown] = useState(false);
  const quizActionDropdownRef = useRef();
  const location = useLocation();
  const timerRef = useRef(null);

  useOutsideClick(quizActionDropdownRef, () => {
    setOpenDropdown(false);
  });

  const cleanData = (data) => {
    if (typeof data === "string") {
      const firstBraceIndex = data.indexOf("{");
      const lastBraceIndex = data.lastIndexOf("}");
      if (
        firstBraceIndex !== -1 &&
        lastBraceIndex !== -1 &&
        lastBraceIndex > firstBraceIndex
      ) {
        return data.slice(firstBraceIndex, lastBraceIndex + 1);
      }
    }
    return data;
  };

  useEffect(() => {
    let testData;
    try {
      const cleanedStateData = cleanData(location.state);
      const cleanedStorageData = cleanData(
        localStorage.getItem(RouteConstant.TAKE_ONLINE_TEST)
      );
      testData = JSON.parse(cleanedStateData || cleanedStorageData);
      if (testData && Array.isArray(testData.questions)) {
        setQuizData(testData);
        setOriginalQuizData(testData);
        setSelectedAnswers(new Array(testData.questions.length).fill(null));
      } else {
        throw new Error("Invalid quiz data structure");
      }
    } catch (error) {
      console.error("Error parsing quiz data:", error);
      setQuizData({
        heading: "Default Quiz",
        questions: [
          {
            question: "Default question",
            options: ["Option 1", "Option 2", "Option 3", "Option 4"],
            correctAnswer: "Option 1",
          },
        ],
      });
      setSelectedAnswers([null]);
    }
    setIsLoading(false);
  }, [location.state]);

  const currentQuestion = quizData?.questions[currentQuestionIndex] || null;

  const handleAnswerSelect = (answer, questionIndex) => {
    const newAnswers = [...selectedAnswers];
    newAnswers[questionIndex] = answer;
    setSelectedAnswers(newAnswers);
  };

  const handleContinue = () => {
    if (currentQuestionIndex < quizData.questions.length - 1) {
      setCurrentQuestionIndex((prevIndex) => prevIndex + 1);
    } else {
      const finalScore = selectedAnswers.reduce((acc, answer, index) => {
        return (
          acc + (answer === quizData.questions[index].correctAnswer ? 1 : 0)
        );
      }, 0);
      setScore(finalScore);
      setIsQuizCompleted(true);
    }
  };

  const handleSkip = handleContinue;

  const shuffleArray = (array) => {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  };

  const handleRestart = (shuffle) => {
    let newQuizData = { ...quizData };
    if (shuffle) {
      newQuizData.questions = shuffleArray(newQuizData.questions);
      newQuizData.questions.forEach((question) => {
        question.options = shuffleArray(question.options);
      });
    }
    setQuizData(newQuizData);
    setCurrentQuestionIndex(0);
    setSelectedAnswers(new Array(newQuizData.questions.length).fill(null));
    setIsQuizCompleted(false);
    setScore(0);
    setShowShuffleDropdown(false); // Cierra el dropdown después de reiniciar
  };

  const cleanAndParseJSON = (str) => {
    const cleaned = str.replace(/^```json\s*/, "").replace(/\s*```$/, "").trim();
    try {
      return JSON.parse(cleaned);
    } catch (error) {
      console.error("Error parsing JSON:", error);
      throw new Error("Invalid JSON structure in AI response");
    }
  };

  const saveQuizToHistory = async (quizData) => {
    try {
      const newItem = {
        toolName: quizData.heading,
        aiResponse: JSON.stringify(quizData),
        timestamp: Date.now(),
        url: window.location.href,
      };
      await addData("ai-prompt-history", newItem);
      await fetchData("ai-prompt-history");
    } catch (error) {
      console.error("Error al guardar el quiz en el historial:", error);
    }
  };

  const generateNewQuiz = async (difficulty) => {
    if (!originalQuizData) return;

    const difficultyTextMap = {
      misma: "the same",
      un_poco_mas_facil: "slightly easier",
      poco_mas_dificil: "slightly more difficult",
      mas_dificil: "more difficult",
      bastante_mas_dificil: "significantly more difficult",
    };

    const aiPrompt = `You are a quiz master. 
      Generate ${originalQuizData.questions.length} new random questions and answers based on the original quiz topic: "${originalQuizData.heading}". 
      The difficulty level should be ${difficultyTextMap[difficulty]} and the quiz type should be the same as the original quiz.
      The quiz must always be in Spanish.
      It is incredibly important that the number of questions is exactly ${originalQuizData.questions.length}.
      The response should be in this format: {heading: "${originalQuizData.heading}", questions: [{ question: "question", options: ["option1", "option2", "option3", "option4"], correctAnswer: "correctAnswer"}]}. Return only the JSON without any additional text or markdown.`;

    try {
      setIsLoading(true);
      const newQuizContent = await handleHitAi(aiPrompt, null, "/share-online-test");

      if (newQuizContent) {
        const parsedNewQuizData = cleanAndParseJSON(newQuizContent);

        if (
          parsedNewQuizData &&
          Array.isArray(parsedNewQuizData.questions) &&
          parsedNewQuizData.questions.length > 0
        ) {
          setQuizData(parsedNewQuizData);
          setSelectedAnswers(new Array(parsedNewQuizData.questions.length).fill(null));
          setCurrentQuestionIndex(0);
          setIsQuizCompleted(false);
          setScore(0);

          await saveQuizToHistory(parsedNewQuizData); // Guarda el nuevo quiz en el historial
        } else {
          throw new Error("Invalid quiz data structure received from AI");
        }
      } else {
        throw new Error("No quiz data received from AI");
      }
    } catch (error) {
      console.error("Error generando nuevas preguntas:", error);
      alert("Hubo un error al generar nuevas preguntas. Por favor, intente de nuevo.");
    } finally {
      setIsLoading(false);
      setShowDifficultyDropdown(false); // Oculta el dropdown después de generar el quiz
    }
  };

  const regenerateQuestion = async (questionIndex) => {
    const aiPrompt = `You are a quiz master. Generate a new question and its answers based on the original quiz topic: "${originalQuizData.heading}". The new question should replace the following question: "${quizData.questions[questionIndex].question}". The difficulty level should be the same as the original quiz. The quiz must always be in Spanish. The response should be in this format: { question: "newQuestion", options: ["option1", "option2", "option3", "option4"], correctAnswer: "correctAnswer" }. Return only the JSON without any additional text or markdown.`;

    try {
      const newQuestionContent = await handleHitAi(aiPrompt, null, "/share-online-test");

      if (newQuestionContent) {
        const parsedNewQuestionData = cleanAndParseJSON(newQuestionContent);

        if (
          parsedNewQuestionData &&
          Array.isArray(parsedNewQuestionData.options) &&
          parsedNewQuestionData.question &&
          parsedNewQuestionData.correctAnswer
        ) {
          const newQuestions = [...quizData.questions];
          newQuestions[questionIndex] = parsedNewQuestionData;
          setQuizData({ ...quizData, questions: newQuestions });
        } else {
          throw new Error("Invalid question data structure received from AI");
        }
      } else {
        throw new Error("No question data received from AI");
      }
    } catch (error) {
      console.error("Error regenerando pregunta:", error);
      alert("Hubo un error al regenerar la pregunta. Por favor, intente de nuevo.");
    }
  };

  const handleShare = (result) => {
    try {
      const parsedResult = result;
      const encodedResult = encodeURIComponent(parsedResult);
      const shareUrl = `${
        window.location.origin
      }${"/share-online-test"}/${encodedResult}`;
      navigator.clipboard.writeText(shareUrl);
      console.log("URL copied to clipboard:", shareUrl);
    } catch (error) {
      console.error("Failed to encode result:", error);
    }
  };

  const toggleDropdown = (index) => {
    setOpenDropdown(openDropdown === index ? null : index);
  };

  const handleEditQuestion = (index) => {
    setEditingQuestion({ ...quizData.questions[index], index });
    setIsEditModalOpen(true);
    setOpenDropdown(null);
  };

  const handleDeleteQuestion = (index) => {
    const newQuestions = quizData.questions.filter((_, i) => i !== index);
    setQuizData({ ...quizData, questions: newQuestions });
    setOpenDropdown(null);
  };

  const handleSaveEdit = (editedQuestion) => {
    const newQuestions = [...quizData.questions];
    newQuestions[editingQuestion.index] = editedQuestion;
    setQuizData({ ...quizData, questions: newQuestions });
    setIsEditModalOpen(false);
    setEditingQuestion(null);
  };

  const handlePreviousQuestion = () => {
    if (currentQuestionIndex > 0) {
      setCurrentQuestionIndex((prevIndex) => prevIndex - 1);
    }
  };

  const getOptionLabel = (index) => {
    return String.fromCharCode(65 + index);
  };

  const handleViewToggle = () => {
    setIsAllView(!isAllView);
  };

  const handleToggleShowAnswers = (showImmediately) => {
    setShowAnswersImmediately(showImmediately);
  };

  const handleCheckAllAnswers = () => {
    const finalScore = selectedAnswers.reduce((acc, answer, index) => {
      return acc + (answer === quizData.questions[index].correctAnswer ? 1 : 0);
    }, 0);
    setScore(finalScore);
    setIsQuizCompleted(true);
  };

  const renderAllQuestions = () => {
    return (
      <>
        {quizData.questions.map((question, index) => {
          const questionSelectedAnswer = selectedAnswers[index];
          const isCorrect = questionSelectedAnswer === question.correctAnswer;

          return (
            <div
              key={index}
              className="flex flex-col items-center gap-[20px] w-full mb-[36px]"
            >
              <span className="flex items-center justify-between px-[16px] py-[28px] bg-[#CAE5FF] text-[#2B3D70] rounded-[12px] border-[#4EA6FE] border-[1px] w-full">
                <span>
                  {index + 1}. {question.question}
                </span>
                <div className="relative">
                  <button
                    onClick={() => toggleDropdown(index)}
                    className="text-gray-600 hover:text-gray-800"
                  >
                    <i className="ri-more-2-fill"></i>
                  </button>
                  {openDropdown === index && (
                    <div
                      ref={quizActionDropdownRef}
                      className="absolute right-0 transform -translate-x-3 mt-2 w-48 bg-white rounded-md shadow-lg z-10 dropdown-animation"
                    >
                      <button
                        onClick={() => {
                          regenerateQuestion(index);
                          setOpenDropdown(false);
                        }}
                        className="block px-4 py-2 text-sm text-[#4EA7FF] hover:bg-[#E8F4FF] w-full text-left"
                      >
                        Re-generar Pregunta por otra
                      </button>
                      <button
                        onClick={() => {
                          handleEditQuestion(index);
                          setOpenDropdown(false);
                        }}
                        className="block px-4 py-2 text-sm text-[#4EA7FF] hover:bg-[#E8F4FF] w-full text-left"
                      >
                        Editar Pregunta
                      </button>
                      <button
                        onClick={() => {
                          handleDeleteQuestion(index);
                          setOpenDropdown(false);
                        }}
                        className="block px-4 py-2 text-sm text-[#4EA7FF] hover:bg-[#E8F4FF] w-full text-left"
                      >
                        Eliminar Pregunta
                      </button>
                    </div>
                  )}
                </div>
              </span>

              <div className="flex flex-col w-full px-[48px] items-center gap-[16px]">
                {question.options.map((option, optionIndex) => {
                  const isSelected = option === questionSelectedAnswer;
                  const isCorrectAnswer = option === question.correctAnswer;

                  const optionClassName = isQuizCompleted
                    ? isCorrectAnswer
                      ? "bg-[#E8FFF9] border-[#02E8AC] text-[#02E8AC]"
                      : isSelected
                      ? "bg-[#FFEEEC] border-[#FF6F61] text-[#FF6F61]"
                      : "bg-[#FFF] border-[#F3F3F3] text-[#2B3D70]"
                    : showAnswersImmediately && isSelected
                    ? isCorrectAnswer
                      ? "bg-[#E8FFF9] border-[#02E8AC] text-[#02E8AC]"
                      : "bg-[#FFEEEC] border-[#FF6F61] text-[#FF6F61]"
                    : isSelected
                    ? "bg-[#CAE5FF] border-[#4EA7FF] text-[#4EA7FF]"
                    : "bg-[#FFF] border-[#F3F3F3] text-[#2B3D70]";

                  return (
                    <span
                      key={optionIndex}
                      className={`flex items-start px-[16px] py-[28px] rounded-[12px] border-[1px] w-full cursor-pointer ${optionClassName}`}
                      onClick={() =>
                        !isQuizCompleted && handleAnswerSelect(option, index)
                      }
                    >
                      {getOptionLabel(optionIndex)}. {option}
                    </span>
                  );
                })}
              </div>
            </div>
          );
        })}
        {!isQuizCompleted && (
          <div className="flex justify-center mt-4">
            <Button
              className="px-[32px] py-[12px] rounded-[8px] text-white bg-[#4EA7FF] hover:bg-[#4EA7FF]/80"
              onClick={handleCheckAllAnswers}
            >
              Comprobar Respuestas
            </Button>
          </div>
        )}
      </>
    );
  };

  const handleTimerClick = () => {
    if (isRunning) {
      clearInterval(timerRef.current);
      setTime(0);
      setIsRunning(false);
    } else {
      setIsRunning(true);
      timerRef.current = setInterval(() => {
        setTime((prevTime) => prevTime + 1);
      }, 1000);
    }
  };

  useEffect(() => {
    return () => {
      clearInterval(timerRef.current);
    };
  }, []);

  const formatTime = (time) => {
    const minutes = String(Math.floor(time / 60)).padStart(2, "0");
    const seconds = String(time % 60).padStart(2, "0");
    return `${minutes}:${seconds}`;
  };

  if (isLoading) return <div>Cargando...</div>;
  if (!quizData) return <div>No hay exámenes disponibles</div>;

  if (isQuizCompleted) {
    return (
      <div className="flex flex-col items-center justify-center">
        <div className="w-full max-w-[674px] flex flex-col items-start h-[90dvh] overflow-y-scroll py-[20px] px-[40px] justify-start gap-[36px]">
          <span className="text-[#4EA7FF] text-[36px] font-bold">
            ¡Has completado tu examen!
          </span>
          <p className="w-full text-slate-500 text-base font-normal">
            Has acertado: {score} de {quizData.questions.length}
          </p>
          <div className="flex justify-between w-full gap-2 mt-4">
            <div className="relative flex-1">
              <Button
                className="w-full border-[#4EA6FE] border-[1px] text-white bg-[#4EA7FF] hover:bg-[#4EA7FF]/80 rounded-[8px] px-[16px] py-[12px]"
                onClick={() =>
                  setShowShuffleDropdown(!showShuffleDropdown)
                }
              >
                ¿Quieres hacer el examen de nuevo?
              </Button>
              {showShuffleDropdown && (
                <div className="absolute top-full left-0 mt-2 w-full bg-white border border-[#4EA7FF] rounded-md shadow-lg z-10">
                  <ul className="py-2 text-[#4EA7FF]">
                    <li
                      className="px-4 py-2 cursor-pointer hover:bg-[#E8F4FF] rounded-md"
                      onClick={() => handleRestart(false)}
                    >
                      Si, y mantener el mismo orden.
                    </li>
                    <li
                      className="px-4 py-2 cursor-pointer hover:bg-[#E8F4FF] rounded-md"
                      onClick={() => handleRestart(true)}
                    >
                      Sí, mezclar el orden de preguntas y respuestas.
                    </li>
                  </ul>
                </div>
              )}
            </div>
            <div className="relative flex-1">
              <Button
                className="w-full border-[#4EA6FE] border-[1px] text-white bg-[#4EA7FF] hover:bg-[#4EA7FF]/80 rounded-[8px] px-[16px] py-[12px]"
                onClick={() =>
                  setShowDifficultyDropdown(!showDifficultyDropdown)
                }
              >
                Generar un nuevo examen con nuevas preguntas
              </Button>
              {showDifficultyDropdown && (
                <div className="absolute top-full left-0 mt-2 w-full bg-white border border-[#4EA7FF] rounded-md shadow-lg z-10">
                  <ul className="py-2 text-[#4EA7FF]">
                    <li
                      className="px-4 py-2 cursor-pointer hover:bg-[#E8F4FF] rounded-md"
                      onClick={() => generateNewQuiz("un_poco_mas_facil")}
                    >
                      Un poco más fácil.
                    </li>
                    <li
                      className="px-4 py-2 cursor-pointer hover:bg-[#E8F4FF] rounded-md"
                      onClick={() => generateNewQuiz("misma")}
                    >
                      Misma dificultad.
                    </li>
                    <li
                      className="px-4 py-2 cursor-pointer hover:bg-[#E8F4FF] rounded-md"
                      onClick={() => generateNewQuiz("poco_mas_dificil")}
                    >
                      Un poco más difícil.
                    </li>
                    <li
                      className="px-4 py-2 cursor-pointer hover:bg-[#E8F4FF] rounded-md"
                      onClick={() => generateNewQuiz("mas_dificil")}
                    >
                      Considerablemente más difícil.
                    </li>
                    <li
                      className="px-4 py-2 cursor-pointer hover:bg-[#E8F4FF] rounded-md"
                      onClick={() => generateNewQuiz("bastante_mas_dificil")}
                    >
                      Bastante más difícil.
                    </li>
                  </ul>
                </div>
              )}
            </div>
          </div>
          <div className="w-full">{renderAllQuestions()}</div>
        </div>
      </div>
    );
  }

  const handlePdfDownload = () => {
    if (!quizData) {
      console.error("No quiz data available for PDF download");
      return;
    }

    const doc = new jsPDF({
      orientation: "portrait",
      unit: "pt",
      format: "a4",
    });

    const margins = {
      top: 40,
      bottom: 40,
      left: 40,
      right: 40,
    };
    const pageWidth = doc.internal.pageSize.width;
    const pageHeight = doc.internal.pageSize.height;
    const contentWidth = pageWidth - margins.left - margins.right;

    let cursorY = margins.top;

    const renderElement = (element, indent = 0) => {
      const lineHeight = 16;
      const paragraphSpacing = 8;
      let headerSpacing = 0;

      switch (element.type) {
        case "heading":
          doc.setFontSize(18);
          doc.setFont("helvetica", "bold");
          cursorY += 20;
          headerSpacing = 16;
          break;
        case "question":
          doc.setFontSize(14);
          doc.setFont("helvetica", "bold");
          cursorY += 14;
          headerSpacing = 12;
          break;
        case "option":
          doc.setFontSize(12);
          doc.setFont("helvetica", "normal");
          indent += 20;
          break;
        case "answer":
          doc.setFontSize(12);
          doc.setFont("helvetica", "italic");
          break;
        default:
          doc.setFontSize(12);
          doc.setFont("helvetica", "normal");
      }

      const text = element.text;
      const lines = doc.splitTextToSize(text, contentWidth - indent);
      lines.forEach((line) => {
        if (cursorY + lineHeight > pageHeight - margins.bottom) {
          doc.addPage();
          cursorY = margins.top;
        }
        doc.text(line, margins.left + indent, cursorY);
        cursorY += lineHeight;
      });
      cursorY += paragraphSpacing + headerSpacing;
    };

    renderElement({ type: "heading", text: quizData.heading });

    quizData.questions.forEach((question, index) => {
      renderElement({
        type: "question",
        text: `Pregunta ${index + 1}: ${question.question}`,
      });
      question.options.forEach((option, optionIndex) => {
        renderElement({
          type: "option",
          text: `${getOptionLabel(optionIndex)}) ${option}`,
        });
      });
      renderElement({
        type: "answer",
        text: `Respuesta Correcta: ${question.correctAnswer}`,
      });
    });

    doc.save(`${quizData.heading.replace(/\s+/g, "_")}_examen.pdf`);
  };

  return (
    <div className="flex flex-col items-center justify-center">
      <div className="w-full max-w-[674px] flex flex-col items-start py-[20px] px-[40px] justify-start gap-[36px]">
        <div className="flex items-center justify-between w-full">
          <span className="flex flex-col">
            <span className="text-[#4EA7FF] text-[28px] font-bold">
              Examen:{" "}
              <span className="text-[#4EA7FF] text-[24px] ">
                {quizData.heading}
              </span>
            </span>{" "}
            <span className="text-[#4EA7FF] text-[28px] font-bold">
              Nº Preguntas:{" "}
              <span className="text-[#4EA7FF] text-[24px] ">
                {quizData?.questions?.length}
              </span>
            </span>
          </span>
          <div className="flex items-center gap-[16px]">
            <Tooltip text="Click para iniciar el cronómetro y click de nuevo para reiniciarlo">
              <div className="cursor-pointer" onClick={handleTimerClick}>
                <StopwatchIcon time={formatTime(time)} isRunning={isRunning} />
              </div>
            </Tooltip>
            <Tooltip text="Descarga el examen o cuestionario en PDF">
              <PdfFileIcon
                className="cursor-pointer"
                onClick={handlePdfDownload}
              />
            </Tooltip>
            <Tooltip text="Cambia la vista de una pregunta a la vez, a ver todas las preguntas en una misma página">
              <div className="cursor-pointer" onClick={handleViewToggle}>
                {isAllView ? <ListViewIcon /> : <GridViewIcon />}
              </div>
            </Tooltip>
            <Tooltip text="Comparte el test 'Próximamente'">
              <i
                onClick={() =>
                  handleShare(
                    location.state ||
                      localStorage.getItem(RouteConstant.TAKE_ONLINE_TEST)
                  )
                }
                className="ri-share-line cursor-pointer text-[20px]"
              ></i>
            </Tooltip>
          </div>
        </div>
        <div className="flex justify-center items-center w-full gap-[16px] mb-[24px]">
          <button
            className={`px-[16px] py-[12px] rounded-[8px] ${
              showAnswersImmediately
                ? "bg-[#4EA7FF] text-white"
                : "bg-white text-[#4EA7FF] border border-[#4EA7FF]"
            } hover:bg-[#4EA7FF]/80`}
            onClick={() => handleToggleShowAnswers(true)}
            title="Ver las respuestas correctas al seleccionar la opción"
          >
            Mostrar respuestas al seleccionar
          </button>
          <button
            className={`px-[16px] py-[12px] rounded-[8px] ${
              !showAnswersImmediately
                ? "bg-[#4EA7FF] text-white"
                : "bg-white text-[#4EA7FF] border border-[#4EA7FF]"
            } hover:bg-[#4EA7FF]/80`}
            onClick={() => handleToggleShowAnswers(false)}
            title="Tu respuesta se seleccionará en azul y podrás hacer el test sin ver la respuesta hasta el final"
          >
            Mostrar respuestas al finalizar
          </button>
        </div>
        <div
          className={`w-full ${
            isAllView ? "max-h-[64dvh]" : "max-h-[70dvh]"
          } overflow-scroll `}
        >
          {isAllView
            ? renderAllQuestions()
            : currentQuestion && (
                <QuizQuestion
                  question={`${currentQuestionIndex + 1}. ${
                    currentQuestion.question
                  }`}
                  options={currentQuestion.options}
                  selectedAnswer={selectedAnswers[currentQuestionIndex]}
                  correctAnswer={currentQuestion.correctAnswer}
                  onAnswerSelect={(answer) =>
                    handleAnswerSelect(answer, currentQuestionIndex)
                  }
                  showAnswersImmediately={showAnswersImmediately}
                  isQuizCompleted={isQuizCompleted}
                  onEdit={() => handleEditQuestion(currentQuestionIndex)}
                  onRegenerate={() => regenerateQuestion(currentQuestionIndex)}
                  onDelete={() => handleDeleteQuestion(currentQuestionIndex)}
                />
              )}
        </div>
        {!isAllView && (
          <div className="flex items-center justify-between w-full gap-[16px]">
            <Button
              className={
                "w-full border-[#4EA6FE] border-[1px] !text-[#4EA7FF] !bg-[#FFF]"
              }
              onClick={handlePreviousQuestion}
              disabled={currentQuestionIndex === 0}
            >
              Pregunta Anterior
            </Button>
            <Button
              className={
                "w-full border-[#4EA6FE] border-[1px] !text-[#4EA7FF] !bg-[#FFF]"
              }
              onClick={handleSkip}
            >
              Saltar
            </Button>
            <Button
              className={"w-full"}
              onClick={handleContinue}
              disabled={!selectedAnswers[currentQuestionIndex]}
            >
              Siguiente Pregunta
            </Button>
          </div>
        )}
      </div>
      {isEditModalOpen && (
        <EditQuestionModal
          question={editingQuestion}
          onSave={handleSaveEdit}
          onClose={() => setIsEditModalOpen(false)}
        />
      )}
    </div>
  );
};

export default TakeOnlineTest;
