import { FaPlay, FaStop, FaRedo, FaBrush, FaFilePdf } from 'react-icons/fa';

export const PlayIcon = (props) => <FaPlay {...props} />;
export const StopIcon = (props) => <FaStop {...props} />;
export const RestartIcon = (props) => <FaRedo {...props} />;
export const BrushIcon = (props) => <FaBrush {...props} />;
export const PdfFileIcon = (props) => <FaFilePdf {...props} />;
