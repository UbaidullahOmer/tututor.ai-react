import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";

function TeamWorkText() {  const { handleHitAi, loading } = useHandleHitAi({ ai: "chatgpt", toolName: "Creador y organizador de trabajo en equipo",});
const [data, setData] = useState({
  contenidos: "",
  course: "",
  subject: "",
  numberOfStudent: "",
  workDuration: "",
});
const prvResponse = getLocalStorage(RouteConstant.TEAM_WORK_OUTPUT);
const navigate = useNavigate();
const [isLoading, setIsLoading] = useState(false);

const handleChange = (e) => {
  setData({ ...data, [e.target.name]: e.target.value });
};

const handleSubmit = async (e) => {
  e.preventDefault();
  if (
    !data.contenidos ||
    !data.course ||
    !data.subject ||
    !data.numberOfStudent ||
    !data.workDuration
  ) {
    alert("Please fill in all fields");
    return;
  }
  const aiPrompt = `As a powerful AI assistant specializing in creating and organizing team work activities for educational settings, please generate the following HTML-structured content:

<html>
<body>
  <h1>Creador y Organizador de Trabajo en Equipo</h1>
  
  <p>Detalles de la actividad:</p>
  <ul>
    <li><strong>Curso:</strong> ${data.course}</li>
    <li><strong>Asignatura:</strong> ${data.subject}</li>
    <li><strong>Número de Estudiantes:</strong> ${data.numberOfStudent}</li>
    <li><strong>Duración del Trabajo:</strong> ${data.workDuration}</li>
  </ul>

  <p>Contenidos a cubrir:</p>
  <p>${data.contenidos}</p>

  <h2>Instrucciones para crear la actividad de trabajo en equipo:</h2>
  
  <ol>
    <li>
      <h3>Objetivo de la Actividad</h3>
      <p>Define el objetivo principal de la actividad de trabajo en equipo, asegurándote de que esté alineado con los contenidos a cubrir y sea apropiado para el nivel del curso.</p>
    </li>
    
    <li>
      <h3>Estructura de los Equipos</h3>
      <p>Sugiere cómo dividir a los ${data.numberOfStudent} estudiantes en equipos efectivos. Incluye:
        <ul>
          <li>Número recomendado de estudiantes por equipo</li>
          <li>Criterios para formar equipos equilibrados</li>
          <li>Roles sugeridos para los miembros del equipo</li>
        </ul>
      </p>
    </li>
    
    <li>
      <h3>Descripción de la Tarea</h3>
      <p>Describe detalladamente la tarea que los equipos deben realizar. Asegúrate de que la tarea:
        <ul>
          <li>Cubra los contenidos especificados</li>
          <li>Sea realizable en el tiempo asignado (${data.workDuration})</li>
          <li>Requiera colaboración genuina entre los miembros del equipo</li>
        </ul>
      </p>
    </li>
    
    <li>
      <h3>Etapas del Trabajo</h3>
      <p>Divide la actividad en etapas claras, especificando:
        <ul>
          <li>Qué debe lograrse en cada etapa</li>
          <li>Tiempo estimado para cada etapa</li>
          <li>Puntos de control o entregables intermedios</li>
        </ul>
      </p>
    </li>
    
    <li>
      <h3>Recursos Necesarios</h3>
      <p>Enumera los recursos que los equipos necesitarán para completar la tarea, como materiales, herramientas o información específica.</p>
    </li>
    
    <li>
      <h3>Estrategias de Facilitación</h3>
      <p>Sugiere 2-3 estrategias que el profesor puede usar para facilitar el trabajo en equipo y resolver posibles conflictos.</p>
    </li>
    
    <li>
      <h3>Reflexión y Cierre</h3>
      <p>Propón una actividad de reflexión para que los equipos evalúen su proceso de trabajo y aprendizaje al finalizar la tarea.</p>
    </li>
  </ol>

  <p>Asegúrate de que la actividad de trabajo en equipo sea desafiante pero realizable, fomente la colaboración genuina, y permita a los estudiantes aplicar y profundizar su comprensión de los contenidos del curso. Responde en español, adaptando el lenguaje y la complejidad al nivel del curso especificado.</p>
</body>
</html>
Generate always the result in this format: HTML`;

console.log(aiPrompt);

  setIsLoading(true);
  try {
    await handleHitAi(aiPrompt, RouteConstant.TEAM_WORK_OUTPUT);
  } catch (error) {
    console.error("Error during API call:", error);
    setIsLoading(false);
  }
};

return (
  <div className="flex flex-col gap-[24px] p-[24px]">
    <AiToolPageHeader
      title="Creador y Organizador de Proyectos Colaborativos: Potenciando el Aprendizaje en Equipo"
      description="Esta herramienta innovadora está diseñada para generar planes detallados de actividades de trabajo en equipo, adaptados a contextos educativos específicos. Facilita la creación de proyectos colaborativos estructurados, promoviendo el aprendizaje activo y el desarrollo de habilidades de trabajo en equipo"
      icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
    />
    <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
      <TextArea
        placeholder="Ej: Proyecto sobre el reciclaje y su importancia en el medio ambiente"
        onChange={handleChange}
        className="col-span-2"
        value={data.contenidos}
        name="contenidos"
        label={"Contenidos a tratar"}
      />
      <InputFieldDropdown
        placeholder="Curso"
        onChange={(value) => {
          setData({ ...data, course: value });
        }}
        name="course"
        type="number"
        value={data.course}
        options={Dropdown.courses}
        require={true}
        label={"Curso"}
      />
      <InputFieldDropdown
        name={"Subject"}
        value={data.subject}
        onChange={(value) => {
          setData({ ...data, subject: value });
        }}
        require={true}
        placeholder={"Asignatura"}
        options={Dropdown.subject}
        label={"Asignatura"}
      />
      <InputField
        name={"numberOfStudent"}
        value={data.numberOfStudent}
        onChange={handleChange}
        placeholder={"Número de estudiantes"}
        type="number"
        require={true}
        label={"Número de estudiantes"}
      />
      <InputField
        name={"workDuration"}
        value={data.workDuration}
        onChange={handleChange}
        placeholder={"Duración de la actividad"}
        require={true}
        label={"Duración"}
      />
      <div className="col-span-2 flex  align-center gap-2 ">
        <Button
          onClick={handleSubmit}
          className={prvResponse ? "w-[80%]" : "w-full"}
          disabled={loading}
        >
          {isLoading ? "Loading..." : "Generate"}
        </Button>
        {prvResponse && (
          <Button
            className={"w-[20%]"}
            onClick={() => {
              navigate(RouteConstant.TEAM_WORK_OUTPUT);
            }}
          >
            Previous
          </Button>
        )}
      </div>
    </div>
  </div>
);
}


export default TeamWorkText