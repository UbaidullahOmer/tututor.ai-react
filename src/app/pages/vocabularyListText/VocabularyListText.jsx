import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";
import FileReaderComponent from "../../components/fileReaderComponent/FileReaderComponent";
import FileReaderInputFIled from "../../components/fileReaderInputFIled/FileReaderInputFIled";

function VocabularyListText() {
  const { handleHitAi, loading } = useHandleHitAi({ ai: "gemini", toolName: "Lista de Vocabulario para un tema", });
  const [data, setData] = useState({
    prompt: "",
    course: "",
    subject: "",
    numberOfWords: "",
  });
  const prvResponse = getLocalStorage(RouteConstant.VOCABULARY_LIST_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      (!data.prompt && data.selectedFiles.length === 0) ||
      !data.course ||
      !data.subject ||
      !data.numberOfWords
    ) {
      alert("Por favor, rellene todos los campos");
      return;
    }
    let simpleVocabularyListGeneratorPrompt = `
Como experto en lingüística y educación, genera una lista de vocabulario con una breve introducción, basada en:

Tema: ${data.prompt}
Curso: ${data.course}
Asignatura: ${data.subject}
Número de palabras: ${data.numberOfWords}

Instrucciones:

1. Comienza con una breve introducción (2-3 oraciones) que explique el tema y la importancia del vocabulario seleccionado.

2. Extrae el número de palabras que se quieren hacer ${data.numberOfWords} (términos más relevantes del tema proporcionado).

3. Para cada término:
   - Escribe el término en negrita.
   - Proporciona una definición clara y concisa, adaptada al nivel del curso. Si el término es compelejo, puedes hacerla con algún ejemplo
   - Si la definición es compleja, añade un breve ejemplo de uso entre paréntesis.

4. Presenta la lista en formato HTML usando la siguiente estructura:

<h2>Vocabulario Clave: [Tema]</h2>

<p>[Introducción breve]</p>

<ul>
  <li><strong>[Término 1]:</strong> [Definición] (Ejemplo si es necesario)</li>
  <li><strong>[Término 2]:</strong> [Definición] (Ejemplo si es necesario)</li>
  ...
</ul>

Asegúrate de que las definiciones sean relevantes para la asignatura y adaptadas al nivel del curso. Responde en español, proporcionando una lista de vocabulario clara y útil para los estudiantes.
The result must always be in this format: HTML`;

    if (data.selectedFiles.length > 0) {
      const imageFiles = data.selectedFiles.filter((file) =>
        file.type.startsWith("image/")
      );
      if (imageFiles.length > 0) {
        aiPrompt +=
          "\nPlease analyze the uploaded images and include relevant questions based on their content.";
      }
    }

    console.log(simpleVocabularyListGeneratorPrompt);

    setIsLoading(true);
    try {
      await handleHitAi(
        simpleVocabularyListGeneratorPrompt,
        RouteConstant.VOCABULARY_LIST_OUTPUT
      );
      navigate(RouteConstant.VOCABULARY_LIST_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      alert(
        "Ocurrió un error al generar la lista de vocabulario. Por favor, inténtalo de nuevo."
      );
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Generador de listas de Vocabulario para temas concretos"
        description="Con esta herramientas podrás generar listas de palabras y sus deficiones de un tema en concreto o de un tema que subas en PDF o en DOC (word). ¡Tú eliges cuántas palabras quieres aprender sobre el tema!"
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <FileReaderInputFIled
          onChange={handleChange}
          data={data}
          setData={setData}
        />
        <InputFieldDropdown
          placeholder="Curso"
          onChange={(value) => {
            setData({ ...data, course: value });
          }}
          name="course"
          type="number"
          value={data.course}
          options={Dropdown.courses}
          require={true}
          label="Curso"
        />
        <InputFieldDropdown
          name="subject"
          value={data.subject}
          onChange={(value) => {
            setData({ ...data, subject: value });
          }}
          require={true}
          placeholder="Asignatura"
          options={Dropdown.subject}
          label="Asignatura"
        />
        <InputField
          name="numberOfWords"
          value={data.numberOfWords}
          onChange={handleChange}
          placeholder="Elige cuántas palabras de vocabulario quieres. Máximo 50"
          type="number"
          require={true}
          className="col-span-2"
          label="Número de palabras a extraer vocabulario"
        />
        <div className="col-span-2 flex align-center gap-2">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={loading || isLoading}
          >
            {isLoading ? "Generando..." : "Generar vocabulario"}
          </Button>
          {prvResponse && (
            <Button
              className="w-[20%"
              onClick={() => {
                navigate(RouteConstant.VOCABULARY_LIST_OUTPUT);
              }}
              disabled={loading || isLoading}
            >
              Anterior Generación
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}
export default VocabularyListText;
