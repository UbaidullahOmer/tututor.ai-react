import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";
import FileReaderInputFIled from "../../components/fileReaderInputFIled/FileReaderInputFIled";

const FormField = ({ title, children, required = false }) => (
  <div className="mb-4">
    <label className="block font-bold text-sm mb-2">
      {title} {required && <span className="text-red-500">*</span>}
    </label>
    {children}
  </div>
);

function TaskFeedbackText() {
  const { handleHitAi, loading } = useHandleHitAi({ ai: "chatgpt", toolName: "Retroalimentación de Tareas", });
  const [data, setData] = useState({
    course: "",
    subject: "",
    evaluationArea: "",
    feedbackPoints: "",
    selectedFiles: [],
  });
  const prvResponse = getLocalStorage(RouteConstant.TASK_FEEDBACK_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleFileChange = (files) => {
    setData({ ...data, selectedFiles: files });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      !data.course ||
      !data.subject ||
      !data.evaluationArea ||
      data.selectedFiles.length === 0 ||
      !data.feedbackPoints
    ) {
      alert("Please fill in all fields and upload a file");
      return;
    }
    const aiPrompt = `
Eres un experto en proporcionar retroalimentación constructiva sobre tareas educativas. Tu tarea es generar una retroalimentación detallada, específica y procesable basada en los siguientes criterios:

Curso: ${data.course}
Asignatura: ${data.subject}
Áreas de Evaluación: ${data.evaluationArea}
Puntos de Retroalimentación: ${data.feedbackPoints}
    
Por favor, proporciona la retroalimentación en el siguiente formato:

<h2>1. Calificación General:</h2>
<p>[Proporciona un ranking cualitativo (Excelente, Satisfactorio, Necesita Mejorar)]</p>
    
<h2>2. Fortalezas:</h2>
<ul>
  <li>[Enumera 2-3 fortalezas específicas, con ejemplos del trabajo del estudiante]</li>
</ul>
    
<h2>3. Áreas a Mejorar:</h2>
<ul>
  <li>[Enumera 2-3 áreas específicas para mejorar, con sugerencias sobre cómo hacerlo]</li>
</ul>
    
<h2>4. Objetivos de Aprendizaje Alcanzados:</h2>
<ul>
  <li>[Enumera los objetivos de aprendizaje clave que el estudiante ha demostrado en esta tarea]</li>
</ul>
    
<h2>5. Próximos Pasos:</h2>
<ul>
  <li>[Proporciona 2-3 sugerencias concretas para el aprendizaje o mejora futura]</li>
</ul>
    
<h2>6. Preguntas de Reflexión:</h2>
<p>[Incluye dos preguntas que fomenten la reflexión del estudiante]</p>
    
<h2>7. Habilidades Transversales:</h2>
<p>[Menciona brevemente cualquier habilidad demostrada como creatividad, pensamiento crítico o comunicación]</p>
    
<h2>8. Originalidad y Uso de Fuentes:</h2>
<p>[Evalúa si el trabajo parece ser original o si hay indicios de que se ha copiado o extraído directamente de algún lugar de internet. Si se detecta plagio, dilo claramente. Si ves que no estás seguro, dilo también. Si el texto está sacado de alguna web tal cual, y ha sido copiada y pegada para el trabajo, dilo.]</p>

En las Áreas de Evaluación, evalúa todos los criterios especificados por el usuario. En los Puntos de Retroalimentación, evalúa los aspectos importantes destacados por el usuario. Asegúrate de que tu retroalimentación sea constructiva, específica y procesable. Responde siempre en español.
El resultado debe ser proporcionado en este formato: HTML
`;

    console.log(aiPrompt);
    
    setIsLoading(true);
    try {
      await handleHitAi(aiPrompt, RouteConstant.TASK_FEEDBACK_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Evaluador de Tareas Educativas: Retroalimentación Constructiva"
        description="Esta herramienta proporciona una retroalimentación detallada y personalizada sobre tareas académicas. Analiza el trabajo del estudiante según criterios específicos, ofreciendo una evaluación integral que incluye fortalezas, áreas de mejora, logros de objetivos y sugerencias para el desarrollo futuro. Diseñada para fomentar el crecimiento académico, esta herramienta ayuda a los educadores a ofrecer comentarios constructivos y motivadores, promoviendo la reflexión del estudiante y el aprendizaje continuo"
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="flex flex-col gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
      <FormField title="Sube el temario del estudiante o pega el texto aquí" required>
            <FileReaderInputFIled
              onChange={handleFileChange}
              data={data}
              setData={setData}
            />
          </FormField>
        <InputFieldDropdown
          placeholder="Curso"
          onChange={(value) => {
            setData({ ...data, course: value });
          }}
          name="course"
          type="number"
          value={data.course}
          options={Dropdown.courses}
          required
          label={"Curso"}
        />
        <InputFieldDropdown
          name={"Subject"}
          value={data.subject}
          onChange={(value) => {
            setData({ ...data, subject: value });
          }}
          placeholder={"Asignatura"}
          options={Dropdown.subject}
          label={"Asignatura"}
        />
        <TextArea
          name={"evaluationArea"}
          value={data.evaluationArea}
          onChange={handleChange}
          placeholder={"Ejemplo: 1. Comprensión lectora: Capacidad para entender e interpretar el poema A una nariz de Francisco de Quevedo. 2. Expresión escrita: Habilidad para comunicar ideas de manera clara, coherente y con un estilo apropiado. 3. Análisis literario: Capacidad para identificar y explicar recursos literarios, estructura del poema y contexto histórico-literario. 4. Uso de vocabulario: Empleo de terminología literaria adecuada y variedad léxica en la redacción. 5. Argumentación: Habilidad para respaldar interpretaciones con evidencias del texto. 6. Pensamiento crítico: Capacidad para ofrecer una interpretación personal fundamentada del poema. etc"}
          label={"Área a Evaluar"}
        />
        <TextArea
          name={"feedbackPoints"}
          value={data.feedbackPoints}
          onChange={handleChange}
          placeholder={"Estructura del ensayo: Evaluar la organización lógica del texto, incluyendo introducción, desarrollo y conclusión, Profundidad del análisis: Valorar la capacidad del estudiante para ir más allá de lo superficial, ofreciendo interpretaciones fundamentadas, Identificación y explicación de recursos literarios: Comprobar si el alumno reconoce y explica adecuadamente las figuras retóricas empleadas por Quevedo, etc"}
          label={"Puntos de feedback"}
          required
        />
        <div className="col-span-2">
        </div>
        <div className="flex justify-between gap-2">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={loading}
          >
            {isLoading ? "Cargando..." : "Generar Evaluador de Tareas"}
          </Button>
          {prvResponse && (
            <Button
              className={"w-[20%]"}
              onClick={() => {
                navigate(RouteConstant.TASK_FEEDBACK_OUTPUT);
              }}
            >
              Generación Anterior
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default TaskFeedbackText;