import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";

function RealWorldText() {

  const { handleHitAi, loading } = useHandleHitAi({ ai: "chatgpt", toolName: "Conexión con el mundo real de un tema", });
  const [data, setData] = useState({
    prompt: "",
    course: "",
    subject: "",
  });
  const prvResponse = getLocalStorage(RouteConstant.REAL_WORLD_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      !data.prompt ||
      !data.subject
    ) {
      alert("Please fill in all fields");
      return;
    }
    const realWorldConnectionPrompt = `

Como experto en educación y aplicaciones prácticas, proporciona conexiones del mundo real y da siempre un enfoque competencial a los contenidos para el siguiente contenido académico:
    Curso: ${data.course}
    Asignatura: ${data.subject}
    Contenido a aplicar en la vida real: ${data.prompt}
<html>
<body>
  <ol>
    <li>
      <h3>¿Para qué sirve?</h3>
      <p>Escribe en 2 párrafos sobre la importancia del contenido y su aplicación en la vida real.</p>
    </li>
    
    <li>
      <h3>Aplicaciones Prácticas</h3>
      <p>Proporciona 3-4 ejemplos concretos de cómo este contenido se aplica en situaciones de la vida real. Para cada ejemplo, incluye:</p>
      <ul>
        <li>Una descripción clara de la situación o escenario</li>
      </ul>
    </li>
    
    <li>
      <h3>Conexiones Profesionales</h3>
      <p>Describe en un párrafo cómo este contenido se utiliza en diferentes carreras o industrias.</p>
    </li>
  
    <li>
      <h3>Reflexión Final</h3>
      <p>Concluye con un párrafo que anime a los estudiantes a buscar constantemente conexiones entre lo que aprenden en clase y el mundo que les rodea.</p>
    </li>
  </ol>

</body>
</html>
Responde en español, asegurándote de que las conexiones sean relevantes, prácticas y motivadoras para los estudiantes del nivel especificado. El objetivo es demostrar el valor y la aplicabilidad del contenido académico en contextos reales y significativos.
The content must always be generated in this format: HTML`;

console.log(realWorldConnectionPrompt);

    setIsLoading(true);
    try {
      await handleHitAi(realWorldConnectionPrompt, RouteConstant.REAL_WORLD_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Conexión Tema - Mundo Real: Puente entre el Aula y la Vida Cotidiana"
        description="¿No entiendes para qué me sirve lo que estás aprendiendo en la escuela? ¡No te preocupes! Con esta herramienta podrás ver cómo los conceptos que aprendes en el aula se aplican en tu vida diaria y futura carrera."
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <TextArea
          placeholder="Ej: Ecuaciones lineales. Ej: Para que me sirve el álgebra o la aritmética"
          onChange={handleChange}
          className="col-span-2"
          value={data.prompt}
          name="prompt"
          label={"Contenido que quieres saber qué aplicación tiene en la vida real"}
        />
        <InputFieldDropdown
          placeholder="Curso"
          onChange={(value) => {
            setData({ ...data, course: value });
          }}
          name="course"
          type="number"
          value={data.course}
          options={Dropdown.courses}
          require={false}
          label={"Curso"}
        />
        <InputFieldDropdown
          name={"Asignatura"}
          value={data.subject}
          onChange={(value) => {
            setData({ ...data, subject: value });
          }}
          require={false}
          placeholder={"Subject"}
          options={Dropdown.subject}
          label={"Asignatura"}
        />
        <div className="col-span-2 flex  align-center gap-2 ">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={loading}
          >
            {isLoading ? "Cargando..." : "¡Dime para qué sirve esto!"}
          </Button>
          {prvResponse && (
            <Button
              className={"w-[20%]"}
              onClick={() => {
                navigate(RouteConstant.REAL_WORLD_OUTPUT);
              }}
            >
              Generación Anterior
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default RealWorldText