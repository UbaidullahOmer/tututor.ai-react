import React, { useEffect, useState } from 'react';
import useFirebaseData from '../../hooks/useFirebaseData';
import useFirebaseImage from '../../hooks/useFirebaseImage';

const ProfilePage = () => {
  const { updateData, user } = useFirebaseData();
  const { uploadImage, imageUrl, isLoading: imageLoading, uploadProgress } = useFirebaseImage();
  const [userData, setUserData] = useState({
    displayName: '',
    email: '',
    photoURL: '',
    school: '',
    city: '',
    country: '',
    role: '',
    subjects: ''
  });
  const [profileImage, setProfileImage] = useState(null);
  const [profileImageURL, setProfileImageURL] = useState(null);

  useEffect(() => {
    if (user) {
      setUserData({
        displayName: user.displayName || '',
        email: user.email || '',
        photoURL: user.photoURL || '',
        school: user.school || '',
        city: user.city || '',
        country: user.country || '',
        role: user.role || '',
        subjects: user.subjects || ''
      });
    }
  }, [user]);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setUserData((prevData) => ({
      ...prevData,
      [name]: value
    }));
  };

  const handleImageChange = (e) => {
    if (e.target.files[0]) {
      setProfileImage(e.target.files[0]);
      setProfileImageURL(URL.createObjectURL(e.target.files[0]));
    }
  };

  const handleUpdateProfile = async () => {
    let data = userData;
    if (profileImage) {
      const uploadedImageUrl = await uploadImage(profileImage);
      data = {
        ...data,
        photoURL: uploadedImageUrl
      };
    }

    updateData('users', user.uid, data);
  };

  return (
    <div className="profile-page max-h-[90vh] overflow-scroll p-6">
      <h1 className="text-3xl font-bold mb-6 text-center">Tu Perfil</h1>
      <div className="profile-form mb-6 p-6 bg-white rounded-lg shadow-md">
        <div className="mb-4 relative flex items-center justify-center w-fit cursor-pointer group">
          <img src={profileImageURL || userData?.photoURL || user?.photoURL} className="rounded-lg cursor-pointer h-[100px] w-[100px] object-cover " alt="Profile" />
          <input type="file" className="absolute w-full h-full opacity-0 z-[999] cursor-pointer" onChange={handleImageChange} />
          <i className="ri-file-edit-line absolute text-white text-2xl opacity-0 group-hover:opacity-100 transition-opacity duration-300 z-[9995]"></i>
        </div>
        <div className="mb-4">
          <label className="block text-gray-700">Nombre:</label>
          <input
            type="text"
            name="displayName"
            value={userData.displayName}
            onChange={handleInputChange}
            className="w-full px-3 py-2 border rounded"
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700">Email:</label>
          <input
            type="email"
            name="email"
            value={userData.email}
            onChange={handleInputChange}
            className="w-full px-3 py-2 border rounded"
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700">Escuela o instituto:</label>
          <input
            type="text"
            name="school"
            value={userData.school}
            onChange={handleInputChange}
            className="w-full px-3 py-2 border rounded"
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700">Ciudad:</label>
          <input
            type="text"
            name="city"
            value={userData.city}
            onChange={handleInputChange}
            className="w-full px-3 py-2 border rounded"
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700">País:</label>
          <input
            type="text"
            name="country"
            value={userData.country}
            onChange={handleInputChange}
            className="w-full px-3 py-2 border rounded"
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700">Rol:</label>
          <select
            name="role"
            value={userData.role}
            onChange={handleInputChange}
            className="w-full px-3 py-2 border rounded"
          >
            <option value="">Seleccione un rol</option>
            <option value="Profesor">Profesor</option>
            <option value="Jefe de Estudios">Jefe de Estudios</option>
            <option value="Director del Centro">Director del Centro</option>
            <option value="Coordinador">Coordinador</option>
          </select>
        </div>
        <div className="mb-4">
          <label className="block text-gray-700">Asignaturas a enseñar:</label>
          <input
            type="text"
            name="subjects"
            value={userData.subjects}
            onChange={handleInputChange}
            className="w-full px-3 py-2 border rounded"
          />
        </div>

        <button
          onClick={handleUpdateProfile}
          className="px-4 py-2 bg-blue-500 text-white rounded"
        >
          Actualiza el Perfil
        </button>
      </div>
    </div>
  );
};

export default ProfilePage;
