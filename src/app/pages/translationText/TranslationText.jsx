import React, { useState } from 'react'
import AiToolPageHeader from '../../components/aiToolPageHeader/AiToolPageHeader';
import InputField from '../../components/inputFiled/InputFiled';
import FileReaderComponent from '../../components/fileReaderComponent/FileReaderComponent';
import { getLocalStorage } from '../../components/local_storage';
import { RouteConstant } from '../../constants/RouteConstant';
import { useNavigate } from 'react-router-dom';
import Button from '../../components/button/Button';
import useHandleHitAi from '../../hooks/useHandleHitAi';

function TranslationText() {
  const {handleHitAi} = useHandleHitAi({ ai: "chatgpt", toolName: "Traductor",})
  const [data, setData] = useState({
    prompt: "",
    languageTo: "",
  });

  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const prvResponse = getLocalStorage(RouteConstant.TRANSLATOR_OUTPUT);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!data.prompt) { 
      alert("Por favor, proporciona un texto para traducir");
      return;
    } else if (!data.languageTo) {
      alert("Por favor, especifica el idioma al que quieres traducir");
      return;
    }
    setIsLoading(true);
    const aiPrompt = `
You are a powerful AI assistant and a professional translator. Your task is to translate the following content to ${data.languageTo}.

Content to translate:
${data.prompt}

Important instructions:
1. Translate all text content, including any text within tables.
2. If there are any tables in the original content, maintain their structure in the translation using HTML table tags (<table>, <tr>, <td>, etc.).
3. Preserve any formatting or structural elements present in the original text.

The results must be given in this format: HTML
Never use markdown

<h2>Traducción al ${data.languageTo}</h2>

[Insert your translation here, including any tables in HTML format]

Remember to maintain the structure and formatting of the original content, especially for tables and lists.
`;
console.log(aiPrompt);

    try {
      await handleHitAi(aiPrompt, RouteConstant.TRANSLATOR_OUTPUT);
      navigate(RouteConstant.TRANSLATOR_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Traductor de textos a cualquier idioma"
        description="Traduce cualquier texto a cualquier idioma, ya sea pegándolo o subiendo un archivo"
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <div className="flex w-full items-end justify-center gap-2 col-span-2">
          <InputField
            placeholder="Pega el texto aquí o sube un archivo para traducir"
            onChange={handleChange}
            className="w-[80%]"
            value={data.prompt}
            name="prompt"
            label="Texto o archivo a traducir"
          />
          <FileReaderComponent 
            className="w-[20%]" 
            onChange={(fileData) => {
              setData({ ...data, prompt: fileData });
            }} 
          />
        </div>
        <InputField
          placeholder="Idioma al que quieres traducir"
          onChange={handleChange}
          className="w-full"
          value={data.languageTo}
          name="languageTo"
          label="Idioma al que quieres traducir"
        />
        <div className="col-span-2 flex align-center gap-2">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={isLoading}
          >
            {isLoading ? "Traduciendo..." : "Traducir"}
          </Button>
          {prvResponse && (
            <Button
              className="w-[20%]"
              onClick={() => {
                navigate(RouteConstant.TRANSLATOR_OUTPUT);
              }}
            >
              Generación Anterior
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default TranslationText;