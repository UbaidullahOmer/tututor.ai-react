// query.js
const url = "https://flowise-production-ea49.up.railway.app/api/v1/prediction/265cee64-e433-4116-b826-b1aa5e00cb02";
const headers = {
  "Content-Type": "application/json",
  "Authorization": "Bearer TSwZdNlj0X-wj7uzxsRglg5hWKWroZg7xu2t2jVINZw"
};

async function query(data) {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers,
      body: JSON.stringify(data)
    });

    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }

    const result = await response.json();
    return result;
  } catch (error) {
    console.error('Error in query function:', error.message);
    return null;
  }
}

export default query;