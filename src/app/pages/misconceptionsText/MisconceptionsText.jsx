import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";

function MisconceptionsText() {
  const { handleHitAi, loading } = useHandleHitAi({ ai: "chatgpt", toolName: "Conceptos Erróneos", });
  const [data, setData] = useState({
    prompt: "",
    course: "",
    subject: "",
  });
  const prvResponse = getLocalStorage(RouteConstant.MISCONCEPTIONS_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      !data.prompt ||
      !data.course ||
      !data.subject
    ) {
      alert("Please fill in all fields");
      return;
    }
    const aiPrompt = `As a powerful AI assistant specializing in identifying and addressing common misconceptions in educational content, please create the following HTML-structured text:
  <h1>Identificador y Corrector de Ideas Equivocadas Comunes</h1>
  
  <p>Detalles del curso:</p>
  <ul>
    <li><strong>Curso:</strong> ${data.course}</li>
    <li><strong>Asignatura:</strong> ${data.subject}</li>
    <li><strong>Número de Estudiantes:</strong> ${data.numberOfStudent}</li>
    <li><strong>Tiempo de Trabajo:</strong> ${data.workTime} horas</li>
  </ul>

  <p>Contenido a analizar:</p>
  <p>${data.prompt}</p>

  <h2>Instrucciones:</h2>
  
  <ol>
    <li>
      <h3>Introducción</h3>
      <p>Escribe un párrafo breve que explique la importancia de identificar y corregir ideas equivocadas en este tema específico.</p>
    </li>
    
    <li>
      <h3>Ideas Equivocadas Comunes</h3>
      <p>Identifica y explica 3 ideas equivocadas comunes relacionadas con el contenido. Para cada una, incluye:</p>
      <ul>
        <li>La idea equivocada expresada claramente</li>
        <li>La explicación correcta del concepto</li>
        <li>Un ejemplo o analogía que ayude a clarificar el concepto correcto</li>
      </ul>
    </li>
    
    <li>
      <h3>Estrategias de Corrección</h3>
      <p>Sugiere 2-3 estrategias o actividades que los profesores puedan utilizar para ayudar a los estudiantes a superar estas ideas equivocadas.</p>
    </li>
    
    <li>
      <h3>Conclusión</h3>
      <p>Concluye con un párrafo que enfatice la importancia de abordar proactivamente estas ideas equivocadas para mejorar la comprensión general del tema.</p>
    </li>
  </ol>

  <p>Responde en español, asegurándote de que las explicaciones sean claras y apropiadas para el nivel del curso especificado. El objetivo es proporcionar a los educadores una herramienta útil para identificar y corregir ideas equivocadas comunes, mejorando así la comprensión de los estudiantes sobre el tema.</p>

Generate aways the content in this format: HTML`;

console.log(aiPrompt);

    setIsLoading(true);
    try {
      await handleHitAi(aiPrompt, RouteConstant.MISCONCEPTIONS_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Corrector de Ideas Comunes Equivocadas"
        description="Esta herramienta corrige errores comunes que la gente suele tener sobre diversos temas. Introduce un concepto o idea y obtendrás los errores comunes que se suelen tener sobre ese tema específico"
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <InputField
          placeholder="Tema o conceptos comunmente erróneos. Ej: Uso incorrecto de 'a' y 'ha' en español. Ej: Errores comunes de la I Guerra Mundial, etc"
          onChange={handleChange}
          className="col-span-2"
          value={data.prompt}
          name="prompt"
          label={"Temas o conceptos de errores comunes"}
        />
        <InputFieldDropdown
          placeholder="Curso"
          onChange={(value) => {
            setData({ ...data, course: value });
          }}
          name="course"
          type="number"
          value={data.course}
          options={Dropdown.courses}
          require={true}
          label={"Curso"}
        />
        <InputFieldDropdown
          name={"Subject"}
          value={data.subject}
          onChange={(value) => {
            setData({ ...data, subject: value });
          }}
          require={true}
          placeholder={"Asignatura"}
          options={Dropdown.subject}
          label={"Asignatura"}
        />
        <div className="col-span-2 flex  align-center gap-2 ">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={loading}
          >
            {isLoading ? "Loading..." : "Generate"}
          </Button>
          {prvResponse && (
            <Button
              className={"w-[20%]"}
              onClick={() => {
                navigate(RouteConstant.MISCONCEPTIONS_OUTPUT);
              }}
            >
              Previous
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default MisconceptionsText 