import React, { useState, useEffect, useRef } from "react";
import ReactMarkdown from 'react-markdown';
import rehypeRaw from 'rehype-raw';
import { Search, Plus, MoreVertical, Send, Paperclip, Mic } from 'lucide-react';
import query from "./query";

interface Message {
  id: number;
  sender: 'user' | 'bot';
  content: string;
  time: string;
}

interface Conversation {
  id: string;
  name: string;
  messages: Message[];
  lastMessage: string;
  time: string;
}

function FieldTripPlanner() {
  const [userInput, setUserInput] = useState<string>('');
  const [messages, setMessages] = useState<Message[]>([]);
  const [conversations, setConversations] = useState<Conversation[]>([]);
  const [activeConversation, setActiveConversation] = useState<string | null>(null);
  const [showOptionsFor, setShowOptionsFor] = useState<string | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const chatContainerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const loadConversations = async () => {
      try {
        setIsLoading(true);
        const savedConversations = JSON.parse(localStorage.getItem('fieldTripPlannerConversations') || '[]');
        if (Array.isArray(savedConversations) && savedConversations.length > 0) {
          setConversations(savedConversations);
          setActiveConversation(savedConversations[0].id);
          setMessages(savedConversations[0].messages || []);
        } else {
          await handleNewChat();
        }
      } catch (error) {
        console.error("Error loading conversations:", error);
      } finally {
        setIsLoading(false);
      }
    };

    loadConversations();
  }, []);

  useEffect(() => {
    if (chatContainerRef.current) {
      chatContainerRef.current.scrollTop = chatContainerRef.current.scrollHeight;
    }
  }, [messages]);

  useEffect(() => {
    if (conversations.length > 0) {
      localStorage.setItem('fieldTripPlannerConversations', JSON.stringify(conversations));
    }
  }, [conversations]);

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setUserInput(e.target.value);
  };

  const handleSend = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!userInput.trim() || !activeConversation) return;

    const newMessage: Message = {
      id: messages.length + 1,
      sender: 'user',
      content: userInput,
      time: new Date().toLocaleString()
    };
    const updatedMessages = [...messages, newMessage];
    setMessages(updatedMessages);

    try {
      const data = {
        question: userInput,
        overrideConfig: {
          sessionId: activeConversation,
          returnSourceDocuments: true,
          systemMessagePrompt: ""
        }
      };

      const response = await query(data);

      if (response && typeof response.text === 'string') {
        const botMessage: Message = {
          id: updatedMessages.length + 1,
          sender: 'bot',
          content: response.text,
          time: new Date().toLocaleString()
        };
        const finalMessages = [...updatedMessages, botMessage];
        setMessages(finalMessages);

        setConversations(prevConversations =>
          prevConversations.map(conv =>
            conv.id === activeConversation
              ? { 
                  ...conv, 
                  messages: finalMessages,
                  lastMessage: userInput,
                  time: new Date().toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })
                }
              : conv
          )
        );
      }
    } catch (error) {
      console.error("Error sending message:", error);
    }

    setUserInput('');
  };

  const handleNewChat = async () => {
    const newConversation: Conversation = {
      id: Date.now().toString(),
      name: `Salida ${conversations.length + 1}`,
      messages: [{
        id: 1,
        sender: 'bot',
        content: `
# Planificador de Salidas Educativas

¡Bienvenido al planificador de salidas educativas! Aquí te ayudo a organizar:

- Planificación de itinerarios: Diseña rutas educativas detalladas.
- Coordinación de transporte: Opciones y logística para el viaje.
- Actividades en el destino: Sugerencias de actividades educativas.
- Seguridad y permisos: Asegúrate de tener todo en orden.

¿A dónde te gustaría ir hoy?
        `,
        time: new Date().toLocaleString()
      }],
      lastMessage: "Nueva salida iniciada",
      time: new Date().toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })
    };
    setConversations(prev => [newConversation, ...prev]);
    setActiveConversation(newConversation.id);
    setMessages(newConversation.messages);
  };

  const handleEditConversation = (id: string, newName: string) => {
    setConversations(conversations.map(conv =>
      conv.id === id ? { ...conv, name: newName } : conv
    ));
  };

  const handleDeleteConversation = (id: string) => {
    setConversations(prev => prev.filter(conv => conv.id !== id));
    if (activeConversation === id) {
      if (conversations.length > 1) {
        const newActiveConversation = conversations.find(conv => conv.id !== id);
        if (newActiveConversation) {
          setActiveConversation(newActiveConversation.id);
          setMessages(newActiveConversation.messages || []);
        }
      } else {
        handleNewChat();
      }
    }
  };

  const handleSelectConversation = (id: string) => {
    setActiveConversation(id);
    const selectedConversation = conversations.find(conv => conv.id === id);
    if (selectedConversation) {
      setMessages(selectedConversation.messages || []);
    }
  };

  if (isLoading) {
    return <div className="flex justify-center items-center h-screen text-xl text-gray-600">Cargando...</div>;
  }

  return (
    <div className="flex flex-col h-[90vh] bg-gray-50 text-gray-800 font-sans">
      <div className="flex flex-1 overflow-hidden">
        {/* Sidebar */}
        <div className="w-72 bg-white border-r border-gray-200 shadow-lg">
          <div className="p-4">
            <div className="mb-4 relative">
              <input 
                type="text" 
                placeholder="Buscar"
                className="w-full px-3 py-2 pl-10 bg-gray-100 rounded-full text-sm focus:outline-none focus:ring-2 focus:ring-blue-500"
              />
              <Search className="absolute left-3 top-1/2 transform -translate-y-1/2 text-gray-400" size={18} />
            </div>
            <button 
              className="w-full bg-gradient-to-r from-green-400 to-blue-600 text-white py-3 px-4 rounded-lg mb-4 text-center flex items-center justify-center hover:from-green-500 hover:to-blue-700 transition duration-300 shadow-md" 
              onClick={() => handleNewChat()}
            >
              <Plus size={20} className="mr-2" /> Nueva Salida
            </button>
            <div className="space-y-4">
              {conversations.map((conv) => (
                <div key={conv.id} className="relative group">
                  <div 
                    className={`flex items-center p-3 rounded-lg cursor-pointer transition ${activeConversation === conv.id ? 'bg-blue-50' : 'hover:bg-gray-50'}`}
                    onClick={() => handleSelectConversation(conv.id)}
                  >
                    <div className="w-12 h-12 bg-gradient-to-br from-purple-500 to-indigo-600 rounded-full flex items-center justify-center text-white font-bold text-xl">
                      {conv.name.charAt(0)}
                    </div>
                    <div className="flex-grow min-w-0 mx-3">
                      <h3 className="font-semibold text-sm truncate text-gray-800">{conv.name}</h3>
                      <p className="text-xs text-gray-500 truncate">{conv.lastMessage}</p>
                    </div>
                    <span className="text-xs text-gray-400 whitespace-nowrap">{conv.time}</span>
                  </div>
                  <div className="absolute right-2 top-1/2 transform -translate-y-1/2 flex items-center opacity-0 group-hover:opacity-100 transition">
                    <button
                      className="text-gray-500 hover:text-gray-700 p-1"
                      onClick={() => setShowOptionsFor(showOptionsFor === conv.id ? null : conv.id)}
                    >
                      <MoreVertical size={20} />
                    </button>
                  </div>
                  {showOptionsFor === conv.id && (
                    <div className="absolute right-0 mt-2 w-48 bg-white rounded-md shadow-lg z-10">
                      <button
                        className="block w-full text-left px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                        onClick={() => {
                          const newName = prompt("Introduce un nuevo nombre para la salida", conv.name);
                          if (newName) handleEditConversation(conv.id, newName);
                          setShowOptionsFor(null);
                        }}
                      >
                        Editar nombre
                      </button>
                      <button
                        className="block w-full text-left px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                        onClick={() => {
                          handleDeleteConversation(conv.id);
                          setShowOptionsFor(null);
                        }}
                      >
                        Eliminar
                      </button>
                    </div>
                  )}
                </div>
              ))}
            </div>
          </div>
        </div>

        {/* Main Content */}
        <div className="flex-1 flex flex-col overflow-hidden bg-white">
          {/* Chat header */}
          <div className="bg-white p-4 flex items-center justify-between border-b shadow-sm">
            <div className="flex items-center">
              <div className="w-10 h-10 bg-gradient-to-r from-purple-500 to-indigo-600 rounded-full mr-3 flex items-center justify-center text-white font-bold text-xl">
                S
              </div>
              <h2 className="font-bold text-xl text-gray-800">Planificador de Salidas Educativas</h2>
            </div>
          </div>

          {/* Messages */}
          <main className="flex-1 overflow-y-auto p-4 bg-gray-50" ref={chatContainerRef}>
            <div className="max-w-3xl mx-auto space-y-4">
              {messages.map((message) => (
                <div key={message.id} className={`flex ${message.sender === 'user' ? 'justify-end' : 'justify-start'}`}>
                  <div className={`max-w-sm lg:max-w-2xl px-5 py-3 rounded-xl shadow-md transition ${
                    message.sender === 'user' ? 'bg-gradient-to-r from-blue-500 to-purple-600 text-white' : 'bg-white border border-gray-200'
                  }`}>
                    <ReactMarkdown 
                      className="markdown-content"
                      rehypePlugins={[rehypeRaw]}
                      components={{
                        p: ({node, ...props}) => <p className="mb-2" {...props} />,
                        ul: ({node, ...props}) => <ul className="list-disc list-inside mb-2" {...props} />,
                        ol: ({node, ...props}) => <ol className="list-decimal list-inside mb-2" {...props} />,
                        li: ({node, ...props}) => <li className="mb-1" {...props} />,
                        h1: ({node, ...props}) => <h1 className="text-2xl font-bold mb-2" {...props} />,
                        h2: ({node, ...props}) => <h2 className="text-xl font-semibold mb-2" {...props} />,
                        h3: ({node, ...props}) => <h3 className="text-lg font-medium mb-2" {...props} />,
                        strong: ({node, ...props}) => <strong className="font-bold" {...props} />,
                        em: ({node, ...props}) => <em className="italic" {...props} />,
                      }}
                    >
                      {message.content}
                    </ReactMarkdown>
                    <span className="text-xs opacity-50 mt-1 block text-right">{message.time}</span>
                  </div>
                </div>
              ))}
            </div>
          </main>

          {/* Message input */}
          <footer className="border-t border-gray-200 p-4 bg-white shadow-lg">
            <div className="max-w-full mx-auto">
              <form onSubmit={handleSend} className="bg-gray-100 rounded-full flex items-center p-3 w-full shadow-md">
                <button type="button" className="p-2 text-gray-500 hover:text-gray-700">
                  <Paperclip size={20} />
                </button>
                <input
                  type="text"
                  value={userInput}
                  onChange={handleInputChange}
                  placeholder="Escribe un mensaje..."
                  className="flex-grow bg-transparent border-none focus:outline-none text-gray-800 placeholder-gray-500 px-4"
                />
                <button type="button" className="p-2 text-gray-500 hover:text-gray-700">
                  <Mic size={20} />
                </button>
                <button type="submit" className="bg-gradient-to-r from-blue-500 to-purple-600 text-white rounded-full p-3 ml-2 hover:from-blue-600 hover:to-purple-700 transition duration-300">
                  <Send size={20} />
                </button>
              </form>
            </div>
          </footer>
        </div>
      </div>
    </div>
  );
}

export default FieldTripPlanner;
