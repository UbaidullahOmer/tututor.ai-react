import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import useAuth from '../../hooks/useAuth';
import { useSnackBarManager } from '../../hooks/useSnackBarManager';
import './styles.css';

const Signup = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { signUpWithEmailAndPassword, signInWithGoogle, signInWithFacebook } = useAuth();
  const { fnShowSnackBar } = useSnackBarManager();
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const result = await signUpWithEmailAndPassword(email, password);
      if (result.user) {
        fnShowSnackBar("Registro exitoso", "success");
        navigate("/dashboard");
      } else {
        // Si no hay usuario en el resultado, asumimos que hubo un error
        throw new Error("Error en el registro");
      }
    } catch (error) {
      console.error("Error during signup:", error);
      fnShowSnackBar("Email o contraseña incorrectos, inténtelo de nuevo", "error");
      setPassword(''); // Limpiar solo la contraseña
    }
  };

  const handleGoogleSignIn = async () => {
    try {
      await signInWithGoogle();
      fnShowSnackBar("Iniciaste sesión con Google exitosamente", "success");
      navigate("/dashboard");
    } catch (error) {
      console.error("Error during Google sign in:", error);
      fnShowSnackBar("Error al iniciar sesión con Google. Inténtelo de nuevo.", "error");
    }
  };

  const handleFacebookSignIn = async () => {
    try {
      await signInWithFacebook();
      fnShowSnackBar("Iniciaste sesión con Facebook exitosamente", "success");
      navigate("/dashboard");
    } catch (error) {
      console.error("Error during Facebook sign in:", error);
      fnShowSnackBar("Error al iniciar sesión con Facebook. Inténtelo de nuevo.", "error");
    }
  };

  return (
    <div className="flex flex-col items-center justify-center min-h-screen bg-white p-4">
      <div className="w-full max-w-md">
        <div className="flex justify-center mb-4">
          <img src="icons/logobackground.png" alt="Logo" className="h-20" />
        </div>
        <h2 className="text-2xl font-bold text-center mb-6">Regístrate</h2>
        <form onSubmit={handleSubmit} className="space-y-4">
          <div>
            <input
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              className="w-full p-3 border border-gray-300 rounded-md focus:ring-2 focus:ring-purple-600 focus:border-purple-600"
              placeholder="Correo electrónico"
              required
            />
          </div>
          <div>
            <input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              className="w-full p-3 border border-gray-300 rounded-md focus:ring-2 focus:ring-purple-600 focus:border-purple-600"
              placeholder="Contraseña"
              required
            />
          </div>
          <button
            type="submit"
            className="w-full gradient-bg button-style"
          >
            Registrarse
          </button>
        </form>
        <div className="mt-4 flex flex-col space-y-3">
          <button
            onClick={handleGoogleSignIn}
            className="w-full p-3 border border-gray-300 rounded-md flex items-center justify-center space-x-2 hover:bg-gray-50 transition duration-300"
          >
            <img src="icons/google.png" alt="Google" className="w-6 h-6" />
            <span>Regístrate con Google</span>
          </button>
          <button
            onClick={handleFacebookSignIn}
            className="w-full p-3 border border-gray-300 rounded-md flex items-center justify-center space-x-2 hover:bg-gray-50 transition duration-300"
          >
            <img src="icons/facebook.png" alt="Facebook" className="w-6 h-6" />
            <span>Registrate con Facebook</span>
          </button>
        </div>
        <div className="mt-4 text-center text-sm">
          <p className="text-gray-600">
            Al registrarte, aceptas la{' '}
            <Link to="/privacy-policy" className="text-purple-600 hover:underline">
              política de privacidad
            </Link>{' '}
            y los{' '}
            <Link to="/terms-of-service" className="text-purple-600 hover:underline">
              términos de servicio
            </Link>
            .
          </p>
        </div>
        <div className="mt-4 text-center">
          <p className="text-gray-600">
            ¿Ya tienes una cuenta?{' '}
            <Link to="/login" className="text-purple-600 hover:underline">
              Iniciar sesión
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Signup;