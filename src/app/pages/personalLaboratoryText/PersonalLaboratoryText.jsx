import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";

function PersonalLaboratoryText() {
  const { handleHitAi, loading } = useHandleHitAi({ ai: "chatgpt", toolName: "Laboratorio Personal", });
  const [data, setData] = useState({
    course: "",
    subject: "",
    contentToCover: "",
    complexityLevel: "",
    materials: "",
    learningObjective: "",
  });
  const prvResponse = getLocalStorage(RouteConstant.PERSONAL_LABORATORY_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      !data.course ||
      !data.subject ||
      !data.contentToCover ||
      !data.complexityLevel ||
      !data.materials ||
      !data.learningObjective
    ) {
      alert("Please fill in all fields");
      return;
    }
    const aiPrompt = `
Como experto en diseño de actividades de laboratorio, crea un laboratorio basado en:

Curso: ${data.course}
Asignatura: ${data.subject}
Contenido a cubrir: ${data.contentToCover}
Nivel de complejidad: ${data.complexityLevel}
Materiales: ${data.materials}
Objetivo de aprendizaje: ${data.learningObjective}

Diseña una actividad de laboratorio que incluya:

<h2>Título del laboratorio</h2>
<h2>Introducción y objetivos</h2>
<p>2-3 frases que describan la introducción y los objetivos del laboratorio.</p>
<h2>Preguntas de investigación o hipótesis</h2>
<ul>
  <li>Pregunta 1</li>
  <li>Pregunta 2</li>
  <li>Pregunta 3</li>
</ul>
<h2>Lista de materiales y equipo necesarios</h2>
<ul>
  <li>Material 1</li>
  <li>Material 2</li>
  <li>Material 3</li>
</ul>
<h2>Procedimiento detallado paso a paso</h2>
<ol>
  <li>Paso 1</li>
  <li>Paso 2</li>
  <li>Paso 3</li>
</ol>
<h2>Duración del experimento</h2>
<ol>
  <li>Detalla claramente el tiempo que se tardará en llevar a cabo el experimento</li>
</ol>
<h2>Pautas de seguridad y precauciones</h2>
<ul>
  <li>Pauta 1</li>
  <li>Pauta 2</li>
  <li>Pauta 3</li>
</ul>
<h2>Sugerencias para la recolección y análisis de datos</h2>
<ul>
  <li>Sugerencia 1</li>
  <li>Sugerencia 2</li>
  <li>Sugerencia 3</li>
</ul>
<h2>Preguntas de discusión o reflexión post-laboratorio</h2>
<ul>
  <li>Pregunta 1</li>
  <li>Pregunta 2</li>
  <li>Pregunta 3</li>
</ul>

Asegúrate de:
- Adaptar el laboratorio al nivel del curso y la complejidad especificados
- Alinear la actividad con el objetivo de aprendizaje proporcionado
- Detallar los conceptos y habilidades clave que se cubrirán
- Optimizar el uso de los materiales disponibles
- Incluir oportunidades para el pensamiento crítico y la resolución de problemas 
- Se creativo y el paso a paso asegúrate que está muy bien detallado y es fácil de entender.
Sugerencias adicionales:
- Proporciona antecedentes teóricos relevantes para el laboratorio
- Sugiere modificaciones o extensiones para diferentes escenarios
- Indica cómo el laboratorio se conecta con los conceptos más amplios del curso
- Considera la inclusión de un componente de trabajo en equipo, si es apropiado
- Siempre avisa que los experimentos los haga en compañía de un adulto, porque puede ser peligroso.
- Menciona también el tiempo que se puede tardar en hacer el experimento

Responde en español y asegúrate de que el laboratorio sea educativo, práctico y alineado con el objetivo de aprendizaje especificado.
El resultado debe ser proporcionado en este formato: HTML. Nunca hagas nada en markdown.
`;

console.log(aiPrompt);

    setIsLoading(true);
    try {
      await handleHitAi(aiPrompt, RouteConstant.PERSONAL_LABORATORY_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Creador de Experimentos Personalizados: Tu Laboratorio Virtual"
        description="¿Quieres practicar experimentos relacionados con los contenidos que enseñas en clase? Esta herramienta te generará experimentos personalizados y emocionantes, permitiéndote explorar y enseñar ciencia de manera práctica y divertida. ¡Transforma tu lugar en un centro de descubrimiento e innovación!"
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <InputFieldDropdown
          placeholder="Curso"
          onChange={(value) => {
            setData({ ...data, course: value });
          }}
          name="course"
          type="number"
          value={data.course}
          options={Dropdown.courses}
          require={true}
          label={"Curso"}
        />
        <InputFieldDropdown
          name={"Subject"}
          value={data.subject}
          onChange={(value) => {
            setData({ ...data, subject: value });
          }}
          require={true}
          placeholder={"Asignatura"}
          options={Dropdown.subject}
          label={"Asignatura"}
        />
        <InputFieldDropdown
          name={"complexityLevel"}
          value={data.complexityLevel}
          onChange={(value) => {
            setData({ ...data, complexityLevel: value });
          }}
          require={true}
          placeholder={"Nivel de Complejidad"}
          options={["Muy fácil", "Fácil", "Moderado", "Difícil", "Muy difícil"]}
          label={"Nivel de Complejidad"}
        />
        <TextArea
          name={"contentToCover"}
          value={data.contentToCover}
          onChange={handleChange}
          placeholder={"Ejemplo: Reacciones químicas"}
          require={true}
          label={"Contenido a cubrir"}
        />
        <TextArea
          name={"materials"}
          value={data.materials}
          onChange={handleChange}
          placeholder={"Ejemplo: Vinagre, bicarbonato de sodio, colorante alimentario, vasos de plástico, cucharas, bandeja. Ej: Materiales que una persona pueda tener normalmente por casa"}
          require={true}
          label={"Materiales"}
        />
        <TextArea
          name={"learningObjective"}
          value={data.learningObjective}
          onChange={handleChange}
          placeholder={"Ejemplo: Comprender las reacciones entre ácidos y bases y observar los cambios químicos. Ej: Aprender sobre la gravedad de los objetos, etc."}
          require={true}
          label={"Objetivos de Aprendizaje"}
        />
        <div className="col-span-2 flex  align-center gap-2 ">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={loading}
          >
            {isLoading ? "Cargando..." : "Generar experimento"}
          </Button>
          {prvResponse && (
            <Button
              className={"w-[20%]"}
              onClick={() => {
                navigate(RouteConstant.PERSONAL_LABORATORY_OUTPUT);
              }}
            >
              Generación Anterior
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}
export default PersonalLaboratoryText;
