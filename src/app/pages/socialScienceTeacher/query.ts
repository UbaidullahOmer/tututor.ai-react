const url = "https://flowise-production-ea49.up.railway.app/api/v1/prediction/42fe6925-dd10-4924-8ceb-856ca05b6cd1";
const headers = {
  "Content-Type": "application/json",
  "Authorization": "Bearer sL7iCHCQlmHnC4PH6T8nZXBTMruIGu01Z1lUtMluhtA"
};

async function query(data: any) {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers: headers,
      body: JSON.stringify(data)
    });

    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }

    const result = await response.json();
    return result;
  } catch (error) {
    console.error('Error in query function:', error);
    return null;
  }
}

export default query;