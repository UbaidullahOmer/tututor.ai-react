import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";

function ExercisesAdaptedText() {
  const { handleHitAi, loading } = useHandleHitAi({ ai: "chatgpt", toolName: "Ejercicios Adaptados al Estudiante" });
  const [data, setData] = useState({
    course: "",
    subject: "",
    topicToWork: "",
    theme: "",
    ammount: ""
  });
  const prvResponse = getLocalStorage(RouteConstant.EXERCISES_ADAPTED_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!data.course || !data.subject || !data.topicToWork || !data.ammount || !data.theme) {
      alert("Por favor, rellena todos los campos");
      return;
    }
    const adaptedExerciseGeneratorPrompt = `Como experto en educación, crea ${data.ammount} ejercicios adaptados basados en:

Curso: ${data.course}
Asignatura: ${data.subject}
Tema a trabajar: ${data.topicToWork}. Si el usuario ha escrito "Haz tu magia" se creativo y haz una temática creativa y que se adapte a la edad de los niños: ${data.course}., contenido a desarrollar: ${data.topicToWork}.  Y adapta siempre el tema a la asignatura: ${data.subject}. Por ejemplo, una temática a inventar puede ser relacionada con pelíuclas, series, dibujos animados, programas, etc. El número de ejercicios a hacer es: ${data.ammount}. Nunca generes imágenes
Tema específico: ${data.theme}
Para cada ejercicio, incluye:

<h2>1. Título breve</h2>
<p>Incluye un título breve para el ejercicio.</p>

<h2>2. Objetivo de aprendizaje</h2>
<p>Indica el objetivo de aprendizaje del ejercicio.</p>

<h2>3. Nivel de dificultad (Fácil, Medio, Difícil)</h2>
<p>Especifica el nivel de dificultad del ejercicio.</p>

<h2>4. Instrucciones concisas</h2>
<p>Proporciona instrucciones concisas para realizar el ejercicio.</p>

<h2>5. El ejercicio</h2>
<p>Incluye el ejercicio, que puede variar entre preguntas de opción múltiple, problemas para resolver, actividades prácticas, etc.</p>

<h2>6. Solución o respuesta correcta</h2>
<p>Proporciona la solución o respuesta correcta del ejercicio.</p>

Asegúrate de:
<ul>
  <li>Variar los tipos de ejercicios y niveles de dificultad</li>
  <li>Incluir al menos un ejercicio que fomente el pensamiento crítico o la aplicación práctica</li>
  <li>Usar lenguaje apropiado para la edad y nivel del curso</li>
  <li>Mantener los ejercicios relevantes para el tema específico</li>
</ul>

Opcional: Sugiere una breve actividad de extensión para profundizar en el tema.

Responde en español y sé creativo mientras mantienes la practicidad educativa.
El resultado debe ser proporcionado en este formato: HTML. Nunca des nada en formato markdown
`;

    console.log(adaptedExerciseGeneratorPrompt);

    setIsLoading(true);
    try {
      await handleHitAi(adaptedExerciseGeneratorPrompt, RouteConstant.EXERCISES_ADAPTED_OUTPUT);
      navigate(RouteConstant.EXERCISES_ADAPTED_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      alert("Ocurrió un error al generar los ejercicios. Por favor, inténtalo de nuevo.");
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Ejercicios Adaptados"
        description="¿Quieres ejercicios que motiven a tus alumnos y con temáticas actuales que les gusten? Esta herramienta te hará ejercicios adaptados a tu contenido y a los gustos de tus alumnos."
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <InputFieldDropdown
          placeholder="Curso"
          onChange={(value) => {
            setData({ ...data, course: value });
          }}
          name="course"
          type="number"
          value={data.course}
          options={Dropdown.courses}
          required
          label="Curso"
        />
        <InputFieldDropdown
          name="subject"
          value={data.subject}
          onChange={(value) => {
            setData({ ...data, subject: value });
          }}
          placeholder="Asignatura"
          options={Dropdown.subject}
          required
          label="Asignatura"
        />
        <TextArea name={'topicToWork'}
          value={data.topicToWork}
          placeholder={'Ej: multiplicaciones de 2 cifras; Ej: Ríos de España'}
          onChange={handleChange}
          label={'Contenido a desarrollar'}
        />
        <TextArea name={'theme'}
          value={data.theme}
          placeholder={'Ej: Películas, series, programas concretos, dibujos animados, etc.. Si no sabes qué temática usar, escribe "Haz tu magia" y la IA buscará una temática por tí'}
          onChange={handleChange}
          label={'Temática a desarrollar'}
        />
        <InputField
          name="ammount"
          value={data.ammount}
          onChange={handleChange}
          type="number"
          placeholder="Ej: 10, 15, etc"
          label="Número de ejercicios"
          required
        />
        <div className="col-span-2 flex align-center gap-2">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={loading || isLoading}
          >
            {isLoading ? "Generando..." : "Generar ejercicios personalizados"}
          </Button>
          {prvResponse && (
            <Button
              className="w-[20%]"
              onClick={() => {
                navigate(RouteConstant.EXERCISES_ADAPTED_OUTPUT);
              }}
              disabled={loading || isLoading}
            >
              Anterior
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default ExercisesAdaptedText;