import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";
import FileReaderInputFIled from "../../components/fileReaderInputFIled/FileReaderInputFIled";

const FormField = ({ title, children, required = false }) => (
  <div className="mb-2">
    <label className="block font-bold text-xs mb-1">
      {title} {required && <span className="text-red-500">*</span>}
    </label>
    {children}
  </div>
);

function PersonalizedEducationText() {
  const [data, setData] = useState({
    subject: "",
    course: "",
    specialEducationalNeeds: "",
    studentProfile: "",
    descriptionOfStrength: "",
    topicDevelop: "",
    selectedFiles: [],
  });

  const { handleHitAi, loading } = useHandleHitAi({ 
    ai: "chatgpt",
    selectedFiles: data.selectedFiles,
    toolName: "Plan de Educación Personalizado",
  });

  const prvResponse = getLocalStorage(RouteConstant.LEARNING_SITUATIONS_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      !data.course ||
      !data.subject ||
      !data.specialEducationalNeeds ||
      !data.studentProfile ||
      !data.descriptionOfStrength
    ) {
      alert("Por favor, seleccione todos los campos para generar el contenido");
      return;
    }
    let personalizedEducationPlanPrompt = `
Como experto en educación especial, crea un Plan de Educación Personalizada (PEP) conciso y práctico para un estudiante con necesidades educativas especiales, basado en:

Asignatura: ${data.subject}
Curso: ${data.course}
Tipo de Necesidad Educativa Especial: ${data.specialEducationalNeeds}
Perfil del Estudiante: ${data.studentProfile}
Descripción de Fortalezas y Áreas de Apoyo: ${data.descriptionOfStrength}
Contenido a tratar con la persona en clase: ${data.topicDevelop}. Es POCIONAL

Proporciona un PEP estructurado con los siguientes elementos:
Comentarios adicionales del profesor: ${data.descriptionOfStrength}

Por favor, proporciona un Plan de Educación Personalizada estructurado con actividdes claras y detalladas que se puedan hacerlo con sus alumnos con necesidades especiales que incluya los siguientes elementos:

<h2>1. Actividades y Ejercicios Específicos:</h2>
<p>Describe 5 actividades o ejercicios concretos, adaptados a la necesidad educativa especial del estudiante.</p>

<h3>Actividad 1: [Nombre de la Actividad]</h3>
<ul>
  <li><strong>Objetivo:</strong> [Objetivo específico]</li>
  <li><strong>Materiales:</strong> [Lista de materiales necesarios]</li>
  <li><strong>Pasos:</strong>
    <ol>
      <li>[Paso 1]</li>
      <li>[Paso 2]</li>
      <li>[Paso 3]</li>
    </ol>
  </li>
  <li><strong>Duración:</strong> [Tiempo estimado]</li>
  <li><strong>Adaptación:</strong> [Cómo adaptar la actividad según el progreso]</li>
</ul>

[Repite esta estructura para cada actividad]

<h2>2. Estrategias de Enseñanza:</h2>
<p>Enumera 3 estrategias de enseñanza específicas que el profesor puede utilizar diariamente.</p>

<h3>Estrategia 1: [Nombre de la Estrategia]</h3>
<p><strong>Descripción:</strong> [Breve descripción de la estrategia]</p>
<p><strong>Implementación:</strong> [Cómo implementar la estrategia, con ejemplos concretos]</p>

[Repite esta estructura para cada estrategia]

<h2>3. Adaptaciones en el Aula:</h2>
<p>Lista 3 adaptaciones específicas del entorno de aprendizaje.</p>

<ul>
  <li><strong>Adaptación 1:</strong> [Descripción de la adaptación]</li>
  <li><strong>Adaptación 2:</strong> [Descripción de la adaptación]</li>
  <li><strong>Adaptación 3:</strong> [Descripción de la adaptación]</li>
</ul>

<h2>4. Evaluación y Seguimiento:</h2>
<p>Proporciona 2-3 métodos de evaluación adaptados, con instrucciones claras para su aplicación.</p>

<h3>Método de Evaluación 1: [Nombre del Método]</h3>
<p><strong>Descripción:</strong> [Breve descripción del método]</p>
<p><strong>Aplicación:</strong> [Instrucciones claras para su aplicación]</p>
<p><strong>Criterios de Progreso:</strong> [Criterios concretos para medir el progreso]</p>

[Repite esta estructura para cada método de evaluación]

<h2>5. Plan de Apoyo:</h2>
<ul>
  <li><strong>Servicios de Apoyo:</strong> [Especifica los servicios necesarios, su frecuencia y duración]</li>
  <li><strong>Roles del Equipo de Apoyo:</strong>
    <ul>
      <li>[Rol 1]: [Descripción de responsabilidades]</li>
      <li>[Rol 2]: [Descripción de responsabilidades]</li>
    </ul>
  </li>
</ul>

IMPORTANTE: 
- Usa ÚNICAMENTE HTML para el formateo. NO uses Markdown ni texto sin formato.
- Asegúrate de que cada sección esté claramente estructurada y separada.
- Utiliza solamente las etiquetas HTML apropiadas para todo el formateo necesario (<strong>, <em>, <ul>, <ol>, <li>, <h1>, <h2>, etc.).
- NO uses asteriscos, guiones, o cualquier otro símbolo que pueda ser interpretado como Markdown.
- Mantén una estructura consistente a lo largo de todo el documento.

Recuerda: El PEP debe ser específico para el tipo de necesidad educativa especial mencionada, aprovechando las fortalezas del estudiante y abordando las áreas que necesitan apoyo. 
Usa un lenguaje claro y directo, enfocándote en acciones concretas que el profesor pueda implementar inmediatamente.
`;

    if (data.selectedFiles.length > 0) {
      const imageFiles = data.selectedFiles.filter((file) =>
        file.type.startsWith("image/")
      );
      if (imageFiles.length > 0) {
        personalizedEducationPlanPrompt +=
          "\nPor favor, analiza las imágenes subidas e incluye información relevante basada en su contenido en el plan de educación personalizada.";
      }
    }

    console.log(personalizedEducationPlanPrompt);

    setIsLoading(true);
    try {
      await handleHitAi(personalizedEducationPlanPrompt, RouteConstant.LEARNING_SITUATIONS_OUTPUT);
      navigate(RouteConstant.LEARNING_SITUATIONS_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      alert("An error occurred while generating the personalized education plan. Please try again.");
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Plan de Educación Personalizado"
        description="¿Tienes un alumno con TDHA, autismo o alumnos extranjeros que no hablan bien el idioma? Con esta herramienta podrás personalizar tu plan de educación a esos alumnos con necesidades educativas especiales"
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <InputFieldDropdown
          placeholder="Curso"
          onChange={(value) => {
            setData({ ...data, course: value });
          }}
          name="course"
          type="number"
          value={data.course}
          options={Dropdown.courses}
          require={true}
          label={"Curso"}
        />
        <InputFieldDropdown
          name={"Subject"}
          value={data.subject}
          onChange={(value) => {
            setData({ ...data, subject: value });
          }}
          placeholder={"Asignatura"}
          options={Dropdown.subject}
          require={true}
          label={"Asignatura"}
        />
        <TextArea
          name={"specialEducationalNeeds"}
          value={data.specialEducationalNeeds}
          onChange={handleChange}
          placeholder={"Trastorno por Déficit de Atención e Hiperactividad (TDAH)"}
          label={"Necesidades educativas especiales del alumno"}
        />
        <TextArea
          name={"studentProfile"}
          value={data.studentProfile}
          onChange={handleChange}
          placeholder={"Estudiante con buena capacidad de razonamiento lógico, pero con dificultades para mantener la atención y seguir instrucciones complejas"}
          label={"Perfil del Estudiante"}
        />
        <TextArea
          name={"topicDevelop"}
          value={data.topicDevelop}
          onChange={handleChange}
          placeholder={"Ejemplo: Actualmente estamos trabajando Unidad Didáctica de expresión corporal en Educación Física"}
          label={"Contenido o tarea a desarrollar con la persona. OPCIONAL"}
          require={false}
        />
        <TextArea
          name={"descriptionOfStrength"}
          value={data.descriptionOfStrength}
          onChange={handleChange}
          placeholder={"Cualquier comentario adicional que quieras añadir, por ejemplo algo concreto que estás trabajando y queires aplicarlo, etc."}
          label={"Comentarios Adicionales"}
        />
        <FormField title="Sube tu temario">
          <FileReaderInputFIled
            onChange={handleChange}
            data={data}
            setData={setData}
          />
        </FormField>
        <div className="col-span-2 flex  align-center gap-2 ">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={loading || isLoading}
          >
            {isLoading ? "Generando..." : "Generar Plan Personalizado"}
          </Button>
          {prvResponse && (
            <Button
              className={"w-[20%]"}
              onClick={() => {
                navigate(RouteConstant.LEARNING_SITUATIONS_OUTPUT);
              }}
              disabled={loading || isLoading}
            >
              Anterior
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default PersonalizedEducationText;