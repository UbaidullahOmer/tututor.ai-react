import React, { useState } from "react";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import InputField from "../../components/inputFiled/InputFiled";
import Button from "../../components/button/Button";
import { RouteConstant } from "../../constants/RouteConstant";
import {
  getLocalStorage,
} from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";

function GamificationsText() {
  const {handleHitAi, loading} = useHandleHitAi({ ai: "chatgpt", toolName: "Gamificaciones",})
  const [data, setData] = useState({
    subject: "",
    educationLevel: "",
    learningGoals: "",
    duration: "",
    typeOfActivity: "",
    basicKnowledge: "",
    rewardSystem: "",
    evaluationCriteria: "",
    theme: "",
    workGroup: "",
    technologyTools: "",
  });
  const prvResponse = getLocalStorage(RouteConstant.GAMIFICATIONS_Text);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };
  const aiPrompt = `Eres un experto en crear gamificaciones para maestros y profesores en un contexto educativo, genera el contenido en formato HTML.
  Detalles de la actividad:
    Asignatura: ${data.subject}
    Nivel Educativo: ${data.educationLevel}
    Duración: ${data.duration}
    Tipo de Actividad: ${data.typeOfActivity}. Si el usuario elige Haz tu magia, debes ser craeativo e innovador con el tipo de actividad.
    Criterios de Evaluación: ${data.evaluationCriteria || "No especificado"}
    Tecnología y Herramientas: ${data.technologyTools || "No especificado"}
    Temática: ${data.theme || "No especificado"}.
    Contenidos Básicos: ${data.basicKnowledge}
    Grupos de Trabajo: ${data.workGroup || "No especificado"}
    Sistema de recompensas: ${data.rewardSystem} || Si el usuario elige Haz tu magia en sistema de recompensas, debes ser craeativo e innovador con el sistema de recompensas

<html>
<body>

  <h2>Elementos Clave de la Gamificación:</h2>
  <ol>
    <li>
      <h3>Narrativa y Contexto</h3>
      <p>Crea una historia atractiva y relevante, adaptada a la edad de los estudiantes y alineada con el contenido curricular.</p>
    </li>
    
    <li>
      <h3>Dinámicas de Juego</h3>
      <p>Incluye elementos que fomenten motivación intrínseca, cooperación y competencia saludable, como narrativa, emociones, progresión y relaciones sociales.</p>
    </li>
    
    <li>
      <h3>Mecánicas de Juego</h3>
      <p>Define reglas, elementos de azar, competencia, cooperación, feedback y transacciones. Proporciona 4-5 ejemplos de habilidades o objetos especiales relacionados con la temática que den ventajas en los retos prácticos.</p>
    </li>
    
    <li>
      <h3>Sistema de Recompensas</h3>
      <p>Diseña recompensas prácticas y motivadoras, incluyendo un premio final creativo y realista (por ejemplo, mejoras en calificaciones, actividades especiales o pequeños viajes educativos).</p>
    </li>
    
    <li>
      <h3>Componentes del Juego</h3>
      <p>Incluye y describe brevemente avatares, equipos, emblemas, logros, misiones, desafíos, niveles, puntos y tableros de clasificación.</p>
    </li>
    
    <li>
      <h3>Estética y Puesta en Escena</h3>
      <p>Describe detalles estéticos y proporciona ejemplos concretos de puesta en escena para que el profesor logre una inmersión real de los estudiantes.</p>
    </li>
    
    <li>
      <h3>Rol del Profesor</h3>
      <p>Define claramente las funciones y el papel del profesor en la gamificación, incluyéndolo como un personaje dentro de la narrativa.</p>
    </li>
  </ol>

  <h3>Estructura de Sesiones</h3>
  <p>Desarrolla una estructura clara para ${data.duration} sesiones, incluyendo para cada sesión:</p>
  <ul>
    <li>Objetivos específicos</li>
    <li>Actividades principales</li>
    <li>Elementos de gamificación utilizados</li>
    <li>Progresión de la narrativa</li>
  </ul>
</body>
</html>

Asegúrate de que la gamificación cumpla con lo siguiente:
  - Sea detallada y tenga un mínimo de 1.500 palabras
  - Se adapte al contexto educativo específico y sea flexible
  -  Integre los objetivos de aprendizaje con la temática principal (sin mostrarlos explícitamente)
  -  Proporcione una estructura clara por sesiones
  - Considere las infraestructuras disponibles y las características del grupo de estudiantes
Responde en español, creando una gamificación educativa completa que cumpla con los requisitos especificados.
Generate the content in this HTML format and never include any markdown format.`;

  const handleSubmit = async (e) => {
    e.preventDefault();

    const requiredFields = [
      { field: "subject", message: "Por favor, proporcione una asignatura" },
      { field: "educationLevel", message: "Por favor, proporcione el nivel educativo" },
      { field: "learningGoals", message: "Por favor, proporcione los objetivos de aprendizaje" },
      { field: "duration", message: "Por favor, proporcione la duración" },
      { field: "typeOfActivity", message: "Por favor, proporcione el tipo de actividad" },
      { field: "rewardSystem", message: "Por favor, proporcione el sistema de recompensas" },
    ];

    for (const { field, message } of requiredFields) {
      if (!data[field]) {
        alert(message);
        return;
      }
    }
    setIsLoading(true);
    try {
      await handleHitAi(aiPrompt, RouteConstant.GAMIFICATIONS_OUTPUT);
      navigate(RouteConstant.GAMIFICATIONS_OUTPUT);
    } catch (error) {
      console.error("Error durante la llamada a la API:", error);
      alert("Ocurrió un error al generar la gamificación. Por favor, inténtelo de nuevo.");
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px] max-h-[80vh] overflow-scroll ">
      <AiToolPageHeader
        title="Gamificación Educativa"
        description="Crea contenido de gamificación educativa en español"
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <InputField
          placeholder="Asignatura a gamificar"
          onChange={handleChange}
          className="col-span-2"
          value={data.subject}
          name="subject"
          require={true}
        />
        <InputFieldDropdown
          placeholder="Nivel educativo"
          onChange={(value) => {
            setData({ ...data, educationLevel: value });
          }}
          value={data.educationLevel}
          name="educationLevel"
          require={true}
          options={[
            "Primaria",
            "Secundaria",
            "Bachillerato",
            "Universidad",
          ]}
        />
        <InputField
          placeholder="Objetivos de aprendizaje"
          onChange={handleChange}
          value={data.learningGoals}
          require={true}
          name="learningGoals"
        />
        <InputField
          placeholder="Ej: 2 sesiones, 1 sesión, 4 sesiones, etc"
          require={true}
          onChange={handleChange}
          value={data.duration}
          name="duration"
          label={"Duración de las Sesiones"}
        />
        <InputFieldDropdown
          placeholder="Tipo de actividad"
          onChange={(value) => {
            setData({ ...data, typeOfActivity: value });
          }}
          require={true}
          value={data.typeOfActivity}
          name="typeOfActivity"
          options={[
            "Cuestionarios",
            "Proyectos",
            "Prácticas",
            "Debates",
            "Generación propia",
          ]}
        />
        <InputFieldDropdown
          placeholder="Sistema de recompensas"
          onChange={(value) => {
            setData({ ...data, rewardSystem: value });
          }}
          value={data.rewardSystem}
          name="rewardSystem"
          require={true}
          options={[
            "Puntos",
            "Insignias",
            "Certificados",
            "Privilegios",
            "Haz tu magia 'La IA será creativa en este punto'",
          ]}
        />
        <InputField
          placeholder="Ej: Temática de el Señor de los Anillos. Ej: Temática de los juegos de hambre, etc."
          onChange={handleChange}
          value={data.theme}
          name="theme"
          label={"Temática"}
        />
        <InputField
          placeholder="Grupo de trabajo"
          onChange={handleChange}
          value={data.workGroup}
          name="workGroup"
        />
        <InputField
          placeholder="Herramientas tecnológicas"
          onChange={handleChange}
          value={data.technologyTools}
          name="technologyTools"
        />
        <TextArea
          placeholder="Conocimientos básicos"
          onChange={handleChange}
          value={data.basicKnowledge}
          name="basicKnowledge"
          label={"Conocimientos básicos"}
        />
        <TextArea
          placeholder="Criterios de evaluación"
          onChange={handleChange}
          value={data.evaluationCriteria}
          name="evaluationCriteria"
          label={"Criterios de Evalución"}
        />
        
        <div className="col-span-2 flex align-center gap-2">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={isLoading || loading}
          >
            {isLoading ? "Generando..." : "Generar"}
          </Button>
          {prvResponse && (
            <Button
              className="w-[20%]"
              onClick={() => {
                navigate(RouteConstant.GAMIFICATIONS_OUTPUT);
              }}
              disabled={isLoading || loading}
            >
              Anterior
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default GamificationsText;