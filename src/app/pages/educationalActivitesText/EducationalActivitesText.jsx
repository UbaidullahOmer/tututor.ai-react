import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import TextArea from "../../components/textArea/TextArea";

function EducationalActivitiesText() {
  const { handleHitAi, loading } = useHandleHitAi({ ai: "chatgpt", toolname: "Generador de Actividades Educativas", });
  const [data, setData] = useState({
    course: "",
    subject: "",
    subjectContent: "",
    learningObjectives: "",
    activityType: "",
    additionalInstructions: "",
  });
  const prvResponse = getLocalStorage(
    RouteConstant.EDUCATIONAL_ACTIVITES_OUTPUT
  );
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      !data.course ||
      !data.subject ||
      !data.subjectContent ||
      !data.learningObjectives ||
      !data.additionalInstructions,
      !data.activityType

    ) {
      alert("Por favor, rellena todos los campos obligatorios");
      return;
    }
    const aiPrompt = `
Eres un experto en diseño de actividades educativas. Tu tarea es crear una actividad educativa detallada y efectiva basada en la siguiente información:

Curso: ${data.course}
Asignatura: ${data.subject}
Contenido de la asignatura: ${data.subjectContent}
Objetivos de aprendizaje: ${data.learningObjectives}
Tipo de actividad: ${data.activityType}
Instrucciones adicionales: ${data.additionalInstructions}

Por favor, diseña una actividad educativa que incluya:

<h2>1. Título de la actividad</h2>
<h2>2. Breve descripción (2-3 frases)</h2>
<p>Incluye una breve descripción de la actividad en 2-3 frases.</p>

<h2>3. Objetivos específicos alineados con los objetivos de aprendizaje proporcionados</h2>
<ul>
  <li>Objetivo 1</li>
  <li>Objetivo 2</li>
  <li>Objetivo 3</li>
</ul>

<h2>4. Duración estimada</h2>
<p>Indicar la duración estimada de la actividad.</p>

<h2>5. Materiales necesarios</h2>
<ul>
  <li>Material 1</li>
  <li>Material 2</li>
  <li>Material 3</li>
</ul>

<h2>6. Instrucciones paso a paso para realizar la actividad</h2>
<ol>
  <li>Paso 1</li>
  <li>Paso 2</li>
  <li>Paso 3</li>
</ol>

<h2>7. Método de evaluación o criterios de éxito</h2>
<ul>
  <li>Método 1</li>
  <li>Método 2</li>
  <li>Método 3</li>
</ul>

Asegúrate de:
<ul>
  <li>Adaptar la actividad al nivel del curso y la asignatura especificados</li>
  <li>Incorporar el contenido de la asignatura de manera significativa</li>
  <li>Diseñar la actividad según el tipo especificado</li>
  <li>Integrar las instrucciones adicionales de manera relevante</li>
  <li>Crear una actividad que sea atractiva, motivadora y apropiada para los estudiantes</li>
</ul>

Responde en español y asegúrate de que la actividad sea educativa, práctica y alineada con los objetivos de aprendizaje especificados.
El resultado debe ser proporcionado en este formato: HTML
`;

console.log(aiPrompt);

    setIsLoading(true);
    try {
      await handleHitAi(aiPrompt, RouteConstant.EDUCATIONAL_ACTIVITES_OUTPUT);
      navigate(RouteConstant.EDUCATIONAL_ACTIVITES_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      alert("Ocurrió un error al generar la actividad educativa. Por favor, inténtalo de nuevo.");
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Generador de Actividades Educativas: Creatividad Pedagógica al Instante"
        description="El Generador de Actividades Educativas es una herramienta versátil que transforma conceptos curriculares en experiencias de aprendizaje dinámicas y atractivas. Basándose en parámetros como el curso, la asignatura y los objetivos de aprendizaje, crea actividades personalizadas que se adaptan a diversos estilos de enseñanza y aprendizaje"
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <InputFieldDropdown
          placeholder="Curso"
          onChange={(value) => {
            setData({ ...data, course: value });
          }}
          name="course"
          type="number"
          value={data.course}
          options={Dropdown.courses}
          require={true}
          label="Curso"
        />
        <InputFieldDropdown
          name="subject"
          value={data.subject}
          onChange={(value) => {
            setData({ ...data, subject: value });
          }}
          require={true}
          placeholder="Asignatura"
          options={Dropdown.subject}
          label="Asignatura"
        />
        <TextArea
          name="subjectContent"
          value={data.subjectContent}
          onChange={handleChange}
          placeholder="Ej: Ecosistemas terrestres y acuáticoss"
          require={true}
          label="Contenido de la asignatura"
        />
        <TextArea
          name="learningObjectives"
          value={data.learningObjectives}
          onChange={handleChange}
          placeholder="Ej: 1. Comprender la estructura y funcionamiento de diferentes ecosistemas. 2. Identificar las relaciones entre los seres vivos en un ecosistema. 3. Analizar el impacto de la actividad humana en los ecosistemas. etc. "
          require={true}
          label="Objetivos de aprendizaje"
        />
        <InputField
          name="activityType"
          value={data.activityType}
          onChange={handleChange}
          placeholder="Ej: Experimento, Debate, Proyecto de investigación, etc"
          require={true}
          label="Tipo de actividad"
        />
        <InputField
          name="additionalInstructions"
          value={data.additionalInstructions}
          onChange={handleChange}
          placeholder="Instrucciones o requisitos adicionales (opcional)"
          label="Información adicionales"
          require={false}
        />
        <div className="col-span-2 flex align-center gap-2">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={loading || isLoading}
          >
            {isLoading ? "Generando..." : "Generar"}
          </Button>
          {prvResponse && (
            <Button
              className="w-[20%]"
              onClick={() => {
                navigate(RouteConstant.EDUCATIONAL_ACTIVITES_OUTPUT);
              }}
              disabled={loading || isLoading}
            >
              Anterior
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default EducationalActivitiesText;