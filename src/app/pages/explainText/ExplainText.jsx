import React, { useState } from "react";
import {
  getLocalStorage,
  setLocalStorage,
} from "../../components/local_storage";
import { RouteConstant } from "../../constants/RouteConstant";
import { useNavigate } from "react-router-dom";
import { toneOptions } from "./constants";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import InputField from "../../components/inputFiled/InputFiled";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import Button from "../../components/button/Button";
import FileReaderComponent from "../../components/fileReaderComponent/FileReaderComponent";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import FileReaderInputFIled from "../../components/fileReaderInputFIled/FileReaderInputFIled";

function ExplainText() {
  const [data, setData] = useState({
    selectedFiles: [],
    prompt: "",
    wordCount: "",
    tone: "",
  });
  const { handleHitAi } = useHandleHitAi({
    ai: "chatgpt",
    selectedFiles: data.selectedFiles,
    toolName: "Explicador de Textos",
  });
  const prvResponse = getLocalStorage(RouteConstant.EXPLAIN_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!data.prompt || !data.tone || !data.wordCount) {
      alert("Please fill in all fields");
      return;
    }
    let aiPrompt = `You are powerful AI assistant and you are a explaination professional. Always respond in Spanish
    Explain the text given below in a ${data.tone} tone 
    Prompt: ${data.prompt}
    Also the number of words must be: ${data.wordCount}
    Generate always the content in this format: HTML and nothing in markdown and well structured`;
    if (data.selectedFiles.length > 0) {
      const imageFiles = data.selectedFiles.filter((file) =>
        file.type.startsWith("image/")
      );
      if (imageFiles.length > 0) {
        aiPrompt +=
          "\nPlease analyze the uploaded images and do what the user tells you to do about the images";
      }
    }
    setIsLoading(true);
    try {
      await handleHitAi(aiPrompt, RouteConstant.EXPLAIN_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      setIsLoading(false);
    }
  };
  return (
    <div className="flex flex-col gap-[24px] p-[24px]">
      <AiToolPageHeader
        title="Explicador de Textos"
        description="¿Hay algún texto o apuntes que no entiendas? Pega el texto o sube tu archivo y esta herramienta te lo explicará sencilla para que lo puedas entender "
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <div className="grid grid-cols-2 gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <FileReaderInputFIled
          onChange={handleChange}
          data={data}
          setData={setData}
        />
        <InputFieldDropdown
          name={"tone"}
          value={data.tone}
          onChange={(value) => {
            setData({ ...data, tone: value });
          }}
          placeholder={"Elige el estilo en el que quieras que escriba el texto"}
          options={toneOptions}
          label={"Estilo de Escritura"}
        />
        <InputField
          placeholder="Ej: 500 palabras (1000 palabras equivalen a unas 3 o 4 páginas con una letra estándar)"
          onChange={handleChange}
          name="wordCount"
          value={data.wordCount}
          type="number"
          label={"Número de palabras que quieres generar"}
        />
        <div className="col-span-2 flex  align-center gap-2 ">
          <Button
            onClick={handleSubmit}
            className={prvResponse ? "w-[80%]" : "w-full"}
          >
            {isLoading ? "Cargando..." : "Explicar texto"}
          </Button>
          {prvResponse && (
            <Button
              className={"w-[20%]"}
              onClick={() => {
                navigate(RouteConstant.SUMMARIZE_TEXT);
              }}
            >
              Explicación anterior
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default ExplainText;
