

export const toneOptions =  [
  "Formal",
  "Casual",
  "Académico",
  "Persuasivo",
  "Detallado",
  "Gracioso",
  "Descriptivo",
  "Analítico",
  "Innovador",
  "Motivacional",
];