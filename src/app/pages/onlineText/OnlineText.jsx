import React from 'react'
import { BrushIcon, HelpChatIcon, PdfFileIcon } from '../../icons/Icons'
import Button from '../../components/button/Button'

function OnlineText() {
    return (
        <div className='flex flex-col items-center justify-center'>
            <div className="w-full max-w-[674px] flex flex-col items-start h-[90dvh] overflow-y-scroll py-[20px] px-[40px] justify-start gap-[36px]">
                <div className="flex items-center justify-between w-full">
                    <span className='text-[#4EA7FF] text-[36px] font-bold'>
                        Fotografia
                    </span>
                    <div className="flex items-center gap-[16px]">
                        <HelpChatIcon className='cursor-pointer' />
                        <BrushIcon className='cursor-pointer' />
                        <PdfFileIcon className='cursor-pointer' />
                    </div>
                </div>
                <div className="flex flex-col items-center gap-[20px] w-full">

                    <span className="flex items-start px-[16px] py-[28px] bg-[#CAE5FF] text-[#2B3D70] rounded-[12px] border-[#4EA6FE] border-[1px] w-full">
                        Lorem ipsum dolor Lorem ipsum dolor
                    </span>

                    <div className="flex flex-col w-full px-[48px] items-center gap-[16px]">
                        <span className="flex items-start px-[16px] py-[28px] bg-[#FFEEEC] text-[#2B3D70] rounded-[12px] border-[#FF6F61] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                        <span className="flex items-start px-[16px] py-[28px] bg-[#FFF] text-[#2B3D70] rounded-[12px] border-[#F3F3F3] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                        <span className="flex items-start px-[16px] py-[28px] bg-[#E8FFF9] text-[#2B3D70] rounded-[12px] border-[#02E8AC] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                        <span className="flex items-start px-[16px] py-[28px] bg-[#FFF] text-[#2B3D70] rounded-[12px] border-[#F3F3F3] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                    </div>

                    <div className="flex flex-col gap-[4px] w-full">
                        <span className="text-indigo-900 text-lg font-semibold">More Info</span>
                        <p className="w-[602px] text-slate-500 text-base font-normal">
                            Lorem ipsum dolor sit amet consectetur. Mi ac platea adipiscing mi mi adipiscing.Lorem ipsum dolor sit amet consectetur. Mi ac platea adipiscing mi mi adipiscing.
                        </p>
                    </div>

                </div>
                <div className="flex flex-col items-center gap-[20px] w-full">
                    <span className="flex items-start px-[16px] py-[28px] bg-[#CAE5FF] text-[#2B3D70] rounded-[12px] border-[#4EA6FE] border-[1px] w-full">
                        Lorem ipsum dolor Lorem ipsum dolor
                    </span>
                    <div className="flex flex-col w-full px-[48px] items-center gap-[16px]">
                        <span className="flex items-start px-[16px] py-[28px] bg-[#FFEEEC] text-[#2B3D70] rounded-[12px] border-[#FF6F61] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                        <span className="flex items-start px-[16px] py-[28px] bg-[#FFF] text-[#2B3D70] rounded-[12px] border-[#F3F3F3] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                        <span className="flex items-start px-[16px] py-[28px] bg-[#E8FFF9] text-[#2B3D70] rounded-[12px] border-[#02E8AC] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                        <span className="flex items-start px-[16px] py-[28px] bg-[#FFF] text-[#2B3D70] rounded-[12px] border-[#F3F3F3] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                    </div>
                    <div className="flex flex-col gap-[4px] w-full">
                        <span className="text-indigo-900 text-lg font-semibold">More Info</span>
                        <p className="w-[602px] text-slate-500 text-base font-normal">
                            Lorem ipsum dolor sit amet consectetur. Mi ac platea adipiscing mi mi adipiscing.Lorem ipsum dolor sit amet consectetur. Mi ac platea adipiscing mi mi adipiscing.
                        </p>
                    </div>
                </div>
                <div className="flex flex-col items-center gap-[20px] w-full">
                    <span className="flex items-start px-[16px] py-[28px] bg-[#CAE5FF] text-[#2B3D70] rounded-[12px] border-[#4EA6FE] border-[1px] w-full">
                        Lorem ipsum dolor Lorem ipsum dolor
                    </span>
                    <div className="flex flex-col w-full px-[48px] items-center gap-[16px]">
                        <span className="flex items-start px-[16px] py-[28px] bg-[#FFEEEC] text-[#2B3D70] rounded-[12px] border-[#FF6F61] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                        <span className="flex items-start px-[16px] py-[28px] bg-[#FFF] text-[#2B3D70] rounded-[12px] border-[#F3F3F3] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                        <span className="flex items-start px-[16px] py-[28px] bg-[#E8FFF9] text-[#2B3D70] rounded-[12px] border-[#02E8AC] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                        <span className="flex items-start px-[16px] py-[28px] bg-[#FFF] text-[#2B3D70] rounded-[12px] border-[#F3F3F3] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                    </div>
                    <div className="flex flex-col gap-[4px] w-full">
                        <span className="text-indigo-900 text-lg font-semibold">More Info</span>
                        <p className="w-[602px] text-slate-500 text-base font-normal">
                            Lorem ipsum dolor sit amet consectetur. Mi ac platea adipiscing mi mi adipiscing.Lorem ipsum dolor sit amet consectetur. Mi ac platea adipiscing mi mi adipiscing.
                        </p>
                    </div>
                </div>
                <div className="flex flex-col items-center gap-[20px] w-full">
                    <span className="flex items-start px-[16px] py-[28px] bg-[#CAE5FF] text-[#2B3D70] rounded-[12px] border-[#4EA6FE] border-[1px] w-full">
                        Lorem ipsum dolor Lorem ipsum dolor
                    </span>
                    <div className="flex flex-col w-full px-[48px] items-center gap-[16px]">
                        <span className="flex items-start px-[16px] py-[28px] bg-[#FFEEEC] text-[#2B3D70] rounded-[12px] border-[#FF6F61] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                        <span className="flex items-start px-[16px] py-[28px] bg-[#FFF] text-[#2B3D70] rounded-[12px] border-[#F3F3F3] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                        <span className="flex items-start px-[16px] py-[28px] bg-[#E8FFF9] text-[#2B3D70] rounded-[12px] border-[#02E8AC] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                        <span className="flex items-start px-[16px] py-[28px] bg-[#FFF] text-[#2B3D70] rounded-[12px] border-[#F3F3F3] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                    </div>
                    <div className="flex flex-col gap-[4px] w-full">
                        <span className="text-indigo-900 text-lg font-semibold">More Info</span>
                        <p className="w-[602px] text-slate-500 text-base font-normal">
                            Lorem ipsum dolor sit amet consectetur. Mi ac platea adipiscing mi mi adipiscing.Lorem ipsum dolor sit amet consectetur. Mi ac platea adipiscing mi mi adipiscing.
                        </p>
                    </div>
                </div>
                <div className="flex flex-col items-center gap-[20px] w-full">
                    <span className="flex items-start px-[16px] py-[28px] bg-[#CAE5FF] text-[#2B3D70] rounded-[12px] border-[#4EA6FE] border-[1px] w-full">
                        Lorem ipsum dolor Lorem ipsum dolor
                    </span>
                    <div className="flex flex-col w-full px-[48px] items-center gap-[16px]">
                        <span className="flex items-start px-[16px] py-[28px] bg-[#FFEEEC] text-[#2B3D70] rounded-[12px] border-[#FF6F61] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                        <span className="flex items-start px-[16px] py-[28px] bg-[#FFF] text-[#2B3D70] rounded-[12px] border-[#F3F3F3] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                        <span className="flex items-start px-[16px] py-[28px] bg-[#E8FFF9] text-[#2B3D70] rounded-[12px] border-[#02E8AC] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                        <span className="flex items-start px-[16px] py-[28px] bg-[#FFF] text-[#2B3D70] rounded-[12px] border-[#F3F3F3] border-[1px] w-full">
                            Lorem ipsum dolor Lorem ipsum dolor
                        </span>
                    </div>
                    <div className="flex flex-col gap-[4px] w-full">
                        <span className="text-indigo-900 text-lg font-semibold">More Info</span>
                        <p className="w-[602px] text-slate-500 text-base font-normal">
                            Lorem ipsum dolor sit amet consectetur. Mi ac platea adipiscing mi mi adipiscing.Lorem ipsum dolor sit amet consectetur. Mi ac platea adipiscing mi mi adipiscing.
                        </p>
                    </div>
                </div>
                <div className="flex items-center justify-between w-full gap-[16px]">
                    <Button className={'w-full border-[#4EA6FE] border-[1px] !text-[#4EA7FF] !bg-[#FFF]'} children='Skip' />
                    <Button className={'w-full'} children='Continue' />
                </div>
            </div>
        </div>
    )
}

export default OnlineText