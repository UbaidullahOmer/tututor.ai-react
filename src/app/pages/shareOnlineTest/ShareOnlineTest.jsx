import React, { useEffect, useState } from 'react';
import { BrushIcon, GroupItemIcon, HelpChatIcon, PdfFileIcon, UnGroupItemIcon } from '../../icons/Icons';
import Button from '../../components/button/Button';
import { useLocation, useParams } from 'react-router-dom';
import { getLocalStorage } from '../../components/local_storage';
import { RouteConstant } from '../../constants/RouteConstant';

function ShareOnlineTest() {
    const [onlineTest, setOnlineTest] = useState([]);
    const {slug} = useParams()
    const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
    const [selectedAnswer, setSelectedAnswer] = useState(null);
    const [isAnswerCorrect, setIsAnswerCorrect] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [isQuizCompleted, setIsQuizCompleted] = useState(false);
    const [score, setScore] = useState(0);
    const [isGrouped, setIsGrouped] = useState(false);
    const sanitizeAndParseJson = (inputString) => {
        try {
          const startIndex = inputString.indexOf('[');
          const endIndex = inputString.lastIndexOf(']') + 1;
          const jsonString = inputString.slice(startIndex, endIndex);
          const jsonObject = JSON.parse(jsonString);
          return jsonObject;
        } catch (error) {
          console.error('Invalid JSON input', error);
          return null;
        }
      };
    useEffect(() => {
        const testData = sanitizeAndParseJson(slug);
        if (testData?.length) {
            setOnlineTest(testData);
            setIsLoading(false);
        }
    }, [slug]);
    const currentQuestion = onlineTest[currentQuestionIndex] || null;

    const handleAnswerSelect = (answer) => {
        if (selectedAnswer !== null) return;
        setSelectedAnswer(answer);
        const isCorrect = answer === currentQuestion?.correctAnswer;
        setIsAnswerCorrect(isCorrect);
        if (isCorrect) setScore(prevScore => prevScore + 1);
    };

    const handleContinue = () => {
        if (currentQuestionIndex < onlineTest.length - 1) {
            setCurrentQuestionIndex(prevIndex => prevIndex + 1);
            setSelectedAnswer(null);
            setIsAnswerCorrect(null);
        } else {
            setIsQuizCompleted(true);
        }
    };

    const handleSkip = handleContinue;

    const handleRestart = () => {
        setCurrentQuestionIndex(0);
        setSelectedAnswer(null);
        setIsAnswerCorrect(null);
        setIsQuizCompleted(false);
        setScore(0);
    };

    if (isLoading) return <div>Loading...</div>;
    if (!onlineTest.length) return <div>No quiz data available.</div>;

    if (isQuizCompleted) {
        return (
            <div className='flex flex-col items-center justify-center'>
                <div className="w-full max-w-[674px] flex flex-col items-start h-[90dvh] overflow-y-scroll py-[20px] px-[40px] justify-start gap-[36px]">
                    <span className='text-[#4EA7FF] text-[36px] font-bold'>Quiz Completed</span>
                    <p className="w-full text-slate-500 text-base font-normal">Your score: {score} out of {onlineTest.length}</p>
                    <Button
                        className={'w-full border-[#4EA6FE] border-[1px] !text-[#4EA7FF] !bg-[#FFF]'}
                        onClick={handleRestart}
                    >
                        Restart Quiz
                    </Button>
                </div>
            </div>
        );
    }

    return (
        <div className='flex flex-col items-center justify-center'>
            <div className="w-full max-w-[674px] flex flex-col items-start h-[90dvh] overflow-y-scroll py-[20px] px-[40px] justify-start gap-[36px]">
                <div className="flex items-center justify-between w-full">
                    <span className='text-[#4EA7FF] text-[36px] font-bold'>Cuestionario</span>
                    <div className="flex items-center gap-[16px]">
                        <HelpChatIcon className='cursor-pointer' />
                        <BrushIcon className='cursor-pointer' />
                        <PdfFileIcon className='cursor-pointer' />
                        <GroupItemIcon className='cursor-pointer' />
                        <UnGroupItemIcon className='cursor-pointer' />
                    </div>
                </div>
                {currentQuestion && (
                    <div className="flex flex-col items-center gap-[20px] w-full">
                        <span className="flex items-start px-[16px] py-[28px] bg-[#CAE5FF] text-[#2B3D70] rounded-[12px] border-[#4EA6FE] border-[1px] w-full">
                            {currentQuestionIndex + 1}. {currentQuestion.question}
                        </span>
                        <div className="flex flex-col w-full px-[48px] items-center gap-[16px]">
                            {currentQuestion.options.map((option, index) => (
                                <span
                                    key={index}
                                    className={`flex items-start px-[16px] py-[28px] 
                                    ${selectedAnswer !== null ?
                                            (option === currentQuestion.correctAnswer ?
                                                'bg-[#E8FFF9] border-[#02E8AC]' :
                                                (selectedAnswer === option ?
                                                    'bg-[#FFEEEC] border-[#FF6F61]' :
                                                    'bg-[#FFF] border-[#F3F3F3]')
                                            ) :
                                            'bg-[#FFF] border-[#F3F3F3]'
                                        } 
                                    text-[#2B3D70] rounded-[12px] border-[1px] w-full cursor-pointer`}
                                    onClick={() => handleAnswerSelect(option)}
                                >
                                    {index + 1}. {option}
                                </span>
                            ))}
                        </div>
                        <div className="flex flex-col gap-[4px] w-full">
                            <span className="text-indigo-900 text-lg font-semibold">Más información</span>
                            <p className="w-[602px] text-slate-500 text-base font-normal">Select the correct answer from the options above.</p>
                        </div>
                    </div>
                )}
                <div className="flex items-center justify-between w-full gap-[16px]">
                    <Button
                        className={'w-full border-[#4EA6FE] border-[1px] !text-[#4EA7FF] !bg-[#FFF]'}
                        onClick={handleSkip}
                    >
                        Skip
                    </Button>
                    <Button
                        className={'w-full'}
                        onClick={handleContinue}
                        disabled={!selectedAnswer}
                    >
                        Continue
                    </Button>
                </div>
            </div>
        </div>
    );
}

export default ShareOnlineTest