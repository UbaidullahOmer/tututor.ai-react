import React, { useState, useEffect, useRef } from "react";
import ReactMarkdown from 'react-markdown';
import rehypeRaw from 'rehype-raw';
import { Search, Send, Paperclip, Mic, Trash, Edit } from 'lucide-react';
import query from "./query";

interface Message {
  id: number;
  sender: 'user' | 'bot';
  content: string;
  time: string;
}

interface Conversation {
  id: string;
  name: string;
  messages: Message[];
  lastMessage: string;
  time: string;
}

function MathScienceTeacher() {
  const [userInput, setUserInput] = useState<string>('');
  const [messages, setMessages] = useState<Message[]>([]);
  const [conversations, setConversations] = useState<Conversation[]>([]);
  const [activeConversation, setActiveConversation] = useState<string | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const chatContainerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const loadConversations = async () => {
      try {
        setIsLoading(true);
        const savedConversations = JSON.parse(localStorage.getItem('mathScienceConversations') || '[]');
        if (Array.isArray(savedConversations) && savedConversations.length > 0) {
          setConversations(savedConversations);
          setActiveConversation(savedConversations[0].id);
          setMessages(savedConversations[0].messages || []);
        } else {
          await handleNewChat(); // Crea un nuevo chat si no hay ninguno guardado
        }
      } catch (error) {
        console.error("Error loading conversations:", error);
      } finally {
        setIsLoading(false);
      }
    };

    loadConversations();
  }, []);

  useEffect(() => {
    if (chatContainerRef.current) {
      chatContainerRef.current.scrollTop = chatContainerRef.current.scrollHeight;
    }
  }, [messages]);

  useEffect(() => {
    if (conversations.length > 0) {
      localStorage.setItem('mathScienceConversations', JSON.stringify(conversations));
    }
  }, [conversations]);

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setUserInput(e.target.value);
  };

  const handleSend = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!userInput.trim() || !activeConversation) return;

    const newMessage: Message = {
      id: messages.length + 1,
      sender: 'user',
      content: userInput,
      time: new Date().toLocaleString([], { hour: '2-digit', minute: '2-digit' })
    };
    const updatedMessages = [...messages, newMessage];
    setMessages(updatedMessages);

    try {
      const data = {
        question: userInput,
        overrideConfig: {
          sessionId: activeConversation,
          returnSourceDocuments: true,
          systemMessagePrompt: ""
        }
      };

      const response = await query(data);

      if (response && typeof response.text === 'string') {
        const botMessage: Message = {
          id: updatedMessages.length + 1,
          sender: 'bot',
          content: response.text,
          time: new Date().toLocaleString([], { hour: '2-digit', minute: '2-digit' })
        };
        const finalMessages = [...updatedMessages, botMessage];
        setMessages(finalMessages);

        setConversations(prevConversations =>
          prevConversations.map(conv =>
            conv.id === activeConversation
              ? { 
                  ...conv, 
                  messages: finalMessages,
                  lastMessage: userInput,
                  time: new Date().toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })
                }
              : conv
          )
        );
      }
    } catch (error) {
      console.error("Error sending message:", error);
    }

    setUserInput('');
  };

  const handleNewChat = async () => {
    const newConversation: Conversation = {
      id: Date.now().toString(),
      name: `Clase ${conversations.length + 1}`,
      messages: [{
        id: 1,
        sender: 'bot',
        content: `
# Profesor de Matemáticas

Bienvenido a tu clase virtual de Matemáticas. Aquí puedes explorar:

- Álgebra: Ecuaciones, funciones y polinomios
- Geometría: Figuras, teoremas y propiedades
- Cálculo: Derivadas, integrales y límites
- Estadística: Análisis de datos, probabilidad y distribuciones
- Trigonometría: Ángulos, senos y cosenos
- Física: Principios físicos y aplicaciones
- Química: Reacciones químicas y propiedades de la materia

¿Qué tema de matemáticas te gustaría explorar hoy?
        `,
        time: new Date().toLocaleString([], { hour: '2-digit', minute: '2-digit' })
      }],
      lastMessage: "Nueva clase iniciada",
      time: new Date().toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })
    };
    setConversations(prev => [newConversation, ...prev]);
    setActiveConversation(newConversation.id);
    setMessages(newConversation.messages);
  };

  const handleEditConversation = (id: string, newName: string) => {
    if (!newName.trim()) return;  // Evita guardar nombres vacíos
    setConversations(conversations.map(conv =>
      conv.id === id ? { ...conv, name: newName } : conv
    ));
  };

  const handleDeleteConversation = (id: string) => {
    setConversations(prev => prev.filter(conv => conv.id !== id));
    if (activeConversation === id) {
      if (conversations.length > 1) {
        const newActiveConversation = conversations.find(conv => conv.id !== id);
        if (newActiveConversation) {
          setActiveConversation(newActiveConversation.id);
          setMessages(newActiveConversation.messages || []);
        }
      } else {
        handleNewChat();
      }
    }
  };

  const handleSelectConversation = (id: string) => {
    setActiveConversation(id);
    const selectedConversation = conversations.find(conv => conv.id === id);
    if (selectedConversation) {
      setMessages(selectedConversation.messages || []);
    }
  };

  if (isLoading) {
    return <div className="flex justify-center items-center h-screen">Cargando...</div>;
  }

  return (
    <div className="flex flex-col h-[90vh] bg-gray-100 text-gray-800">
      <div className="flex flex-1 overflow-hidden">
        {/* Sidebar */}
        <div className="w-64 bg-white border-r border-gray-200 overflow-y-auto">
          <div className="p-4">
            <div className="mb-4 relative">
              <input 
                type="text" 
                placeholder="Buscar"
                className="w-full px-3 py-2 pl-10 bg-gray-100 rounded-full text-sm focus:outline-none focus:ring-2 focus:ring-blue-500"
              />
              <Search className="absolute left-3 top-1/2 transform -translate-y-1/2 text-gray-400" size={18} />
            </div>
            <button 
              className="w-full bg-blue-600 text-white py-2 rounded-lg mb-4 text-center hover:bg-blue-700 transition duration-300" 
              onClick={() => handleNewChat()}
            >
              Nueva Clase
            </button>
            <div className="space-y-3">
              {conversations.map((conv) => (
                <div key={conv.id} className="relative">
                  <div 
                    className={`flex flex-col p-2 rounded-lg cursor-pointer ${activeConversation === conv.id ? 'bg-blue-100' : 'hover:bg-gray-50'}`}
                    onClick={() => handleSelectConversation(conv.id)}
                  >
                    <p className="text-sm font-semibold text-blue-600 truncate">{conv.lastMessage}</p>
                    <div className="flex justify-between mt-1">
                      <span className="text-xs text-gray-500">{new Date(conv.time).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })}</span>
                      <span className="text-xs text-gray-500">{new Date(conv.time).toLocaleDateString()}</span>
                    </div>
                  </div>
                  <div className="absolute right-2 top-[40%] transform -translate-y-1/2 flex space-x-2">
                    <button
                      className="text-gray-500 hover:text-gray-700"
                      onClick={() => {
                        const newName = prompt("Introduce un nuevo nombre para la clase", conv.name);
                        if (newName) handleEditConversation(conv.id, newName);
                      }}
                    >
                      <Edit size={16} />
                    </button>
                    <button
                      className="text-gray-500 hover:text-gray-700"
                      onClick={() => handleDeleteConversation(conv.id)}
                    >
                      <Trash size={16} />
                    </button>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>

        {/* Main Content */}
        <div className="flex-1 flex flex-col overflow-hidden bg-white">
          {/* Chat header */}
          <div className="bg-white p-4 flex items-center justify-between border-b shadow-sm">
            <div className="flex items-center">
              <div className="w-10 h-10 bg-blue-600 rounded-full mr-3 flex items-center justify-center text-white font-bold text-xl">
                M
              </div>
              <h2 className="font-bold text-xl text-gray-800">Profesor de Matemáticas</h2>
            </div>
          </div>

          {/* Messages */}
          <main className="flex-1 overflow-y-auto p-4 bg-gray-50" ref={chatContainerRef}>
            <div className="max-w-3xl mx-auto space-y-4">
              {messages.map((message) => (
                <div key={message.id} className={`flex ${message.sender === 'user' ? 'justify-end' : 'justify-start'}`}>
                  <div className={`max-w-sm lg:max-w-2xl px-4 py-2 rounded-lg ${
                    message.sender === 'user' ? 'bg-blue-600 text-white' : 'bg-white border border-gray-200'
                  }`}>
                    <ReactMarkdown 
                      className="markdown-content"
                      rehypePlugins={[rehypeRaw]}
                      components={{
                        p: ({node, ...props}) => <p className="mb-2" {...props} />,
                        ul: ({node, ...props}) => <ul className="list-disc list-inside mb-2" {...props} />,
                        ol: ({node, ...props}) => <ol className="list-decimal list-inside mb-2" {...props} />,
                        li: ({node, ...props}) => <li className="mb-1" {...props} />,
                        h1: ({node, ...props}) => <h1 className="text-2xl font-bold mb-2" {...props} />,
                        h2: ({node, ...props}) => <h2 className="text-xl font-semibold mb-2" {...props} />,
                        h3: ({node, ...props}) => <h3 className="text-lg font-medium mb-2" {...props} />,
                        strong: ({node, ...props}) => <strong className="font-bold" {...props} />,
                        em: ({node, ...props}) => <em className="italic" {...props} />,
                      }}
                    >
                      {message.content}
                    </ReactMarkdown>
                    <span className="text-xs opacity-50 mt-1 block">{message.time}</span>
                  </div>
                </div>
              ))}
            </div>
          </main>

          {/* Message input */}
          <footer className="border-t border-gray-200 p-4 bg-white">
            <div className="max-w-full mx-auto">
              <form onSubmit={handleSend} className="bg-gray-100 rounded-lg flex items-center p-2 w-full">
                <button type="button" className="p-2 text-gray-500 hover:text-gray-700">
                  <Paperclip size={20} />
                </button>
                <input
                  type="text"
                  value={userInput}
                  onChange={handleInputChange}
                  placeholder="Haz una pregunta sobre matemáticas..."
                  className="flex-grow bg-transparent border-none focus:outline-none text-gray-800 placeholder-gray-500 px-2 w-full"
                />
                <button type="button" className="p-2 text-gray-500 hover:text-gray-700">
                  <Mic size={20} />
                </button>
                <button type="submit" className="bg-blue-600 text-white rounded-full p-2 hover:bg-blue-700 transition duration-300">
                  <Send size={20} />
                </button>
              </form>
            </div>
          </footer>
        </div>
      </div>
    </div>
  );
}

export default MathScienceTeacher;
