// query.js
const url = "https://flowise-production-ea49.up.railway.app/api/v1/prediction/4be72a47-dfc4-4b57-b8a1-a2eac00d1a28";
const headers = {
  "Content-Type": "application/json",
  "Authorization": "Bearer I8kaaYbwWpL-lC9a8I_D91UAfXupWD3NXfNCBaDXxCw"
};

async function query(data) {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers,
      body: JSON.stringify(data)
    });

    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }

    const result = await response.json();
    return result;
  } catch (error) {
    console.error('Error in query function:', error.message);
    return null;
  }
}

export default query;