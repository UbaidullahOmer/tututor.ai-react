import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";

const FormField = ({ title, children, required = false }) => (
  <div className="mb-4">
    <label className="block font-bold text-sm mb-2">
      {title} {required && <span className="text-red-500">*</span>}
    </label>
    {children}
  </div>
);

const InputStyles = "w-full p-2 bg-gray-100 border border-gray-300 rounded-md text-gray-700 placeholder-gray-500";

const TextInput = ({ placeholder, value, onChange, name, type = "text" }) => (
  <input
    type={type}
    name={name}
    value={value}
    onChange={onChange}
    placeholder={placeholder}
    className={InputStyles}
  />
);

const TextArea = ({ placeholder, value, onChange, name, rows = 4 }) => (
  <textarea
    name={name}
    value={value}
    onChange={onChange}
    placeholder={placeholder}
    rows={rows}
    className={InputStyles}
  />
);

function DidacticUnitsText() {
  const [data, setData] = useState({
    course: "",
    subject: "",
    mainTopic: "",
    learningObject: "",
    didacticUnitDuration: "",
    availableResources: "",
    evaluationTypes: "",
    competenciesToDevelop: "", 
    pais: "",
    adicional: "",
    criteriosEvaluacion: "",
  });

  const { handleHitAi, loading } = useHandleHitAi({ 
    ai: "chatgpt",
    selectedFiles: data.selectedFiles,
    toolName: "Unidad Didáctica",
  });

  const prvResponse = getLocalStorage(RouteConstant.DIDACTIC_UNITS_OUTPUT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      !data.course.trim() ||
      !data.subject.trim() ||
      !data.mainTopic.trim() ||
      !data.learningObject.trim() ||
      !data.didacticUnitDuration.trim() ||
      !data.availableResources.trim() ||
      !data.evaluationTypes.trim() ||
      !data.pais.trim() ||
      !data.competenciesToDevelop.trim()
    ) {
      alert("Por favor, rellena todos los campos");
      return;
    }

    let didacticUnitCreatorPrompt = `
Eres un experto en diseño curricular y creación de unidades didácticas. Tu tarea es generar una unidad didáctica detallada y efectiva basada en la siguiente información:

Curso: ${data.course}.
Asignatura: ${data.subject}.
Tema principal: ${data.mainTopic}.
Objetivo de aprendizaje: ${data.learningObject}.
Duración de la unidad didáctica: ${data.didacticUnitDuration}.
Recursos disponibles: ${data.availableResources}.
Tipos de evaluación: ${data.evaluationTypes}.
Sigue estos criterios de evaluación para desarrollar la Unidad Didáctica: ${data.criteriosEvaluacion}.
Competencias específicas a desarrollar: ${data.competenciesToDevelop}.
Información adicional a tener en cuenta ${data.adicional}.

Por favor, proporciona una unidad didáctica estructurada que incluya los siguientes elementos:

<h2>1. Introducción:</h2>
<ul>
  <li>Descripción general de la unidad didáctica. Crea una secuencicación de sesiones resumida, donde en cada sesión digas qué se va a trabajar. 
  Aquí te dejo un ejemplo de una Unidad Didáctica de Badminton en Educación Física: 
  Sesión 1: Presentación de la UD. Explicación de los principales conceptos de la UD y puesta en práctica de algunas actividades relacionadas.
  Sesión 2: Golpeos técnico-tácticos I. Aprendizaje de los gestos técnicos de "saque de derecha", "saque de tenis" y "drive"
  Sesión 3: Aprendizaje de los gestos técnicos "clear", "drop" y "net drop"
  Sesión 4: Aprendizaje de las acciones tácticas de búsqueda de la zona desprotegida, desplazar al rival de la zona central y buscar el punto débil del rivel.
  Sesión 5: Competición formal y evaluación. Evaluación de los aspectos ténnico-tácticos mediante situación real de partido y competición.
  Etc. 
  (Ju</li>
</ul>

<h2>2. Secuencia de sesiones y actividades:</h2>
<p>Desarrolla una secuencia de sesiones según la duración especificada. Cada sesión debe incluir sus propias actividades, mostrando una clara progresión en el aprendizaje.</p>

<h2>2. Secuencia de sesiones y actividades:</h2>
<p>Desarrolla una secuencia de sesiones según la duración especificada. Cada sesión debe estar estructurada en tres partes: una parte inicial con una actividad introductoria, una parte principal con 1 a 4 actividades principales, y una parte final que deje al alumno con una sensación positiva.</p>
<h2>Sesión #{número de la sesión}</h2>
<h3>1. Parte inicial (10-15 minutos):</h3>
<ul>
  <li>Actividad introductoria: [Nombre atractivo y único para esta actividad]
    <ul>
      <li>Duración: [Tiempo específico]</li>
      <li>Materiales: [Lista detallada de materiales necesarios]</li>
      <li>Descripción: [Descripción creativa y original de la actividad]</li>
    </ul>
  </li>
</ul>
<h3>2. Parte principal (30-35 minutos):</h3>
<ul>
  <li>Actividad 1: [Nombre atractivo, innovador y único para esta actividad]
    <ul>
      <li>Duración: [Tiempo específico]</li>
      <li>Materiales: [Lista detallada de materiales originales y creativos]</li>
      <li>Preparación: [Pasos de preparación únicos y detallados]</li>
      <li>Desarrollo: [Descripción detallada, innovadora, creativa y original del desarrollo de la actividad]</li>
      <li>Rol del profesor: [Descripción única del papel del profesor en esta actividad]</li>
    </ul>
  </li>
  <li>Actividad 2: [Nombre específico y original de la actividad]
    <ul>
      <li>Duración: [Tiempo específico]</li>
      <li>Materiales: [Lista detallada de materiales únicos]</li>
      <li>Descripción: [Pasos detallados y originales]</li>
    </ul>
  </li>
  <li>Actividad 3: [Nombre específico y original de la actividad, si aplica]
    <ul>
      <li>Duración: [Tiempo específico]</li>
      <li>Materiales: [Lista detallada de materiales únicos]</li>
      <li>Descripción: [Pasos detallados y originales]</li>
    </ul>
  </li>
</ul>
<h3>3. Parte final (5-10 minutos):</h3>
<ul>
  <li>Actividad de cierre: [Nombre creativo y único]
    <ul>
      <li>Duración: [Tiempo]</li>
      <li>Materiales: [Lista de materiales específicos y originales]</li>
      <li>Descripción: [Desarrollo innovador y que sea interesante para los alumnos]</li>
      <li>Componente reflexivo: [Reflexión única sobre lo aprendido]</li>
      <li>Anticipación: [Conexión creativa con la próxima sesión]</li>
    </ul>
  </li>
</ul>

<h2>4. Metodología:</h2>
<ul>
  <li>Describir el enfoque pedagógico general</li>
  <li>Proponer y explica 3 estrategias de enseñanza-aprendizaje diversas y efectivas de manera resumida</li>
</ul>

<h2>5. Evaluación:</h2>
<ul>
  <li>Diseñar un plan de evaluación que incluya los tipos de evaluación mencionados. Si no se te especifica criterios de evaluación, no digas nada sobre esto.</li>
  <ul>
    <li>Criterios de evaluación específicos. Recuerda que los criterios de evaluación son dados por el profesor en el prompt. Si el profesor no da criterios de evaluación, no nombres ninguno.</li>
    <li>Instrumentos de evaluación</li>
    <li>Momento de aplicación</li>
  </ul>
</ul>

<h2>6. Cierre de la unidad:</h2>
<ul>
  <li>Diseñar una actividad de síntesis o proyecto final</li>
</ul>

Asegúrate de que la unidad didáctica sea coherente, esté alineada con los objetivos de aprendizaje y las competencias a desarrollar, y haga un uso efectivo de los recursos disponibles. 
Ajusta la profundidad y extensión de los contenidos a la duración especificada de la unidad. Mantén una progresión clara en las sesiones y actividades, asegurándote de que cada sesión se base en el aprendizaje de la anterior.
Sé creativo en tus propuestas, pero mantén la unidad realista y aplicable al contexto educativo dado. Responde siempre en español.
El formato de salida debe siempre ser: HTML y no incluyas ningún formato markdown.
`;

    setIsLoading(true);
    try {
      await handleHitAi(didacticUnitCreatorPrompt, RouteConstant.DIDACTIC_UNITS_OUTPUT);
      navigate(RouteConstant.DIDACTIC_UNITS_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      alert("An error occurred while generating the didactic unit. Please try again.");
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px] max-h-[80vh] overflow-scroll">
      <AiToolPageHeader
        title="Creador de Unidades Didácticas"
        description="Esta herramienta te ayudará a diseñar Unidades Didácticas creativas, atractivas e innovadoras para el contenido que necesites"
        icon="ri-edit-2-line prompt-[32px] prompt-[#2B3D70]"
      />
      <form onSubmit={handleSubmit} className="flex gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit flex-col">
        <div className="grid grid-cols-2 gap-4">
          <FormField title="Curso" required>
            <InputFieldDropdown
              placeholder="Curso"
              onChange={(value) => {
                setData({ ...data, course: value });
              }}
              name="course"
              value={data.course}
              options={Dropdown.courses}
            />
          </FormField>
          <FormField title="Asignatura" required>
            <InputFieldDropdown
              name={"subject"}
              value={data.subject}
              onChange={(value) => {
                setData({ ...data, subject: value });
              }}
              placeholder={"Asignatura"}
              options={Dropdown.subject}
            />
          </FormField>
        </div>
        <FormField title="País" required>
          <InputFieldDropdown
            name="pais"
            value={data.pais}
            onChange={(value) => {
              setData({ ...data, pais: value });
            }}
            placeholder="Selecciona un país"
            options={Dropdown.spanishSpeakingCountries}
          />
        </FormField>
        <FormField title="Sesiones de la Unidad Didáctica" required>
          <TextInput
            name={"didacticUnitDuration"}
            value={data.didacticUnitDuration}
            onChange={handleChange}
            placeholder={"Ejemplo: 2 sesiones, 4 sesiones, etc."}
          />
        </FormField>
        <FormField title="Contenido / Saberes Básicos" required>
          <TextArea
            name={"mainTopic"}
            value={data.mainTopic}
            onChange={handleChange}
            placeholder={"Ejemplo: La Revolución Industrial"}
            rows={3}
          />
        </FormField>
        <FormField title="Objetivos de aprendizaje" required>
          <TextArea
            name={"learningObject"}
            value={data.learningObject}
            onChange={handleChange}
            placeholder={"Comprender las causas, el desarrollo y las consecuencias de la Revolución Industrial"}
            rows={3}
          />
        </FormField>
        <FormField title="Recursos disponibles" required>
          <TextArea
            name={"availableResources"}
            value={data.availableResources}
            onChange={handleChange}
            placeholder={"Ejemplo: Libros de texto, acceso a Internet, videos documentales, recursos interactivos"}
            rows={3}
          />
        </FormField>
        <FormField title="Tipo de evaluación" required>
          <TextArea
            name={"evaluationTypes"}
            value={data.evaluationTypes}
            onChange={handleChange}
            placeholder={"Ejemplo: Exámenes escritos, proyectos de investigación, presentaciones orales, etc"}
            rows={3}
          />
        </FormField>
        <FormField title="Criterios de Evaluación (OPCIONAL)">
          <TextArea
            name={"criteriosEvaluacion"}
            value={data.criteriosEvaluacion}
            onChange={handleChange}
            placeholder={"Ejemplo: Cita los criterios de evaluación que deseas incluir"}
            rows={3}
          />
        </FormField>
        <FormField title="Competencias específicas a Desarrollar" required>
          <TextArea
            name={"competenciesToDevelop"}
            value={data.competenciesToDevelop}
            onChange={handleChange}
            placeholder={"Ejemplo: Pensamiento crítico, habilidades de investigación, comunicación oral y escrita"}
            rows={3}
          />
        </FormField>
        <FormField title="Información Adicional / Contextualización">
          <TextArea
            name={"adicional"}
            value={data.adicional}
            onChange={handleChange}
            placeholder={"Ejemplo: Tengo una clase de 28 alumnos. Quiero que sean actividades grupales en lo máximo posible. También quiero que sea una Unidad Didáctica emocionante. Si tienes alumnos con necesidades especiales, etc. Cuanta más información proporciones, de mayor calidad será tu Unidad Didáctica."}
            rows={3}
          />
        </FormField>
        <div className="flex justify-center gap-4 mt-8">
          <button
            type="submit"
            className={`w-full py-2.5 px-5 bg-blue-600 text-white rounded-lg hover:bg-blue-700 transition duration-300 ${isLoading ? 'opacity-50 cursor-not-allowed' : ''}`}
            disabled={isLoading}
          >
            {isLoading ? "Generando..." : "Generar Unidad Didáctica"}
          </button>
          {prvResponse && (
            <button
              type="button"
              className="w-full py-2.5 px-5 bg-gray-200 text-gray-700 rounded-lg hover:bg-gray-300 transition duration-300"
              onClick={() => navigate(RouteConstant.DIDACTIC_UNITS_OUTPUT)}
            >
              Ver generación previa
            </button>
          )}
        </div>
      </form>
    </div>
  );
}

export default DidacticUnitsText;
