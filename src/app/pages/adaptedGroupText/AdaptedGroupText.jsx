import React, { useState } from "react";
import {
  getLocalStorage,
  setLocalStorage,
} from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import InputField from "../../components/inputFiled/InputFiled";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";

const FormField = ({ title, children, required = false }) => (
  <div className="mb-4">
    <label className="block font-bold text-sm mb-2">
      {title} {required && <span className="text-red-500">*</span>}
    </label>
    {children}
  </div>
);

const InputStyles = "w-full p-2 border border-gray-300 rounded-md text-gray-700 placeholder-gray-500";

const TextInput = ({ placeholder, value, onChange, name, type = "text" }) => (
  <input
    type={type}
    name={name}
    value={value}
    onChange={onChange}
    placeholder={placeholder}
    className={InputStyles}

  />
);

const TextArea = ({ placeholder, value, onChange, name, rows = 6 }) => (
  <textarea
    name={name}
    value={value}
    onChange={onChange}
    placeholder={placeholder}
    rows={rows}
    className={InputStyles}
  />
);

function AdaptedGroupText() {
  const [data, setData] = useState({
    topic: "",
    course: "",
    subject: "",
    numberOfStudent: "",
    workTime: "",
    caracteristicas: "",
  });
  const { handleHitAi, loading } = useHandleHitAi({ ai: "chatgpt", toolname: "Grupos Adaptados", });


  const prvResponse = getLocalStorage(RouteConstant.ADAPTED_GROUPS_TEXT);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      !data.subject.trim() ||
      !data.course.trim() ||
      !data.topic.trim() ||
      !data.numberOfStudent.trim() ||
      !data.caracteristicas.trim() ||
      !data.workTime.trim()
    ) {
      alert("Please fill in all fields");
      return;
    }

    const aiPrompt = `Como experto en diseño de actividades educativas y formación de grupos, crea una propuesta de grupos adaptados y una actividad educativa basada en:

Tema: ${data.topic}
Curso: ${data.course}
Asignatura: ${data.subject}
Número de Estudiantes: ${data.numberOfStudent}
Tiempo de Trabajo: ${data.workTime}
Características de la Clase: ${data.caracteristicas}

Genera el contenido en formato HTML, siguiendo esta estructura:

<h1>Grupos Adaptados y Actividad para [Asignatura]</h1>

<h2>Grupos Formados:</h2>
[Genera 3, 4, 5 o 6 grupos usando esta estructura para cada uno:]
<div className="grupo">
  <h3>Grupo [Número]</h3>
  <p><strong>Composición:</strong> [Descripción breve de los integrantes]</p>
  <p><strong>Justificación:</strong> [Explicación concisa de la formación]</p>
  <p><strong>Adaptaciones:</strong> [1-2 sugerencias específicas]</p>
</div>

<h2>Actividad grupal sugerida:</h2>
<h3>[Título de la Actividad]</h3>
<p><strong>Descripción:</strong> [Breve descripción de la actividad]</p>
<p><strong>Objetivos:</strong> [2-3 objetivos específicos]</p>
<p><strong>Materiales:</strong> [Lista breve de materiales necesarios]</p>
<h4>Pasos de la Actividad:</h4>
<ol>
  <li>[Paso 1]</li>
  <li>[Paso 2]</li>
  <li>[Paso 3]</li>
</ol>

Asegúrate de que los grupos y la actividad sean apropiados para el nivel del curso, la asignatura y las características de la clase. La actividad debe ser atractiva, práctica, creativa y alineada con el tema a tratar. 
Responde siempre en español, proporcionando un plan conciso pero completo.
La generación debe estar siempre en este formato: HTML`;

    setIsLoading(true);
    try {
      await handleHitAi(aiPrompt, RouteConstant.STORY_TELLING_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      setIsLoading(false);
    }
  };

  return (
    <div className="flex flex-col gap-[24px] p-[24px] max-h-[80vh] overflow-scroll">
      <AiToolPageHeader
        title="Grupos Adaptados: Planificación Educativa a Medida"
        description="Crea grupos adaptados y actividades a medida"
        icon="ri-edit-2-line text-3xl text-blue-600"
      />
      <form onSubmit={handleSubmit} className="flex flex-col gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <div className="grid grid-cols-2 gap-4">
          <FormField title="Curso" required>
            <InputFieldDropdown
              placeholder="Curso"
              onChange={(value) => {
                setData({ ...data, course: value });
              }}
              name="course"
              type="number"
              value={data.course}
              options={Dropdown.courses}
            />
          </FormField>
          <FormField title="Asignatura" required>
            <InputFieldDropdown
              name={"subject"}
              value={data.subject}
              onChange={(value) => {
                setData({ ...data, subject: value });
              }}
              placeholder={"Asignatura"}
              options={Dropdown.subject}
            />
          </FormField>
        </div>
        <FormField title="Número de Estudiantes" required>
          <TextInput
            name={"numberOfStudent"}
            value={data.numberOfStudent}
            onChange={handleChange}
            placeholder={"20, 25, 28, etc."}
            type="number"
          />
        </FormField>
        <FormField title="Duración de la sesión" required>
          <TextInput
            name={"workTime"}
            value={data.workTime}
            onChange={handleChange}
            placeholder={"55 minutos, etc."}
          />
        </FormField>
        <FormField title="Contenido a tratar" required>
          <TextArea
            name={"topic"}
            value={data.topic}
            onChange={handleChange}
            placeholder="La temática que quieres"
            rows={3}
          />
        </FormField>
        <FormField title="Características de los alumnos" required>
          <TextArea
            name={"caracteristicas"}
            value={data.caracteristicas}
            onChange={handleChange}
            placeholder="Si tienes algún niño con dilexia, con alguna necesidad especial, etc"
            rows={3}
          />
        </FormField>
        <div className="flex justify-center gap-4 mt-8">
          <Button
            type="submit"
            className={isLoading ? "opacity-50 cursor-not-allowed w-full" : "w-full"}
            disabled={isLoading}
          >
            {isLoading ? "Cargando..." : "Genera los grupos"}
          </Button>
          {prvResponse && (
            <Button
              type="button"
              className="w-full py-2.5 px-5 bg-gray-200 text-gray-700 rounded-lg hover:bg-gray-300 transition duration-300"
              onClick={() => navigate(RouteConstant.ADAPTED_GROUPS_TEXT)}
            >
              Ver generación previa
            </Button>
          )}
        </div>
      </form>
    </div>
  );
}

export default AdaptedGroupText;
