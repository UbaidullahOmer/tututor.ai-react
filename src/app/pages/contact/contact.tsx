import React from 'react';

const ContactForm: React.FC = () => {
    return (
        <div className="bg-gray-100 min-h-screen flex items-center justify-center p-4">
            <div className="bg-white rounded-lg shadow-lg overflow-hidden max-w-6xl w-full flex">
                {/* Left side - Form */}
                <div className="w-full md:w-1/2 p-8">
                    <h1 className="text-3xl font-bold mb-6">Escríbenos</h1>
                    <p className="text-gray-600 mb-8">
                        Si necesitas ayuda con cualquier cosa, no dudes en escribirnos. Intentaremos resolverte las dudas en menos de 24h
                    </p>
                    
                    <form>
                        <div className="grid grid-cols-2 gap-4 mb-4">
                            <div>
                                <label htmlFor="firstName" className="block text-sm font-medium text-gray-700 mb-1">Nombre</label>
                                <input
                                    type="text"
                                    id="firstName"
                                    name="firstName"
                                    className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500"
                                />
                            </div>
                            <div>
                                <label htmlFor="lastName" className="block text-sm font-medium text-gray-700 mb-1">Apellidos</label>
                                <input
                                    type="text"
                                    id="lastName"
                                    name="lastName"
                                    className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500"
                                />
                            </div>
                        </div>
                        
                        <div className="mb-4">
                            <label htmlFor="jobTitle" className="block text-sm font-medium text-gray-700 mb-1">Profesor o Estudiante?</label>
                            <input
                                type="text"
                                id="jobTitle"
                                name="jobTitle"
                                className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500"
                            />
                        </div>
                        
                        <div className="mb-4">
                            <label htmlFor="workEmail" className="block text-sm font-medium text-gray-700 mb-1">Correo Electrónico</label>
                            <input
                                type="email"
                                id="workEmail"
                                name="workEmail"
                                className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500"
                            />
                        </div>
                        
                        <div className="mb-6">
                            <label htmlFor="phone" className="block text-sm font-medium text-gray-700 mb-1">Teléfono</label>
                            <div className="flex">
                                <select className="px-3 py-2 border border-gray-300 rounded-l-md focus:outline-none focus:ring-2 focus:ring-blue-500">
                                <option value="ES">🇪🇸 +34</option>
                                <option value="AR">🇦🇷 +54</option>
<option value="BO">🇧🇴 +591</option>
<option value="CL">🇨🇱 +56</option>
<option value="CO">🇨🇴 +57</option>
<option value="CR">🇨🇷 +506</option>
<option value="CU">🇨🇺 +53</option>
<option value="DO">🇩🇴 +1-809</option>
<option value="EC">🇪🇨 +593</option>
<option value="SV">🇸🇻 +503</option>
<option value="GQ">🇬🇶 +240</option>
<option value="GT">🇬🇹 +502</option>
<option value="HN">🇭🇳 +504</option>
<option value="MX">🇲🇽 +52</option>
<option value="NI">🇳🇮 +505</option>
<option value="PA">🇵🇦 +507</option>
<option value="PY">🇵🇾 +595</option>
<option value="PE">🇵🇪 +51</option>
<option value="UY">🇺🇾 +598</option>
<option value="VE">🇻🇪 +58</option>
<option value="AF">🇦🇫 +93</option>
<option value="AL">🇦🇱 +355</option>
<option value="DZ">🇩🇿 +213</option>
<option value="AS">🇦🇸 +1-684</option>
<option value="AD">🇦🇩 +376</option>
<option value="AO">🇦🇴 +244</option>
<option value="AI">🇦🇮 +1-264</option>
<option value="AG">🇦🇬 +1-268</option>
<option value="AM">🇦🇲 +374</option>
<option value="AW">🇦🇼 +297</option>
<option value="AU">🇦🇺 +61</option>
<option value="AT">🇦🇹 +43</option>
<option value="AZ">🇦🇿 +994</option>
<option value="BS">🇧🇸 +1-242</option>
<option value="BH">🇧🇭 +973</option>
<option value="BD">🇧🇩 +880</option>
<option value="BB">🇧🇧 +1-246</option>
<option value="BY">🇧🇾 +375</option>
<option value="BE">🇧🇪 +32</option>
<option value="BZ">🇧🇿 +501</option>
<option value="BJ">🇧🇯 +229</option>
<option value="BM">🇧🇲 +1-441</option>
<option value="BT">🇧🇹 +975</option>
<option value="BA">🇧🇦 +387</option>
<option value="BW">🇧🇼 +267</option>
<option value="BR">🇧🇷 +55</option>
<option value="BN">🇧🇳 +673</option>
<option value="BG">🇧🇬 +359</option>
<option value="BF">🇧🇫 +226</option>
<option value="BI">🇧🇮 +257</option>
<option value="KH">🇰🇭 +855</option>
<option value="CM">🇨🇲 +237</option>
<option value="CA">🇨🇦 +1</option>
<option value="CV">🇨🇻 +238</option>
<option value="KY">🇰🇾 +1-345</option>
<option value="CF">🇨🇫 +236</option>
<option value="TD">🇹🇩 +235</option>
<option value="CN">🇨🇳 +86</option>
<option value="KM">🇰🇲 +269</option>
<option value="CG">🇨🇬 +242</option>
<option value="CD">🇨🇩 +243</option>
<option value="HR">🇭🇷 +385</option>
<option value="CY">🇨🇾 +357</option>
<option value="CZ">🇨🇿 +420</option>
<option value="DK">🇩🇰 +45</option>
<option value="DJ">🇩🇯 +253</option>
<option value="DM">🇩🇲 +1-767</option>
<option value="EG">🇪🇬 +20</option>
<option value="ER">🇪🇷 +291</option>
<option value="EE">🇪🇪 +372</option>
<option value="ET">🇪🇹 +251</option>
<option value="FJ">🇫🇯 +679</option>
<option value="FI">🇫🇮 +358</option>
<option value="FR">🇫🇷 +33</option>
<option value="GA">🇬🇦 +241</option>
<option value="GM">🇬🇲 +220</option>
<option value="GE">🇬🇪 +995</option>
<option value="DE">🇩🇪 +49</option>
<option value="GH">🇬🇭 +233</option>
<option value="GR">🇬🇷 +30</option>
<option value="GD">🇬🇩 +1-473</option>
<option value="GN">🇬🇳 +224</option>
<option value="GW">🇬🇼 +245</option>
<option value="GY">🇬🇾 +592</option>
<option value="HT">🇭🇹 +509</option>
<option value="HU">🇭🇺 +36</option>
<option value="IS">🇮🇸 +354</option>
<option value="IN">🇮🇳 +91</option>
<option value="ID">🇮🇩 +62</option>
<option value="IR">🇮🇷 +98</option>
<option value="IQ">🇮🇶 +964</option>
<option value="IE">🇮🇪 +353</option>
<option value="IL">🇮🇱 +972</option>
<option value="IT">🇮🇹 +39</option>
<option value="JM">🇯🇲 +1-876</option>
<option value="JP">🇯🇵 +81</option>
<option value="JO">🇯🇴 +962</option>
<option value="KZ">🇰🇿 +7</option>
<option value="KE">🇰🇪 +254</option>
<option value="KI">🇰🇮 +686</option>
<option value="KP">🇰🇵 +850</option>
<option value="KR">🇰🇷 +82</option>
<option value="KW">🇰🇼 +965</option>
<option value="KG">🇰🇬 +996</option>
<option value="LA">🇱🇦 +856</option>
<option value="LV">🇱🇻 +371</option>
<option value="LB">🇱🇧 +961</option>
<option value="LS">🇱🇸 +266</option>
<option value="LR">🇱🇷 +231</option>
<option value="LY">🇱🇾 +218</option>
<option value="LI">🇱🇮 +423</option>
<option value="LT">🇱🇹 +370</option>
<option value="LU">🇱🇺 +352</option>
<option value="MG">🇲🇬 +261</option>
<option value="MW">🇲🇼 +265</option>
<option value="MY">🇲🇾 +60</option>
<option value="MV">🇲🇻 +960</option>
<option value="ML">🇲🇱 +223</option>
<option value="MT">🇲🇹 +356</option>
<option value="MH">🇲🇭 +692</option>
<option value="MR">🇲🇷 +222</option>
<option value="MU">🇲🇺 +230</option>
<option value="FM">🇫🇲 +691</option>
<option value="MD">🇲🇩 +373</option>
<option value="MC">🇲🇨 +377</option>
<option value="MN">🇲🇳 +976</option>
<option value="ME">🇲🇪 +382</option>
<option value="MA">🇲🇦 +212</option>
<option value="MZ">🇲🇿 +258</option>
<option value="MM">🇲🇲 +95</option>
<option value="NA">🇳🇦 +264</option>
<option value="NR">🇳🇷 +674</option>
<option value="NP">🇳🇵 +977</option>
<option value="NL">🇳🇱 +31</option>
<option value="NZ">🇳🇿 +64</option>
<option value="NE">🇳🇪 +227</option>
<option value="NG">🇳🇬 +234</option>
<option value="NO">🇳🇴 +47</option>
<option value="OM">🇴🇲 +968</option>
<option value="PK">🇵🇰 +92</option>
<option value="PW">🇵🇼 +680</option>
<option value="PG">🇵🇬 +675</option>
<option value="PH">🇵🇭 +63</option>
<option value="PL">🇵🇱 +48</option>
<option value="PT">🇵🇹 +351</option>
<option value="QA">🇶🇦 +974</option>
<option value="RO">🇷🇴 +40</option>
<option value="RU">🇷🇺 +7</option>
<option value="RW">🇷🇼 +250</option>
<option value="KN">🇰🇳 +1-869</option>
<option value="LC">🇱🇨 +1-758</option>
<option value="VC">🇻🇨 +1-784</option>
<option value="WS">🇼🇸 +685</option>
<option value="SM">🇸🇲 +378</option>
<option value="ST">🇸🇹 +239</option>
<option value="SA">🇸🇦 +966</option>
<option value="SN">🇸🇳 +221</option>
<option value="RS">🇷🇸 +381</option>
<option value="SC">🇸🇨 +248</option>
<option value="SL">🇸🇱 +232</option>
<option value="SG">🇸🇬 +65</option>
<option value="SK">🇸🇰 +421</option>
<option value="SI">🇸🇮 +386</option>
<option value="SB">🇸🇧 +677</option>
<option value="SO">🇸🇴 +252</option>
<option value="ZA">🇿🇦 +27</option>
<option value="SS">🇸🇸 +211</option>
<option value="LK">🇱🇰 +94</option>
<option value="SD">🇸🇩 +249</option>
<option value="SR">🇸🇷 +597</option>
<option value="SZ">🇸🇿 +268</option>
<option value="SE">🇸🇪 +46</option>
<option value="CH">🇨🇭 +41</option>
<option value="SY">🇸🇾 +963</option>
<option value="TW">🇹🇼 +886</option>
<option value="TJ">🇹🇯 +992</option>
<option value="TZ">🇹🇿 +255</option>
<option value="TH">🇹🇭 +66</option>
<option value="TL">🇹🇱 +670</option>
<option value="TG">🇹🇬 +228</option>
<option value="TO">🇹🇴 +676</option>
<option value="TT">🇹🇹 +1-868</option>
<option value="TN">🇹🇳 +216</option>
<option value="TR">🇹🇷 +90</option>
                                </select>
                                <input
                                    type="tel"
                                    id="phone"
                                    name="phone"
                                    placeholder="654 324 565"
                                    className="flex-grow px-3 py-2 border border-gray-300 rounded-r-md focus:outline-none focus:ring-2 focus:ring-blue-500"
                                />
                            </div>
                        </div>
                        
                        <div className="mb-6">
                            <p className="block text-sm font-medium text-gray-700 mb-2">¿Eres una persona o un centro educativo?</p>
                            <div className="space-y-2">
                                <label className="flex items-center p-3 border border-gray-300 rounded-md cursor-pointer hover:bg-gray-50">
                                    <input type="radio" name="employees" value="solo" className="mr-2" />
                                    <span className="font-medium">Soy una persona individual</span>
                                </label>
                                <label className="flex items-center p-3 border border-gray-300 rounded-md cursor-pointer hover:bg-gray-50">
                                    <input type="radio" name="employees" value="team" className="mr-2" />
                                    <span className="font-medium">Soy un centro educativo</span>
                                </label>
                            </div>
                        </div>
                        
                        <button type="submit" className="w-full bg-blue-600 text-white font-semibold py-3 rounded-md hover:bg-blue-700 transition duration-300">
                            Contacta
                        </button>
                    </form>
                </div>
                
                {/* Right side - Image and quote */}
                <div className="hidden md:block w-1/2 relative">
                    <div className="absolute inset-0 bg-gradient-to-br from-purple-600 to-blue-600 opacity-90"></div>
                    <img
                        src="https://dl.dropbox.com/scl/fi/gw0othv3zewgv17tqihum/1.jpg?rlkey=43mwkk1405lms0i2d4j8t6gzn&dl=0"
                        alt="Background"
                        className="object-cover w-full h-full"
                    />
                    <div className="absolute inset-0 flex flex-col justify-end p-8 text-white">
                        <p className="text-2xl font-light mb-4">
                        "Impulsamos la educación del futuro, brindando herramientas tecnológicas avanzadas para apoyar a profesores y estudiantes en su camino hacia el éxito académico."
                        </p>
                        <p className="font-semibold">Álvaro Pérez</p>
                        <p className="text-sm">Fundador & CEO</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ContactForm;
