import React, { useState } from "react";
import { getLocalStorage } from "../../components/local_storage";
import { useNavigate } from "react-router-dom";
import { RouteConstant } from "../../constants/RouteConstant";
import AiToolPageHeader from "../../components/aiToolPageHeader/AiToolPageHeader";
import Button from "../../components/button/Button";
import InputFieldDropdown from "../../components/inputFiledDropdown/InputFiledDropdown";
import { Dropdown } from "../../constants/Dropdowns";
import useHandleHitAi from "../../hooks/useHandleHitAi";
import FileReaderInputFIled from "../../components/fileReaderInputFIled/FileReaderInputFIled";

const FormField = ({ title, children, required = false }) => (
  <div className="mb-4">
    <label className="block font-bold text-sm mb-2">
      {title} {required && <span className="text-red-500">*</span>}
    </label>
    {children}
  </div>
);

const InputStyles = "w-full p-2 border border-gray-300 rounded-md text-gray-700 placeholder-gray-500";

const TextInput = ({ placeholder, value, onChange, name, type = "text" }) => (
  <input
    type={type}
    name={name}
    value={value}
    onChange={onChange}
    placeholder={placeholder}
    className={InputStyles}
  />
);

const TextArea = ({ placeholder, value, onChange, name, rows = 4 }) => (
  <textarea
    name={name}
    value={value}
    onChange={onChange}
    placeholder={placeholder}
    rows={rows}
    className={InputStyles}
  />
);

function ClassIdeatorText() {
  const [data, setData] = useState({
    course: "",
    subject: "",
    classContent: "",
    learningObjective: "",
    classDuration: "",
    availableResources: "",
    otrasConsideraciones: "",
    numSesiones: "",
    teachingStyle: "",
    competencias: "",
    numSesion: "",
    selectedFiles: [],
    pais: "",
  });

  const { handleHitAi, loading: aiLoading } = useHandleHitAi({ 
    ai: "chatgpt",
    selectedFiles: data.selectedFiles,
    toolName: "Ideador de Clases",
  });

  const prvResponse = getLocalStorage(RouteConstant.CLASS_IDEATOR_OUTPUT);
  const navigate = useNavigate();
  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const requiredFields = ['course', 'subject', 'classContent', 'learningObjective', 'classDuration', 'numSesiones', 'teachingStyle', 'pais'];
    const missingFields = requiredFields.filter(field => !data[field]);
    
    if (missingFields.length > 0) {
      alert("Please fill in all required fields");
      return;
    }
    
    setIsSubmitting(true);
    try {
      let classIdeatorPrompt = `Como experto en diseño e ideador de clases y sesiones para colegios e institutos, crea un plan detallado para ${data.numSesiones} sesión(s) basado en:

Adapta el contenido al nivel y edad de los estudiantes según el curso: ${data.course}
Asignatura: ${data.subject}
Contenido de la sesión: ${data.classContent}
Objetivo(s) de aprendizaje: ${data.learningObjective}
Duración de la sesión: ${data.classDuration}
Número de sesiones a realizar: ${data.numSesiones}
Las actividades creadas deben estar adaptadas al número de sesión que corresponda respetando la progresión y dificultad. 
El número de las sesiones será: ${data.numSesion}.
Recursos: ${data.availableResources}
Adapta las competencias clave al país: ${data.pais}
Considera este estilo de enseñanza como principal en la generación, pero que no sea exclusivo: ${data.teachingStyle}
Otras consideraciones: ${data.otrasConsideraciones}

Comienza con una introducción general de aproximadamente 150 palabras que explique lo que se va a trabajar en la(s) sesión(es). Esta introducción debe incluir:
- Duración total de la(s) sesión(es)
- Nombre de la tarea principal. IMPORTANTE: Asegúrate que sea innovadora, cautivadora y ÚNICA para esta generación. NO uses nombres o conceptos repetitivos como "viaje en el tiempo". Sé creativo y original en cada generación.
- Número de la tarea

Para cada sesión, proporciona un plan estructurado y atractivo:

<h2>Sesión #{número de la sesión}</h2>

<h3>1. Introducción (5-10 minutos):</h3>
<ul>
  <li>Saludo y presentación:
    <ul>
      <li>Duración: [Tiempo]</li>
      <li>Descripción: [Cómo el profesor introduce el tema de manera única y creativa]</li>
      <li>Materiales: [Lista de materiales específicos y originales para esta parte]</li>
    </ul>
  </li>
  <li>Activación de conocimientos previos:
    <ul>
      <li>Duración: [Tiempo]</li>
      <li>Método: [Conexión innovadora con conocimientos previos]</li>
      <li>Materiales: [Lista de materiales específicos y originales para esta parte]</li>
    </ul>
  </li>
  <li>Presentación del objetivo:
    <ul>
      <li>Duración: [Tiempo]</li>
      <li>Estrategia: [Forma creativa y única de comunicar el objetivo]</li>
      <li>Materiales: [Lista de materiales específicos y originales para esta parte]</li>
    </ul>
  </li>
</ul>

<h3>2. Parte principal (30-35 minutos):</h3>
<ul>
  <li>Actividad central: [Nombre atractivo, innovador y ÚNICO para esta actividad]
    <ul>
      <li>Duración: [Tiempo específico]</li>
      <li>Materiales: [Lista detallada de materiales originales y creativos]</li>
      <li>Preparación: [Pasos de preparación únicos y detallados]</li>
      <li>Desarrollo: [Descripción detallada, innovadora, creativa y original del desarrollo de la actividad]</li>
      <li>Rol del profesor: [Descripción única del papel del profesor en esta actividad]</li>
      <li>Factor motivacional: [Aspectos motivacionales innovadores y específicos para esta actividad]</li>
      <li>Adaptaciones: [Adaptaciones creativas para diferentes necesidades]</li>
    </ul>
  </li>
  <li>Actividad de refuerzo: [Nombre específico y original de la actividad]
    <ul>
      <li>Duración: [Tiempo específico]</li>
      <li>Materiales: [Lista detallada de materiales únicos]</li>
      <li>Descripción: [Pasos detallados y originales]</li>
      <li>Conexión con el aprendizaje: [Puntos de conexión innovadores]</li>
    </ul>
  </li>
</ul>

<h3>3. Parte final (5-10 minutos):</h3>
<ul>
  <li>Actividad de cierre: [Nombre creativo y único]
    <ul>
      <li>Duración: [Tiempo]</li>
      <li>Materiales: [Lista de materiales específicos y originales]</li>
      <li>Descripción: [Desarrollo innovador]</li>
      <li>Componente reflexivo: [Reflexión única sobre lo aprendido]</li>
      <li>Anticipación: [Conexión creativa con la próxima sesión]</li>
    </ul>
  </li>
</ul>

Repite esta estructura para cada sesión si es necesario, asegurando progresión lógica entre ellas. Cada plan debe ser detallado, atractivo, interactivo y coherente con el objetivo de aprendizaje, utilizando creativamente los recursos disponibles. 

IMPORTANTE: Asegúrate de que cada actividad, nombre y descripción sea ÚNICA y ORIGINAL para esta generación específica. Evita repetir conceptos o nombres de actividades que hayas usado antes. Sé creativo y diverso en cada aspecto del plan de clase.

Es muy importante que las actividades sigan una progresión en nivel de dificultad. Asegúrate de que las actividades centrales sean innovadoras, atractivas para los alumnos y estén claramente estructuradas y desarrolladas. Proporciona explicaciones detalladas y pasos específicos para cada actividad.

Responde en español, con un tono profesional y motivador. El texto debe generarse en este formato: HTML y nunca nada en markdown`;

      if (data.selectedFiles.length > 0) {
        const imageFiles = data.selectedFiles.filter((file) =>
          file.type.startsWith("image/")
        );
        if (imageFiles.length > 0) {
          classIdeatorPrompt +=
            "\nPor favor, analiza las imágenes subidas y utiliza el contenido visual para complementar y enriquecer las sesiones de clase. Asegúrate de integrar la información visual en las actividades, explicaciones y recursos, adaptando el contenido de la sesión al contexto proporcionado por las imágenes. La sesión debe ser innovadora, creativa y reflejar el tema del temario que las imágenes representan. Considera que el uso de las imágenes debe ser central en el desarrollo de la sesión, sirviendo como base para explicaciones, actividades prácticas y discusión en clase.";
        }
      }

      await handleHitAi(classIdeatorPrompt, RouteConstant.CLASS_IDEATOR_OUTPUT);
      navigate(RouteConstant.CLASS_IDEATOR_OUTPUT);
    } catch (error) {
      console.error("Error during API call:", error);
      alert("An error occurred while generating the class plan. Please try again.");
    } finally {
      setIsSubmitting(false);
    }
  };

  const isLoading = isSubmitting || aiLoading;

  return (
    <div className="flex flex-col gap-[24px] p-[24px] max-h-[80vh] overflow-scroll">
      <AiToolPageHeader
        title="Ideador de sesiones / clases"
        description="Con esta herramienta podrás diseñar e idear sesiones y clases completas innovadoras, creativas y que hagan a tus estudiantes que se interesen en lo que aprenden."
        icon="ri-edit-2-line text-xl text-blue-600"
      />
      <form onSubmit={handleSubmit} className="flex flex-col gap-[20px] rounded-[20px] bg-[#FFF] p-[32px] h-fit">
        <div className="grid grid-cols-2 gap-4">
          <FormField title="Curso" required>
            <InputFieldDropdown
              placeholder="Curso"
              onChange={(value) => setData({ ...data, course: value })}
              name="course"
              value={data.course}
              options={Dropdown.courses}
            />
          </FormField>
          <FormField title="Asignatura" required>
            <InputFieldDropdown
              name="subject"
              value={data.subject}
              onChange={(value) => setData({ ...data, subject: value })}
              placeholder="Asignatura"
              options={Dropdown.subject}
            />
          </FormField>
        </div>
        <FormField title="Estilo de Enseñanza" required>
          <InputFieldDropdown
            name="teachingStyle"
            value={data.teachingStyle}
            onChange={(value) => setData({ ...data, teachingStyle: value })}
            placeholder="Estilo de Enseñanza Principal"
            options={Dropdown.teachingStyles}
          />
        </FormField>
        <FormField title="País" required>
          <InputFieldDropdown
            name="pais"
            value={data.pais}
            onChange={(value) => setData({ ...data, pais: value })}
            placeholder="Selecciona un país"
            options={Dropdown.spanishSpeakingCountries}
          />
        </FormField>
        <FormField title="Duración de la Sesión" required>
          <TextInput
            name="classDuration"
            value={data.classDuration}
            onChange={handleChange}
            placeholder="Ejemplo: 55 minutos, etc"
          />
        </FormField>
        <FormField title="¿Cuántas sesiones a realizar?" required>
          <TextInput
            name="numSesiones"
            value={data.numSesiones}
            onChange={handleChange}
            placeholder="Ejemplo: 1 sesión, 3 sesiones, etc"
          />
        </FormField>
        <FormField title="Número de la sesión" required>
          <TextInput
            name="numSesion"
            value={data.numSesion}
            onChange={handleChange}
            placeholder="Sesión número 2 o 3 de 8 sesiones de la Unidad Didáctica. O sesión número 3,4 y 5 de 6 de tu Unidad Didáctica. Especificar el número es importante para que la IA tenga contexto."
          />
        </FormField>
        <FormField title="Contenido de la Clase" required>
          <TextArea
            name="classContent"
            value={data.classContent}
            onChange={handleChange}
            placeholder="La fotosíntesis"
            rows={4}
          />
        </FormField>
        <FormField title="Objetivos de aprendizaje" required>
          <TextArea
            name="learningObjective"
            value={data.learningObjective}
            onChange={handleChange}
            placeholder="Ejemplo: Comprender el proceso de la fotosíntesis y su importancia para la vida en la Tierra"
            rows={4}
          />
        </FormField>
        <FormField title="Recursos disponibles">
          <TextArea
            name="availableResources"
            value={data.availableResources}
            onChange={handleChange}
            placeholder="Ejemplo: Proyector, laboratorio de ciencias, acceso a internet, etc"
            rows={4}
          />
        </FormField>
        <FormField title="Otras consideraciones">
          <TextArea
            name="otrasConsideraciones"
            value={data.otrasConsideraciones}
            onChange={handleChange}
            placeholder="Ej: Quiero que me añadas bastantes ejemplos. Ej: Quiero que haya muchos ejercicios prácticos, que haya grupos, etc."
            rows={4}
          />
        </FormField>
        <FormField title="Sube tu temario">
          <FileReaderInputFIled
            onChange={handleChange}
            data={data}
            setData={setData}
          />
        </FormField>
        <div className="flex justify-center gap-2 mt-4">
          <Button
            type="submit"
            className={prvResponse ? "w-[80%]" : "w-full"}
            disabled={isLoading}
          >
            {isLoading ? "Generando..." : "Generar sesiones"}
          </Button>
          {prvResponse && (
            <Button
              type="button"
              className="w-[20%]"
              onClick={() => navigate(RouteConstant.CLASS_IDEATOR_OUTPUT)}
              disabled={isLoading}
            >
              Ver generación previa
            </Button>
          )}
        </div>
      </form>
    </div>
  );
}

export default ClassIdeatorText;
