import React, { useEffect, useState } from 'react'
import { Link, useLocation } from 'react-router-dom';
import { getLocalStorage } from '../../components/local_storage';
import { RouteConstant } from '../../constants/RouteConstant';
import TextEditor from '../../components/textEditor/TextEditor';

function ClassIdeatorOutput() {
  const [result, setResult] = useState(null);
  const location = useLocation();

    useEffect(()=> {
        setResult(location.state || getLocalStorage(RouteConstant.CLASS_IDEATOR_OUTPUT))
    },[location.state])

  return (
    <>
      <div>
        <div className="flex items-center gap-3">
          <Link to={RouteConstant.CLASS_IDEATOR_TEXT}>
          <i className="ri-arrow-left-s-line text-[36px] text-[#2B3D70] ml-[20px] pt-[24px] "></i>
          </Link>
          <h1 className="font-[600] text-[#2B3D70] text-[32px]">
            Volver a la herramienta Ideador de Clases
          </h1>
        </div>
      </div>
      <TextEditor result={result} />
    </>
  );
}

export default ClassIdeatorOutput