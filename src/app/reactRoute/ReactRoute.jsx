import React, { Suspense, lazy } from "react";
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import Home from "../pages/home/Home";
import Sidebar from "../components/sidebar/Sidebar";
import { RouteConstant, RouteList } from "../constants/RouteConstant.jsx";
import Navbar from "../components/navbar/Navbar";
import { getLocalStorage } from "../components/local_storage";
import useAuth from "../hooks/useAuth";
import { useSelector } from "react-redux";
import { selectUser } from "../redux/UserReducer";
import GlobalLoader from "../components/loader/GlobalLoader";
const ShareText = lazy(() => import("../pages/shareText/ShareText"));
const ShareOnlineTest = lazy(() => import("../pages/shareOnlineTest/ShareOnlineTest"));
const Signup = lazy(() => import("../pages/signUp/SignUp"));
const Login = lazy(() => import("../pages/login/Login"));

function ReactRoute() {
  const { 
    loading
  } = useAuth();
  const user = useSelector(selectUser)
  if(loading){
    <GlobalLoader show={true}/>
  }
  return (
    <BrowserRouter>
      <div className="flex items-start w-full h-full">
        {user && <Sidebar />}
        <div className="w-full">
          {user && <Navbar />}
          <Routes>
            <Route path={RouteConstant.SHARE_ONLINE_TEST} element={<Suspense fallback={<GlobalLoader show={true}/>}><ShareOnlineTest /></Suspense>} />
            <Route path={RouteConstant.SHARE_TEXT} element={<Suspense fallback={<GlobalLoader show={true}/>}><ShareText /></Suspense>} />
            {!user ? (
              <>
                <Route path={RouteConstant.LOGIN} element={<Suspense fallback={<GlobalLoader show={true}/>}><Login /></Suspense>} />
                <Route path={RouteConstant.SIGNUP} element={<Suspense fallback={<GlobalLoader show={true}/>}><Signup /></Suspense>} />
                <Route path="*" element={<Navigate to={RouteConstant.LOGIN} replace />} />
              </>
            ) : (
              <>
                <Route index element={<Home />} />
                {RouteList.map((route) => (
                  <Route
                    key={route.path}
                    path={route.path}
                    element={
                      <Suspense fallback={<GlobalLoader show={true}/>}>
                        {route.element}
                      </Suspense>
                    }
                  />
                ))}
                <Route path="*" element={<Navigate to="/" replace />} />
              </>
            )}
          </Routes>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default ReactRoute;
