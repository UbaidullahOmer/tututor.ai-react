import { initializeApp } from "firebase/app";
import {getAuth, GoogleAuthProvider} from "firebase/auth"
import {getFirestore} from "firebase/firestore"
import { getStorage } from "firebase/storage";
const firebaseConfig = {
  apiKey: "AIzaSyCq9k48ex92bXcz_CeWLecYHf3LFeE1W1E",
  authDomain: "tututorai.firebaseapp.com",
  projectId: "tututorai",
  storageBucket: "tututorai.appspot.com",
  messagingSenderId: "853818301149",
  appId: "1:853818301149:web:763752958bc9d02c521934",
  measurementId: "G-RT0DEK86QN"
};
const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);
export const googleProvider = new GoogleAuthProvider()
export const storage = getStorage(app)
export const db = getFirestore(app)