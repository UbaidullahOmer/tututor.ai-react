import { createSlice } from "@reduxjs/toolkit";

const loadUserFromLocalStorage = () => {
  const storedUser = localStorage.getItem('user');
  return storedUser ? JSON.parse(storedUser) : null;
};

export const slice = createSlice({
  name: "UserReducer",
  initialState: {
    user: loadUserFromLocalStorage(),
  },
  reducers: {
    setUser: (state, action) => {
      const user = action.payload;
      state.user = user ? {
        uid: user.uid,
        email: user.email,
        displayName: user.displayName,
        photoURL: user.photoURL,
      } : null;
      
      // Save to localStorage
      if (state.user) {
        localStorage.setItem('user', JSON.stringify(state.user));
      } else {
        localStorage.removeItem('user');
      }
    },
  },
});

export const { setUser } = slice.actions;
export const selectUser = (state) => state.UserReducer.user;
export default slice.reducer;