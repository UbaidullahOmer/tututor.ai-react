import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { tututorAI } from "./storeApis";
import { persistReducer, persistStore, FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER } from "redux-persist";
import storage from "redux-persist/lib/storage";

import SnackBarReducer from "./SnackBarReducer";
import LoadingReducer from "./LoadingReducer";
import UserReducer from "./UserReducer";
import FavToolsReducer from "./FavToolsReducer";


const snackBarReducer = "SnackBarReducer";
const ConditionalModelReducer = "ConditionalModelReducer";
const loadingReducer = "LoadingReducer";
const userReducer = "UserReducer";
const favToolsReducer = "FavToolsReducer";

const persistConfig = {
  key: "root",
  storage,
  blacklist: [tututorAI.reducerPath, snackBarReducer, loadingReducer, userReducer, favToolsReducer],
};

const appReducer = combineReducers({
  SnackBarReducer,
  ConditionalModelReducer,
  LoadingReducer,
  UserReducer,
  FavToolsReducer,
  [tututorAI.reducerPath]: tututorAI.reducer,
});

const rootReducer = (state, action) => {
  if (action.type === "logout") {
    state = undefined;
  }
  return appReducer(state, action);
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [
          FLUSH,
          REHYDRATE,
          PAUSE,
          PERSIST,
          PURGE,
          REGISTER,
          "ConditionalModelReducer/showModal"
        ],
      },
    }).concat(tututorAI.middleware),
});

export const persistor = persistStore(store);

setupListeners(store.dispatch);