import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { Config } from "../constants/Index";
const TAG_TYPES = {};
export const tututorAI = createApi({
  reducerPath: "juriiApi",
  baseQuery: fetchBaseQuery({
    baseUrl: Config.serverApiUrl,
    prepareHeaders: async (headers, { getState, endpoint }) => {
      return headers;
    },
  }),
  tagTypes: [],
  endpoints: (builder) => ({
    getReceivedEmailList: builder.query({
      query: () => "email/received-emails",
      transformErrorResponse: (response) => response?.data,
      providesTags: () => [TAG_TYPES.email],
    }),
  }),
});
export const { useGetReceivedEmailListQuery } = tututorAI;
