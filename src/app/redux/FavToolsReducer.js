import { createSlice } from "@reduxjs/toolkit";
import { getLocalStorage, setLocalStorage } from "../components/local_storage";
const initialState = {
  tools: JSON.parse(getLocalStorage("favTools")) || [],
};

export const slice = createSlice({
  name: "FavToolsReducer",
  initialState,
  reducers: {
    toggleFavorite: (state, action) => {
      const toolId = action.payload;
      const existingIndex = state.tools.findIndex((tool) => tool.id === toolId);
      if (existingIndex !== -1) {
        state?.tools?.splice(existingIndex, 1);
      } else {
        state?.tools?.push({ id: toolId, isFavorite: true });
      }
      setLocalStorage("favTools", JSON.stringify(state?.tools));
    },
  },
});

export const { toggleFavorite } = slice.actions;
export const selectTools = (state) => state.FavToolsReducer.tools;
export default slice.reducer;
