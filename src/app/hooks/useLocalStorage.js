import { useCallback } from "react";

const useLocalStorage = () => {
  const setLocalStorage = useCallback((key, data) => {
    try {
      if (key && data !== undefined) {
        localStorage.setItem(key, JSON.stringify(data));
      }
    } catch (error) {
      console.error(`Error setting localStorage item: ${error}`);
    }
  }, []);

  const getLocalStorage = useCallback((key) => {
    try {
      if (key) {
        const item = localStorage.getItem(key);
        return item ? JSON.parse(item) : null;
      }
    } catch (error) {
      console.error(`Error getting localStorage item: ${error}`);
      return null;
    }
    return null;
  }, []);

  const removeLocalStorage = useCallback((key) => {
    try {
      if (key) {
        localStorage.removeItem(key);
      }
    } catch (error) {
      console.error(`Error removing localStorage item: ${error}`);
    }
  }, []);

  const clearLocalStorage = useCallback(() => {
    try {
      localStorage.clear();
    } catch (error) {
      console.error(`Error clearing localStorage: ${error}`);
    }
  }, []);

  return {
    setLocalStorage,
    getLocalStorage,
    removeLocalStorage,
    clearLocalStorage,
  };
};

export default useLocalStorage;
