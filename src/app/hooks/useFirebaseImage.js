import { useState } from "react";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { useSnackBarManager } from "./useSnackBarManager";
import useHandleLoading from "./useHandleLoading";
import { storage } from "../firebase/Firebase";
import { selectUser } from "../redux/UserReducer";
import { useSelector } from "react-redux";

function useFirebaseImage() {
  const [imageUrl, setImageUrl] = useState(null);
  const [uploadProgress, setUploadProgress] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const { fnHandleLoading } = useHandleLoading();
  const { fnShowSnackBar } = useSnackBarManager();
  const user = useSelector(selectUser);

  const uploadImage = async (file, returnType = "url") => {
    if (!user) {
      fnShowSnackBar("User not authenticated", "error");
      return Promise.reject(new Error("User not authenticated"));
    }

    setIsLoading(true);
    fnHandleLoading(true);
    return new Promise((resolve, reject) => {
      const storageRef = ref(storage, `images/${user.uid}/${file.name}`);
      const uploadTask = uploadBytesResumable(storageRef, file);

      uploadTask.on(
        "state_changed",
        (snapshot) => {
          const progress =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          setUploadProgress(progress);
        },
        (error) => {
          console.error("Error uploading image:", error);
          fnShowSnackBar("Error uploading image", "error");
          setIsLoading(false);
          fnHandleLoading(false);
          reject(error);
        },
        () => {
          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
            setImageUrl(downloadURL);
            fnShowSnackBar("Image uploaded successfully");
            setIsLoading(false);
            fnHandleLoading(false);
            if (returnType === "url") {
              resolve(downloadURL);
            } else if (returnType === "path") {
              resolve(storageRef.fullPath);
            }
          });
        }
      );
    });
  };

  const uploadImages = async (files, returnType = "url") => {
    if (!user) {
      fnShowSnackBar("User not authenticated", "error");
      return Promise.reject(new Error("User not authenticated"));
    }

    setIsLoading(true);
    fnHandleLoading(true);
    const uploadPromises = files.map((file) => uploadImage(file, returnType));

    try {
      const results = await Promise.all(uploadPromises);
      setIsLoading(false);
      fnHandleLoading(false);
      return results;
    } catch (error) {
      console.error("Error uploading images:", error);
      fnShowSnackBar("Error uploading images", "error");
      setIsLoading(false);
      fnHandleLoading(false);
      throw error;
    }
  };

  return { imageUrl, uploadProgress, uploadImage, uploadImages, isLoading };
}

export default useFirebaseImage;
