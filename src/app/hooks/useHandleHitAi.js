import { GoogleGenerativeAI } from "@google/generative-ai";
import OpenAI from "openai";
import React, { useState, useCallback, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Anthropic from "@anthropic-ai/sdk";
import { setLocalStorage } from "../components/local_storage";
import useHandleLoading from "./useHandleLoading";
import useFirebaseData from "./useFirebaseData";

const genAI = new GoogleGenerativeAI(import.meta.env.VITE_GEMINI_API_KEY);
const openai = new OpenAI({
  apiKey: import.meta.env.VITE_OPENAI_API_KEY,
  dangerouslyAllowBrowser: true,
});
const CORS_PROXY = "http://localhost:8010/proxy/";

const anthropic = new Anthropic({
  apiKey: import.meta.env.VITE_ANTHROPIC_API_KEY,
  baseURL: CORS_PROXY,
});

const ALLOWED_IMAGE_TYPES = [
  "image/png",
  "image/jpeg",
  "image/webp",
  "image/heic",
  "image/heif",
];

async function fileToGenerativePart(file) {
  const base64EncodedDataPromise = new Promise((resolve) => {
    const reader = new FileReader();
    reader.onloadend = () => resolve(reader.result.split(",")[1]);
    reader.readAsDataURL(file);
  });
  return await base64EncodedDataPromise;
}

function useHandleHitAi({ ai, modal, store = true, selectedFiles, toolName }) {
  const { addData } = useFirebaseData();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [response, setResponse] = useState(null);
  const navigate = useNavigate();
  const { fnHandleLoading } = useHandleLoading();
  const handleHitAi = async (
    prompt,
    navigateLink,
    navigateToLink = "/share-text"
  ) => {
    setLoading(true);
    setError(null);
    try {
      let data;
      if (ai === "chatGpt") {
        const response = await openai.chat.completions.create({
          model: modal || "gpt-4o-mini",
          messages: [{ role: "user", content: prompt }],
        });
        data = response.data;
      } else if (ai === "anthropic") {
        const response = await anthropic.messages.create({
          model: modal || "claude-3.5-sonnet-20240229",
          max_tokens: 1000,
          messages: [{ role: "user", content: prompt }],
        });
        data = response;
      } else {
        const model = genAI.getGenerativeModel({
          model: "gemini-1.5-flash-exp-0827",
        });

        let parts = [];
        if (selectedFiles && selectedFiles.length > 0) {
          const validImageFiles = selectedFiles.filter((file) =>
            ALLOWED_IMAGE_TYPES.includes(file.type)
          );
          for (const image of validImageFiles) {
            const imageData = await fileToGenerativePart(image);
            parts.push({
              inlineData: {
                mimeType: image.type,
                data: imageData,
              },
            });
          }
        }
        parts.push({ text: prompt });

        const content = {
          contents: [
            {
              parts: parts,
            },
          ],
        };

        data = await model.generateContent(content);
      }

      setResponse(data);
      console.log(data, "AI response");

      let content;
      if (ai === "chatGpt") {
        content = data?.choices?.[0]?.message?.content;
      } else if (ai === "anthropic") {
        content = data?.content?.[0]?.text;
      } else {
        content = data?.response?.text();
      }
      const urlForSend = createUrl(navigateToLink, content);
      const storeData = {
        prompt: prompt,
        toolName: toolName,
        aiResponse: content,
        url: urlForSend,
      };

      await handleSaveResponse(storeData);

      handleAfterGetResponse(content, navigateLink);
      setLoading(false);
      return content;
    } catch (err) {
      console.error("Error during API call:", err);
      setError(err.message);
    } finally {
      setLoading(false);
      fnHandleLoading(false);
    }
  };

  const createUrl = (navigateToLink, content) => {
    const encodedResult = encodeURIComponent(content);
    return `${"https://tututor.vercel.app"}${
      navigateToLink || "/share-text"
    }/${encodedResult}`;
  };

  useEffect(() => {
    fnHandleLoading(loading);
  }, [loading, fnHandleLoading]);

  const handleAfterGetResponse = useCallback(
    (data, navigateLink) => {
      setLocalStorage(navigateLink, data);
      if (navigateLink) {
        navigate(navigateLink, {
          state: data,
        });
      }
    },
    [navigate]
  );

  const handleSaveResponse = async (data) => {
    if (store) {
    try {
      await addData("ai-prompt-history", {
        aiResponse: data.aiResponse,
        prompt: data.prompt,
        toolName: data.toolName || "ToolName Will here",
        url: data.url,
        date: new Date().toLocaleString(),
      });
      console.log("Document successfully updated!");
    } catch (error) {
      console.error("Error updating document: ", error);
    }
    }
  };

  return { handleHitAi, response, loading, error, createUrl };
}

export default useHandleHitAi;
