// useChangeMode.js
import { useState, useEffect } from "react";

function useChangeMode() {
  const [changeModeOn, setChangeModeOn] = useState(false);
  const [sectionId, setSectionId] = useState(null);

  useEffect(() => {
    if (changeModeOn) {
      // document.body.style.height = "100%";
      // document.body.style.overflow = "hidden";
      if (sectionId) {
        const section = document.getElementById(sectionId);
        if (section) {
          section.scrollIntoView({ behavior: "smooth" });
        }
      }
    } else {
      document.body.style.height = "auto";
      document.body.style.overflow = "visible";
    }
    return () => {
      document.body.style.height = "auto";
      document.body.style.overflow = "visible";
    };
  }, [changeModeOn, sectionId]);

  const toggleChangeMode = () => {
    setChangeModeOn((prevMode) => !prevMode);
  };

  const onChangeMode = (sectionId) => {
    setSectionId(sectionId);
    setChangeModeOn(true);
  };

  const offChangeMode = () => {
    setChangeModeOn(false);
  };

  return { changeModeOn, toggleChangeMode, onChangeMode, offChangeMode };
}

export default useChangeMode;
