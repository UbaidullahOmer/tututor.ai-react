import { useState } from "react";
import { ref, deleteObject } from "firebase/storage";
import { storage } from "../firebase/firebase";

const useDeleteImage = () => {
  const [isDeleting, setIsDeleting] = useState(false);
  const [error, setError] = useState(null);

  const deleteImage = async (path) => {
    setIsDeleting(true);
    setError(null);
    const imageRef = ref(storage, path);

    try {
      await deleteObject(imageRef);
      setIsDeleting(false);
      return true;
    } catch (error) {
      console.error("Error deleting image:", error);
      setError(error);
      setIsDeleting(false);
      return false;
    }
  };

  return { deleteImage, isDeleting, error };
};

export default useDeleteImage;
