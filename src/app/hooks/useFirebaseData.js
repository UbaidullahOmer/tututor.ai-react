import { useState } from "react";
import {
  getDocs,
  collection,
  doc,
  updateDoc,
  addDoc,
  deleteDoc,
  query,
  where,
} from "firebase/firestore";
import { db } from "../firebase/Firebase";
import useHandleLoading from "./useHandleLoading";
import { useSnackBarManager } from "./useSnackBarManager";
import { useSelector } from "react-redux";
import { selectUser } from "../redux/UserReducer";
function useFirebaseData() {
  const { fnHandleLoading } = useHandleLoading();
  const { fnShowSnackBar } = useSnackBarManager();
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const user = useSelector(selectUser);

  const fetchData = async (collectionId) => {
    if (!user) {
      fnShowSnackBar("User not authenticated", "error");
      return;
    }

    fnHandleLoading(true);
    setIsLoading(true);
    const collectionRef = collection(db, collectionId);
    try {
      const q = query(collectionRef, where("userId", "==", user?.uid));
      const dataSnapshot = await getDocs(q);
      const fetchedData = dataSnapshot.docs.map((doc) => ({
        ...doc.data(),
        id: doc.id,
      }));
      setData(fetchedData);
      fnShowSnackBar("Data fetched successfully");
    } catch (error) {
      console.error("Error fetching data: ", error);
      fnShowSnackBar("Error fetching data", "error");
    } finally {
      fnHandleLoading(false);
      setIsLoading(false);
    }
  };

  const updateData = async (collectionId, documentId, updatedData) => {
    if (!user) {
      fnShowSnackBar("User not authenticated", "error");
      return;
    }

    fnHandleLoading(true);
    setIsLoading(true);
    try {
      const docRef = doc(db, collectionId, documentId);
      await updateDoc(docRef, { ...updatedData, userId: user.uid });
      fnShowSnackBar("Data successfully updated!");
      fetchData(collectionId)
    } catch (error) {
      console.error("Error updating document: ", error);
      fnShowSnackBar("Error updating document", "error");
    } finally {
      fnHandleLoading(false);
      setIsLoading(false);
    }
  };

  const addData = async (collectionId, newData) => {
    if (!user) {
      fnShowSnackBar("User not authenticated", "error");
      return;
    }

    fnHandleLoading(true);
    setIsLoading(true);
    try {
      const collectionRef = collection(db, collectionId);
      await addDoc(collectionRef, { ...newData, userId: user.uid });
      fetchData(collectionId);
      fnShowSnackBar("Data successfully added!");
    } catch (error) {
      console.error("Error adding document: ", error);
      fnShowSnackBar("Error adding document", "error");
    } finally {
      fnHandleLoading(false);
      setIsLoading(false);
    }
  };

  const removeData = async (collectionId, documentId) => {
    if (!user) {
      fnShowSnackBar("User not authenticated", "error");
      return;
    }

    fnHandleLoading(true);
    setIsLoading(true);
    try {
      const docRef = doc(db, collectionId, documentId);
      await deleteDoc(docRef);
      fnShowSnackBar("Data successfully deleted!");
      fetchData(collectionId);
    } catch (error) {
      console.error("Error deleting document: ", error);
      fnShowSnackBar("Error deleting document", "error");
    } finally {
      fnHandleLoading(false);
      setIsLoading(false);
    }
  };

  return { data, fetchData, updateData, addData, removeData, isLoading,user };
}

export default useFirebaseData;
