import { useState, useEffect } from 'react';
import { 
  auth, 
  googleProvider, 
  db 
} from '../firebase/Firebase';
import { 
  signInWithPopup, 
  createUserWithEmailAndPassword, 
  signInWithEmailAndPassword as firebaseSignInWithEmailAndPassword, 
  signOut, 
  onAuthStateChanged,
  sendPasswordResetEmail
} from 'firebase/auth';
import { doc, setDoc, getDoc } from 'firebase/firestore';
import { useSnackBarManager } from './useSnackBarManager';
import { useDispatch } from 'react-redux';
import { setUser } from '../redux/UserReducer';

function useAuth() {
  const [loading, setLoading] = useState(true);
  const { fnShowSnackBar } = useSnackBarManager();
  const dispatch = useDispatch();

  useEffect(() => {
    const userFromLocalStorage = JSON.parse(localStorage.getItem('user'));
    
    if (userFromLocalStorage) {
      dispatch(setUser(userFromLocalStorage));
      setLoading(false);
    } else {
      const unsubscribe = onAuthStateChanged(auth, (user) => {
        if (user) {
          const userData = {
            uid: user.uid,
            email: user.email,
            displayName: user.displayName,
            photoURL: user.photoURL,
          };
          dispatch(setUser(userData));
          localStorage.setItem('user', JSON.stringify(userData));
        } else {
          dispatch(setUser(null));
          localStorage.removeItem('user');
        }
        setLoading(false);
      });

      return () => unsubscribe();
    }
  }, [dispatch]);

  const signInWithGoogle = async () => {
    try {
      const result = await signInWithPopup(auth, googleProvider);
      await createUserProfileDocument(result.user);
      fnShowSnackBar('Successfully signed in with Google');
    } catch (error) {
      console.error('Error signing in with Google', error);
      fnShowSnackBar('Error signing in with Google', 'error');
    }
  };

  const signUpWithEmailAndPassword = async (email, password, displayName) => {
    try {
      const result = await createUserWithEmailAndPassword(auth, email, password);
      await createUserProfileDocument(result.user, { displayName });
      fnShowSnackBar('Successfully signed up');
    } catch (error) {
      console.error('Error signing up', error);
      fnShowSnackBar(`Error signing up: ${error.message}`, 'error');
    }
  };

  const signInWithEmailAndPassword = async (email, password) => {
    try {
      await firebaseSignInWithEmailAndPassword(auth, email, password);
      fnShowSnackBar('Successfully signed in');
    } catch (error) {
      console.error('Error signing in', error);
      fnShowSnackBar(`Error signing in: ${error.message}`, 'error');
    }
  };

  const signOutUser = async () => {
    try {
      await signOut(auth);
      dispatch(setUser(null));
      localStorage.removeItem('user');
      fnShowSnackBar('Successfully signed out');
    } catch (error) {
      console.error('Error signing out', error);
      fnShowSnackBar('Error signing out', 'error');
    }
  };

  const sendPasswordReset = async (email) => {
    try {
      await sendPasswordResetEmail(auth, email);
      fnShowSnackBar('Password reset email sent');
    } catch (error) {
      console.error('Error sending password reset email', error);
      fnShowSnackBar(`Error sending password reset email: ${error.message}`, 'error');
    }
  };

  const createUserProfileDocument = async (user, additionalData) => {
    if (!user) return;

    const userRef = doc(db, 'users', user.uid);
    const snapshot = await getDoc(userRef);

    if (!snapshot.exists()) {
      const { displayName, email, photoURL } = user;
      const createdAt = new Date();

      try {
        await setDoc(userRef, {
          displayName,
          email,
          photoURL,
          createdAt,
          ...additionalData
        });
      } catch (error) {
        console.error('Error creating user document', error);
      }
    }

    return userRef;
  };

  return {
    loading,
    signInWithGoogle,
    signUpWithEmailAndPassword,
    signInWithEmailAndPassword,
    signOutUser,
    sendPasswordReset
  };
}

export default useAuth;